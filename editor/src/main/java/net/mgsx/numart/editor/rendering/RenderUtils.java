package net.mgsx.numart.editor.rendering;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import net.mgsx.numart.editor.action.RenderAction;
import net.mgsx.numart.editor.converter.PreviewNodeConverter;
import net.mgsx.numart.editor.converter.ThumbNodeConverter;
import net.mgsx.numart.framework.utils.ReflectUtils;

import org.jdesktop.swingx.JXTaskPane;

public class RenderUtils 
{
	public static interface PostRenderAction
	{
		void run(Icon icon);
	}
	public static class PostRenderActionTaskPane implements PostRenderAction
	{
		private JXTaskPane object;
		
		public PostRenderActionTaskPane(JXTaskPane object) {
			super();
			this.object = object;
		}

		public void run(Icon icon){
			object.setIcon(icon);
			object.revalidate();
		}
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof PostRenderActionTaskPane)
			{
				return object.equals(((PostRenderActionTaskPane)obj).object);
			}
			return super.equals(obj);
		}
	}
	public static class PostRenderActionButton implements PostRenderAction
	{
		private AbstractButton object;
		
		public PostRenderActionButton(AbstractButton object) {
			super();
			this.object = object;
		}

		public void run(Icon icon){
			object.setIcon(icon);
			object.revalidate();
		}
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof PostRenderActionButton)
			{
				return object.equals(((PostRenderActionButton)obj).object);
			}
			return super.equals(obj);
		}
	}
	
	public static boolean autoUpdate = true;
	
	static Map<Object, Icon> mapObjectIcon = new HashMap<Object, Icon>();
	static Map<Object, List<PostRenderAction>> mapObjectActions = new HashMap<Object, List<PostRenderAction>>();
	
	public synchronized static void dettachIcon(final Object object, final PostRenderAction postAction)
	{
		if(mapObjectIcon.containsKey(object))
		{
			mapObjectActions.get(object).remove(postAction);
		}
	}
	
	static List<Object> renderingObject = new ArrayList<Object>();
	
	public synchronized static void updateIcon(final Object object)
	{
		if(!autoUpdate) return;
		
		if(renderingObject.contains(object))
		{
			return;
		}
		renderingObject.add(object);
		
		final Object realObject;
		if(object instanceof Class<?>)
		{
			realObject = ReflectUtils.createDefault((Class<?>)object);
		}
		else
		{
			realObject = object;
		}
		renderToIcon(realObject, new PostRenderAction() {
			@Override
			public void run(Icon icon) {
				synchronized (RenderUtils.class) {
					mapObjectIcon.put(object, icon);
					for(PostRenderAction action : mapObjectActions.get(object))
					{
						action.run(icon);
					}
					renderingObject.remove(object);
				}
			}
		}, 24, 24); // TODO const ou var 
	}
	public synchronized static void attachIcon(final Object object, final PostRenderAction postAction)
	{
		if(mapObjectIcon.containsKey(object))
		{
			mapObjectActions.get(object).add(postAction);
			if(mapObjectIcon.get(object) != null)
			{
				postAction.run(mapObjectIcon.get(object));
			}
		}
		else
		{
			mapObjectIcon.put(object, null);
			List<PostRenderAction> actions = new ArrayList<RenderUtils.PostRenderAction>();
			actions.add(postAction);
			mapObjectActions.put(object, actions);
			updateIcon(object);
		}
	}
	
	private static Action renderToIcon(Object object, final PostRenderAction postAction, final int width, final int height)
	{
		JPanel p = new JPanel();
		p.setPreferredSize(new Dimension(width, height));
		p.setSize(width, height);
		final RenderManager renderManager = new RenderManager(
			new JPanel(){
				private static final long serialVersionUID = 1L;
				@Override
				public int getWidth() {
					return width;
				}
				@Override
				public int getHeight() {
					return height;
				}
				@Override
				public void paint(Graphics g) {
				}
			}, 
			new JProgressBar(){
				private static final long serialVersionUID = 1L;
			  @Override
				public void setValue(int n) {
					// TODO Auto-generated method stub
					  // lblName.setText("r"+n);
				}
			  });
		
		renderManager.onComplete(new Runnable() {
			@Override
			public void run() {
				BufferedImage img = renderManager.getRenderingImage();
				if(img != null)
				{
					postAction.run(new ImageIcon(img));
				}
			}
		});
		
		final RenderAction action = new RenderAction(object, renderManager, new ThumbNodeConverter(new PreviewNodeConverter()));
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				action.actionPerformed(null);
				
			}
		});
		
		return action;
	}
}
