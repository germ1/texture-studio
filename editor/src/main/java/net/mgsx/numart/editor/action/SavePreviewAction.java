package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import net.mgsx.numart.editor.rendering.RenderManager;
import net.mgsx.numart.persistence.PersistenceUtil;
import net.mgsx.numart.procedural.texture.util.ImageUtils;

public class SavePreviewAction  extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private RenderManager renderManager;
	
	public SavePreviewAction(RenderManager renderManager) {
		super("Save preview as ...");
		this.renderManager = renderManager;
	}


	@Override
	public void actionPerformed(ActionEvent event) {
		
		JFileChooser fc = new JFileChooser(PersistenceUtil.defaultFolder);
		if(JFileChooser.APPROVE_OPTION == fc.showSaveDialog(null))
		{
			PersistenceUtil.defaultFolder = fc.getCurrentDirectory();
			File file = fc.getSelectedFile();
			try {
				ImageUtils.saveImageAsPng(file, renderManager.getRenderingImage());
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}

}
