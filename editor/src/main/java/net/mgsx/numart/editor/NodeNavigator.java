package net.mgsx.numart.editor;

public interface NodeNavigator {

	void navigateToNode(Object previousNode, Object newNode);
}
