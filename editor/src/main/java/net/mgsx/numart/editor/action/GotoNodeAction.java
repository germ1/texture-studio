package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import net.mgsx.numart.editor.NodeNavigator;

public class GotoNodeAction extends AbstractAction
{

	private static final long serialVersionUID = 1L;

	private Object current;
	private Object next;
	private NodeNavigator navigator;
	
	public GotoNodeAction(NodeNavigator navigator, Object current, Object next) {
		super(next.getClass().getSimpleName());
		this.current = current;
		this.next = next;
		this.navigator = navigator;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		navigator.navigateToNode(current, next);
	}
	
}