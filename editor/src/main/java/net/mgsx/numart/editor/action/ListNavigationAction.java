package net.mgsx.numart.editor.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.lang.reflect.Field;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import net.mgsx.numart.editor.NodeNavigator;
import net.mgsx.numart.editor.rendering.RenderUtils;
import net.mgsx.numart.editor.rendering.RenderUtils.PostRenderActionButton;
import net.mgsx.numart.framework.utils.ReflectUtils;

public class ListNavigationAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private Object host;
	
	private Field field;
	
	private NodeNavigator navigator;
	
	
	
	public ListNavigationAction(Object host, Field field,
			NodeNavigator navigator) {
		super();
		this.host = host;
		this.field = field;
		this.navigator = navigator;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		Component invoker = (Component)e.getSource();
		
		JPopupMenu popup = new JPopupMenu();
		
		for(Object element : (Iterable<?>)ReflectUtils.get(host, field))
		{
			JMenuItem item = new JMenuItem(new GotoNodeAction(navigator, host, element));
			RenderUtils.attachIcon(element, new PostRenderActionButton(item));
			popup.add(item);
		}
		
		popup.show(invoker, 0, 0);
	}

}
