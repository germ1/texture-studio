package net.mgsx.numart.editor.rendering;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.swing.JPanel;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.control.impl.MousePointer;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.framework.utils.ReflectUtils.Visitor;
import net.mgsx.numart.realtime.RTRenderer;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.FPSAnimator;

public class RTCanvas extends JPanel implements GLEventListener
{
	private static final long serialVersionUID = 1L;

	private RTRenderer renderer;
	
	private Animator animator;
	
	private GLCanvas canvas;
	
	public RTCanvas() 
	{
		super();
		setSize(320, 240);
		
		// GLCapabilities caps = new GLCapabilities();
		
		// caps.setDepthBits(16);
		// caps.setNumSamples(2);
		// canvas = new GLCanvas(caps);
		
		canvas = new GLCanvas();
		
		this.setLayout(new BorderLayout());
	    this.add(canvas, BorderLayout.CENTER);
	    canvas.addGLEventListener(this);
		// animator = new Animator(canvas); 
		animator = new FPSAnimator(canvas, 60, true);
		//animator = new Animator(canvas);
		//animator.setRunAsFastAsPossible(true);
		
		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				MousePointer.clicked = true;
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				MousePointer.clicked = false;
			}
		});
		canvas.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				MousePointer.px = (double)e.getX() / (double)getWidth();
				MousePointer.py = (double)e.getY() / (double)getHeight();
			}
		});
		
	}
	
	public void start(RTRenderer renderer)
	{
		this.renderer = renderer;
		animator.start();
	}
	
	public void stop()
	{
		animator.stop();
		renderer.dispose();
		
		ReflectUtils.visitGraph(renderer, new Visitor() {
			
			@Override
			public void visit(Object object) {
				if(object instanceof NamedElement)
				{
					((NamedElement) object).destroy();
				}
			}
		});
	}

	@Override
	public void display(GLAutoDrawable drawable) 
	{
		renderer.render(drawable.getGL(), drawable.getWidth(), drawable.getHeight());
	}

	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
		// nothing todo
	}

	private static boolean initOnce = false;
	
	@Override
	public void init(GLAutoDrawable drawable) 
	{
		if(!initOnce)
		{
			initOnce = true;
			System.out.println("GL_VENDOR " + drawable.getGL().glGetString(GL.GL_VENDOR));
			System.out.println("GL_RENDERER " + drawable.getGL().glGetString(GL.GL_RENDERER));
			System.out.println("GL_VERSION " + drawable.getGL().glGetString(GL.GL_VERSION));
			System.out.println("GL_SHADING_LANGUAGE_VERSION " + drawable.getGL().glGetString(GL.GL_SHADING_LANGUAGE_VERSION));
			System.out.println("GL_EXTENSIONS " + drawable.getGL().glGetString(GL.GL_EXTENSIONS));
		}
		renderer.init(drawable.getGL());
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		// nothing todo
	}
}
