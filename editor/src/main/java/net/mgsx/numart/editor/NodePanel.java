package net.mgsx.numart.editor;

import net.mgsx.numart.editor.rendering.RenderUtils;
import net.mgsx.numart.editor.rendering.RenderUtils.PostRenderActionTaskPane;
import net.mgsx.tools.structure.graph.Graph;
import net.mgsx.tools.structure.graph.NodeListener;

import org.jdesktop.swingx.JXTaskPane;

public class NodePanel extends JXTaskPane implements NodeListener<Object, String> 
{
	private static final long serialVersionUID = 1L;

	private Object node;
	private Graph<Object, String> graph;
	
	public NodePanel(Graph<Object, String> graph, Object node) {
		super();
		this.node = node;
		this.graph = graph;
		
		init();
		
		graph.addNodeListener(this, node);
		
		// TODO setToolTipText(DocumentationUtils.getPlainTextSimpleDoc(node));
	}
	private void init()
	{
		RenderUtils.attachIcon(node, new PostRenderActionTaskPane(this));
	}
	
	
	private void updateIcon()
	{
		RenderUtils.updateIcon(node);
	}
	
	private void updateIconNow()
	{
		RenderUtils.updateIcon(node);
	}
	
	@Override
	public void onLinkAdded(Object sourceNode, Object targetNode,
			String linkData) {
		// updateIconNow();
	}
	@Override
	public void onLinkRemoved(Object sourceNode, Object targetNode,
			String linkData) {
		// updateIconNow();
	}
	@Override
	public void onLinkChanged(Object sourceNode, String linkData,
		Object oldTargetNode, Object newTargetNode) {
		updateIconNow();
	}
	@Override
	public void onRemoved() {
		graph.removeNodeListener(this, node);
	}
	@Override
	public void onUpdate() 
	{
		updateIcon();
	}
}
