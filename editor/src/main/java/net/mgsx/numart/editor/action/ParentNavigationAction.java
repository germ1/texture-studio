package net.mgsx.numart.editor.action;

import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import net.mgsx.numart.editor.NodeNavigator;
import net.mgsx.numart.editor.rendering.RenderUtils;
import net.mgsx.numart.editor.rendering.RenderUtils.PostRenderActionButton;
import net.mgsx.tools.structure.graph.Graph;

public class ParentNavigationAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private Object host;
	
	private Graph<Object, String> graph;
	
	private NodeNavigator navigator;
	
	
	public ParentNavigationAction(Object host, Graph<Object, String> graph,
			NodeNavigator navigator) {
		super();
		this.host = host;
		this.graph = graph;
		this.navigator = navigator;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Component invoker = (Component)e.getSource();
		
		JPopupMenu popup = new JPopupMenu();
		
		for(Object parent : graph.getAllSources(host))
		{
			JMenuItem item = new JMenuItem(new GotoNodeAction(navigator, host, parent));
			RenderUtils.attachIcon(parent, new PostRenderActionButton(item));
			popup.add(item);
		}
		
		popup.show(invoker, 0, 0);
	}

}
