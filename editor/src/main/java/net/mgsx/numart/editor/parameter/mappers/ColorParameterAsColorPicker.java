package net.mgsx.numart.editor.parameter.mappers;

import java.lang.reflect.Field;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;

public class ColorParameterAsColorPicker extends AbstractParameter
{
	public ColorParameterAsColorPicker(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();
		add(AbstractParameter.createParameterLabel(field));
		add(new ColorValueAsColorPicker().create(this, host, field, parameterCompleteHandler));
	}
}
