package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import net.mgsx.numart.editor.rendering.RenderManager;
import net.mgsx.numart.realtime.RTRenderer;
import net.mgsx.numart.runtime.rendering.FullscreenFrame;
import net.mgsx.numart.runtime.rendering.ScreenManager.Screen;

public class FullscreenAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private RenderManager renderManager;
	
	private Screen screen;
	
	public FullscreenAction(String title, RenderManager renderManager, Screen screen) {
		super(title);
		this.renderManager = renderManager;
		this.screen = screen;
	}



	@Override
	public void actionPerformed(ActionEvent event) 
	{
		Object currentRenderer = renderManager.getRenderer();
		if(currentRenderer instanceof RTRenderer)
		{
			renderManager.cancel();
			FullscreenFrame.open((RTRenderer)currentRenderer, screen);
		}
	}

}
