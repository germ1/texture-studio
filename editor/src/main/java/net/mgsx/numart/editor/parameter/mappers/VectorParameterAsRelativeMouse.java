package net.mgsx.numart.editor.parameter.mappers;

import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.editor.util.AdaptiveMouseControl;
import net.mgsx.numart.editor.util.AdaptiveMouseListener;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Polar;

public class VectorParameterAsRelativeMouse extends AbstractParameter
{
	@SuppressWarnings("unused")
	private AdaptiveMouseControl amc;
	
	private JLabel lbl;
	
	private void updateLabel(Field field, Polar value)
	{
		lbl.setText(field.getName() + String.format(" ( %.2f %.2f )", value.phi, value.theta));
	}

	public VectorParameterAsRelativeMouse(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();
		
		JPanel panel = this;
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JPanel inPanel = new JPanel();
		inPanel.setLayout(new FlowLayout());
		
		lbl = new JLabel();
		panel.add(lbl);
		
		updateLabel(field, new Polar((Point3D)ReflectUtils.get(host, field)));
		
		amc = new AdaptiveMouseControl(lbl, new AdaptiveMouseListener() {
			@Override
			public void onComplete(MouseEvent event, int dx, int dy) 
			{
				parameterCompleteHandler.complete(VectorParameterAsRelativeMouse.this);
			}
			@Override
			public void onChange(MouseEvent event, int idx, int idy) 
			{
				double dx = (double)idx * 6 / 1000;
				double dy = (double)idy * 6 / 1000;
				
				double thetaSldValue = -dy;
				double phiSldValue = dx;
				
				Point3D value = (Point3D)ReflectUtils.get(host, field);

				Polar polar = new Polar(value);
				
				polar.theta += thetaSldValue;
				polar.phi += phiSldValue;
				
				Point3D uv = polar.toUnitVector();
				value.x = uv.x;
				value.y = uv.y;
				value.z = uv.z;
				
				updateLabel(field, polar);
			}
		});
	}

}
