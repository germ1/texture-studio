package net.mgsx.numart.editor.parameter.mappers;

import java.awt.Component;
import java.lang.reflect.Field;
import java.util.EventObject;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;

import net.mgsx.numart.editor.NodeNavigator;
import net.mgsx.numart.editor.action.EditListAction;
import net.mgsx.numart.editor.action.ListNavigationAction;
import net.mgsx.numart.editor.model.GraphUIContext;
import net.mgsx.numart.editor.model.NodeGraphUtil;
import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.QuickParameterFactory;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.tools.structure.graph.Graph;
import net.mgsx.tools.structure.graph.NodeAdapter;

import org.jdesktop.swingx.VerticalLayout;

/**
 * @see http://docs.oracle.com/javase/tutorial/uiswing/components/list.html
 * 
 * @author mgsx
 *
 */
public class ListParameterAsEditor extends AbstractParameter
{
	public ListParameterAsEditor(final Object host, final Field field, final Graph<Object, String> graph, final NodeNavigator nodeNavigator)
	{
		super();
		
		JButton editButton = new JButton(new EditListAction(host, field, graph));
		editButton.setText("Edit");
		
		JButton gotoButton = new JButton(new ListNavigationAction(host, field, nodeNavigator));
		gotoButton.setText("Goto...");
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.add(AbstractParameter.createParameterLabel(field));
		panel.add(editButton);
		panel.add(gotoButton);
		
		JTable list = new JTable(new ParameterListModel(graph, host, field));
		list.setCellEditor(new TableCellEditor() {
			
			@Override
			public boolean stopCellEditing() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean shouldSelectCell(EventObject anEvent) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public void removeCellEditorListener(CellEditorListener l) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean isCellEditable(EventObject anEvent) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Object getCellEditorValue() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public void cancelCellEditing() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void addCellEditorListener(CellEditorListener l) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public Component getTableCellEditorComponent(JTable table, Object value,
					boolean isSelected, int row, int column) {
				GraphUIContext context = new GraphUIContext();
				context.panel = ListParameterAsEditor.this;
				context.graph = graph;
				context.host = host;
				context.nodeNavigator = nodeNavigator;
				return QuickParameterFactory.getQuickEditComponent(context, value);
			}
		});
		
//		list.setCellRenderer(new ListCellRenderer(){
//
//			@Override
//			public Component getListCellRendererComponent(
//					JList list,
//	                Object value,
//	                int index,
//	                boolean isSelected,
//	                boolean cellHasFocus) 
//			{
//				GraphUIContext context = new GraphUIContext();
//				context.panel = ListParameterAsEditor.this;
//				context.graph = graph;
//				context.host = host;
//				context.nodeNavigator = nodeNavigator;
//				return QuickParameterFactory.getQuickEditComponent(context, value);
//			}
//			
//		});
		
		setLayout(new VerticalLayout());
		add(panel);
		// add(list);
	}
	
	private static class ParameterListModel extends AbstractTableModel
	{
		private Graph<Object, String> graph;
		private Object host;
		private Field field;
		
		public ParameterListModel(Graph<Object, String> graph, Object host,
				final Field field) {
			super();
			this.graph = graph;
			this.host = host;
			this.field = field;
			addGraphListener();
		}
		
		private void addGraphListener()
		{
			graph.addNodeListener(new NodeAdapter<Object, String>(){
				@Override
				public void onLinkAdded(Object sourceNode, Object targetNode,
						String linkData) {
					if(NodeGraphUtil.isListLinkData(linkData, field))
					{
						int index = getListField().indexOf(targetNode);
						// fireIntervalAdded(this, index, index);
						fireTableRowsInserted(index, index);
					}
				}
				@Override
				public void onLinkRemoved(Object sourceNode, Object targetNode,
						String linkData) {
					if(NodeGraphUtil.isListLinkData(linkData, field))
					{
						// TODO pas vrai
						int index = getListField().size(); 
						// fireIntervalRemoved(this, index, index);
						fireTableRowsDeleted(index, index);
					}
				}
				@Override
				public void onLinkChanged(Object sourceNode, String linkData,
						Object oldTargetNode, Object newTargetNode) {
					if(NodeGraphUtil.isListLinkData(linkData, field))
					{
						int index = getListField().indexOf(newTargetNode);
						// fireContentsChanged(this, index, index);
						fireTableRowsUpdated(index, index);
					}
				}
			}, host);
		}

		public Object getElementAt(int index) 
		{
			return getListField().get(index);
		}

		public int getSize() 
		{
			return getListField().size();
		}
		
		private List<?> getListField()
		{
			return (List<?>)ReflectUtils.get(host, field);
		}

		@Override
		public int getColumnCount() {
			return 1;
		}

		@Override
		public int getRowCount() {
			return getSize();
		}

		@Override
		public Object getValueAt(int row, int col) {
			return getElementAt(row);
		}
		
	}
	
}
