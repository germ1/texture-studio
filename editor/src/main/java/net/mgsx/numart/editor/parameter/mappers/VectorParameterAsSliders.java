package net.mgsx.numart.editor.parameter.mappers;

import java.lang.reflect.Field;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Polar;

public class VectorParameterAsSliders extends AbstractParameter
{
	public VectorParameterAsSliders(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();
		JPanel panel = this;
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JPanel inPanel = new JPanel();
		inPanel.setLayout(new BoxLayout(inPanel, BoxLayout.Y_AXIS));
		
		panel.add(new JLabel(field.getName()));
		panel.add(inPanel);
		
		Point3D value = (Point3D)ReflectUtils.get(host, field);
		
		Polar polar = new Polar(value);
		
		double thetaValue = polar.theta / (Math.PI);
		double phiValue = polar.phi / (Math.PI*2) + 0.5;
		
		final JLabel thetaLabelValue = new JLabel();
		updateParameterValue(thetaLabelValue, thetaValue);
		final JLabel phiLabelValue = new JLabel();
		updateParameterValue(phiLabelValue, phiValue);
		
		final JSlider thetaSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, (int)(thetaValue * 100));
		final JSlider phiSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, (int)(phiValue * 100));
		
		ChangeListener changeListener = new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent event) {
				if(!thetaSlider.getValueIsAdjusting() &&
						!phiSlider.getValueIsAdjusting())
				{
					parameterCompleteHandler.complete(VectorParameterAsSliders.this);
				}
				double thetaSldValue = (double)thetaSlider.getValue() / 100.0;
				double phiSldValue = (double)phiSlider.getValue() / 100.0;
				
				double thetaValue = Math.PI * thetaSldValue;
				double phiValue =  2 * Math.PI * (phiSldValue - 0.5);
				
				Point3D value = (Point3D)ReflectUtils.get(host, field);

				Polar polar = new Polar(thetaValue, phiValue);
				Point3D uv = polar.toUnitVector();
				value.x = uv.x;
				value.y = uv.y;
				value.z = uv.z;
				
				updateParameterValue(thetaLabelValue, thetaSldValue);
				updateParameterValue(phiLabelValue, phiSldValue);
			}
		};
		
		thetaSlider.addChangeListener(changeListener);
		phiSlider.addChangeListener(changeListener);
		
		JPanel thetaPanel = new JPanel();
		thetaPanel.setLayout(new BoxLayout(thetaPanel, BoxLayout.X_AXIS));
		thetaPanel.add(thetaLabelValue);
		thetaPanel.add(thetaSlider);
		inPanel.add(thetaPanel);
		
		JPanel phiPanel = new JPanel();
		phiPanel.setLayout(new BoxLayout(phiPanel, BoxLayout.X_AXIS));
		phiPanel.add(phiLabelValue);
		phiPanel.add(phiSlider);
		inPanel.add(phiPanel);
	}

}
