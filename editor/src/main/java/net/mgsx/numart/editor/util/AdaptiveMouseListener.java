package net.mgsx.numart.editor.util;

import java.awt.event.MouseEvent;

public interface AdaptiveMouseListener 
{
	public void onChange(MouseEvent event, int dx, int dy);
	public void onComplete(MouseEvent event, int dx, int dy);
}
