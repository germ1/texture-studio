package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import net.mgsx.numart.editor.EditorFrame;
import net.mgsx.numart.editor.InteractiveResolver;
import net.mgsx.numart.persistence.PersistenceUtil;

public class LoadAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private Object host;
	
	private Field field;
	
	private EditorFrame frame;

	public LoadAction(Object host, Field field, EditorFrame frame) {
		super("Open...");
		this.host = host;
		this.field = field;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JFileChooser fc = new JFileChooser(PersistenceUtil.defaultFolder);
		
		if(JFileChooser.APPROVE_OPTION == fc.showOpenDialog(null))
		{
			PersistenceUtil.defaultFolder = fc.getCurrentDirectory();
			File file = fc.getSelectedFile();
			Object root = null;
			try {
				root = new PersistenceUtil(new InteractiveResolver(frame)).load(file);
			} catch (FileNotFoundException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}
			if(field != null)
			{
				try {
					field.set(host, root);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			frame.setRenderer(root);
		}

	}

}
