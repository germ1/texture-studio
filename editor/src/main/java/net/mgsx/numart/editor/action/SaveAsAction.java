package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import net.mgsx.numart.editor.EditorFrame;
import net.mgsx.numart.persistence.PersistenceUtil;

public class SaveAsAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private Object object;
	
	public SaveAsAction(Object object) {
		super("Save as ...");
		this.object = object;
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		
		JFileChooser fc = new JFileChooser(PersistenceUtil.defaultFolder);
		if(JFileChooser.APPROVE_OPTION == fc.showSaveDialog(null))
		{
			PersistenceUtil.defaultFolder = fc.getCurrentDirectory();
			File file = fc.getSelectedFile();
			try {
				// TODO faire quelquechose de plus abstrait.
				new PersistenceUtil().save(file, object instanceof EditorFrame ? ((EditorFrame)object).getRenderer() : object);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}

}
