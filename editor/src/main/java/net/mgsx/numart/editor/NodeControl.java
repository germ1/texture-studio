package net.mgsx.numart.editor;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import net.mgsx.numart.editor.action.HelpAction;
import net.mgsx.numart.editor.action.ParentNavigationAction;
import net.mgsx.numart.editor.action.RenderAction;
import net.mgsx.numart.editor.action.SaveAsAction;
import net.mgsx.numart.editor.converter.PreviewNodeConverter;
import net.mgsx.numart.editor.rendering.RenderManager;
import net.mgsx.tools.structure.graph.Graph;

public class NodeControl extends JPanel
{
	private Graph<Object, String> graph;
	private RenderManager renderManager;
	
	
	public NodeControl(Graph<Object, String> graph, Object host, NodeNavigator navigator, RenderManager renderManager) {
		super();
		this.graph = graph;
		this.renderManager = renderManager;
		createCommonControls(this, host, navigator);
	}

	private void createCommonControls(JPanel root, Object host, NodeNavigator navigator)
	{
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		createParentNavigator(panel, host, navigator);
		createContextRender(panel, host);
		createContextSave(panel, host);
		createContextHelp(panel, host);
		
		root.add(panel);
	}
	
	private void createContextRender(JPanel root, Object host)
	{
		JButton renderButton = new JButton(new RenderAction(host, renderManager, new PreviewNodeConverter()));
		renderButton.setText("Render");
		root.add(renderButton);
	}
	private void createContextSave(JPanel root, Object host)
	{
		JButton saveButton = new JButton(new SaveAsAction(host));
		saveButton.setText("Save...");
		root.add(saveButton);
	}
	private void createContextHelp(JPanel root, Object host)
	{
		JButton helpButton = new JButton(new HelpAction(host, root));
		helpButton.setText("Help");
		root.add(helpButton);
	}
	private void createParentNavigator(JPanel root, Object host, NodeNavigator navigator)
	{
		JButton gotoButton = new JButton(new ParentNavigationAction(host, graph, navigator));
		gotoButton.setText("From...");
		root.add(gotoButton);
	}

}
