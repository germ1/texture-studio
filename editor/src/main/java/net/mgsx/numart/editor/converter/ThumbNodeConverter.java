package net.mgsx.numart.editor.converter;

import net.mgsx.numart.editor.NodeConverter;
import net.mgsx.numart.framework.Renderer;
import net.mgsx.numart.procedural.renderer.TextureRenderer;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.Disk;
import net.mgsx.numart.procedural.texture.impl.Inverse;
import net.mgsx.numart.procedural.texture.impl.Sinusoidal;
import net.mgsx.numart.procedural.texture.impl.Step;
import net.mgsx.numart.procedural.texture.impl.Tiles;
import net.mgsx.numart.raytracing.core.Geometry;
import net.mgsx.numart.realtime.RTRenderer;
import net.mgsx.tools.ipc.LoopWorkflow;

public class ThumbNodeConverter implements NodeConverter
{
	private NodeConverter baseConverter;
	
	public ThumbNodeConverter(NodeConverter baseConverter) {
		super();
		this.baseConverter = baseConverter;
	}

	@Override
	public Renderer convertToRenderer(Object object) 
	{
		Renderer renderer;
		// TODO XXX !!!
//		Icon icon = object.getClass().getAnnotation(Icon.class);
//		if(icon != null)
//		{
//			renderer = (Renderer)ReflectUtils.newInstance(icon.value());
//		}
		if(object instanceof LoopWorkflow)
		{
			renderer = null;
		}
		else
		{
			renderer = baseConverter.convertToRenderer(object);
			if(renderer == null)
			{
				renderer = getDefaultRenderer(object);
			}
			else if(renderer instanceof RTRenderer)
			{
				renderer = getDefaultRTRenderer(object);
			}
		}
		return renderer;
	}
	
	private Renderer getDefaultRenderer(Object objectToRender)
	{
		final Object iconObject;
		if(objectToRender instanceof Geometry)
		{
			Sinusoidal sin = new Sinusoidal();
			sin.phase = 0.0;
			sin.frequency = 3;
			sin.operand = new Tiles();
			Step step = new Step();
			step.operand = sin;
			step.range = 0.5;
			step.threshold = 0.5;
			iconObject = step;
		}
		else
		{
			iconObject = new Tiles();
		}
		return getSimpleRenderer(iconObject);
	}
	private Renderer getDefaultRTRenderer(Object objectToRender)
	{
		return getSimpleRenderer(new Disk());
	}
	private Renderer getSimpleRenderer(Object objectToRender)
	{
		if(objectToRender instanceof ScalarFromSpace)
		{
			Inverse inv = new Inverse();
			inv.operand = (ScalarFromSpace)objectToRender;
			objectToRender = inv;
		}
		TextureRenderer renderer = (TextureRenderer)baseConverter.convertToRenderer(objectToRender);
		renderer.size.x = 1;
		renderer.size.y = 1;
		return renderer;
	}

}
