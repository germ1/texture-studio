package net.mgsx.numart.editor.parameter.mappers;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;

import net.mgsx.numart.editor.parameter.Parameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;

public class ColorValueAsColorPicker implements Parameter
{
	
	public Component create(final Container container, final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		Color value = (Color)ReflectUtils.get(host, field);
		if(value == null){
			value = new Color(0);
			ReflectUtils.set(host, field, value);
		}
		
		java.awt.Color initialColor = new java.awt.Color((float)value.r, (float)value.g, (float)value.b, (float)value.a);
		
		final JButton colorPicker = new JButton("   ");
		colorPicker.setSize(64, 48);
		colorPicker.setBackground(initialColor);
		colorPicker.setForeground(initialColor);
		
		final JColorChooser colorChooser = new JColorChooser(initialColor);
		
		final ActionListener validateColorAction = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				java.awt.Color chooserColor = colorChooser.getColor();
				Color value = (Color)ReflectUtils.get(host, field);
				float [] comps = chooserColor.getRGBComponents(null);
				value.r = comps[0];
				value.g = comps[1];
				value.b = comps[2];
				value.a = comps[3];
				parameterCompleteHandler.complete(ColorValueAsColorPicker.this);
				colorPicker.setBackground(chooserColor);
				colorPicker.setForeground(chooserColor);
			}
		};
		
		
		colorPicker.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = JColorChooser.createDialog(container, "Select your color", true, colorChooser, validateColorAction, null);
				dialog.setVisible(true);
			}
		});
		
		return colorPicker;

	}

}
