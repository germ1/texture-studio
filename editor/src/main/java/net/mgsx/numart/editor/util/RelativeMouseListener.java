package net.mgsx.numart.editor.util;

import java.awt.event.MouseEvent;

/**
 * Listener du composant {@link RelativeMouseControl}.
 * 
 * Les coordonnées XY sont en pixels de gauche à droite et du haut vers le bas.
 * 
 * @author mgsx
 *
 */
public interface RelativeMouseListener 
{
	/**
	 * Appelé au début de la phase (au click).
	 * @param event l'événement de la souris d'origine
	 */
	public void onBegin(MouseEvent event);
	
	/**
	 * Appelé lors d'un changement
	 * @param event l'événement de la souris d'origine
	 * @param dx le différentiel x
	 * @param dy le différentiel y
	 */
	public void onChange(MouseEvent event, int dx, int dy);
	
	/**
	 * Appelé en fin de phase (au relachement du bouton)
	 * @param event l'événement de la souris d'origine
	 */
	public void onComplete(MouseEvent event);
}
