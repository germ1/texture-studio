package net.mgsx.numart.editor.parameter.mappers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;

public class BoolParameterAsCheckBox extends AbstractParameter
{
	public BoolParameterAsCheckBox(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();
		boolean value = false;
		try {
			value = field.getBoolean(host);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final JCheckBox cb = new JCheckBox();
		cb.setSelected(value);
		cb.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					field.setBoolean(host, cb.isSelected());
					parameterCompleteHandler.complete(BoolParameterAsCheckBox.this);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		JPanel panel = this;
		panel.add(AbstractParameter.createParameterLabel(field));
		panel.add(cb);

	}
}
