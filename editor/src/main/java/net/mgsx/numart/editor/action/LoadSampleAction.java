package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import net.mgsx.numart.editor.EditorFrame;
import net.mgsx.numart.framework.Renderer;
import net.mgsx.numart.framework.utils.ReflectUtils;

public class LoadSampleAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private EditorFrame frame;
	
	private Class<? extends Renderer> cls;
	
	
	
	public LoadSampleAction(EditorFrame frame, Class<? extends Renderer> cls, String name) {
		super(name);
		this.frame = frame;
		this.cls = cls;
	}



	@Override
	public void actionPerformed(ActionEvent event) {
		frame.setRenderer(ReflectUtils.createDefault(cls));
	}

}
