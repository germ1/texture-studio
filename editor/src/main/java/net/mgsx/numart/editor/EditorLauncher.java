package net.mgsx.numart.editor;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.FontUIResource;

import net.mgsx.tools.system.PlatformUtils;

public class EditorLauncher {

	/**
	 * @param args
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		
		// change the lookandfeel
		if(PlatformUtils.isUnix())
		{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
		}
		
		UIManager.put("Slider.paintValue", false);
		
		UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		
		UIManager.put("Label.font",new Font("Dialog",Font.PLAIN,10));
		UIManager.put("TitledBorder.font",new Font("Dialog",Font.BOLD,10));
		UIManager.put("Button.font",new Font("Dialog",Font.BOLD,10));
		UIManager.put("Tooltip.font",new Font("Dialog",Font.PLAIN,10));

		  // setting taskpanecontainer defaults
		  UIManager.put("TaskPaneContainer.useGradient", Boolean.FALSE);
		  UIManager.put("TaskPaneContainer.background", Color.LIGHT_GRAY);

		  // setting taskpane defaults
		  UIManager.put("TaskPane.font", new FontUIResource(new Font("Verdana", Font.PLAIN, 10)));
		  UIManager.put("TaskPane.titleBackgroundGradientStart", Color.WHITE);
		  UIManager.put("TaskPane.titleBackgroundGradientEnd", new Color(0xAAAAFF));
		  
		  
		UIManager.getDefaults().put("TaskPaneContainer.border",
				new BorderUIResource(BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		// ???
		UIManager.getDefaults().put("TaskPane.border",
				new BorderUIResource(BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		
		// create the frame
		
		EditorFrame frame = new EditorFrame();
		// SwingUtilities.updateComponentTreeUI(frame);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	

}
