package net.mgsx.numart.editor.rendering;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker.StateValue;

import net.mgsx.numart.framework.Renderer;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.procedural.renderer.DirectRenderer;
import net.mgsx.numart.procedural.renderer.SoftwareRenderer;
import net.mgsx.numart.realtime.RTRenderer;

public class RenderManager 
{
	private JProgressBar progressBar;
	
	private JPanel renderingPanel;

	private RenderingWorker renderingWorker;
	private DirectRenderingWorker directRenderingWorker;
	
	private Object renderer;
	
	private RTCanvas rtCanvas;
	
	private Runnable onCompleteCallback;
	
	public RenderManager(JPanel renderingPanel, JProgressBar progressBar) {
		this.renderingPanel = renderingPanel;
		this.progressBar = progressBar;
	}
	public void setRenderer(Object renderer) {
		this.renderer = renderer;
	}
	public Object getRenderer() {
		return renderer;
	}
	public void start(Renderer newRenderer)
	{
		this.renderer = newRenderer;
		start();
	}
	public void start()
	{
		cancel();
		if(renderer instanceof RTRenderer)
		{
			// precompute
			ReflectUtils.checkForPrecomputation(renderer);
			
			rtCanvas = new RTCanvas();
			renderingPanel.setLayout(new BorderLayout());
			renderingPanel.add(rtCanvas, BorderLayout.CENTER);
			renderingPanel.revalidate();
			rtCanvas.start((RTRenderer)renderer);
		}
		else if(renderer instanceof DirectRenderer)
		{
			directRenderingWorker = new DirectRenderingWorker(renderingPanel, (DirectRenderer)renderer);
			directRenderingWorker.execute();
		}
		else if(renderer instanceof SoftwareRenderer)
		{
			renderingWorker = new RenderingWorker(renderingPanel, progressBar);
			renderingWorker.addPropertyChangeListener(new PropertyChangeListener() {
				
				@Override
				public void propertyChange(PropertyChangeEvent e) {
					if(e.getPropertyName().equals("state"))
					{
						if(e.getNewValue().equals(StateValue.DONE))
						{
							if(onCompleteCallback != null)
							{
								onCompleteCallback.run();
							}
						}
					}
					
				}
			});
			renderingWorker.execute((SoftwareRenderer)renderer);
		}
		else
		{
			System.err.println("not handled renderer");
		}
	}
	public void cancel()
	{
		if(rtCanvas != null)
		{
			rtCanvas.stop();
			renderingPanel.remove(rtCanvas);
			rtCanvas = null;
		}
		if(renderingWorker != null)
		{
			renderingWorker.cancel(true);
			renderingWorker = null;
		}
		if(directRenderingWorker != null)
		{
			directRenderingWorker.end = true;
			directRenderingWorker = null;
		}
	}
	
	public BufferedImage getRenderingImage() {
		return renderingWorker == null ? (directRenderingWorker == null ? null : directRenderingWorker.renderingImage) : renderingWorker.getRenderingImage();
	}
	
	public void onComplete(Runnable onCompleteCallback)
	{
		this.onCompleteCallback = onCompleteCallback;
	}
}
