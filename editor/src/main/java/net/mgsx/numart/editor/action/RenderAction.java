package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import net.mgsx.numart.editor.NodeConverter;
import net.mgsx.numart.editor.rendering.RenderManager;
import net.mgsx.numart.framework.Renderer;

public class RenderAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private Renderer renderer;
	
	private RenderManager renderManager;
	
	private Object objectToRender;
	
	private NodeConverter converter;
	
	public RenderAction(Object objectToRender, RenderManager renderManager, NodeConverter converter) {
		super();
		this.renderManager = renderManager;
		this.converter = converter;
		this.renderer = converter.convertToRenderer(objectToRender);
		setEnabled(renderer != null);
		this.objectToRender = objectToRender;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		renderManager.start(converter.convertToRenderer(objectToRender));
	}

}
