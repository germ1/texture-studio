package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import net.mgsx.tools.structure.graph.Graph;

public class ClearAction extends AbstractAction {
	
	private static final long serialVersionUID = 1L;
	
	private Graph<Object, String> graph;
	
	
	
	public ClearAction(String name, Graph<Object, String> graph) {
		super(name);
		this.graph = graph;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		graph.clear();
	}

}
