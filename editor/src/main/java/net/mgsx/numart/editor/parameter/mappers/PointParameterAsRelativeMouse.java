package net.mgsx.numart.editor.parameter.mappers;

import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.editor.util.AdaptiveMouseControl;
import net.mgsx.numart.editor.util.AdaptiveMouseListener;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;

/**
 * 
 * TODO comments
 * 
 * @author mgsx
 *
 */
public class PointParameterAsRelativeMouse extends AbstractParameter
{
	@SuppressWarnings("unused")
	private AdaptiveMouseControl amcX;
	@SuppressWarnings("unused")
	private AdaptiveMouseControl amcY;
	@SuppressWarnings("unused")
	private AdaptiveMouseControl amcZ;
	@SuppressWarnings("unused")
	private AdaptiveMouseControl amcXY;
	@SuppressWarnings("unused")
	private AdaptiveMouseControl amcYZ;
	@SuppressWarnings("unused")
	private AdaptiveMouseControl amcZX;
	@SuppressWarnings("unused")
	private AdaptiveMouseControl amcALL;
	
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblZ;
	
	private static double adaptive(double base, int inc)
	{
		double log = Math.floor(Math.log10(Math.abs(base) + 0.01));
		double range = Math.pow(10, log) / 100;
		if(range < 0.01) range = 0.01;
		return (Math.round(base / range) + (double)inc) * range;
	}
	
	private void updateLabel(Point3D value)
	{
		lblX.setText(String.format("%.2f", value.x));
		lblY.setText(String.format(" %.2f", value.y));
		lblZ.setText(String.format(" %.2f", value.z));
	}
	
	public PointParameterAsRelativeMouse(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();

		JPanel panel = this;
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JPanel inPanel = new JPanel();
		inPanel.setLayout(new FlowLayout());
		
		JLabel lbl = new JLabel(field.getName());
		panel.add(lbl);
		
		JLabel lblBeg = new JLabel(" ( ");
		panel.add(lblBeg);
		
		lblX = new JLabel();
		panel.add(lblX);
		lblY = new JLabel();
		panel.add(lblY);
		lblZ = new JLabel();
		panel.add(lblZ);
		
		JLabel lblEnd = new JLabel(" ) ");
		panel.add(lblEnd);

		panel.add(inPanel);
		
		// lbl.setForeground(Color.BLUE);
		
		JButton btXY = new JButton("XY");
		inPanel.add(btXY);
		JButton btYZ = new JButton("YZ");
		inPanel.add(btYZ);
		JButton btZX = new JButton("ZX");
		inPanel.add(btZX);
		JButton btALL = new JButton("ALL");
		inPanel.add(btALL);
		
		updateLabel((Point3D)ReflectUtils.get(host, field));
		
		amcX = new AdaptiveMouseControl(lblX, new AdaptiveMouseListener() {
			@Override
			public void onComplete(MouseEvent event, int dx, int dy) 
			{
				parameterCompleteHandler.complete(PointParameterAsRelativeMouse.this);
			}
			@Override
			public void onChange(MouseEvent event, int dx, int dy) 
			{
				Point3D value = (Point3D)ReflectUtils.get(host, field);
				value.x = adaptive(value.x, -dy);
				updateLabel(value);
			}
		});
		amcY = new AdaptiveMouseControl(lblY, new AdaptiveMouseListener() {
			@Override
			public void onComplete(MouseEvent event, int dx, int dy) 
			{
				parameterCompleteHandler.complete(PointParameterAsRelativeMouse.this);
			}
			@Override
			public void onChange(MouseEvent event, int dx, int dy) 
			{
				Point3D value = (Point3D)ReflectUtils.get(host, field);
				value.y = adaptive(value.y, -dy);
				updateLabel(value);
			}
		});
		amcZ = new AdaptiveMouseControl(lblZ, new AdaptiveMouseListener() {
			@Override
			public void onComplete(MouseEvent event, int dx, int dy) 
			{
				parameterCompleteHandler.complete(PointParameterAsRelativeMouse.this);
			}
			@Override
			public void onChange(MouseEvent event, int dx, int dy) 
			{
				Point3D value = (Point3D)ReflectUtils.get(host, field);
				value.z = adaptive(value.z, -dy);
				updateLabel(value);
			}
		});
		amcXY = new AdaptiveMouseControl(btXY, new AdaptiveMouseListener() {
			@Override
			public void onComplete(MouseEvent event, int dx, int dy) 
			{
				parameterCompleteHandler.complete(PointParameterAsRelativeMouse.this);
			}
			@Override
			public void onChange(MouseEvent event, int dx, int dy) 
			{
				Point3D value = (Point3D)ReflectUtils.get(host, field);
				value.x = adaptive(value.x, dx);
				value.y = adaptive(value.y, -dy);
				updateLabel(value);
			}
		});
		amcYZ = new AdaptiveMouseControl(btYZ, new AdaptiveMouseListener() {
			@Override
			public void onComplete(MouseEvent event, int dx, int dy) 
			{
				parameterCompleteHandler.complete(PointParameterAsRelativeMouse.this);
			}
			@Override
			public void onChange(MouseEvent event, int dx, int dy) 
			{
				Point3D value = (Point3D)ReflectUtils.get(host, field);
				value.z = adaptive(value.z, dx);
				value.y = adaptive(value.y, -dy);
				updateLabel(value);
			}
		});
		amcZX = new AdaptiveMouseControl(btZX, new AdaptiveMouseListener() {
			@Override
			public void onComplete(MouseEvent event, int dx, int dy) 
			{
				parameterCompleteHandler.complete(PointParameterAsRelativeMouse.this);
			}
			@Override
			public void onChange(MouseEvent event, int dx, int dy) 
			{
				Point3D value = (Point3D)ReflectUtils.get(host, field);
				value.x = adaptive(value.x, dx);
				value.z = adaptive(value.z, -dy);
				updateLabel(value);
			}
		});
		amcALL = new AdaptiveMouseControl(btALL, new AdaptiveMouseListener() {
			@Override
			public void onComplete(MouseEvent event, int dx, int dy) 
			{
				parameterCompleteHandler.complete(PointParameterAsRelativeMouse.this);
			}
			@Override
			public void onChange(MouseEvent event, int dx, int dy) 
			{
				Point3D value = (Point3D)ReflectUtils.get(host, field);
				value.x = value.y = value.z = adaptive(value.x, -dy);
				updateLabel(value);
			}
		});
	}
}
