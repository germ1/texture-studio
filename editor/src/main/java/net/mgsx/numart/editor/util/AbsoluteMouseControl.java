package net.mgsx.numart.editor.util;

import java.awt.Component;
import java.awt.event.MouseEvent;

public class AbsoluteMouseControl implements RelativeMouseListener 
{
	@SuppressWarnings("unused")
	private RelativeMouseControl baseControl;
	
	private int dx, dy;
	
	private AbsoluteMouseListener handler;
	
	public AbsoluteMouseControl(Component hostComponent, AbsoluteMouseListener pHandler) 
	{
		baseControl = new RelativeMouseControl(hostComponent, this);
		this.handler = pHandler;
	}

	@Override
	public void onBegin(MouseEvent event) {
		handler.onBegin(event);
	}

	@Override
	public void onChange(MouseEvent event, int dx, int dy) {
		this.dx += dx;
		this.dy += dy;
		handler.onChange(event, this.dx, this.dy);
	}

	@Override
	public void onComplete(MouseEvent event) {
		handler.onComplete(event, dx, dy);
	}
}
