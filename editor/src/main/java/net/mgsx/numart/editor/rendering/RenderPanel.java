package net.mgsx.numart.editor.rendering;

import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class RenderPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private RenderManager renderManager;
	
	public RenderPanel(JProgressBar progressBar) {
		super();
		this.renderManager = new RenderManager(this, progressBar);
	}

	public void paint(java.awt.Graphics g) {
		super.paint(g);
		if(renderManager != null && renderManager.getRenderingImage() != null){
			g.drawImage(renderManager.getRenderingImage(), 0, 0, null);
		}
	};
	
	public RenderManager getRenderManager() {
		return renderManager;
	}
}
