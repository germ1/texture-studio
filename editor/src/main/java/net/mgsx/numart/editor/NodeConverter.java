package net.mgsx.numart.editor;

import net.mgsx.numart.framework.Renderer;

public interface NodeConverter {
	Renderer convertToRenderer(Object object);
}
