package net.mgsx.numart.editor.parameter.mappers;

import java.lang.reflect.Field;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.utils.ReflectUtils;

public class IntParameterAsSlider extends AbstractParameter
{
	public IntParameterAsSlider(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();
		int value = 0;
		try {
			value = field.getInt(host);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final ParamInt aParamInt = field.getAnnotation(ParamInt.class);
		if(value < aParamInt.min())
		{
			value = aParamInt.min();
			ReflectUtils.set(host, field, value);
		}
		else if(value > aParamInt.max())
		{
			value = aParamInt.max();
			ReflectUtils.set(host, field, value);
		}
		
		final JLabel labelValue = new JLabel();
		AbstractParameter.updateParameterValue(labelValue, value);
		
		final JSlider slider = new JSlider(JSlider.HORIZONTAL, aParamInt.min(), aParamInt.max(), value);
			
		
		slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent event) {
				if(!slider.getValueIsAdjusting())
				{
					parameterCompleteHandler.complete(IntParameterAsSlider.this);
				}

				int value = slider.getValue();
				try {
					field.setInt(host, value);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				AbstractParameter.updateParameterValue(labelValue, value);
			}
		});
		
		JPanel panel = this;
		panel.add(AbstractParameter.createParameterLabel(field));
		panel.add(labelValue);
		panel.add(slider);

	}
}
