package net.mgsx.numart.editor.parameter.mappers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;

import javax.swing.JButton;
import javax.swing.JPanel;

import net.mgsx.numart.editor.parameter.AbstractParameter;

public class TriggerAsButton extends AbstractParameter
{
	public TriggerAsButton(final Object host, final Method method)
	{
		super();
		
		JButton button = new JButton(method.getName());
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					method.invoke(host);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		JPanel panel = this;
		panel.add(button);

	}
}
