package net.mgsx.numart.editor.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import net.mgsx.numart.editor.NodePanel;
import net.mgsx.numart.editor.converter.PreviewNodeConverter;
import net.mgsx.numart.editor.converter.ThumbNodeConverter;
import net.mgsx.numart.editor.rendering.RenderUtils;
import net.mgsx.numart.editor.rendering.RenderUtils.PostRenderActionButton;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.tools.structure.graph.Graph;

public class SwapAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	// TODO mettre ailleurs (logique)
	private static List<Field> getSuitableFields(Class<?> clazz, Field field)
	{
		List<Field> fields = new ArrayList<Field>();
		for(Field property : clazz.getFields())
		{
			if(property.getType().isAssignableFrom(field.getType()))
			{
				fields.add(property);
			}
		}
		return fields;
	}
	
	private static boolean isChildOf(Object object, Object child)
	{
		// TODO prevent recursion even if its not possible ...
		if(object != null)
		{
			if(object == child)
			{
				return true;
			}
			for(Field property : object.getClass().getFields())
			{
				if(!property.getType().isPrimitive())
				{
					if(isChildOf(ReflectUtils.get(object, property), child))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private Object host;
	
	private Field field;
	
	private Graph<Object, String> graph;
	
	private Map<Object, JPanel> mapNodePanel;

	public SwapAction(Object host, Field field, Map<Object, JPanel> mapNodePanel, Graph<Object, String> graph) {
		super();
		this.host = host;
		this.field = field;
		this.graph = graph;
		this.mapNodePanel = mapNodePanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// make the list
		List<Class<?>> suitableClasses = ReflectUtils.getInstanciableSubClasses(field.getType());
		
		Collections.sort(suitableClasses, new Comparator<Class<?>>() {
			@Override
			public int compare(Class<?> o1, Class<?> o2) {
				return o1.getSimpleName().compareTo(o2.getSimpleName());
			}
		});
		
		// build the popup menu
		Component invoker = (Component)e.getSource();
		
		JPopupMenu popup = new JPopupMenu();
		
		popup.add(new JMenuItem(new RemoveAction("Remove")));
		
		JMenu itemReplace = new JMenu("Set New ...");
		popup.add(itemReplace);
		
		JMenu itemLink = new JMenu("Set Existing ...");
		popup.add(itemLink);
		
		JMenu itemInsert = new JMenu("Insert New ...");
		popup.add(itemInsert);

		for(Object key : mapNodePanel.keySet())
		{
			Object currentValue = ReflectUtils.get(host, field);
			if(field.getType().isAssignableFrom(key.getClass()) &&
					key != currentValue &&
					!isChildOf(key, host))
			{
				itemLink.add(createItem(new LinkAction(key), key));
			}
		}
		
		for(Class<?> suitableClass : suitableClasses)
		{
			itemReplace.add(createItem(new ReplaceAction(suitableClass, suitableClass.getSimpleName()), suitableClass));
			
			List<Field> suitableFields = getSuitableFields(suitableClass, field);
			if(suitableFields.size() > 1)
			{
				JMenu insertMenu = new JMenu(suitableClass.getSimpleName());
				itemInsert.add(insertMenu);
				for(Field property : suitableFields)
				{
					insertMenu.add(createItem(new InsertAction(suitableClass, property, property.getName()), suitableClass));
				}
			}
			else if(suitableFields.size() > 0)
			{
				Field property = suitableFields.iterator().next();
				itemInsert.add(createItem(new InsertAction(suitableClass, property, suitableClass.getSimpleName()), suitableClass));
			}
		}
		
		popup.show(invoker, 0, 0);

	}
	
	private JMenuItem createItem(Action action, Class<?> suitableClass)
	{
		Object object = ReflectUtils.newInstance(suitableClass);
		object = new ThumbNodeConverter(new PreviewNodeConverter()).convertToRenderer(object);
		JMenuItem item = new JMenuItem(action);
		RenderUtils.attachIcon(object, new PostRenderActionButton(item));
		return item;
	}
	private JMenuItem createItem(Action action, Object object)
	{
		JMenuItem item = new JMenuItem(action);
		item.setIcon(((NodePanel)mapNodePanel.get(object)).getIcon());
		return item;
	}
	
	private class LinkAction extends AbstractAction
	{

		private static final long serialVersionUID = 1L;

		private Object object;
		
		public LinkAction(Object object) {
			super(object.getClass().getSimpleName()); // TODO real named element name.
			this.object = object;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// update the property
			ReflectUtils.set(host, field, object);
			// modify graph link
			graph.link(host, object, field.getName());
		}
	}
	
	private class RemoveAction extends AbstractAction
	{

		private static final long serialVersionUID = 1L;

		public RemoveAction(String name) {
			super(name);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// update property.
			ReflectUtils.set(host, field, null);
			// update graph.
			graph.link(host, null, field.getName());
		}
	}
	abstract private class ItemAction extends AbstractAction
	{

		private static final long serialVersionUID = 1L;

		protected Class<?> clazz;
		
		public ItemAction(Class<?> clazz, String name) {
			super(name);
			this.clazz = clazz;
		}
	}
	
	private class ReplaceAction extends ItemAction
	{

		private static final long serialVersionUID = 1L;


		public ReplaceAction(Class<?> clazz, String name) {
			super(clazz, name);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// set a new object
			Object object = ReflectUtils.createDefault(clazz);
			ReflectUtils.set(host, field, object);
			// update graph
			graph.link(host, object, field.getName());
		}
		
	}
	private class InsertAction extends ItemAction
	{

		private static final long serialVersionUID = 1L;

		private Field property;
		
		public InsertAction(Class<?> clazz, Field property, String name) {
			super(clazz, name);
			this.property = property;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// insert new object
			Object oldObject = ReflectUtils.get(host, field);
			Object newObject = ReflectUtils.newInstance(clazz);
			ReflectUtils.set(host, field, newObject);
			ReflectUtils.set(newObject, property, oldObject);
			// update graph
			graph.link(newObject, oldObject, property.getName());
			graph.link(host, newObject, field.getName());
		}
		
	}

}
