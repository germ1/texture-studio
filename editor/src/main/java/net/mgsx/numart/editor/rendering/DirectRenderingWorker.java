package net.mgsx.numart.editor.rendering;

import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.SwingWorker;

import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.procedural.renderer.DirectRenderer;
import net.mgsx.tools.ipc.LoopWorkflow;
import net.mgsx.tools.ipc.ResourceContext;
import net.mgsx.tools.ipc.SingleResourceContext;

public class DirectRenderingWorker extends SwingWorker<Integer, Integer>
{
	public volatile boolean end = false;
	
	private JPanel renderingPanel;
	
	private DirectRenderer renderer;
	
	BufferedImage renderingImage;
	
	public DirectRenderingWorker(JPanel renderingPanel, DirectRenderer renderer) {
		this.renderingPanel = renderingPanel;
		this.renderer = renderer;
		// precompute tree
		ReflectUtils.checkForPrecomputation(renderer);
		renderingImage = new BufferedImage(renderingPanel.getWidth(), renderingPanel.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
	}
	
	@Override
	protected Integer doInBackground() throws Exception 
	{
		ResourceContext ctx = new SingleResourceContext();
		try{
			((LoopWorkflow)renderer).setup(ctx);
			while(!end)
			{
				try
				{
					renderer.render(renderingImage);
				}
				catch(Throwable e)
				{
					e.printStackTrace();
					return null;
				}
				publish(0);
			}
		}finally{
			((LoopWorkflow)renderer).cleanup(ctx);
		}
		return null;
	}
	
	@Override
	protected void process(List<Integer> chunks) {
		renderingPanel.repaint();
	}
	
	@Override
	protected void done() {
	}
	
}