package net.mgsx.numart.editor.parameter.mappers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.mgsx.midi.MidiManager;
import net.mgsx.midi.MidiManager.MidiListener;
import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.utils.ReflectUtils;

public class DoubleParameterAsSlider extends AbstractParameter
{
	public DoubleParameterAsSlider(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		this(host, field, parameterCompleteHandler, field.getAnnotation(ParamDouble.class), field.getAnnotation(Logarithmic.class));
	}
	public DoubleParameterAsSlider(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler, final ParamDouble annotation, final Logarithmic aLog)
	{
		super();
		double value = 0;
		try {
			value = field.getDouble(host);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(value < annotation.min())
		{
			value = annotation.min();
			ReflectUtils.set(host, field, value);
		}
		else if(value > annotation.max())
		{
			value = annotation.max();
			ReflectUtils.set(host, field, value);
		}
		
		final double curMin, curMax;
		double cursorValue, curVal;
		if(aLog != null)
		{
			curMax = Math.log(annotation.max()) / Math.log(aLog.value());
			curMin = Math.log(annotation.min()) / Math.log(aLog.value());
			curVal = Math.log(value) / Math.log(aLog.value());
		}
		else
		{
			curMax = annotation.max();
			curMin = annotation.min();
			curVal = value;
		}
		cursorValue = (curVal - curMin) / (curMax - curMin);
		
		int val = (int)(cursorValue * 100.0);
		
		final JLabel labelValue = new JLabel();
		AbstractParameter.updateParameterValue(labelValue, value);
		
		final JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 100, val);
		
		JPanel panel = this;
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.add(AbstractParameter.createParameterLabel(field));
		panel.add(labelValue);
		panel.add(slider);
		
		final MidiListener midiListener = new MidiListener() {
			@Override
			public void onMessage(double value) {
				final double fvalue = value;
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						slider.setValue((int)(fvalue * 100));
					}
				});
			}
		};
		slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent event) {
				if(!slider.getValueIsAdjusting())
				{
					parameterCompleteHandler.complete(DoubleParameterAsSlider.this);
				}
				double value;
				double nValue = (double)slider.getValue() / 100.0;
				double rValue = nValue * (curMax - curMin) + curMin;
				if(aLog != null)
				{
					value = Math.pow(aLog.value(), rValue);
				}
				else
				{
					value = rValue;
				}
				
				try {
					field.setDouble(host, value);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				AbstractParameter.updateParameterValue(labelValue, value);
				
				MidiManager.send(midiListener, nValue);
			}
		});
		
		JButton learn = new JButton("X");
		learn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				MidiManager.learn(midiListener, (double)slider.getValue() / 100.0);			
			}
		});
		panel.add(learn);
	}
}
