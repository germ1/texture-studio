package net.mgsx.numart.editor.parameter;

public interface ParameterCompleteHandler 
{
	ParameterCompleteHandler noHandler = new ParameterCompleteHandler(){
		@Override
		public void complete(Parameter parameter) {
			// nothing
		}};
	
	public void complete(Parameter parameter);
}
