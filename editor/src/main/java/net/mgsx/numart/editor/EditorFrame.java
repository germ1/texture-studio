package net.mgsx.numart.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import net.mgsx.midi.MidiManager;
import net.mgsx.midi.MidiManager.MidiPort;
import net.mgsx.numart.editor.action.ClearAction;
import net.mgsx.numart.editor.action.FullscreenAction;
import net.mgsx.numart.editor.action.LoadAction;
import net.mgsx.numart.editor.action.LoadSampleAction;
import net.mgsx.numart.editor.action.SaveAsAction;
import net.mgsx.numart.editor.action.SavePreviewAction;
import net.mgsx.numart.editor.model.GraphUIContext;
import net.mgsx.numart.editor.model.NodeGraphUtil;
import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.Parameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.editor.parameter.mappers.BoolParameterAsCheckBox;
import net.mgsx.numart.editor.parameter.mappers.ColorParameterAsColorPicker;
import net.mgsx.numart.editor.parameter.mappers.DoubleParameterAsSlider;
import net.mgsx.numart.editor.parameter.mappers.FileParameterAsBrowser;
import net.mgsx.numart.editor.parameter.mappers.GroupParameterAsEditor;
import net.mgsx.numart.editor.parameter.mappers.IntParameterAsSlider;
import net.mgsx.numart.editor.parameter.mappers.ListParameterAsEditor;
import net.mgsx.numart.editor.parameter.mappers.LongParameterAsRandomButton;
import net.mgsx.numart.editor.parameter.mappers.PointParameterAsRelativeMouse;
import net.mgsx.numart.editor.parameter.mappers.TriggerAsButton;
import net.mgsx.numart.editor.parameter.mappers.VectorParameterAsRelativeMouse;
import net.mgsx.numart.editor.rendering.RenderManager;
import net.mgsx.numart.editor.rendering.RenderUtils;
import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamFile;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.annotations.ParamList;
import net.mgsx.numart.framework.annotations.ParamLong;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.framework.annotations.Trigger;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.procedural.renderer.TextureRenderer;
import net.mgsx.numart.raytracing.renderer.SceneRenderer;
import net.mgsx.numart.realtime.RTSceneRenderer;
import net.mgsx.numart.runtime.rendering.ScreenManager;
import net.mgsx.numart.runtime.rendering.ScreenManager.Screen;
import net.mgsx.tools.lang.LangTools;
import net.mgsx.tools.logging.LogEntry;
import net.mgsx.tools.logging.Logger;
import net.mgsx.tools.structure.graph.Graph;
import net.mgsx.tools.structure.graph.GraphAdapter;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jdesktop.swingx.VerticalLayout;

// TODO découper cette classe

// TODO faire un save preview automatique ... ou demander ... ou option

// TODO mettre en gestion de conf quand même !

// TODO génére les params aléatoirement pour toute la grappe !!!

/**
 * Technical Notes :
 * <p>{@link JXTaskPane} are created without animation during the UI
 * construction phase to prevent animated collapsing and the frame
 * to be redimmensionned. they are flagged as animated after frame pack to
 * have animated behavior during manual collapsing.</p>
 * 
 * 
 * 
 */
public class EditorFrame extends JFrame implements NodeNavigator
{
	private static final long serialVersionUID = 1L;

	@ParamGroup
	public Object renderer;
	
	JPanel previewPanel;
	
	private RenderManager renderManager;
	
	JProgressBar progressBar;
	
	public Map<Object, JPanel> mapNodePanel = new HashMap<Object, JPanel>();
	
	private Stack<Object> history = new Stack<Object>();
	
	private Graph<Object, String> graph = new Graph<Object, String>();
	
	public EditorFrame() 
	{
		graph.addGraphListener(new GraphAdapter<Object, String>(){
			@Override
			public void onNodeAdded(Object node) {
				
				// filtrage : on ne construit pas de panel pour les
				// node qui possède un Quick Parameter.
				if(ReflectUtils.getQuickParameterField(node.getClass()) == null)
				{
					buildDynamicParams(paramsPanel, node, -1, node.getClass().getSimpleName());
				}
				// XXX ((JXTaskPane)mapNodePanel.get(node)).setAnimated(true);
			}
			@Override
			public void onNodeRemoved(Object node) 
			{
				// TODO faire ça dans un gestionnaire ...
				if(node instanceof NamedElement)
				{
					((NamedElement)node).destroy();
				}
				
				JPanel panel = mapNodePanel.get(node);
				if(panel != null)
				{
					mapNodePanel.remove(node);
					paramsPanel.remove(panel);
					paramsPanel.revalidate();
				}
			}
		});
		
		
		build();
		
		Action altLeftAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(history.size() > 0)
				{
					navigateToNode(null, history.pop());
				}
			}
		};
		((JComponent)getContentPane()).setFocusable(true);
		((JComponent)getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), "myAltLeft");
		((JComponent)getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, KeyEvent.ALT_MASK), "myAltLeft");
		((JComponent)getContentPane()).getActionMap().put("myAltLeft", altLeftAction);
		
		// Position la fenetre en haut à gauche.
		// Important pour le mode double écran
		// en bureau étendu, la fenetre s'ouvre
		// sur l'écran de droite par défaut.
		setLocation(0, 0);
	}
	
	public Object getRenderer() {
		return renderer;
	}
	
	public final void setRenderer(Object renderer) 
	{
		// graph.clear();
		
		this.renderer = renderer;
		
		graph.addNode(renderer);
		
		renderManager.setRenderer(renderer);
		
		// paramsPanel.revalidate();
		// build();
		
		pack();
		
//		for(JPanel p : mapNodePanel.values())
//		{
//			((JXTaskPane)p).setAnimated(true);
//		}
	}
	
	
	private void buildMenu()
	{
		// TODO créer les menus dans une autre classe
		
		// build menu.
		JMenuBar menuBar = new JMenuBar();
		JMenu menu;
		JMenuItem menuItem;
		
		// File menu
		
		menu = new JMenu("Fichier");
		menuBar.add(menu);
		
		menuItem = new JMenuItem(new SaveAsAction(this));
		menu.add(menuItem);
		
		menuItem = new JMenuItem(new LoadAction(this, null, this));
		menu.add(menuItem);
		
		menuItem = new JMenuItem(new SavePreviewAction(renderManager));
		menu.add(menuItem);
		
		// Exemple menu
		
		menu = new JMenu("Edition");
		menuBar.add(menu);
		
		menuItem = new JMenuItem(new ClearAction("Clear", graph));
		menu.add(menuItem);
		
		menuItem = new JMenuItem(new LoadSampleAction(this, TextureRenderer.class, "Texture"));
		menu.add(menuItem);
		
		menuItem = new JMenuItem(new LoadSampleAction(this, SceneRenderer.class, "Raytracing"));
		menu.add(menuItem);
		
		menuItem = new JMenuItem(new LoadSampleAction(this, RTSceneRenderer.class, "Real-Time"));
		menu.add(menuItem);
		
		// Display menu
		
		menu = new JMenu("Display");
		menuBar.add(menu);
		
		
		for(Screen s : ScreenManager.getConfigurations())
		{
			String title = String.format(
					"%s [%s] - %s Bits - %dx%d - %s Hz", 
					LangTools.map(s.device.getType(), "???",
							GraphicsDevice.TYPE_RASTER_SCREEN, "Raster",
							GraphicsDevice.TYPE_PRINTER, "Printer",
							GraphicsDevice.TYPE_IMAGE_BUFFER, "Image Buffer"),
					s.device.getIDstring(),
					LangTools.map(s.displayMode.getBitDepth(), 
							String.valueOf(s.displayMode.getBitDepth()),
							DisplayMode.BIT_DEPTH_MULTI, "Multi"),
					s.displayMode.getWidth(),
					s.displayMode.getHeight(),
					LangTools.map(s.displayMode.getRefreshRate(),
							String.valueOf(s.displayMode.getRefreshRate()),
							DisplayMode.REFRESH_RATE_UNKNOWN, "N.C.")
							);

			menuItem = new JMenuItem(new FullscreenAction(title, renderManager, s));
			menu.add(menuItem);
		}
		
		// Midi menu
		
		menu = new JMenu("Midi in");
		menuBar.add(menu);
		
		for(MidiPort mp : MidiManager.getInputDevices())
		{
			final MidiPort fmp = mp;
			menuItem = new JCheckBoxMenuItem(new AbstractAction(mp.name){
				private static final long serialVersionUID = 1L;
				@Override
				public void actionPerformed(ActionEvent event) {
					MidiManager.enable(fmp, ((JCheckBoxMenuItem)event.getSource()).isSelected());
				}
			});
			menu.add(menuItem);
		}
		
		menu = new JMenu("Midi out");
		menuBar.add(menu);
		
		for(MidiPort mp : MidiManager.getOuputDevices())
		{
			final MidiPort fmp = mp;
			menuItem = new JCheckBoxMenuItem(new AbstractAction(mp.name){
				private static final long serialVersionUID = 1L;
				@Override
				public void actionPerformed(ActionEvent event) {
					MidiManager.enable(fmp, ((JCheckBoxMenuItem)event.getSource()).isSelected());
				}
			});
			menu.add(menuItem);
		}
		
		// Options menu
		
		menu = new JMenu("Options");
		menuBar.add(menu);
		
		menuItem = new JCheckBoxMenuItem(new AbstractAction("Auto-refresh") {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				RenderUtils.autoUpdate = ((JCheckBoxMenuItem)e.getSource()).isSelected();
			}
		});
		((JCheckBoxMenuItem)menuItem).setSelected(RenderUtils.autoUpdate);
		
		menu.add(menuItem);

		// Log menu
		
		menu = new JMenu("Logging");
		menuBar.add(menu);
		
		for(Field field : Logger.class.getFields())
		{
			if(field.getType() == LogEntry.class)
			{
				final LogEntry le = (LogEntry)ReflectUtils.get(Logger.class, field);
				JCheckBoxMenuItem cbmenuItem = new JCheckBoxMenuItem(new AbstractAction(le.title) {
					private static final long serialVersionUID = 1L;
					@Override
					public void actionPerformed(ActionEvent e) {
						// ((JCheckBoxMenuItem)e.getSource()).setSelected(!((JCheckBoxMenuItem)e.getSource()).isSelected());
						Logger.enable(le, ((JCheckBoxMenuItem)e.getSource()).isSelected());
					}
				});
				cbmenuItem.setSelected(le.enabled);
				menu.add(cbmenuItem);
			}
		}

		// end
		
		setJMenuBar(menuBar);
	}
	
	private void build()
	{
		previewPanel = new JPanel(){
			private static final long serialVersionUID = 1L;
			public void paint(java.awt.Graphics g) {
				super.paint(g);
				if(renderManager != null && renderManager.getRenderingImage() != null){
					g.drawImage(renderManager.getRenderingImage(), 0, 0, null);
				}
			};
		};
		
		previewPanel.setPreferredSize(new Dimension(256, 256));
		
		progressBar = new JProgressBar();
		
		renderManager = new RenderManager(previewPanel, progressBar);
		
		// menu needs to be build after renderManager creation.
		buildMenu();

		JButton renderButton = new JButton();
		renderButton.setText("Render");
		renderButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				renderManager.start();
			}
		});
		
		JButton cancelButton = new JButton();
		cancelButton.setText("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				renderManager.cancel();
			}
		});
		
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout());
		
		
		JPanel subPanel = new JPanel();
		subPanel.setLayout(new BorderLayout());
		subPanel.add(previewPanel, BorderLayout.CENTER);
		
		JPanel subSubPanel = new JPanel();
		subSubPanel.setLayout(new BoxLayout(subSubPanel, BoxLayout.X_AXIS));
		subSubPanel.add(renderButton);
		subSubPanel.add(cancelButton);
		subSubPanel.add(progressBar);
		
		subPanel.add(subSubPanel, BorderLayout.SOUTH);
		
		panel.add(taskPanel(subPanel, "Preview"), BorderLayout.CENTER);
		
		panel.add(buildParamsPanel(), BorderLayout.WEST);

		pack();
	}
	
	private static JPanel taskPanel(JPanel panel, String title)
	{
		JXTaskPane taskPane = new JXTaskPane();
		taskPane.setTitle(title);
		taskPane.setLayout(new BorderLayout());
		taskPane.add(panel, BorderLayout.CENTER);
		return taskPane;
	}
	
	private JXTaskPaneContainer paramsPanel;
	
	private JPanel buildParamsPanel()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		paramsPanel = new JXTaskPaneContainer();
		((VerticalLayout)paramsPanel.getLayout()).setGap(0);
		// paramsPanel.setLayout(new BoxLayout(paramsPanel, BoxLayout.Y_AXIS));
		
		JScrollPane sp = new JScrollPane(paramsPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		sp.setPreferredSize(new Dimension(450, 256)); // TODO const
		panel.add(sp);
		
		graph.addNode(renderer);
		// buildDynamicParams(paramsPanel, this, -1, "Editor");
		
		return panel;
	}
	
	private void buildDynamicParams(Container rootPanel, Object node, int index, String name)
	{
		// prevent recursion or multiple creations
		if(mapNodePanel.get(node) != null)
		{
			return;
		}
		
		
		final JXTaskPane panel = new NodePanel(graph, node);
		panel.setSpecial(false);
		panel.setAnimated(false);
		//Border border = panel.getBorder();
		// panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
		//((JComponent)panel.getContentPane()).setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		
		
		panel.setCollapsed(true);
		// panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		// store the object and its associated panel.
		mapNodePanel.put(node, panel);
		
		rootPanel.add(panel);	
		
		if(name != null && !name.isEmpty())
		{
			panel.setTitle(name);
			// panel.setBorder(BorderFactory.createTitledBorder(name));
		}
		panel.setMinimumSize(new Dimension(350, 30));
		
		// create common controls
		panel.add(new NodeControl(graph, node, this, renderManager));
		
		// scan object fields
		
		Field [] allFields = node.getClass().getFields();
		
		for(Field field : allFields)
		{
			// TODO faire une factory
			if(field.getAnnotation(ParamInt.class) != null)
			{
				panel.add(createParameter(node, field, IntParameterAsSlider.class));
			}
			if(field.getAnnotation(ParamDouble.class) != null)
			{
				panel.add(createParameter(node, field, DoubleParameterAsSlider.class));
			}
			if(field.getAnnotation(ParamColor.class) != null)
			{
				panel.add(createParameter(node, field, ColorParameterAsColorPicker.class));
			}
			if(field.getAnnotation(ParamLong.class) != null)
			{
				panel.add(createParameter(node, field, LongParameterAsRandomButton.class));
			}
			if(field.getAnnotation(ParamPoint.class) != null)
			{
				panel.add(createParameter(node, field, PointParameterAsRelativeMouse.class));
			}
			if(field.getAnnotation(ParamFile.class) != null)
			{
				panel.add(createParameter(node, field, FileParameterAsBrowser.class));
			}
			if(field.getAnnotation(ParamBoolean.class) != null)
			{
				panel.add(createParameter(node, field, BoolParameterAsCheckBox.class));
			}
			if(field.getAnnotation(ParamVector.class) != null)
			{
				panel.add(createParameter(node, field, VectorParameterAsRelativeMouse.class));
			}
			ParamList aParamList = field.getAnnotation(ParamList.class);
			if(aParamList != null)
			{
				@SuppressWarnings("unchecked")
				Iterable<Object> host = (Iterable<Object>)ReflectUtils.get(node, field);
				if(host != null)
				{
					for(Object o : host)
					{
						graph.addNode(o);
						graph.link(node, o, NodeGraphUtil.getListLinkData(field, o));
					}
				}
				panel.add(new ListParameterAsEditor(node, field, graph, this));
			}
			ParamGroup aParamGroup = field.getAnnotation(ParamGroup.class);
			if(aParamGroup != null)
			{
				if(aParamGroup.swappable() && !aParamGroup.merge())
				{
					GraphUIContext context = new GraphUIContext();
					context.graph = graph;
					context.nodeNavigator = this;
					context.host = node;
					context.panel = panel;
					panel.add(new GroupParameterAsEditor(context, field, mapNodePanel));
				}
				
				Object host = ReflectUtils.get(node, field);
				if(host != null)
				{
					graph.addNode(host);
					graph.link(node, host, field.getName());
				}
			}
		}
		
		// interpret methods
		for(Method m : node.getClass().getMethods())
		{
			Trigger aTrigger = m.getAnnotation(Trigger.class);
			if(aTrigger != null)
			{
				panel.add(new TriggerAsButton(node, m));
			}
		}
	}
	
	
	private JPanel createParameter(final Object host, final Field field, Class<? extends AbstractParameter> cls)
	{
		try {
			return cls.getConstructor(Object.class, Field.class, ParameterCompleteHandler.class)
					.newInstance(host, field, new ParameterCompleteHandler() {
						@Override
						public void complete(Parameter parameter) {
							graph.updateNode(host);
						}
					});
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	@Override
	public void navigateToNode(Object previousNode, Object node) {
		if(previousNode != null)
		{
			history.push(previousNode);
		}
		if(node != null)
		{
			for(int i=0 ; i<paramsPanel.getComponentCount() ; i++)
			{
				Component c = paramsPanel.getComponent(i);
				if(c instanceof JXTaskPane)
				{
					JXTaskPane tp = (JXTaskPane)c;
					tp.setCollapsed(true);
				}
			}
			JXTaskPane tp = (JXTaskPane)mapNodePanel.get(node);
			if(tp != null)
			{
				tp.setCollapsed(false);
				tp.scrollRectToVisible(tp.getBounds());
			}
		}
	}

	
}
