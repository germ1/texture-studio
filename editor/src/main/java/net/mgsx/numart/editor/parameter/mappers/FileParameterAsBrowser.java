package net.mgsx.numart.editor.parameter.mappers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.Field;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.persistence.PersistenceUtil;

public class FileParameterAsBrowser extends AbstractParameter
{
	public FileParameterAsBrowser(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();
		String value = (String)ReflectUtils.get(host, field);
		
		final JLabel filetext = new JLabel(value == null ? null : value.length() < 30 ? value : value.substring(value.length() - 30));
		filetext.setSize(50, 24); // TODO limiter la longueur ... proprement !
		
		final JButton btBrowse = new JButton("browse...");
		
		btBrowse.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				
				JFileChooser fc = new JFileChooser(PersistenceUtil.defaultFolder);
				if(JFileChooser.APPROVE_OPTION == fc.showOpenDialog(null))
				{
					PersistenceUtil.defaultFolder = fc.getCurrentDirectory();
					File file = fc.getSelectedFile();
					ReflectUtils.set(host, field, file.getAbsolutePath());
					filetext.setText(file.getAbsolutePath());
					parameterCompleteHandler.complete(FileParameterAsBrowser.this);
				}
			}
		});
		
		JPanel panel = this;
		panel.add(AbstractParameter.createParameterLabel(field));
		panel.add(filetext);
		panel.add(btBrowse);

	}
}
