package net.mgsx.numart.editor.util;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;

/**
 * Ce composant a pour rôle de capturer les actions de
 * drag'n'drop de souris sur un composant.<br>
 * Lors d'un click sur le composant, le curseur est masqué puis
 * est rétabli lorsque la souris est relachée.<br>
 * Les mouvements de la souris (XY) incrémentent deux valeurs.<br>
 * 
 * @author mgsx
 *
 */
public class RelativeMouseControl
{
	/** composant utilisé pour déplacer la souris par programmation. */
	private Robot robot;
	
	/** origine du curseur : position dans l'écran au moment du click. */
	private Point oMouse;
	
	/** position précédente du curseur */
	private Point pMouse;
	
	/** composant d'IHM qui reçoit les événements de souris */
	private Component hostComponent;

	/** curseur du composant */
	private Cursor prevCursor;
	
	/** indique si le curseur a déjà été mémorisé */
	private boolean prevCursorDefined = true;
	
	private RelativeMouseListener handler;
	
	private Cursor nullCursor;
	
	/**
	 * Constructeur.
	 * @param hostComponent composant d'IHM auquel est appliqué ce controlleur.
	 * @param pHandler handler des événements.
	 */
	public RelativeMouseControl(Component hostComponent, RelativeMouseListener pHandler) {
		super();
		this.hostComponent = hostComponent;
		this.handler = pHandler;
		
		// création du robot
		try {
			robot = new Robot();
		} catch (AWTException e) {
			throw new IllegalStateException(e);
		}
		
		// création d'un curseur transparent pour masquer le curseur.
		nullCursor = Toolkit.getDefaultToolkit().createCustomCursor(
				new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB), new Point(0, 0),
	            "null");
		
		prevCursorDefined = false;
		
		pMouse = new Point();
		
		// ajout des listeners
		
		hostComponent.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) 
			{
				// événement de début
				handler.onBegin(e);
				
				// mémorisation de la position du curseur
				oMouse = e.getLocationOnScreen();
				
				// masquage du curseur
				setCursorVisible(false);
				
				// centrage du curseur
				resetCursor();
			}
			@Override
			public void mouseReleased(MouseEvent event) 
			{
				// on rétabli la position du curseur
				robot.mouseMove((int)oMouse.getX(), (int)oMouse.getY());
				
				// réinitialisation de l'origine
				oMouse = null;
				
				// affichage du curseur.
				setCursorVisible(true);
				
				// événement de fin
				handler.onComplete(event);
			}
		});
		
		hostComponent.addMouseMotionListener(new MouseMotionAdapter() {
			
			@Override
			public void mouseDragged(MouseEvent event) 
			{
				Point screenMouse = event.getLocationOnScreen();
				
				if(pMouse.x == screenMouse.x && pMouse.y == screenMouse.y)
				{
					return;
				}
				
				// calcul de la différence
				int idx = screenMouse.x - pMouse.x;
				int idy = screenMouse.y - pMouse.y;
				
				// déclenchement de l'événement
				handler.onChange(event, idx, idy);
				
				// on replace le curseur au centre
				resetCursor();
			}
		});
	}
	private void setCursorVisible(boolean visible)
	{
		if(visible && prevCursorDefined)
		{
			hostComponent.setCursor(prevCursor);
			prevCursorDefined = false;
		}
		else if(!prevCursorDefined)
		{
			prevCursor = hostComponent.getCursor();
			hostComponent.setCursor(nullCursor);
			prevCursorDefined = true;
		}
	}
	
	private void resetCursor()
	{
		pMouse.x = (int)(hostComponent.getLocationOnScreen().getX() + hostComponent.getWidth()/2);
		pMouse.y = (int)(hostComponent.getLocationOnScreen().getY() + hostComponent.getHeight()/2);
		robot.mouseMove(pMouse.x, pMouse.y);
	}
	
}
