package net.mgsx.numart.editor.model;

import java.lang.reflect.Field;

public class NodeGraphUtil 
{
	public static String getSimpleLinkData(Field field)
	{
		return field.getName();
	}
	public static String getListLinkData(Field field, Object object)
	{
		return field.getName() + "-" + object.hashCode();
	}
	public static boolean isListLinkData(String linkData, Field field)
	{
		return linkData.startsWith(field.getName() + "-");
	}
}
