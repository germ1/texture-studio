package net.mgsx.numart.editor.parameter.mappers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.math.RandomUtils;

public class LongParameterAsRandomButton extends AbstractParameter
{
	public LongParameterAsRandomButton(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();
		long value = 0;
		try {
			value = field.getLong(host);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final JLabel labelValue = new JLabel();
		updateParameterValue(labelValue, value);
		
		JButton button = new JButton("Generate");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				long value = RandomUtils.randomLong();
				try {
					field.setLong(host, value);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				updateParameterValue(labelValue, value);
				parameterCompleteHandler.complete(LongParameterAsRandomButton.this);
			}
		});
		
		JPanel panel = this;
		panel.add(createParameterLabel(field));
		panel.add(labelValue);
		panel.add(button);

	}
}
