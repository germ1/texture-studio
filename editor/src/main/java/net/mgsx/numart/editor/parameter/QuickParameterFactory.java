package net.mgsx.numart.editor.parameter;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.lang.reflect.Field;

import javax.swing.AbstractAction;
import javax.swing.JButton;

import net.mgsx.numart.editor.model.GraphUIContext;
import net.mgsx.numart.editor.parameter.mappers.ColorValueAsColorPicker;
import net.mgsx.numart.editor.parameter.mappers.GroupParameterAsEditor;
import net.mgsx.numart.editor.rendering.RenderUtils;
import net.mgsx.numart.editor.rendering.RenderUtils.PostRenderActionButton;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.procedural.texture.impl.SolidColor;
import net.mgsx.tools.structure.graph.LinkAdapter;
import net.mgsx.tools.structure.graph.LinkListener;

public class QuickParameterFactory {

	public static Component getQuickEditComponent(GraphUIContext context, final Field field)
	{
		return getQuickEditComponent(context, ReflectUtils.get(context.host, field), field);
	}
	public static Component getQuickEditComponent(GraphUIContext context, Object value)
	{
		return getQuickEditComponent(context, value, null);
	}
	public static Component getQuickEditComponent(GraphUIContext context, Object value, Field field)
	{
		if(value != null)
		{
			Field quickParameterField = ReflectUtils.getQuickParameterField(value.getClass());
			if(quickParameterField != null)
			{
				if(quickParameterField.getType() == Color.class)
				{
					return new ColorValueAsColorPicker().create(context.panel, value, ReflectUtils.getQuickParameterField(SolidColor.class), ParameterCompleteHandler.noHandler);
				}
				// TODO suite du mapping
			}
		}
		return getDefaultQuickEdit(context, value, field);
	}

	public static Component getDefaultQuickEdit(final GraphUIContext context, final Object value, final Field field)
	{
		final JButton gotoButton = new JButton();
		
		gotoButton.setAction(new AbstractAction() {
			private static final long serialVersionUID = 1L;
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				context.nodeNavigator.navigateToNode(context.host, value);
			}
		});
		
		gotoButton.setText(GroupParameterAsEditor.getLibelle(value));
		
		gotoButton.setPreferredSize(new Dimension(150, 32));
		gotoButton.setMinimumSize(new Dimension(150, 32));
		gotoButton.setMaximumSize(new Dimension(150, 32));
		gotoButton.setHorizontalAlignment(JButton.LEADING);
		
		final LinkListener<Object, String> listener = new LinkAdapter<Object, String>(){
			@Override
			public void onChanged(Object oldTargetNode, Object newTargetNode) {
				gotoButton.setText(GroupParameterAsEditor.getLibelle(value));
				RenderUtils.dettachIcon(oldTargetNode, new PostRenderActionButton(gotoButton));
				RenderUtils.attachIcon(newTargetNode, new PostRenderActionButton(gotoButton));
			}
		};
		// listener registration
		context.panel.addContainerListener(new ContainerAdapter() {
			@Override
			public void componentRemoved(ContainerEvent e) {
				if(e.getChild() == gotoButton)
				{
					RenderUtils.dettachIcon(value, new PostRenderActionButton(gotoButton));
					if(field != null)
					{
						context.graph.removeLinkListener(listener, context.host, field.getName());
					}
				}
			}
			@Override
			public void componentAdded(ContainerEvent e) {
				if(e.getChild() == gotoButton)
				{
					RenderUtils.attachIcon(value, new PostRenderActionButton(gotoButton));
					if(field != null)
					{
						context.graph.addLinkListener(listener, context.host, field.getName());
					}
				}
			}
		});
		
		return gotoButton;
	}

}
