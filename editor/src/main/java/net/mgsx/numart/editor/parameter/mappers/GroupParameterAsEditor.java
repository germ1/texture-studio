package net.mgsx.numart.editor.parameter.mappers;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.lang.reflect.Field;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.mgsx.numart.editor.action.SwapAction;
import net.mgsx.numart.editor.model.GraphUIContext;
import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.QuickParameterFactory;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.tools.structure.graph.LinkAdapter;

import org.jdesktop.swingx.JXButton;

public class GroupParameterAsEditor extends AbstractParameter
{
	public static String getLibelle(Object host, Field field)
	{
		Object value = ReflectUtils.get(host, field);
		return getLibelle(value);
	}
	public static String getLibelle(Object value)
	{
		if(value == null)
		{
			return "undefined";
		}
		else
		{
			return value.getClass().getSimpleName();
		}
	}
	
	private Component quickEditComponent;
	
	public GroupParameterAsEditor(final GraphUIContext context, final Field field, final Map<Object, JPanel> mapNodePanel)
	{
		super();
		final JPanel panel = this;
		panel.setLayout(new FlowLayout());
		
		SwapAction action = new SwapAction(context.host, field, mapNodePanel, context.graph);
		
		JButton editButton = new JXButton();
		
		editButton.setAction(action);
		
		editButton.setText("Edit");
		editButton.setPreferredSize(new Dimension(60, 32));
		editButton.setMinimumSize(new Dimension(60, 32));
		editButton.setMaximumSize(new Dimension(60, 32));
		editButton.setHorizontalAlignment(JButton.CENTER);
		
		JLabel libelle = AbstractParameter.createParameterLabel(field); // new JLabel(field.getName());
//		libelle.setPreferredSize(new Dimension(100, 32));
//		libelle.setMinimumSize(new Dimension(100, 32));
//		libelle.setMaximumSize(new Dimension(100, 32));
//		libelle.setHorizontalAlignment(JLabel.RIGHT);
		
		panel.add(libelle);
		panel.add(editButton);
		
		context.graph.addLinkListener(new LinkAdapter<Object, String>(){
			@Override
			public void onChanged(Object oldTargetNode, Object newTargetNode) 
			{
				if(oldTargetNode != newTargetNode)
				{
					if(quickEditComponent != null)
					{
						panel.remove(quickEditComponent);
					}
					quickEditComponent = QuickParameterFactory.getQuickEditComponent(context, field);
					panel.add(quickEditComponent);
					
				}
			}
		}, context.host, field.getName());
		
		// update quick component
		context.panel = panel;
		quickEditComponent = QuickParameterFactory.getQuickEditComponent(context, field);
		panel.add(quickEditComponent);
	}
	
}
