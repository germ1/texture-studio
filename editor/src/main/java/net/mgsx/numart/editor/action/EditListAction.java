package net.mgsx.numart.editor.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import net.mgsx.numart.editor.model.NodeGraphUtil;
import net.mgsx.numart.editor.rendering.RenderUtils;
import net.mgsx.numart.editor.rendering.RenderUtils.PostRenderActionButton;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.tools.structure.graph.Graph;

public class EditListAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private Object host;
	
	private Field field;
	
	private Graph<Object, String> graph;

	public EditListAction(Object host, Field field, Graph<Object, String> graph) {
		super();
		this.host = host;
		this.field = field;
		this.graph = graph;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		
		Iterable<?> list = (Iterable<?>)ReflectUtils.get(host, field);
		
		List<Class<?>> suitableClasses = ReflectUtils.getInstanciableSubClasses((Class<?>)((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0]);
		
		Collections.sort(suitableClasses, new Comparator<Class<?>>() {
			@Override
			public int compare(Class<?> o1, Class<?> o2) {
				return o1.getSimpleName().compareTo(o2.getSimpleName());
			}
		});

		// build the popup menu
		Component invoker = (Component)e.getSource();
		
		JPopupMenu popup = new JPopupMenu();
		
		JMenu itemAdd = new JMenu("Add New ...");
		popup.add(itemAdd);
		
		JMenu itemRem = new JMenu("Remove Existing ...");
		popup.add(itemRem);
		
		for(Class<?> suitableClass : suitableClasses)
		{
			JMenuItem item = new JMenuItem(new AddToListAction(suitableClass, suitableClass.getSimpleName()));
			RenderUtils.attachIcon(suitableClass, new PostRenderActionButton(item));
			itemAdd.add(item);
		}
		
		for(Object existing : list)
		{
			JMenuItem item = new JMenuItem(new RemoveFromListAction(existing, existing.getClass().getSimpleName()));
			RenderUtils.attachIcon(existing, new PostRenderActionButton(item));
			itemRem.add(item);
		}
		
		popup.show(invoker, 0, 0);
	}
	
	private class AddToListAction extends AbstractAction
	{

		private static final long serialVersionUID = 1L;

		private Class<?> cls;
		
		public AddToListAction(Class<?> cls, String name) {
			super(name);
			this.cls = cls;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			// create a new object
			Object newObject = ReflectUtils.createDefault(cls);
			// add it to the list property
			Collection<Object> list = (Collection<Object>)ReflectUtils.get(host, field);
			list.add(newObject);
			// add the new object to the graph
			graph.addNode(newObject);
			graph.link(host, newObject, NodeGraphUtil.getListLinkData(field, newObject));
		}
		
	}
	private class RemoveFromListAction extends AbstractAction
	{

		private static final long serialVersionUID = 1L;

		private Object object;
		
		public RemoveFromListAction(Object object, String name) {
			super(name);
			this.object = object;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// remove object from the list property
			Collection<?> list = (Collection<?>)ReflectUtils.get(host, field);
			list.remove(object);
			// remove object from graph
			graph.removeNode(object);
			graph.unlink(host, NodeGraphUtil.getListLinkData(field, object));
		}
		
	}

}
