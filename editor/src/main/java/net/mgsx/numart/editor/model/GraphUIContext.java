package net.mgsx.numart.editor.model;

import javax.swing.JPanel;

import net.mgsx.numart.editor.NodeNavigator;
import net.mgsx.tools.structure.graph.Graph;

public class GraphUIContext 
{
	/** le graph du context */
	public Graph<Object, String> graph;
	
	/** le noeud dans le graph */
	public Object host;
	
	/** un panel de l'IHM (pour les boites de dialogue) */
	public JPanel panel;
	
	/** un controlleur de navigation dans le graph */
	public NodeNavigator nodeNavigator;
	
}
