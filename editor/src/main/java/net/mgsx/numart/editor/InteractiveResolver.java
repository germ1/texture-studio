package net.mgsx.numart.editor;

import java.awt.Component;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.persistence.ModelResolver;

public class InteractiveResolver implements ModelResolver {

	private Component parent;
	
	private static final String skip = "[Ne pas résoudre]";
	
	private Map<String, String> resolution = new HashMap<String, String>();
	
	public InteractiveResolver(Component parent) 
	{
		this.parent = parent;
	}
	
	private int weight(String name, String basename)
	{
		int match = 0;
		for(int i=0 ; i<basename.length() ; i++)
		{
			if(i < name.length() && basename.charAt(basename.length() - i - 1) ==
					name.charAt(name.length() - i - 1)){
				match++;
			}
		}
		return match;
	}
	
	private Object[] getAllTypes(final String name)
	{
		Set<Class<?>> types = ReflectUtils.getTypes();
		List<Class<?>> lstTypes = new ArrayList<Class<?>>(types);
		Collections.sort(lstTypes, new Comparator<Class<?>>() {
			@Override
			public int compare(Class<?> o1, Class<?> o2) 
			{
				return Double.compare(weight(o2.getName(), name), weight(o1.getName(), name));
			}
		});
		
		Object[] lst = new Object[lstTypes.size() + 1];
		int i=0;
		lst[i++] = skip;
		for(Class<?> type : lstTypes)
		{
			lst[i++] = type.getName();
		}
		return lst;
	}
	private Object[] getProperties(Object object)
	{
		List<Field> fields = ReflectUtils.getFields(object.getClass());
		Object[] lst = new Object[fields.size() + 1];
		int i=0;
		lst[i++] = skip;
		for(Field field : fields)
		{
			lst[i++] = field.getName();
		}
		return lst;
	}
	
	@Override
	public String resolveTargetType(String sourceType) 
	{
		if(resolution.containsKey(sourceType))
		{
			return resolution.get(sourceType);
		}
		Object [] alltypes = getAllTypes(sourceType);
		Object response = askUser("résolution du type " + sourceType, alltypes, alltypes.length > 1 ? alltypes[1] : skip);
		if(response != skip)
			resolution.put(sourceType, response.toString());
		return response == skip ? null : response.toString();
	}

	@Override
	public String resolveTargetProperty(Object object, String sourceProperty) {
		if(object == null) return null;
		if(resolution.containsKey(object.getClass().getName() + ":" + sourceProperty))
		{
			return resolution.get(object.getClass().getName() + ":" + sourceProperty);
		}
		Object response = askUser("résolution de la propriété " + sourceProperty, getProperties(object), skip);
		if(response != skip)
			resolution.put(object.getClass().getName() + ":" + sourceProperty, response.toString());
		return response == skip ? null : response.toString();
	}

	@Override
	public String resolveSourceProperty(Object object, String targetProperty) {
		// TODO Auto-generated method stub
		return null;
	}

	private Object askUser(String message, Object[] possibilities, Object defaultPossibility)
	{
		return JOptionPane.showInputDialog(
		                    parent,
		                    message,
		                    "Résolution",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    possibilities,
		                    defaultPossibility);
	}
	
}
