package net.mgsx.numart.editor.rendering;

import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.procedural.renderer.Rasterizer;
import net.mgsx.numart.procedural.renderer.SoftwareRenderer;
import net.mgsx.tools.logging.Logger;

public class RenderingWorker extends SwingWorker<Integer, Integer>
{
	private JProgressBar progressBar;
	
	private JPanel renderingPanel;
	
	private SoftwareRenderer renderer;
	
	private BufferedImage renderingImage;
	
	public RenderingWorker(JPanel renderingPanel, JProgressBar progressBar) {
		this.renderingPanel = renderingPanel;
		this.progressBar = progressBar;
		addListeners();
	}
	
	public void execute(SoftwareRenderer renderer)
	{
		this.renderer = renderer;
		execute();
	}
	
	public BufferedImage getRenderingImage() {
		return renderingImage;
	}
	
	private void addListeners()
	{
		addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                if("progress".equals(evt.getPropertyName())) {
                   progressBar.setValue((Integer) evt.getNewValue());
                }
            }
        });
	}
	
	@Override
	protected Integer doInBackground() throws Exception 
	{
		long timeMs = System.currentTimeMillis();
		Logger.log(Logger.RENDER, "start ...");
		
		setProgress(0);
		renderingImage = new BufferedImage(renderingPanel.getWidth(), renderingPanel.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		
		Rasterizer rasterizer = new Rasterizer() {
			@Override
			public void setPixel(int x, int y, Color color) {
				int rgb =
				(int)(color.r * 255) << 16 |
				(int)(color.g * 255) << 8 |
				(int)(color.b * 255);
				renderingImage.setRGB(x, y, rgb);
			}
		};
		
		// precompute tree
		ReflectUtils.checkForPrecomputation(renderer);
		
		// perform scanline
		for(int y=0 ; y<renderingImage.getHeight() && !isCancelled() ; y++)
		{
			renderer.render(rasterizer, 
					renderingImage.getWidth(), renderingImage.getHeight(), 
					0, y,
					renderingImage.getWidth(), y+1);
			setProgress((int)(100 * ((double)y / (double)renderingImage.getHeight())));
			publish(y);
		}
		
		Logger.log(Logger.RENDER, "complete : " + (System.currentTimeMillis() - timeMs) / 1000.0 + " s");
		
		return null;
	}
	
	@Override
	protected void process(List<Integer> chunks) {
		renderingPanel.repaint();
	}
	
	@Override
	protected void done() {
		setProgress(100);
		renderingPanel.repaint();
	}
	
}