package net.mgsx.numart.editor.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import net.mgsx.numart.framework.utils.DocumentationUtils;

public class HelpAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private Object host;
	private JComponent component;
	
	
	
	public HelpAction(Object host, JComponent component) {
		super();
		this.host = host;
		this.component = component;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JOptionPane.showMessageDialog(component, DocumentationUtils.getPlainTextDoc(host), host.getClass().getSimpleName(), JOptionPane.INFORMATION_MESSAGE);
	}

}
