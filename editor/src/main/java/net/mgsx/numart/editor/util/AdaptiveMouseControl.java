package net.mgsx.numart.editor.util;

import java.awt.Component;
import java.awt.event.MouseEvent;

public class AdaptiveMouseControl implements RelativeMouseListener 
{
	@SuppressWarnings("unused")
	private RelativeMouseControl baseControl;
	
	private AdaptiveMouseListener handler;

	public AdaptiveMouseControl(Component hostComponent,
			AdaptiveMouseListener handler) {
		super();
		this.baseControl = new RelativeMouseControl(hostComponent, this);
		this.handler = handler;
	}

	@Override
	public void onBegin(MouseEvent event) {
	}

	@Override
	public void onChange(MouseEvent event, int dx, int dy) {
		handler.onChange(event, dx, dy);
	}

	@Override
	public void onComplete(MouseEvent event) {
		handler.onComplete(event, 0, 0);
	}

	
	
}
