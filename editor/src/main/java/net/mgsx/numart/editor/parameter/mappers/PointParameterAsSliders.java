package net.mgsx.numart.editor.parameter.mappers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.mgsx.numart.editor.parameter.AbstractParameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;

public class PointParameterAsSliders extends AbstractParameter
{
	public PointParameterAsSliders(final Object host, final Field field, final ParameterCompleteHandler parameterCompleteHandler)
	{
		super();
		JPanel panel = this;
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JPanel inPanel = new JPanel();
		inPanel.setLayout(new BoxLayout(inPanel, BoxLayout.Y_AXIS));
		
		panel.add(new JLabel(field.getName()));
		panel.add(inPanel);
		
		Point3D value = (Point3D)ReflectUtils.get(host, field);
		
		final ParamDouble annotation = field.getAnnotation(ParamDouble.class);
		final Logarithmic aLog = field.getAnnotation(Logarithmic.class);
		
		final double min = annotation.min();
		final double max = annotation.max();
		
		ParamDouble aParamDouble = new ParamDouble() {
			
			@Override
			public Class<? extends Annotation> annotationType() {
				return ParamDouble.class;
			}
			
			@Override
			public double min() {
				return min;
			}
			
			@Override
			public double max() {
				return max;
			}
		};
		
		try {
			inPanel.add(new DoubleParameterAsSlider(value, Point3D.class.getDeclaredField("x"), parameterCompleteHandler, aParamDouble, aLog));
			inPanel.add(new DoubleParameterAsSlider(value, Point3D.class.getDeclaredField("y"), parameterCompleteHandler, aParamDouble, aLog));
			inPanel.add(new DoubleParameterAsSlider(value, Point3D.class.getDeclaredField("z"), parameterCompleteHandler, aParamDouble, aLog));
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
