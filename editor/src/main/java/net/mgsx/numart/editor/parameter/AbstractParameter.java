package net.mgsx.numart.editor.parameter;

import java.awt.Dimension;
import java.lang.reflect.Field;

import javax.swing.JLabel;
import javax.swing.JPanel;

import net.mgsx.numart.framework.annotations.Caution;

public abstract class AbstractParameter extends JPanel implements Parameter
{
	private static final long serialVersionUID = 1L;

	public static void updateParameterValue(JLabel label, double value)
	{
		updateParameterValue(label, String.format("%.2f", value));
	}
	public static void updateParameterValue(JLabel label, int value)
	{
		updateParameterValue(label, String.valueOf(value));
	}
	public static void updateParameterValue(JLabel label, long value)
	{
		updateParameterValue(label, "0x" + Long.toHexString(value));
	}
	public static void updateParameterValue(JLabel label, String value)
	{
		label.setText("(" + value + ")");
	}
	
	public static JLabel createParameterLabel(final Field field)
	{
		Caution aCaution = field.getAnnotation(Caution.class);
		
		String parameterName = null;
		String parameterDescription = null;
		
		if(aCaution != null)
		{
			parameterDescription = aCaution.value();
		}
		
		if(parameterName == null || parameterName.isEmpty())
		{
			parameterName = field.getName();
		}
		
		JLabel label = new JLabel(parameterName);
		label.setPreferredSize(new Dimension(100, 20));
		label.setMinimumSize(new Dimension(100, 20));
		label.setMaximumSize(new Dimension(100, 20));
		label.setHorizontalAlignment(JLabel.RIGHT);
		
		if(parameterDescription != null && !parameterDescription.isEmpty())
		{
			label.setToolTipText(parameterDescription);
		}
		if(aCaution != null)
		{
			label.setForeground(java.awt.Color.RED);
		}
		
		return label;
	}
}
