package net.mgsx.numart.editor.converter;

import net.mgsx.numart.editor.NodeConverter;
import net.mgsx.numart.framework.Renderer;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Polar;
import net.mgsx.numart.procedural.renderer.TextureRenderer;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.Damier;
import net.mgsx.numart.procedural.texture.impl.SolidColor;
import net.mgsx.numart.procedural.texture.impl.TextureMixer;
import net.mgsx.numart.raytracing.core.Material;
import net.mgsx.numart.raytracing.core.Observer;
import net.mgsx.numart.raytracing.core.Scene;
import net.mgsx.numart.raytracing.geometry.Plan;
import net.mgsx.numart.raytracing.geometry.Sphere;
import net.mgsx.numart.raytracing.light.DirectionalLight;
import net.mgsx.numart.raytracing.material.BasicMaterial;
import net.mgsx.numart.raytracing.renderer.SceneRenderer;

public class PreviewNodeConverter implements NodeConverter 
{
	@Override
	public Renderer convertToRenderer(Object objectToRender) {
		if(objectToRender instanceof Renderer)
		{
			return (Renderer)objectToRender;
		}
		if(objectToRender instanceof Scene)
		{
			SceneRenderer renderer = new SceneRenderer((Scene)objectToRender);
			renderer.antialias = 1;
			renderer.maxBounce = 1;
			return convertToRenderer(renderer);
		}
		if(objectToRender instanceof ColorFromSpace)
		{
			TextureRenderer renderer = new TextureRenderer((ColorFromSpace)objectToRender);
			renderer.antialias = 1;
			renderer.offset = new Point3D(-2,-2,0.5);
			renderer.size = new Point3D(4,4,4);
			return convertToRenderer(renderer);
		}
		if(objectToRender instanceof ScalarFromSpace)
		{
			TextureMixer mixer = new TextureMixer();
			mixer.mix = (ScalarFromSpace)objectToRender;
			mixer.a = new SolidColor(new Color(0, 0, 0));
			mixer.b = new SolidColor(new Color(1, 1, 1));
			return convertToRenderer(mixer);
		}
		if(objectToRender instanceof Material)
		{
			Scene scene = new Scene();
			
			scene.observer = new Observer();
			scene.observer.direction = new Point3D(0, 0, 1);
			scene.observer.up = new Point3D(0, 1, 0);
			scene.observer.position = new Point3D(0, 0, -6);
			scene.observer.transformer = null;
			
			Sphere sphere = new Sphere(new Point3D(0,0,0), 2);
			sphere.material = (Material)objectToRender;
			scene.addGeometry(sphere);
			
			DirectionalLight light = new DirectionalLight();
			light.ambientColor = new Color(0.2);
			light.diffuseColor = new Color(0.6);
			light.specularColor = new Color(1.0);
			light.direction = new Polar(Math.PI / 4, -Math.PI / 2.5).toUnitVector();
			scene.addLight(light);
			
			// add plan for reflections
			TextureMixer planTex = new TextureMixer();
			planTex.a = new SolidColor(new Color(0,0,0));
			planTex.b = new SolidColor(new Color(1,1,1));
			planTex.mix = new Damier();

			BasicMaterial planMat = new BasicMaterial();
			planMat.reflexion = 0;
			planMat.texture = planTex;
			planMat.emmission = 1;
			
			Plan plan = new Plan();
			plan.position = new Point3D(0, -2, 0);
			plan.normal = new Point3D(0, 1, 0);
			plan.material = planMat;
			
			scene.addGeometry(plan);
			
			return convertToRenderer(scene);
		}
		return null;
	}
	


}
