package net.mgsx.numart.editor.util;

import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class RelativeMouseControlTestUI extends JFrame implements AbsoluteMouseListener
{
	public static void main(String[] args) 
	{
		RelativeMouseControlTestUI frame = new RelativeMouseControlTestUI();
		frame.setSize(640, 480);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private JButton button;
	
	private JLabel labelInfo;
	
	private int numChangeEvents;
	
	@SuppressWarnings("unused")
	private AbsoluteMouseControl control;
	
	public RelativeMouseControlTestUI() 
	{
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		button = new JButton("Action");
		getContentPane().add(button);
		
		labelInfo = new JLabel();
		getContentPane().add(labelInfo);
		
		control = new AbsoluteMouseControl(button, this);
	}

	@Override
	public void onChange(MouseEvent event, int dx, int dy) 
	{
		numChangeEvents++;
		labelInfo.setText("change : e=" + numChangeEvents + ", x=" + dx + ", y=" + dy);
	}

	@Override
	public void onComplete(MouseEvent event, int dx, int dy) 
	{
		labelInfo.setText("complete : e=" + numChangeEvents + ", x=" + dx + ", y=" + dy);
	}

	@Override
	public void onBegin(MouseEvent event) 
	{
		numChangeEvents = 0;
		labelInfo.setText("begin");
	}
}
