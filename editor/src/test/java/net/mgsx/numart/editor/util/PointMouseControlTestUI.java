package net.mgsx.numart.editor.util;

import java.lang.reflect.Field;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import net.mgsx.numart.editor.parameter.Parameter;
import net.mgsx.numart.editor.parameter.ParameterCompleteHandler;
import net.mgsx.numart.editor.parameter.mappers.PointParameterAsRelativeMouse;
import net.mgsx.numart.math.Point3D;

public class PointMouseControlTestUI extends JFrame implements ParameterCompleteHandler
{
	public static void main(String[] args) throws Exception 
	{
		PointMouseControlTestUI frame = new PointMouseControlTestUI();
		frame.setSize(640, 480);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static class MyHost
	{
		public Point3D point = new Point3D();
	}
	
	private MyHost host = new MyHost();
	
	
	@SuppressWarnings("unused")
	private AbsoluteMouseControl control;
	
	public PointMouseControlTestUI() throws Exception 
	{
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		Field field = MyHost.class.getField("point");
		PointParameterAsRelativeMouse control = new PointParameterAsRelativeMouse(host, field, this);
		getContentPane().add(control);
	}

//	@Override
//	public void onChange(MouseEvent event, int dx, int dy) 
//	{
//		numChangeEvents++;
//		labelInfo.setText("change : e=" + numChangeEvents + ", x=" + dx + ", y=" + dy);
//	}
//
//	@Override
//	public void onComplete(MouseEvent event, int dx, int dy) 
//	{
//		labelInfo.setText("complete : e=" + numChangeEvents + ", x=" + dx + ", y=" + dy);
//	}
//
//	@Override
//	public void onBegin(MouseEvent event) 
//	{
//		numChangeEvents = 0;
//		labelInfo.setText("begin");
//	}

	@Override
	public void complete(Parameter parameter) {
		
	}
}
