package net.mgsx.numart.sound;

import net.mgsx.numart.sound.openal.OALClip;

public class JOALTest {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException 
	{
		OALClip clip = new OALClip();
		clip.loadWave("src/test/resources/sample.wav", 2, 1f);
		
		double duration = 2;
		double freq = 10;
		
		for(int i=0 ; i<(int)(duration * freq) ; i++)
		{
		
			clip.play();
			
			Thread.currentThread().sleep((int)(1000.0 / freq));
			
		}
		
		Thread.currentThread().sleep(2000);
	}
	
}
