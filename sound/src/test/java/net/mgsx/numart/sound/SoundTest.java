package net.mgsx.numart.sound;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer.Info;

public class SoundTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		for(Info i : AudioSystem.getMixerInfo())
		{
			String s = String.format("[%s] [%s] [%s] (%s)\n%s", i.getName(), i.getVendor(), i.getVersion(), i.getDescription(), i.toString());
			System.out.println(s);
		}
	}

}
