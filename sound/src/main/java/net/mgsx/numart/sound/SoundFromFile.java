package net.mgsx.numart.sound;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamFile;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.audio.Sound;
import net.mgsx.numart.sound.openal.OALClip;

public class SoundFromFile extends NamedElement implements Sound 
{
	@ParamFile
	public String file;
	
	@ParamInt(min=1, max=5)
	public int polyphony = 1;
	
	@ParamDouble
	public double volume = 1;

	
	private OALClip clip;
	
	@Override
	public void prepare()
	{
		clip = new OALClip();
		clip.loadWave(file, polyphony, (float)volume);
	}
	
	@Override
	public void play()
	{
		if(clip != null)
		{
			clip.play();
		}
	}

	@Override
	public void setVolume(double volume) {
		clip.setVolume(this.volume * volume);
	}
}
