package net.mgsx.numart.sound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.Mixer.Info;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamFile;
import net.mgsx.numart.framework.audio.Sound;

/**
 * @deprecated ne fonctionne pas sous linux/ALSA
 * 
 * @author mgsx
 *
 */
@Deprecated()
public class SoundFromFileJava extends NamedElement implements Sound 
{
	@ParamFile
	public String file;
	
	private Clip clip = null;
	
	@Override
	public void prepare()
	{
		if(file != null /*&& clip == null*/)
		{
			try {
				File f = new File(file);
				
				Info mixer = null;
				for(Info i : AudioSystem.getMixerInfo())
				{
					if(i.getName().startsWith("default"))
					{
						mixer = i;
					}
				}
				
				
		        // Get a sound clip resource.
		        clip = AudioSystem.getClip();
		        
		        
		        
				//AudioFileFormat.Type.WAVE
				AudioInputStream audioIn = AudioSystem.getAudioInputStream(f);
				
				AudioFormat fFile = audioIn.getFormat();
				AudioFormat fClip = new AudioFormat(44100, 16, 2, true, true);
				
				if(AudioSystem.isConversionSupported(fClip, fFile))
				{
				
					AudioInputStream convAudioIn = AudioSystem.getAudioInputStream(fClip, audioIn);
					 
			        // Open audio clip and load samples from the audio input stream.
			        clip.open(convAudioIn);
			        
			        FloatControl vol = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
			        vol.setValue(vol.getMinimum() + 0.1f * (vol.getMaximum() - vol.getMinimum()));
				}
		        
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void play()
	{
		if (clip != null) {
	         if (clip.isRunning())
	            clip.stop();   // Stop the player if it is still running
	         clip.setFramePosition(0); // rewind to the beginning
	         clip.start();     // Start playing
	      }	
		}

	@Override
	public void setVolume(double volume) {
		// TODO Auto-generated method stub
		
	}
}
