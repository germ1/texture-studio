package net.mgsx.numart.sound.openal;

import com.jogamp.openal.AL;
import com.jogamp.openal.ALFactory;
import com.jogamp.openal.util.ALut;

public class OALSystem 
{
	protected static AL al;
	
	private static boolean inited = false;
	
	protected static void init()
	{
		if(!inited)
		{
			synchronized (OALSystem.class) 
			{
				if(!inited)
				{
					al = ALFactory.getAL();
			        ALut.alutInit();
			        if(al.alGetError() != AL.AL_NO_ERROR)
			            return ; // TODO
			        
					Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				        public void run() {
				        	ALut.alutExit();
				        }
				    }));
					
					inited = true;
				}
			}
		}
	}
	

}
