package net.mgsx.numart.sound.openal;

import java.nio.ByteBuffer;

import com.jogamp.openal.AL;
import com.jogamp.openal.util.ALut;

public class OALClip extends OALSystem {

    private int polyphony = 1;
    
    private int voice = 0;
    
    // Buffers hold sound data.
    private int[] buffer = new int[1];
    // Buffers hold sound data.
    private int[] source;
    // Position of the source sound.
    private float[] sourcePos = { 0.0f, 0.0f, 0.0f };

    // Velocity of the source sound.
    private float[] sourceVel = { 0.0f, 0.0f, 0.0f };
    
    private double volume;
    
	private void destroy()
    {
    	if(source != null)
    	{
	    	al.alDeleteSources(polyphony, source, 0);
	        al.alDeleteBuffers(1, buffer, 0);
	        source = null;
    	}
    }
    public void play()
    {
    	if(source != null)
    	{
    		al.alSourcePlay(source[voice % polyphony]);
    		voice+=1;
    	}
    }
    public void setVolume(double gain)
    {
        for(int i=0 ; i<polyphony ; i++)
        {
	        al.alSourcef (source[i], AL.AL_GAIN,     (float)(gain * volume)     );
        }
    	
    }
    public void loadWave(String file) 
    {
    	loadWave(file, 1, 1);
    }
    public void loadWave(String file, int polyphony, float gain) 
    {
    	init();
    	destroy();
    	
    	this.polyphony = polyphony;
    	this.volume = gain;
    	
        // variables to load into
   
        int[] format = new int[1];
        int[] size = new int[1];
        ByteBuffer[] data = new ByteBuffer[1];
        int[] freq = new int[1];
        int[] loop = new int[1];

        // Load wav data into a buffer.
        al.alGenBuffers(1, buffer, 0);
        if (al.alGetError() != AL.AL_NO_ERROR)
            return;

        ALut.alutLoadWAVFile(file, format, data, size, freq, loop);
        al.alBufferData(buffer[0], format[0], data[0], size[0], freq[0]);
        
        source = new int[polyphony];
        // Bind buffer with a source.
        al.alGenSources(polyphony, source, 0);

        if (al.alGetError() != AL.AL_NO_ERROR)
        	return;

        for(int i=0 ; i<polyphony ; i++)
        {
	        al.alSourcei (source[i], AL.AL_BUFFER,   buffer[0]   );
	        al.alSourcef (source[i], AL.AL_PITCH,    1.0f     );
	        al.alSourcef (source[i], AL.AL_GAIN,    gain     );
	        al.alSourcefv(source[i], AL.AL_POSITION, sourcePos, 0);
	        al.alSourcefv(source[i], AL.AL_VELOCITY, sourceVel, 0);
	        al.alSourcei (source[i], AL.AL_LOOPING,  loop[0]     );
        }
        // Do another error check and return.
        if(al.alGetError() == AL.AL_NO_ERROR)
        	return;

    }

}
