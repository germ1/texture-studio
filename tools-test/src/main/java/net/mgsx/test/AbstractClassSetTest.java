package net.mgsx.test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypesScanner;

public class AbstractClassSetTest {
protected String name;
	public AbstractClassSetTest(String name) {
		this.name = name;
	}
	protected static Collection<Object[]> generateTests(String pkg, Class<?> cls)
	{
		List<Object[]> suites = new ArrayList<Object[]>();
		for(Class<?> clazz : getAllClassesToTest(pkg, cls))
		{
			if(!clazz.isInterface() && !Modifier.isAbstract(clazz.getModifiers()))
			{
				Object[] parameters = {clazz.getSimpleName(), clazz};
				suites.add(parameters);
			}
		}
		return suites;
	}
	protected static <T> Set<Class<? extends T>> getAllClassesToTest(String packageToScan, Class<T> subTypeOf)
	{
		TypesScanner scanner = new TypesScanner();
		
		SubTypesScanner stScanner = new SubTypesScanner(false);
		
		Reflections ref = new Reflections(packageToScan, scanner, stScanner);
		
		return ref.getSubTypesOf(subTypeOf);
	}
	
}
