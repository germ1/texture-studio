package net.mgsx.test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

import junit.framework.AssertionFailedError;



public class SynchronizedMock <T>
{
	public SynchronizedMock(Class<T> cls) 
	{
		this.cls = cls;
	}
	private Class<T> cls;
	
	private static class STATUS{
		boolean locked = false;
		boolean waiting = false;
		int calls = 0;
		Class<? extends Throwable> exception;
		int allowedCalls = 0;
		Semaphore joinPoint = new Semaphore(0);
		Method m;
		
		public STATUS(Method m) {
			super();
			this.m = m;
		}

		@Override
		public String toString() {
			return m.getName() + " (" + calls + "/" + allowedCalls + ")" ;
		}
	}
	
	Map<Method, STATUS> map = new HashMap<Method, STATUS>();
	
	private synchronized STATUS get(Method m)
	{
		STATUS s = map.get(m);
		if(s == null)
		{
			s = new STATUS(m);
			map.put(m, s);
		}
		return s;
	}
	
	private void lock(Method m)
	{
		
	}
	
	public T lock()
	{
		return aop(new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method m, Object[] args)
							throws Throwable {
						STATUS s = get(m);
						synchronized (s) {
							s.locked = true;
						}
						return null;
					}
				});
	}
	@Deprecated
	public boolean waiting(String m) throws SecurityException, NoSuchMethodException
	{
		return get(cls.getMethod(m)).waiting;
	}
	
	public T unlock()
	{
		return aop(new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method m, Object[] args)
							throws Throwable {
						STATUS s = get(m);
						synchronized (s) {
							if(s.locked)
							{
								s.notifyAll();
								s.locked = false;
							}
						}
						return null;
					}
				});
	}
	
	public boolean validate()
	{
		for(STATUS s : map.values())
		{
			if(s.calls != s.allowedCalls)
			{
				throw new AssertionFailedError(getMessage());
			}
		}
		return true;
	}
	
	String getMessage()
	{
		String str = "error :\n";
		for(STATUS s : map.values())
		{
			str += s.toString() + "\n";
		}
		return str;
	}
	
	public T waitForCall()
	{
		return aop(new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method m, Object[] args)
							throws Throwable {
						STATUS s = get(m);
						s.allowedCalls++;
						
						System.out.println("wait for " + m.getName() + " (" + s.calls + "/" + s.allowedCalls + ")");
						s.joinPoint.acquire();
						return null;
					}
				});
	}
	public T exception(final Class<? extends Throwable> ex)
	{
		return aop(new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method m, Object[] args)
							throws Throwable {
						get(m).exception = ex;
						return null;
					}
				});
	}
	public T allow(final int n)
	{
		return aop(new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method m, Object[] args)
							throws Throwable {
						STATUS s = get(m);
						s.allowedCalls += n;
						s.exception = null;
						System.out.println("allow : " + m.getName() + " (" + s.calls + "/" + s.allowedCalls + ")");
						return null;
					}
				});
	}
	public T mock()
	{
		return mock(0);
	}
	public T mock(final int waitTime)
	{
		return aop(new InvocationHandler() {
					
				@Override
				public Object invoke(Object proxy, Method m, Object[] args)
					throws Throwable {
					STATUS s = get(m);
					s.calls++;
					System.out.println("new call : " + m.getName() + " (" + s.calls + "/" + s.allowedCalls + ")");
					s.joinPoint.release();
					if(s.exception != null)
					{
						throw s.exception.newInstance();
					}
					synchronized (s) {
						if(s.locked)
						{
							s.waiting= true;
							try{
								s.wait();
							}catch(Throwable e){
								throw e;
							}
							s.waiting= false; 
						}
					}
					if(waitTime > 0)
					{
						try{
							Thread.sleep(waitTime);
//							synchronized (s) {
//								s.wait(waitTime);
//							}
						}catch(Throwable e){
							throw e;
						}
					}
					return null;
				}
			});
	}
	
	
	
	@SuppressWarnings("unchecked")
	private T aop(final InvocationHandler handler)
	{
		return (T)Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[]{cls},
				new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method m, Object[] args)
							throws Throwable {
						if(m.getDeclaringClass() == cls)
						{
							return handler.invoke(proxy, m, args);
						}
						else if(m.getDeclaringClass() == Object.class)
						{
							return m.invoke(this, args);
						}
						return null;
					}
				});
	}

}
