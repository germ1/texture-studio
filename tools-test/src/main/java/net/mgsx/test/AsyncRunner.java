package net.mgsx.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.internal.runners.statements.InvokeMethod;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

public class AsyncRunner extends BlockJUnit4ClassRunner 
{
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface Timeout 
	{
		int value();
	}

	public AsyncRunner(Class<?> klass) throws InitializationError {
		super(klass);
	}
	
	@Override
	protected Statement methodInvoker(FrameworkMethod method, Object test) {
		final Timeout aTimeout = method.getMethod().getAnnotation(Timeout.class);
		if(aTimeout != null)
		{
			return new InvokeMethod(method, test)
				{
					@Override
					public void evaluate() throws Throwable {
						long t1 = System.currentTimeMillis();
						super.evaluate();
						long t2 = System.currentTimeMillis();
						if(t2 - t1 > aTimeout.value())
						{
							// TODO use JUnit rules mecanisme instead. 
							throw new RuntimeException("expected " + aTimeout.value() + " ms, actual " + (t2 - t1));
						}
					}
				};
		}
		else
		{
			return super.methodInvoker(method, test);
		}
	}

}
