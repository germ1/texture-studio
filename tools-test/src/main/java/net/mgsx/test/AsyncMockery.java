package net.mgsx.test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 
 * UNDER CONSTRUCTION
 * 
 * @author mgsx
 *
 */
public class AsyncMockery 
{
	private static class Invocation
	{
		Object o;
		Method m;
		Object[] args;
		public Invocation(Object o, Method m, Object[] args) {
			super();
			this.o = o;
			this.m = m;
			this.args = args;
		}
		
	}
	
	private ConcurrentLinkedQueue<Invocation> executions = new ConcurrentLinkedQueue<AsyncMockery.Invocation>();
	
	private Thread thread;
	
	public AsyncMockery(final String name) 
	{
		thread = new Thread(new Runnable() {
			@Override
			public void run() 
			{
				for(;;)
				{
					Invocation invocation = executions.poll();
					try {
						invocation.m.invoke(invocation.o, invocation.args);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}, name);
		thread.start();
	}
	
	public <T> T mock(Class<T> cls)
	{
		return aop(cls, new InvocationHandler() {
			@Override
			public Object invoke(final Object o, final Method m, final Object[] args) throws Throwable {
				executions.add(new Invocation(o, m, args));
				return null;
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	private <T> T aop(final Class<T> cls, final InvocationHandler handler)
	{
		return (T)Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[]{cls},
				new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method m, Object[] args)
							throws Throwable {
						if(m.getDeclaringClass() == cls)
						{
							return handler.invoke(proxy, m, args);
						}
						else if(m.getDeclaringClass() == Object.class)
						{
							return m.invoke(this, args);
						}
						return null;
					}
				});
	}
}
