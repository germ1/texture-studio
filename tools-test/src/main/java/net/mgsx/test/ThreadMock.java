package net.mgsx.test;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Outils de test pour le multi-threading.
 * 
 * <p>
 * Simule un thread
 * </p>
 * 
 * @author mgsx
 *
 */
public class ThreadMock 
{
	private Thread thread = null;
	private Lock endLock = new ReentrantLock();
	private volatile Throwable exception = null;
	
	private Semaphore token = new Semaphore(0);
	
	private LinkedBlockingQueue<Runnable> tasks = new LinkedBlockingQueue<Runnable>();
	
	private volatile boolean done = false;
	private volatile boolean running = false;
	
	public ThreadMock() 
	{
		endLock.lock();
		thread = new Thread(new Runnable() {
			@Override
			public void run() 
			{
				try {
					while(!done)
					{
						Runnable task = tasks.take();
						try{
							running = true;
							task.run();
						}catch(Throwable e)
						{
							exception = e;
						}finally{
							running = false;
							token.release();
						}
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					done = true;
				}
				endLock.unlock();
			}
		});
		thread.start();
	}
	
	public void assertRunning()
	{
		try {
			if(!token.tryAcquire(100, TimeUnit.MILLISECONDS))
			{
				if(!running)
					throw new AssertionError("thread is not running");
			}
			else
			{
				token.release();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AssertionError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void executeAsync(Runnable runnable)
	{
		execute(runnable, null, false);
	}
	public void executeAsync(Runnable runnable, Class<? extends Throwable> clsEx)
	{
		execute(runnable, clsEx, false);
	}
	public void executeSync(Runnable runnable)
	{
		execute(runnable, null, true);
	}
	public void executeSync(Runnable runnable, Class<? extends Throwable> clsEx)
	{
		execute(runnable, clsEx, true);
	}
	private void execute(Runnable runnable, Class<? extends Throwable> clsEx, boolean wait)
	{
		try {
			tasks.put(runnable);
			if(wait)
			{
				if(!token.tryAcquire(100, TimeUnit.MILLISECONDS))
				{
					throw new AssertionError("thread locked in current task");
				}
			}
			if(clsEx == null && exception != null)
			{
				throw new AssertionError(exception);
			}
			if(clsEx != null && exception == null)
			{
				throw new AssertionError("expected exception " + clsEx.getName());
			}
			if(clsEx != null && exception != null && !clsEx.isInstance(exception))
			{
				throw new AssertionError("expected exception " + clsEx.getName() + " having " + exception.getClass().getName());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void dispose()
	{
		done = true;
		try {
			tasks.put(new Runnable() {
				@Override
				public void run() {
					
				}
			});
			// attend que le thread finisse
			if(!endLock.tryLock(100, TimeUnit.MILLISECONDS))
			{
				throw new AssertionError("thread locked in current task");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
