package net.mgsx.test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.reflections.Reflections;

public class PerfMockery 
{
	final int N = 10000;
	
	private static class Report
	{
		double durationMs;
		Throwable error;
		Class<?> cls;
		Method m;
	}
	
	public Map<Class<?>, Object> mockMap = new HashMap<Class<?>, Object>();
	
	private List<Report> reports = new ArrayList<PerfMockery.Report>();
	
	private <T> Object invokeNTimes(T object, Method m, Object[] args) throws Throwable
	{
		Report report = new Report();
		report.cls = object.getClass();
		report.m = m;
		Object result = null;
		long ptime = System.currentTimeMillis();
		try
		{
			for(int i=0 ; i<N ; i++)
			{
				result = m.invoke(object, args);
			}
		}catch(Throwable e)
		{
			report.error = e;
		}
		long ctime = System.currentTimeMillis();
		report.durationMs = (double)(ctime - ptime) * 1000 / (double)N;
		reports.add(report);
		if(report.error != null)
		{
			report.error.printStackTrace();
		}
		return result;
	}
	
	public static interface Factory<T>
	{
		T newInstance(Class<T> cls) throws Exception;
	}
	
	public <T> T mock(final Class<T> cls, final String pkg, final Factory<T> f)
	{
		return aop(cls, new InvocationHandler(){

			@Override
			public Object invoke(Object proxy, Method m, Object[] args)
					throws Throwable {
				for(Class<T> subcls : getInstanciableSubClasses(cls, pkg))
				{
					try{
						T object = f.newInstance(subcls);
						if(object !=null)
						{
							for(Field field : subcls.getFields())
							{
								if(Modifier.isPublic(field.getModifiers()) &&
										!field.getType().isPrimitive() && field.get(object)==null)
								{
									Object value = mockMap.get(field.getType());
									if(value == null)
										throw new AssertionError("type " + field.getType().getName() + " missing in mapping for " + subcls.getName());
									field.set(object, value);
								}
							}
							
							invokeNTimes(object, m, args);
						}
					}catch(Throwable e)
					{
						e.printStackTrace();
					}
				}
				return null;
			}});
	}
	public <T> T mock(Class<T> cls, final T object)
	{
		return aop(cls, new InvocationHandler(){

			@Override
			public Object invoke(Object proxy, Method m, Object[] args)
					throws Throwable {
				try{
				return invokeNTimes(object, m, args);
				}catch(Throwable e){e.printStackTrace();}
				return null;
			}});
	}
	@SuppressWarnings("unchecked")
	private static <T> List<Class<T>> getInstanciableSubClasses(Class<T> type, String pkg)
	{
		List<Class<T>> suitableClasses = new ArrayList<Class<T>>();
		Reflections ref = new Reflections(pkg);
		
		if(!type.isInterface() && !Modifier.isAbstract(type.getModifiers()) &&
				type.getAnnotation(Deprecated.class) == null)
		{
			suitableClasses.add(type);
		}
		for(Class<? extends T> clazz : ref.getSubTypesOf(type))
		{
			if(!clazz.isInterface() && !Modifier.isAbstract(clazz.getModifiers()) &&
					clazz.getAnnotation(Deprecated.class) == null)
			{
				suitableClasses.add((Class<T>)clazz);
			}
		}
		return suitableClasses;
	}
	
	@SuppressWarnings("unchecked")
	private <T> T aop(final Class<T> cls, final InvocationHandler handler)
	{
		return (T)Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[]{cls},
				new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method m, Object[] args)
							throws Throwable {
						if(m.getDeclaringClass() == cls)
						{
							return handler.invoke(proxy, m, args);
						}
						else
						{
							return m.invoke(proxy, args);
						}
					}
				});
	}
	
	public String getReport()
	{
		String s = "class,method,time,error\n";
		for(Report report : reports)
		{
			s += report.cls.getSimpleName() + "," + 
					report.m.getName() + "," + 
					report.durationMs + "," + 
					(report.error == null ? "" : report.error.getMessage()) + "\n";
		}
		return s;
	}
	
}
