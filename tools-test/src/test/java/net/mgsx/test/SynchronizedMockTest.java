package net.mgsx.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SynchronizedMockTest 
{
	public static interface TestMeth
	{
		void methA();
		void methB();
		void methC();
	}
	
	private SynchronizedMock<TestMeth> subject;
	
	@Before
	public void setup()
	{
		subject = new SynchronizedMock<TestMeth>(TestMeth.class);
	}
	
	@Test(timeout=100)
	public void testMock()
	{
		TestMeth mock = subject.mock();
		Assert.assertNotNull(mock);
		mock.methA();
		
		subject.lock().methA();
		
		mock.methB();
		mock.methC();
		
		subject.unlock().methA();
		
		mock.methA();
	}
	@Test
	public void testMockScope()
	{
		TestMeth mock = subject.mock();
		
		mock.hashCode();
		
	}
	
}
