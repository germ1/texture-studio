package net.mgsx.midi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Transmitter;

import net.mgsx.tools.logging.Logger;

public class MidiManager 
{
	public static class MidiPort
	{
		public String name;
		private MidiDevice device;
	}
	
	public static interface MidiListener
	{
		void onMessage(double value);
	}
	
	private static Map<Integer, MidiListener> listeners = new HashMap<Integer, MidiManager.MidiListener>();
	
	private static Map<MidiDevice, Receiver> outputs = new HashMap<MidiDevice, Receiver>();
	
	public static List<MidiPort> getInputDevices()
	{
		return getDevices(true, false);
	}
	public static List<MidiPort> getOuputDevices()
	{
		return getDevices(false, true);
	}
	public static List<MidiPort> getDevices(boolean input, boolean output)
	{
		List<MidiPort> lst = new ArrayList<MidiPort>();
		for(Info info : MidiSystem.getMidiDeviceInfo())
		{
			MidiDevice device;
			try {
				device = MidiSystem.getMidiDevice(info);
				if(input && device.getMaxTransmitters() != 0 ||
						output && device.getMaxReceivers() != 0)
				{
					MidiPort mp = new MidiPort();
					mp.device = device;
					mp.name = info.getName() + " " + info.getDescription();
					lst.add(mp);
				}
			} catch (MidiUnavailableException e) {
				Logger.error(Logger.MIDI, e);
			}
		}
		return lst;
	}
	public static synchronized void send(MidiListener listener, double value)
	{
		for(Entry<Integer, MidiListener> e : listeners.entrySet())
		{
			if(e.getValue() == listener)
			{
				int key = e.getKey();
				ShortMessage msg = new ShortMessage();
				try {
					msg.setMessage(
							(key >> 8) & 0xFF,
							(key >> 16) & 0xFF, 
							(key >> 0) & 0xFF,
							(int)Math.ceil(value * 127));
					
					send(msg);
					
					return;
					
					
				} catch (InvalidMidiDataException e1) {
					Logger.error(Logger.MIDI, e1);
				}
			}
		}
	}
	public static synchronized void send(ShortMessage message)
	{
		for(Receiver rc : outputs.values())
		{
			final long timestamp = 0; // TODO timestamp
			rc.send(message, timestamp); 
			Logger.log(Logger.MIDI, "midi out : " + timestamp + " c" + message.getChannel() + " " + message.getCommand() + " " + message.getData1() + " " + message.getData2());
		}
	}
	
	private static void inFlow(MidiPort port, MidiMessage message, long timeStamp)
	{
		if(message instanceof ShortMessage)
		{
			ShortMessage sm = (ShortMessage)message;
			int chan = sm.getChannel();
			int cc = sm.getData1();
			int value = sm.getData2();
			int cmd = sm.getCommand();
			
			int key = chan << 16 | cmd << 8 | cc;
			
			if(learning != null)
			{
				synchronized(MidiManager.class)
				{
					listeners.put(key, learning);
					Logger.log(Logger.MIDI, "learning done : " + key);
				}
				send(learning, learningValue);
				learning = null;
			}
			else
			{
				MidiListener listener = null;
				synchronized(MidiManager.class)
				{
					listener = listeners.get(key);
				}
				if(listener != null)
				{
					listener.onMessage((double)value / 127.0);
//					for(Receiver rc : outputs.values())
//					{
//						//rc.send(message, timeStamp);
//					}
				}
			}
			
			Logger.log(Logger.MIDI, "midi in  : " + timeStamp + " c" + chan + " " + cmd + " " + cc + " " + value);
		}
	}
	
	public static void enable(final MidiPort port, boolean state)
	{
		if(state)
		{
			try {
				port.device.open();
				if(port.device.getMaxReceivers() != 0)
				{
					Receiver rc = port.device.getReceiver();
					outputs.put(port.device, rc);
				}
				else if(port.device.getMaxTransmitters() != 0)
				{
					Transmitter tr = port.device.getTransmitter();
					tr.setReceiver(new Receiver() {
						
						@Override
						public void send(MidiMessage message, long timeStamp) {
							inFlow(port, message, timeStamp);
						}
						
						@Override
						public void close() {
						}
					});
				}
				else
				{
					Logger.warn(Logger.MIDI, "no input or output on device");
				}
				
			} catch (MidiUnavailableException e) {
				Logger.error(Logger.MIDI, e);
			}
			
		}
		else
		{
			// TODO remove
			if(port.device.getMaxReceivers() != 0)
			{
				outputs.remove(port.device);
			}
			else if(port.device.getMaxTransmitters() != 0)
			{
				// TODO remove !
			}
			
		}
	}
	
	private static MidiListener learning;
	private static double learningValue;
	
	public static void learn(MidiListener listener, double value)
	{
		if(learning == listener)
		{
			release(listener);
			learning = null;
			Logger.log(Logger.MIDI, "learning canceled");
		}
		else
		{
			learning = listener;
			learningValue = value;
			Logger.log(Logger.MIDI, "learning started");
		}
	}
	public synchronized static void release(MidiListener listener)
	{
		for(Entry<Integer, MidiListener> e : listeners.entrySet())
		{
			if(e.getValue() == listener)
			{
				listeners.remove(e.getKey());
				return;
			}
		}
	}
	
	
}
