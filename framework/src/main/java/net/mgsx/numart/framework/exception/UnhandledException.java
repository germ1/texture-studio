package net.mgsx.numart.framework.exception;

public class UnhandledException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnhandledException() {
		super();
	}

	public UnhandledException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnhandledException(String message) {
		super(message);
	}

	public UnhandledException(Throwable cause) {
		super(cause);
	}

}
