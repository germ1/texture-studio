package net.mgsx.numart.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Déclare un champs critique pouvant causer
 * des problèmes de performance pour des valeurs élevées
 * 
 * @author mgsx
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Caution {

	String value() default "";
	
}
