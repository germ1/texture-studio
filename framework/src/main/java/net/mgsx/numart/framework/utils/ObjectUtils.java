package net.mgsx.numart.framework.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamLong;
import net.mgsx.numart.framework.utils.ReflectUtils.Visitor;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.MathUtil;

/**
 * Utilitaire pour la manipulation de beans
 * 
 * @author mgsx
 *
 */
public class ObjectUtils 
{
	public static class Parameter
	{
		public Object host;
		public Field field;
		public Parameter(Object host, Field field) {
			super();
			this.host = host;
			this.field = field;
		}
		
	}
	
	public static List<Parameter> getParameters(Object object, boolean deep)
	{
		final List<Parameter> lst = new ArrayList<Parameter>();
		if(deep)
		{
			ReflectUtils.visitGraph(object, new Visitor() {
				
				@Override
				public void visit(Object object) {
					lst.addAll(getParameters(object, false));
				}
			});
		}
		
		for(Field field : object.getClass().getFields())
		{
			
			if(ReflectUtils.isParameter(field))
			{
				lst.add(new Parameter(object, field));
			}
		}
		
		return lst;
	}

	private static Map<Object, Object> createCopyMap(Object object, boolean deep)
	{
		final Map<Object, Object> copyMap = new HashMap<Object, Object>();
		
		if(deep)
		{
			ReflectUtils.visitGraph(object, new Visitor() {
				
				@Override
				public void visit(Object object) {
					copyMap.put(object, ReflectUtils.newInstance(object.getClass(), true));
				}
			});
		}
		else
		{
			ReflectUtils.visitGraph(object, new Visitor() {
				
				@Override
				public void visit(Object object) {
					copyMap.put(object, object);
				}
			});
			copyMap.put(object, ReflectUtils.newInstance(object.getClass(), true));
		}
		return copyMap;
	}
	
	public static Object copy(Object object, boolean deep)
	{
		final Map<Object, Object> copyMap = createCopyMap(object, deep);
		List<Parameter> params = getParameters(object, deep);
		
		for(Parameter param : params)
		{
			Object hostCopy = copyMap.get(param.host);
			Object value = ReflectUtils.get(param.host, param.field);
			if(value != null)
			{
				Object valueCopy = copyMap.get(value);
				if(valueCopy == null)
				{
					valueCopy = value;
				}
				ReflectUtils.set(hostCopy, param.field, valueCopy);
			}
		}
		
		return copyMap.get(object);
	}
	
	public static Object randomizeCopy(Object object, boolean deep, final double randomness)
	{
		final Map<Object, Object> copyMap = createCopyMap(object, deep);
		List<Parameter> params = getParameters(object, deep);
		
		for(Parameter param : params)
		{
			Object hostCopy = copyMap.get(param.host);
			Object baseValue = ReflectUtils.get(param.host, param.field);
			if(baseValue != null)
			{
				Object valueCopy = copyMap.get(baseValue);
				if(valueCopy == null)
				{
					valueCopy = baseValue;
				}
				if(valueCopy != null)
				{
					Field field = param.field;
					if(field.getAnnotation(ParamDouble.class) != null)
					{
						ParamDouble a = field.getAnnotation(ParamDouble.class);
						double value = MathUtil.random(a.min(), a.max(), (Double)baseValue, randomness);
						ReflectUtils.set(hostCopy, field, value);
					}
					else if(field.getAnnotation(ParamLong.class) != null)
					{
						ReflectUtils.set(hostCopy, field, new Random().nextLong());
					}
					else if(field.getAnnotation(ParamColor.class) != null)
					{
						Color colorBase = (Color)ReflectUtils.get(param.host, field);
						Color color = (Color)ReflectUtils.get(hostCopy, field);
						color.r = MathUtil.random(0, 1, colorBase.r, randomness);
						color.g = MathUtil.random(0, 1, colorBase.g, randomness);
						color.b = MathUtil.random(0, 1, colorBase.b, randomness);
						color.a = MathUtil.random(0, 1, colorBase.a, randomness);
					}else
					{
						ReflectUtils.set(hostCopy, param.field, valueCopy);
					}
					
					
				}
				
			}
		}
		
		return copyMap.get(object);
		
	}

}
