package net.mgsx.numart.framework.control;

public interface Pointer 
{
	/**
	 * x y synchronization by aquirement
	 */
	public void update();
	
	/**
	 * @return -1 (left), +1 (right)
	 */
	public double getX();
	
	/**
	 * @return -1 (bottom), +1 (top)
	 */
	public double getY();
}
