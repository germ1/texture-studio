package net.mgsx.numart.framework.audio;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO piste de son avec control de volume
 * permet de gérer le volume de plusieurs clips.
 * 
 * @author mgsx
 *
 */
public class SoundTrack 
{
	public List<Sound> clips = new ArrayList<Sound>();
	
	double volume = 1;
	
	public void setVolume(double gain) 
	{
		if(volume != gain)
		{
			for(Sound clip : clips)
				clip.setVolume(gain);
			volume = gain;
		}
	}

}
