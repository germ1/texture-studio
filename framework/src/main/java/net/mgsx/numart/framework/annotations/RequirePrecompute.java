package net.mgsx.numart.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Déclare un champs nécessitant un pré-calcul.
 * La méthode de précalcul est appelé lorsque le champs a
 * été modifié.
 * 
 * @author mgsx
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RequirePrecompute {

}
