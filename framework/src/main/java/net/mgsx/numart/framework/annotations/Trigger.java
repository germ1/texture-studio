package net.mgsx.numart.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Déclare une méthode pouvant être déclanchée
 * par l'utilisateur.
 * Se matérialise par un bouton.
 * 
 * @author mgsx
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Trigger 
{

}
