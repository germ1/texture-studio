package net.mgsx.numart.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Indique que le champs à lui seul permet
 * de contrôler le composant entier.
 * Utilisé principalement pour les composants simple
 * faisant office de wrapper d'un type simple.
 * Attention, seule un champs d'un module peut
 * bénéficier de cette caractéristique.
 * 
 * @author mgsx
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface QuickParameter {

}
