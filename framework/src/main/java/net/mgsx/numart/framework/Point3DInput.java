package net.mgsx.numart.framework;

import net.mgsx.numart.math.Point3D;

public interface Point3DInput {

	Point3D getPoint3D();
	Point3D [] getPoints3D();
}
