package net.mgsx.numart.framework;

/**
 * Classe de base pour les beans
 * 
 * @author mgsx
 *
 */
public class NamedElement {

	public String name;
	
	public void refresh()
	{
		
	}
	
	/**
	 * Sub-classes may override this method to release
	 * all aquired resources (like threads or system resources)
	 */
	public void destroy()
	{
		
	}
}
