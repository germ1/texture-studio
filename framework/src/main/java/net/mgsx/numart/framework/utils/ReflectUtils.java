package net.mgsx.numart.framework.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Persistent;
import net.mgsx.numart.framework.annotations.Precompute;
import net.mgsx.numart.framework.annotations.QuickParameter;
import net.mgsx.numart.framework.annotations.RequirePrecompute;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypesScanner;

/**
 * Utilitaire dédié à la réflexion
 * 
 * @author mgsx
 *
 */
public class ReflectUtils 
{
	// TODO make a registry system to add more than one package to scan
	public static final String SCAN_PACKAGE = "net.mgsx.numart"; 
	
	public static Set<Class<?>> getTypes()
	{
		TypesScanner scanner = new TypesScanner();
		
		SubTypesScanner stScanner = new SubTypesScanner(false);
		
		Reflections ref = new Reflections(ReflectUtils.SCAN_PACKAGE, scanner, stScanner);
		
		return ref.getSubTypesOf(Object.class);
	}
	public static List<Field> getFields(Class<?> cls)
	{
		List<Field> lst = new ArrayList<Field>();
		for(Field field : cls.getFields())
		{
			if(isPersistentField(field))
			{
				lst.add(field);
			}
		}
		return lst;
	}
	public static boolean instanceOf(Object object, String fieldName, Class<?> type)
	{
		try {
			return type.isAssignableFrom(object.getClass().getField(fieldName).getType());
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T createDefault(Class<? extends T> cls)
	{
		T obj = (T)ReflectUtils.newInstance(cls);
		for(Method m : cls.getMethods())
		{
			if(m.getAnnotation(DefaultFactory.class) != null)
			{
				try {
					m.invoke(obj);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return obj;
	}
	
	public static Field getField(Class<?> cls, String fieldName)
	{
		Field field = null;
		try {
			field = cls.getField(fieldName);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return field;
	}
	
	public static Field getQuickParameterField(Class<?> cls)
	{
		for(Field field : cls.getDeclaredFields())
		{
			if(field.getAnnotation(QuickParameter.class) != null)
			{
				return field;
			}
		}
		return null;
	}
	
	/**
	 * Find all instanciable subtypes.
	 * @param type a type
	 * @return list of classes
	 */
	public static List<Class<?>> getInstanciableSubClasses(Class<?> type)
	{
		List<Class<?>> suitableClasses = new ArrayList<Class<?>>();
		Reflections ref = new Reflections(SCAN_PACKAGE);
		
		if(!type.isInterface() && !Modifier.isAbstract(type.getModifiers()) &&
				type.getAnnotation(Deprecated.class) == null)
		{
			suitableClasses.add(type);
		}
		for(Class<?> clazz : ref.getSubTypesOf(type))
		{
			if(!clazz.isInterface() && !Modifier.isAbstract(clazz.getModifiers()) &&
					clazz.getAnnotation(Deprecated.class) == null)
			{
				suitableClasses.add(clazz);
			}
		}
		return suitableClasses;
	}
	
	/**
	 * Instanciation simplifiée d'une classe
	 * @param cls la classe à instancier
	 * @return une nouvelle instance ou null en cas d'erreur.
	 */
	public static Object newInstance(String cls)
	{
		try {
			return newInstance(Class.forName(cls));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static Object newInstance(Class<?> cls)
	{
		Object instance = null;
		try {
			instance = cls.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return instance;
	}
	public static Object newInstance(Class<?> cls, boolean precompute)
	{
		Object instance = newInstance(cls);
		if(precompute)
		{
			checkForPrecomputation(instance);
		}
		return instance;
	}
	public static Object get(Object host, Field field)
	{
		Object value = null;
		if(host != null && field != null)
		{
			if(field.getGenericType() != null)
			{
				//ield.
			}
			try {
				value = field.get(host);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return value;
	}
	public static Object get(Object host, String field)
	{
		try {
			return get(host, host.getClass().getField(field));
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static boolean trySet(Object host, String field, Object value)
	{
		try {
			if(value != null && host != null && host.getClass().getField(field) != null)
			{
				return host.getClass().getField(field).getType().isPrimitive() || host.getClass().getField(field).getType().isAssignableFrom(value.getClass());
			}
			else if(value == null)
			{
				return true;
			}
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static void set(Object host, String field, Object value)
	{
		try {
			set(host, host.getClass().getField(field), value);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void set(Object host, Field field, Object value)
	{
		if(host != null && field != null)
		{
			try {
				field.set(host, value);
				checkForPrecomputation(host, field);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static boolean isPersistentField(Field field)
	{
		if(Modifier.isPublic(field.getModifiers()))
		{
			if(field.getType().isPrimitive())
			{
				return true;
			}
			for(Annotation a : field.getAnnotations())
			{
				if(a.annotationType().getAnnotation(Persistent.class) != null)
				{
					return true;
				}
			}
		}
		return false;
	}
	public static boolean isParameter(Field field)
	{
		// TODO pas forcément persistant
		return isPersistentField(field);
	}
	public static boolean isRequirePrecomputation(Field field)
	{
		return field != null && field.getAnnotation(RequirePrecompute.class) != null;
	}
	
	public static void precompute(Object object)
	{
		if(object != null)
		{
			Class<?> cls = object.getClass();
			for(Method method : cls.getMethods())
			{
				if(method.getAnnotation(Precompute.class) != null)
				{
					try {
						method.invoke(object);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public static void checkForPrecomputation(Object object, Field field)
	{
		if(isRequirePrecomputation(field))
		{
			precompute(object);
		}
	}
	public static void checkForPrecomputation(Object object)
	{
		visitGraph(object, new Visitor() {
			@Override
			public void visit(Object object) {
				precompute(object);
			}
		});
	}
	
	public static interface Visitor
	{
		void visit(Object object);
	}
	public static void visitGraph(Object node, Visitor visitor)
	{
		visitGraphRecursive(node, visitor, new ArrayList<Object>());
	}
	private static void visitGraphRecursive(Object object, Visitor visitor, List<Object> scannedObject)
	{
		// prevent recursion with already proceeded objects.
		if(object != null && !scannedObject.contains(object))
		{
			scannedObject.add(object);
			visitor.visit(object);
			for(Field field : object.getClass().getFields())
			{
				// prevent infinite recursion with primitive implicit 
				// conversion to wrapper (exemple : double => new Double)
				// done with the reflection get method.
				if(!field.getType().isPrimitive() && field.getType() != String.class)
				{
					visitGraphRecursive(get(object, field), visitor, scannedObject);
				}
			}
			if(object instanceof Iterable<?>)
			{
				for(Iterator<?> i = ((Iterable<?>)object).iterator() ; i.hasNext() ; )
				{
					visitGraphRecursive(i.next(), visitor, scannedObject);
				}
			}
		}
		
	}
	
	
	
	
}
