package net.mgsx.numart.framework.control.impl;

import java.awt.DisplayMode;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;

import net.mgsx.numart.framework.control.Pointer;
import net.mgsx.numart.framework.control.Trigger;

public class MousePointer implements Pointer, Trigger
{
	private double x;
	private double y;
	private double w;
	private double h;
	
	public static boolean clicked = false;
	public static double px;
	public static double py;

	public static boolean relative = true;

	
	@Override
	public void update() {
		if(relative)
		{
			w = h = 1;
			x = px * 2 - 1;
			y = (1 - py) * 2 - 1;
		}
		else
		{
			PointerInfo info = MouseInfo.getPointerInfo();
			Point p = info.getLocation();
			
			if(w == 0 || h == 0)
			{
				DisplayMode dm = info.getDevice().getDisplayMode();
				w = (double)dm.getWidth();
				h = (double)dm.getHeight();
				
			}
			x = ((double)p.x / (double)w) * 2 - 1;
			y = (1 - (double)p.y / (double)h) * 2 - 1;
		}
	}

	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public boolean aquire() {
		boolean value = clicked;
		clicked = false;
		return value;
	}

}
