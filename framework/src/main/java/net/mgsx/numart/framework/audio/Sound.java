package net.mgsx.numart.framework.audio;

public interface Sound {

	public abstract void prepare();

	public abstract void play();

	public void setVolume(double volume);
}