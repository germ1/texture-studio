package net.mgsx.numart.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import net.mgsx.numart.framework.Renderer;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Icon 
{
	Class<? extends Renderer> value();
}
