package net.mgsx.numart.framework.control;

public interface TwoPlayerBoxControl {
	public void update();
	public double getX(int player, int pointer);
	public double getY(int player, int pointer);
}
