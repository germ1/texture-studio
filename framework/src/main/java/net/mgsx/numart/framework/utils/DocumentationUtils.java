package net.mgsx.numart.framework.utils;

import java.lang.reflect.Field;

import net.mgsx.numart.framework.annotations.UserDoc;

/**
 * Utilitaire pour manipuler les commentaires
 * issue des annotations.
 * 
 * @author mgsx
 *
 */
public class DocumentationUtils {

	public static String getPlainTextSimpleDoc(Object node)
	{
		String message = "";
		if(node != null)
		{
			UserDoc aDoc = node.getClass().getAnnotation(UserDoc.class);
			if(aDoc != null)
			{
				message += aDoc.title() + " : " + aDoc.description();
			}
			else
			{
				message += "<" + node.getClass().getSimpleName() + ">";
			}
		}
		return message;
		
	}
	public static String getPlainTextDoc(Object node)
	{
		String message = getPlainTextSimpleDoc(node);
		if(node != null)
		{
			for(Field field : node.getClass().getFields())
			{
				message += "\n- ";
				if(field.getAnnotation(UserDoc.class) != null)
				{
					message += field.getAnnotation(UserDoc.class).description() + " : " + field.getAnnotation(UserDoc.class).description();
				}
				else
				{
					message += field.getName() + " : ---";
				}
			}
		}
		return message;
		
	}
}
