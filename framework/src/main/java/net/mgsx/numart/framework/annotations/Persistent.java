package net.mgsx.numart.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Déclare une annotation persistante
 * Les champs décarés avec une annotation déclarée persistente
 * sera persistée.
 * 
 * @author mgsx
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface Persistent {

}
