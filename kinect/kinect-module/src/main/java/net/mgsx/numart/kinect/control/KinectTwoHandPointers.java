package net.mgsx.numart.kinect.control;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.control.MultiPointer;
import net.mgsx.numart.kinect.KinectUser;
import net.mgsx.numart.math.Point3D;

public class KinectTwoHandPointers implements MultiPointer
{
	@ParamGroup
	public KinectUser kinectUser;
	
	private double [] x = {0,0};
	private double [] y = {0,0};
	
	public void update()
	{
		Point3D[] pts = kinectUser.getPoints3D();
		
		if(pts != null && pts.length >= 4)
		{
			Point3D pCom = pts[0];
			Point3D pHead = pts[1];
			Point3D pLeft = pts[2];
			Point3D pRight = pts[3];
			
			if(pHead != null && pLeft != null && pRight != null)
			{
				Point3D dLeft = Point3D.subtract(pLeft, pCom);
				Point3D dRight = Point3D.subtract(pRight, pCom);
				double scale = Point3D.subtract(pHead, pCom).length();
				
				double ratio = 1;
				
				// version mains légèrement spéparées
//				x[0] = 0.5 + (2 * dLeft.x / scale) * ratio;
//				y[0] = (1.5 * ( - dLeft.y) / scale - 1.0) * ratio;
//				
//				x[1] = -0.5 + (2 * dRight.x / scale) * ratio;
//				y[1] = (1.5 * ( - dRight.y) / scale - 1.0) * ratio;
				
				// version mains jointes
				x[0] = 0.3 + (2 * dLeft.x / scale) * ratio;
				y[0] = (1.5 * ( - dLeft.y) / scale - 1.0) * ratio;
				
				x[1] = -0.3 + (2 * dRight.x / scale) * ratio;
				y[1] = (1.5 * ( - dRight.y) / scale - 1.0) * ratio;
			}
		}
	}


	@Override
	public double getX(int index) {
		return x[index];
	}


	@Override
	public double getY(int index) {
		return y[index];
	}


	@Override
	public int getCount() {
		return 2;
	}
}
