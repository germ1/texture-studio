package net.mgsx.numart.kinect;

import java.nio.ShortBuffer;

import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

import org.OpenNI.GeneralException;
import org.OpenNI.IRGenerator;
import org.OpenNI.IRMetaData;

@Deprecated
public class KinectIR extends AbstractKinect implements ScalarFromSpace 
{
	private IRGenerator iRGen;
	private int width;
	private int height;
	private short [] buffer;
	private int depthMax;
	
	@Override
	protected void onInit() throws GeneralException
	{
        iRGen = IRGenerator.create(getContext());
        IRMetaData depthMD = iRGen.getMetaData();
        depthMax = 1 << 15;
        width = depthMD.getFullXRes();
        height = depthMD.getFullYRes();
        buffer = new short [width * height];
	}
	
	@Override
	public double getValue(Point3D point) 
	{
		init(); // XXX
		int x = (int)((point.x + 0.5) * width);
		int y = (int)((point.y + 0.5) * height);
		int offset = y*width+x;
		int size = width*height;
		offset = (offset % size + size) % size;
		double value = (double)buffer[offset] / (double)100;
        return 1 - value;
	}

	@Override
	protected void onUpdate() throws GeneralException 
	{
		IRMetaData depthMD = iRGen.getMetaData();
		getContext().waitAnyUpdateAll();
		// depthMD.getData().createShortBuffer();
        ShortBuffer depth = depthMD.getData().createShortBuffer();
        depth.rewind();

        int points = 0;
        while(depth.remaining() > 0)
        {
        	buffer[points] = depth.get();
            points++;
        }
		
	}

}
