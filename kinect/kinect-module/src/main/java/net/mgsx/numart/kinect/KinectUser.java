package net.mgsx.numart.kinect;

import java.nio.ShortBuffer;

import net.mgsx.numart.framework.Point3DInput;
import net.mgsx.numart.math.Point3D;
import net.mgsx.tools.ipc.ResourceContext;

import org.OpenNI.CalibrationProgressEventArgs;
import org.OpenNI.CalibrationProgressStatus;
import org.OpenNI.DepthGenerator;
import org.OpenNI.DepthMetaData;
import org.OpenNI.GeneralException;
import org.OpenNI.IObservable;
import org.OpenNI.IObserver;
import org.OpenNI.PoseDetectionCapability;
import org.OpenNI.PoseDetectionEventArgs;
import org.OpenNI.SceneMetaData;
import org.OpenNI.SkeletonCapability;
import org.OpenNI.SkeletonJoint;
import org.OpenNI.SkeletonJointPosition;
import org.OpenNI.SkeletonProfile;
import org.OpenNI.StatusException;
import org.OpenNI.UserEventArgs;
import org.OpenNI.UserGenerator;

public class KinectUser extends KinectTask implements Point3DInput
{
	private DepthGenerator iRGen;
	
	private UserGenerator userGen;
	
    private SkeletonCapability skeletonCap;
    private PoseDetectionCapability poseDetectionCap;

    String calibPose = null;
    
	class NewUserObserver implements IObserver<UserEventArgs>
	{
		@Override
		public void update(IObservable<UserEventArgs> observable,
				UserEventArgs args)
		{
			System.out.println("New user " + args.getId());
			try
			{
				if (skeletonCap.needPoseForCalibration())
				{
					poseDetectionCap.startPoseDetection(calibPose, args.getId());
				}
				else
				{
					skeletonCap.requestSkeletonCalibration(args.getId(), true);
				}
			} catch (StatusException e)
			{
				e.printStackTrace();
			}
		}
	}
	class LostUserObserver implements IObserver<UserEventArgs>
	{
		@Override
		public void update(IObservable<UserEventArgs> observable,
				UserEventArgs args)
		{
			System.out.println("Lost user " + args.getId());
			// joints.remove(args.getId());
		}
	}
	
	class CalibrationCompleteObserver implements IObserver<CalibrationProgressEventArgs>
	{
		@Override
		public void update(IObservable<CalibrationProgressEventArgs> observable,
				CalibrationProgressEventArgs args)
		{
			System.out.println("Calibraion complete: " + args.getStatus());
			try
			{
				if (args.getStatus() == CalibrationProgressStatus.OK)
				{
					System.out.println("starting tracking "  +args.getUser());
						skeletonCap.startTracking(args.getUser());
		                // joints.put(new Integer(args.getUser()), new HashMap<SkeletonJoint, SkeletonJointPosition>());
				}
				else if (args.getStatus() != CalibrationProgressStatus.MANUAL_ABORT)
				{
					if (skeletonCap.needPoseForCalibration())
					{
						poseDetectionCap.startPoseDetection(calibPose, args.getUser());
					}
					else
					{
						skeletonCap.requestSkeletonCalibration(args.getUser(), true);
					}
				}
			} catch (StatusException e)
			{
				e.printStackTrace();
			}
		}
	}
	class PoseDetectedObserver implements IObserver<PoseDetectionEventArgs>
	{
		@Override
		public void update(IObservable<PoseDetectionEventArgs> observable,
				PoseDetectionEventArgs args)
		{
			System.out.println("Pose " + args.getPose() + " detected for " + args.getUser());
			try
			{
				poseDetectionCap.stopPoseDetection(args.getUser());
				skeletonCap.requestSkeletonCalibration(args.getUser(), true);
			} catch (StatusException e)
			{
				e.printStackTrace();
			}
		}
	}
	@Override
	public void setup(ResourceContext ctx) 
	{
        try {
        	super.createKinectContext();
        	
			iRGen = DepthGenerator.create(getContext());
			userGen = UserGenerator.create(getContext());
			// imageGen = ImageGenerator.create(getContext());
			
			userGen.getSkeletonCapability().setSkeletonProfile(SkeletonProfile.ALL);
			
            skeletonCap = userGen.getSkeletonCapability();
            poseDetectionCap = userGen.getPoseDetectionCapability();
            
            userGen.getNewUserEvent().addObserver(new NewUserObserver());
            userGen.getLostUserEvent().addObserver(new LostUserObserver());
            skeletonCap.getCalibrationCompleteEvent().addObserver(new CalibrationCompleteObserver());
            poseDetectionCap.getPoseDetectedEvent().addObserver(new PoseDetectedObserver());

            calibPose = skeletonCap.getSkeletonCalibrationPose();
            
			
            getContext().startGeneratingAll();
 			
		} catch (GeneralException e) {
			e.printStackTrace();
		}
 	}
	
	final int maxUsers = 4;
	final int nbElementsByPlayer = 4;
	
	@Override
	public void proceed(ResourceContext ctx) 
	{
			try {
				getContext().waitAnyUpdateAll();
			} catch (StatusException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			
			DepthMetaData depthMD = iRGen.getMetaData();
			SceneMetaData sceneMD = userGen.getUserPixels(0);
			// ImageMetaData imageMD = imageGen.getMetaData();
	        ShortBuffer depth = depthMD.getData().createShortBuffer();
	        ShortBuffer scene = sceneMD.getData().createShortBuffer();
			
	        // ByteBuffer image = imageMD.getData().createByteBuffer();
	        int[] users = null;
	        try {
				users = userGen.getUsers();
			} catch (StatusException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        depth.rewind();
	        scene.rewind();
	
	        // section critique
	        ctx.acquire();
	        try
	        {
				try {
			        for(int i=0 ; i<users.length && i<maxUsers ; i++)
			        {
			        	int user = users[i];
			        	
			        	org.OpenNI.Point3D com, pos;
			        	pos = userGen.getUserCoM(user);
						com = iRGen.convertRealWorldToProjective(pos);
			        	
						org.OpenNI.Point3D phead = getBodyPoint(user, SkeletonJoint.HEAD);
						org.OpenNI.Point3D plh = getBodyPoint(user, SkeletonJoint.LEFT_HAND);
						org.OpenNI.Point3D prh = getBodyPoint(user, SkeletonJoint.RIGHT_HAND);
						
						
						
						pts[i*nbElementsByPlayer + 0] = normalize(com);
						pts[i*nbElementsByPlayer + 1] = normalize(phead);
						pts[i*nbElementsByPlayer + 2] = normalize(plh);
						pts[i*nbElementsByPlayer + 3] = normalize(prh);
//						pts[4] = normalize(getBodyPoint(user, SkeletonJoint.WAIST));
//						pts[5] = normalize(getBodyPoint(user, SkeletonJoint.NECK));
//						pts[6] = normalize(getBodyPoint(user, SkeletonJoint.TORSO));
						
//						pts[7] = normalize(getBodyPoint(user, SkeletonJoint.LEFT_SHOULDER));
//						pts[8] = normalize(getBodyPoint(user, SkeletonJoint.LEFT_FINGER_TIP));
//						pts[9] = normalize(getBodyPoint(user, SkeletonJoint.LEFT_ELBOW));
						
						if(phead != null && com != null && plh != null && prh != null)
						{
							orientation = normalize(prh);
						}
						
			        }
				} catch (StatusException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	        finally
	        {
	        	ctx.release();
	        }
	        
	}
	
	private Point3D normalize(org.OpenNI.Point3D p3D)
	{
		if(p3D == null)
			return null;
		double w = 1000;
		double r = 300;
		return  new Point3D(p3D.getX() / r, p3D.getY() / r, (p3D.getZ()) / w);
	}

	private org.OpenNI.Point3D getBodyPoint(int user, SkeletonJoint joint)
	{
		try {
			SkeletonJointPosition a = userGen.getSkeletonCapability().getSkeletonJointPosition(user, joint);	
			if(a != null)
				return iRGen.convertRealWorldToProjective(a.getPosition());
		} catch (StatusException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	Point3D orientation = new Point3D(0,1,0);
	
	@Override
	public Point3D getPoint3D() 
	{
		// TODO Auto-generated method stub
		return orientation;
	}

	Point3D [] pts = new Point3D[nbElementsByPlayer * maxUsers];
	
	
	@Override
	public Point3D[] getPoints3D() {
		return pts;
	}

}
