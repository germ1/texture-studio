package net.mgsx.numart.kinect.control;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.ValueControl;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.kinect.KinectUser;
import net.mgsx.numart.math.Point3D;

public class HandPointsControl extends NamedElement implements ValueControl
{
	@ParamGroup
	public KinectUser kinectUser;
	
	private double value = 0;
	
	public double getValue()
	{
		Point3D[] pts = kinectUser.getPoints3D();
		
		if(pts != null && pts.length >= 3)
		{
			Point3D pHead = pts[0];
			Point3D pLeft = pts[1];
			Point3D pRight = pts[2];
			
			if(pHead != null && pLeft != null && pRight != null)
			{
				double scaleLeft = Point3D.subtract(pLeft, pHead).length();
				double scaleRight = Point3D.subtract(pRight, pHead).length();
				double scale = Math.max(scaleLeft, scaleRight);
				double newValue = Math.abs((pRight.x - pLeft.x) / scale);
				
				value = Math.min(1, newValue);
			}
		}
		return value;
	}
}
