package net.mgsx.numart.kinect;

import net.mgsx.numart.framework.Task;
import net.mgsx.numart.framework.annotations.Icon;
import net.mgsx.numart.framework.utils.NullRenderer;
import net.mgsx.tools.ipc.LoopWorkflow;
import net.mgsx.tools.ipc.ResourceContext;

import org.OpenNI.Context;
import org.OpenNI.GeneralException;
import org.OpenNI.OutArg;
import org.OpenNI.ScriptNode;

@Icon(NullRenderer.class)
public abstract class KinectTask extends Task implements LoopWorkflow
{
    private static volatile Context context;
    private static final String SAMPLE_XML_FILE = "SamplesConfig.xml";    

    protected Context getContext()
    {
    	return context;
    }
    
	public void createKinectContext() throws GeneralException {
		if(context != null)
			return;
		context = Context.createFromXmlFile(SAMPLE_XML_FILE, new OutArg<ScriptNode>());
	}

	@Override
	public void cleanup(ResourceContext ctx) 
	{
		if(context != null)
		{
			context.release();
			context = null;
		}
	}


}
