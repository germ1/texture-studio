package net.mgsx.numart.kinect;

import java.nio.ShortBuffer;

import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

import org.OpenNI.ActiveHandEventArgs;
import org.OpenNI.DepthGenerator;
import org.OpenNI.DepthMetaData;
import org.OpenNI.GeneralException;
import org.OpenNI.HandsGenerator;
import org.OpenNI.IObservable;
import org.OpenNI.IObserver;

@Deprecated
public class KinectHandTrack extends AbstractKinect implements ScalarFromSpace 
{
	private DepthGenerator iRGen;
	private int width;
	private int height;
	private short [] buffer;
	private int depthMax;
	
	private HandsGenerator handsGen;
	
	@Override
	protected void onInit() throws GeneralException
	{
        iRGen = DepthGenerator.create(getContext());
        DepthMetaData depthMD = iRGen.getMetaData();
        depthMax = 1 << 15;
        width = depthMD.getFullXRes();
        height = depthMD.getFullYRes();
        buffer = new short [width * height];
        
        handsGen = HandsGenerator.create(getContext());
        handsGen.getHandUpdateEvent().addObserver(new IObserver<ActiveHandEventArgs>() {
			
			@Override
			public void update(IObservable<ActiveHandEventArgs> observable,
					ActiveHandEventArgs args) {
				// TODO Auto-generated method stub
				args.getPosition().getX();
			}
		});
	}
	
	@Override
	public double getValue(Point3D point) 
	{
		init(); // XXX
		int x = (int)((point.x + 0.5) * width);
		int y = (int)((point.y + 0.5) * height);
		int offset = y*width+x;
		int size = width*height;
		offset = (offset % size + size) % size;
		double value = (double)buffer[offset] / (double)10000;
        return value;
	}

	@Override
	protected void onUpdate() throws GeneralException 
	{
		DepthMetaData depthMD = iRGen.getMetaData();
		getContext().waitAnyUpdateAll();
		// depthMD.getData().createShortBuffer();
        ShortBuffer depth = depthMD.getData().createShortBuffer();
        depth.rewind();

        int points = 0;
        while(depth.remaining() > 0)
        {
        	buffer[points] = depth.get();
            points++;
        }
		
	}

}
