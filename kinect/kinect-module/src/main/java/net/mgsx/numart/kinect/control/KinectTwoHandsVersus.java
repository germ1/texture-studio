package net.mgsx.numart.kinect.control;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.control.MultiPointer;
import net.mgsx.numart.framework.control.TwoPlayerBoxControl;
import net.mgsx.numart.kinect.KinectUser;
import net.mgsx.numart.math.Point3D;

public class KinectTwoHandsVersus implements TwoPlayerBoxControl
{
	@ParamGroup
	public KinectUser kinectUser;
	
	private double [] x = {0,0,0,0};
	private double [] y = {0,0,0,0};
	
	public void update()
	{
		Point3D[] pts = kinectUser.getPoints3D();
		
		for(int p=0;p<2;p++)
		{
			if(pts != null && pts.length >= (1+p)*4)
			{
				Point3D pCom = pts[p*4+0];
				Point3D pHead = pts[p*4+1];
				Point3D pLeft = pts[p*4+2];
				Point3D pRight = pts[p*4+3];
				
				if(pHead != null && pLeft != null && pRight != null)
				{
					Point3D dLeft = Point3D.subtract(pLeft, pCom);
					Point3D dRight = Point3D.subtract(pRight, pCom);
					double scale = Point3D.subtract(pHead, pCom).length();
					
					double ratio = 1;
					
					// version mains légèrement spéparées
	//				x[0] = 0.5 + (2 * dLeft.x / scale) * ratio;
	//				y[0] = (1.5 * ( - dLeft.y) / scale - 1.0) * ratio;
	//				
	//				x[1] = -0.5 + (2 * dRight.x / scale) * ratio;
	//				y[1] = (1.5 * ( - dRight.y) / scale - 1.0) * ratio;
					
					// version mains jointes
					x[p*2+0] = 0.3 + (2 * dLeft.x / scale) * ratio;
					y[p*2+0] = (1.5 * ( - dLeft.y) / scale - 1.0) * ratio;
					
					x[p*2+1] = -0.3 + (2 * dRight.x / scale) * ratio;
					y[p*2+1] = (1.5 * ( - dRight.y) / scale - 1.0) * ratio;
				}
			}
		}
	}


	@Override
	public double getX(int player, int pointer) {
		return x[player*2+pointer];
	}


	@Override
	public double getY(int player, int pointer) {
		return y[player*2+pointer];
	}
}
