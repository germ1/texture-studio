package net.mgsx.numart.kinect;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.Icon;
import net.mgsx.numart.framework.utils.NullRenderer;

import org.OpenNI.Context;
import org.OpenNI.GeneralException;

@Icon(NullRenderer.class)
public abstract class AbstractKinect extends NamedElement
{
	private static boolean inited = false;
	
    private static volatile Context context;
    private static final String SAMPLE_XML_FILE = "SamplesConfig.xml";    
    
    @Override
    public void destroy() {
    	context.release();
    	context.dispose();
    	context = null;
    	inited = false;
    	super.destroy();
    }
    
    @Override
    public void refresh() {
    	update();
    }
    
	protected static Context getContext()
	{
//		if(context == null)
//		{
//			// TODO double null check synchronized
//	        try {
//				context = Context.createFromXmlFile(SAMPLE_XML_FILE, new OutArg<ScriptNode>());
//			} catch (GeneralException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
        return context;
	}
	/**
	 * Appeler une seule fois
	 */
	final public void init()
	{
		if(!inited)
		{
			
			try {
				onInit();
				onUpdate(); // XXX doit être fait à chaque frame !
				inited = true;
			} catch (GeneralException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Appeler une seule fois
	 */
	final public void update()
	{
		init();
		try {
			onUpdate();
		} catch (GeneralException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected abstract void onInit() throws GeneralException;
	protected abstract void onUpdate() throws GeneralException;
}
