package net.mgsx.numart.kinect.control;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.control.Pointer;
import net.mgsx.numart.kinect.KinectUser;
import net.mgsx.numart.math.Point3D;

public class KinectPointer implements Pointer
{
	@ParamGroup
	public KinectUser kinectUser;
	
	private double x;
	private double y;
	
	public void update()
	{
		Point3D[] pts = kinectUser.getPoints3D();
		
		if(pts != null && pts.length >= 4)
		{
			Point3D pCom = pts[0];
			Point3D pHead = pts[1];
			Point3D pLeft = pts[2];
			Point3D pRight = pts[3];
			
			if(pHead != null && pLeft != null && pRight != null)
			{
				Point3D diff = Point3D.subtract(pRight, pLeft);
				double scale = Point3D.subtract(pHead, pCom).length();
				
				x = 3 * diff.x / scale - 1 - 0.5;
				y = 2.5 * ( - diff.y) / scale - 1;
			}
		}
	}


	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}
}
