package net.mgsx.numart.kinect.control;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.Point3DInput;
import net.mgsx.numart.framework.ValueControl;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.kinect.KinectUser;
import net.mgsx.numart.math.Point3D;

public class HandValueControl extends NamedElement implements Point3DInput
{
	@ParamGroup
	public KinectUser kinectUser;
	
	private double value = 0;
	
	public double getValue()
	{
		Point3D[] pts = kinectUser.getPoints3D();
		
		if(pts != null && pts.length >= 3)
		{
			Point3D pHead = pts[0];
			Point3D pLeft = pts[1];
			Point3D pRight = pts[2];
			
			if(pHead != null && pLeft != null && pRight != null)
			{
				double scaleLeft = Point3D.subtract(pLeft, pHead).length();
				double scaleRight = Point3D.subtract(pRight, pHead).length();
				double scale = Math.max(scaleLeft, scaleRight);
				double newValue = Math.abs((pRight.x - pLeft.x) / scale);
				
				value = Math.min(1, newValue);
			}
		}
		return value;
	}

	@Override
	public Point3D getPoint3D() {
		return getPoints3D()[0];
	}

	private Point3D [] points = null;
	
	@Override
	public Point3D[] getPoints3D() {
		Point3D[] pts = kinectUser.getPoints3D();
		
		if(points == null)
		{
			points = new Point3D [2];
			points[0] = new Point3D();
			points[1] = new Point3D();
		}
		
		if(pts != null && pts.length >= 3)
		{
			Point3D pHead = pts[0];
			Point3D pLeft = pts[1];
			Point3D pRight = pts[2];
			
			if(pHead != null && pLeft != null && pRight != null)
			{
				double scaleLeft = Point3D.subtract(pLeft, pHead).length();
				double scaleRight = Point3D.subtract(pRight, pHead).length();
				double scale = Math.max(scaleLeft, scaleRight);
				
				points[0].x = 0.5+(1 * (pRight.x - pHead.x)) / scale;
				// points[0].y = (pHead.y - pRight.y) / scale;
				points[1].x = 0.5+ 1 * (-pLeft.x + pHead.x) / scale;
				// points[1].y = (pHead.y - pLeft.y) / scale;
				
			}
		}
		return points;
	}
}
