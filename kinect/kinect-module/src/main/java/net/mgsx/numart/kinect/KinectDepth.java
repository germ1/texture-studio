package net.mgsx.numart.kinect;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.nio.ShortBuffer;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.renderer.DirectRenderer;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.tools.ipc.ResourceContext;

import org.OpenNI.CalibrationProgressEventArgs;
import org.OpenNI.CalibrationProgressStatus;
import org.OpenNI.DepthGenerator;
import org.OpenNI.DepthMetaData;
import org.OpenNI.GeneralException;
import org.OpenNI.IObservable;
import org.OpenNI.IObserver;
import org.OpenNI.ImageGenerator;
import org.OpenNI.PoseDetectionCapability;
import org.OpenNI.PoseDetectionEventArgs;
import org.OpenNI.SceneMetaData;
import org.OpenNI.SkeletonCapability;
import org.OpenNI.SkeletonJoint;
import org.OpenNI.SkeletonJointPosition;
import org.OpenNI.SkeletonProfile;
import org.OpenNI.StatusException;
import org.OpenNI.UserEventArgs;
import org.OpenNI.UserGenerator;

public class KinectDepth extends KinectTask implements ScalarFromSpace, 
	DirectRenderer
{
	private DepthGenerator iRGen;
	private int width;
	private int height;
	private double [] buffer;
	private int depthMax;
	
	@Logarithmic
	@ParamDouble(min=0.00001,max=1)
	public double frontCut = 0;
	
	@Logarithmic
	@ParamDouble(min=0.00001,max=1)
	public double backCut = 1;
	
	@ParamBoolean
	public boolean paused = false;
	@ParamInt(min=1,max=24)
	public int lowPassFilter = 1;
	
	private UserGenerator userGen;
	private ImageGenerator imageGen;
	
    private SkeletonCapability skeletonCap;
    private PoseDetectionCapability poseDetectionCap;

    String calibPose = null;
    
	class NewUserObserver implements IObserver<UserEventArgs>
	{
		@Override
		public void update(IObservable<UserEventArgs> observable,
				UserEventArgs args)
		{
			System.out.println("New user " + args.getId());
			try
			{
				if (skeletonCap.needPoseForCalibration())
				{
					poseDetectionCap.startPoseDetection(calibPose, args.getId());
				}
				else
				{
					skeletonCap.requestSkeletonCalibration(args.getId(), true);
				}
			} catch (StatusException e)
			{
				e.printStackTrace();
			}
		}
	}
	class LostUserObserver implements IObserver<UserEventArgs>
	{
		@Override
		public void update(IObservable<UserEventArgs> observable,
				UserEventArgs args)
		{
			System.out.println("Lost user " + args.getId());
			// joints.remove(args.getId());
		}
	}
	
	class CalibrationCompleteObserver implements IObserver<CalibrationProgressEventArgs>
	{
		@Override
		public void update(IObservable<CalibrationProgressEventArgs> observable,
				CalibrationProgressEventArgs args)
		{
			System.out.println("Calibraion complete: " + args.getStatus());
			try
			{
			if (args.getStatus() == CalibrationProgressStatus.OK)
			{
				System.out.println("starting tracking "  +args.getUser());
					skeletonCap.startTracking(args.getUser());
	                // joints.put(new Integer(args.getUser()), new HashMap<SkeletonJoint, SkeletonJointPosition>());
			}
			else if (args.getStatus() != CalibrationProgressStatus.MANUAL_ABORT)
			{
				if (skeletonCap.needPoseForCalibration())
				{
					poseDetectionCap.startPoseDetection(calibPose, args.getUser());
				}
				else
				{
					skeletonCap.requestSkeletonCalibration(args.getUser(), true);
				}
			}
			} catch (StatusException e)
			{
				e.printStackTrace();
			}
		}
	}
	class PoseDetectedObserver implements IObserver<PoseDetectionEventArgs>
	{
		@Override
		public void update(IObservable<PoseDetectionEventArgs> observable,
				PoseDetectionEventArgs args)
		{
			System.out.println("Pose " + args.getPose() + " detected for " + args.getUser());
			try
			{
				poseDetectionCap.stopPoseDetection(args.getUser());
				skeletonCap.requestSkeletonCalibration(args.getUser(), true);
			} catch (StatusException e)
			{
				e.printStackTrace();
			}
		}
	}
	@Override
	public void setup(ResourceContext ctx) 
	{
        try {
        	super.createKinectContext();
        	
			iRGen = DepthGenerator.create(getContext());
			userGen = UserGenerator.create(getContext());
			// imageGen = ImageGenerator.create(getContext());
			
			userGen.getSkeletonCapability().setSkeletonProfile(SkeletonProfile.HEAD_HANDS);
			
            skeletonCap = userGen.getSkeletonCapability();
            poseDetectionCap = userGen.getPoseDetectionCapability();
            
            userGen.getNewUserEvent().addObserver(new NewUserObserver());
            userGen.getLostUserEvent().addObserver(new LostUserObserver());
            skeletonCap.getCalibrationCompleteEvent().addObserver(new CalibrationCompleteObserver());
            poseDetectionCap.getPoseDetectedEvent().addObserver(new PoseDetectedObserver());

            calibPose = skeletonCap.getSkeletonCalibrationPose();
            
            //userGen.getSkeletonCapability().startTracking(0);
          
//			skeletonCap = userGen.getSkeletonCapability();
//            poseDetectionCap = userGen.getPoseDetectionCapability();
//          skeletonCap.getCalibrationCompleteEvent().addObserver(new CalibrationCompleteObserver());
//          poseDetectionCap.getPoseDetectedEvent().addObserver(new PoseDetectedObserver());
//          
//          calibPose = skeletonCap.getSkeletonCalibrationPose();
//          joints = new HashMap<Integer, HashMap<SkeletonJoint,SkeletonJointPosition>>();
//          
//          skeletonCap.setSkeletonProfile(SkeletonProfile.ALL);
            
//            userGen.getNewUserEvent().addObserver(new IObserver<UserEventArgs>() {
//				
//				@Override
//				public void update(IObservable<UserEventArgs> observable, UserEventArgs args) {
//					System.out.println("getNewUserEvent");
//				}
//			});
//            userGen.getGenerationRunningChangedEvent().addObserver(new IObserver<EventArgs>() {
//				@Override
//				public void update(IObservable<EventArgs> observable, EventArgs args) {
//					System.out.println("getGenerationRunningChangedEvent");
//				}
//			});
//            
//            userGen.getLostUserEvent().addObserver(new IObserver<UserEventArgs>() {
//				
//				@Override
//				public void update(IObservable<UserEventArgs> observable, UserEventArgs args) {
//					System.out.println("getLostUserEvent");
//				}
//			});
            
//            userGen.getNewDataAvailableEvent().addObserver(new IObserver<EventArgs>() {
//				
//				@Override
//				public void update(IObservable<EventArgs> observable, EventArgs args) {
//					// System.out.println("getNewDataAvailableEvent");
//				}
//			});
//            userGen.getUserExitEvent().addObserver(new IObserver<UserEventArgs>() {
//				
//				@Override
//				public void update(IObservable<UserEventArgs> observable, UserEventArgs args) {
//					System.out.println("getUserExitEvent");
//				}
//			});
//            userGen.getUserReenterEvent().addObserver(new IObserver<UserEventArgs>() {
//				
//				@Override
//				public void update(IObservable<UserEventArgs> observable, UserEventArgs args) {
//					System.out.println("getUserReenterEvent");
//				}
//			});
			
            getContext().startGeneratingAll();
 			
	      	DepthMetaData depthMD = iRGen.getMetaData();
	      	SceneMetaData sceneMD = userGen.getUserPixels(0);
	        depthMax = 1 << 15;
	        
	        ctx.acquire();
	        try
	        {
	            width = depthMD.getFullXRes();
	            height = depthMD.getFullYRes();
	        	buffer = new double [width * height];
	        }
	        finally
	        {
	        	ctx.release();
	        }
		} catch (GeneralException e) {
			e.printStackTrace();
		}
 	}
	
	private static double max = 0;
	
	@Override
	public double getValue(Point3D point) 
	{
		if(buffer == null)
			return 0;
		
		int x = (int)((point.x + 0.5) * width);
		int y = (int)((point.y + 0.5) * height);
		int offset = y*width+x;
		int size = width*height;
		offset = (offset % size + size) % size;
		double value = buffer[offset];
		if(value > max)
		{
			max = value;
			System.out.println(value);
		}
		value = value < frontCut ? frontCut : value;
		value = value > backCut ? backCut : value;
		value = (value - frontCut) / (backCut - frontCut) + 0.5;
        return value;
	}

	double [][] filters;
	final static int filterdim = 32; 
	int filtersize = 0;
	int filtercount = 0;
	private double filter(int index, double value)
	{
		if(filters == null)
		{
			filters = new double[width*height][filterdim + 1];
		}
		double [] f = filters[index];
//		if(f == null || f.length != filterdim + 1)
//		{
//			f = filters[index] = new double[filterdim + 1];
//		}
		
		if(filtersize < lowPassFilter)
		{
			if(filtersize == 0)
			{
				f[filterdim] = value;
			}
			else
			{
				f[filterdim] = (double)(f[filterdim] * filtersize + value) / (double)(filtersize + 1);
			}
			
			filtersize++;
		}
		else
		{
			double old = f[filtercount];
			f[filterdim] += (value - old) / (double)lowPassFilter;
		}
		f[filtercount] = value;
		return f[filterdim];
	}

	@Override
	public void proceed(ResourceContext ctx) 
	{
		if(!paused)
		{
			try {
				getContext().waitAnyUpdateAll();
			} catch (StatusException e) {
				throw new RuntimeException(e);
			}
			
			DepthMetaData depthMD = iRGen.getMetaData();
			SceneMetaData sceneMD = userGen.getUserPixels(0);
			// ImageMetaData imageMD = imageGen.getMetaData();
	        ShortBuffer depth = depthMD.getData().createShortBuffer();
	        ShortBuffer scene = sceneMD.getData().createShortBuffer();
			
	        // ByteBuffer image = imageMD.getData().createByteBuffer();
	        int[] users = null;
	        try {
				users = userGen.getUsers();
			} catch (StatusException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        depth.rewind();
	        scene.rewind();
	
	        int points = 0;
	        
	        // section critique
	        ctx.acquire();
	        try
	        {
		        while(depth.remaining() > 0)
		        {
		        	short depthValue = depth.get();
		        	short sceneValue = scene.get();
		        	
//		        	double imageR = (double)image.get() / 255;
//		        	double imageG = (double)image.get() / 255;
//		        	double imageB = (double)image.get() / 255;
		        	//buffer[points] = filter(points, (double)(depth.get()) / (double)8192);
		        	 buffer[points] = (double)(depthValue) / (double)8192;
			         buffer[points] = 1 - sceneValue;
		        	 points++;
		        }
//		        quadFilter();
//		        timeFilter();
		        
//		        for(int i=0 ; i<users.length ; i++)
//		        {
//		        	int user = users[i];
//		        	org.OpenNI.Point3D com = iRGen.convertRealWorldToProjective(userGen.getUserCoM(users[i]));
//		        	com.getX();
//		        	com.getY();
//		        	com.getZ();
//		        }
	        }
	        finally
	        {
	        	ctx.release();
	        }
	        
	        filtercount = (filtercount + 1) % lowPassFilter;
		}
	}
	
	private double [] [] pairs;
	
	private double [] mBuffer = null;
	
	private void timeFilter()
	{
		if(pairs == null || pairs.length != buffer.length)
		{
			pairs = new double [buffer.length][2];
		}
		else
		{
			for(int i=0 ; i<height*width ; i++)
			{
				double pA = pairs[i][1];
				double pB = pairs[i][0];
				double pC = buffer[i];
				
				double dAC = Math.abs(pA - pC);
				double dBC = Math.abs(pB - pC);
				double dAB = Math.abs(pB - pA);
				
				double v;
				
				// C divergeant : moyenne des 2 anciens
				if(dBC + dAC >  2 * dAB)
				{
					v = (pA + pB) / 2;
				}
				// C convergenant : le nouveau
				else if(dBC < dAB)
				{
					v = pC;
				}
				// indéterminé : moyenne des 3
				else
				{
					v = (pA + pB + pC ) / 3;
				}
				
				buffer[i] = v;
				pairs[i][1] = pairs[i][0];
				pairs[i][0] = pC;
			}
		}
		
	}
	
	private void quadFilter()
	{
		if(mBuffer == null || mBuffer.length != buffer.length)
		{
			mBuffer = new double [buffer.length];
		}
		else
		{
			for(int x=1 ; x<width ; x++)
			{
				double a = buffer[(x+0)];
				double b = buffer[(x-1)];
				buffer[(x+0)] = (a + b) / 2;
			}
			for(int y=1 ; y<height ; y++)
			{
				double a = buffer[(y+0)*width];
				double b = buffer[(y-1)*width];
				buffer[(y+0)*width] = (a + b) / 2;
				for(int x=1 ; x<width ; x++)
				{
					double c = buffer[(y+0)*width+(x+0)];
					double d = buffer[(y+0)*width+(x-1)];
					double e = buffer[(y-1)*width+(x+0)];
					double f = buffer[(y-1)*width+(x-1)];
					
					buffer[(y+0)*width+(x+0)] = (c + d + e + f) / (4) ;
				}
			}
			for(int i=0 ; i<width*height ; i++)
			{
				mBuffer[i] = buffer[i];
			}
		}
		
	}

	@Override
	public void render(BufferedImage target) 
	{
		if(!paused)
		{
			try {
				getContext().waitAnyUpdateAll();
			} catch (StatusException e) {
				throw new RuntimeException(e);
			}
			
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			DepthMetaData depthMD = iRGen.getMetaData();
			SceneMetaData sceneMD = userGen.getUserPixels(0);
			// ImageMetaData imageMD = imageGen.getMetaData();
	        ShortBuffer depth = depthMD.getData().createShortBuffer();
	        ShortBuffer scene = sceneMD.getData().createShortBuffer();
			
	        int bpp = sceneMD.getData().getBytesPerPixel();
	        // ByteBuffer image = imageMD.getData().createByteBuffer();
	        int[] users = null;
	        try {
				users = userGen.getUsers();
			} catch (StatusException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        depth.rewind();
	        scene.rewind();
	
	        int points = 0;
	        
	        	
		        while(depth.remaining() > 0)
		        {
		        	short depthValue = depth.get();
		        	short sceneValue = scene.get();
		        	
		        	int x = points % width;
		        	int y = points / width;
		        	if(x < target.getWidth() && y < target.getHeight())
		        	{
		        		double level = (double)depthValue/8192;
		        		level = (level - frontCut) / (backCut - frontCut);
		        		int lvl = (int)(level * 255);
		        		int color = lvl << 16 | lvl << 8 | lvl;
		        		target.setRGB(x, y, sceneValue > 0 ? color : 0);
		        	}
		        	
		        	
//		        	double imageR = (double)image.get() / 255;
//		        	double imageG = (double)image.get() / 255;
//		        	double imageB = (double)image.get() / 255;
		        	//buffer[points] = filter(points, (double)(depth.get()) / (double)8192);
//		        	 buffer[points] = (double)(depthValue) / (double)8192;
//			         buffer[points] = 1 - sceneValue;
		        	 points++;
		        }
//		        quadFilter();
//		        timeFilter();
		        
				try {
			        for(int i=0 ; i<users.length ; i++)
			        {
			        	int user = users[i];
			        	
			        	target.getGraphics().setColor(Color.CYAN);
			        	
			        	org.OpenNI.Point3D com;
						com = iRGen.convertRealWorldToProjective(userGen.getUserCoM(user));
			        	
						org.OpenNI.Point3D phead = getBodyPoint(user, SkeletonJoint.HEAD);
						org.OpenNI.Point3D plh = getBodyPoint(user, SkeletonJoint.LEFT_HAND);
						org.OpenNI.Point3D prh = getBodyPoint(user, SkeletonJoint.RIGHT_HAND);
						
						drawPart(target.getGraphics(), com, phead);
						drawPart(target.getGraphics(), com, plh);
						drawPart(target.getGraphics(), com, prh);
						
						drawPoint(target, com, 0xFF0000);
						drawPoint(target, phead, 0xFF0000);
						drawPoint(target, plh, 0xFFFF00);
						drawPoint(target, prh, 0xFF7F00);
							
			        }
				} catch (StatusException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        
	        filtercount = (filtercount + 1) % lowPassFilter;
		}
		
	}
	private void drawPart(Graphics g, org.OpenNI.Point3D a, org.OpenNI.Point3D b)
	{
		if(a != null && b != null)
		{
			g.drawLine((int)a.getX(), (int)a.getY(), (int)b.getX(), (int)b.getY());
		}
	}
	private org.OpenNI.Point3D getBodyPoint(int user, SkeletonJoint joint)
	{
		try {
			SkeletonJointPosition a = userGen.getSkeletonCapability().getSkeletonJointPosition(user, joint);	
			if(a != null)
				return iRGen.convertRealWorldToProjective(a.getPosition());
		} catch (StatusException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	private void drawPoint(BufferedImage i, org.OpenNI.Point3D a, int color)
	{
    	int x = (int)a.getX();
    	int y = (int)a.getY();
    	if(x>=0 && y >= 0 && x < i.getWidth() && y < i.getHeight())
    	{
    		i.setRGB(x, y, color);
    	}
	}


}
