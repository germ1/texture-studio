# Texture Studio

## User Section
### Installation

* install requirements : java 1.6+
* download latest version [here](https://bitbucket.org/germ1/texture-studio/downloads)
* unzip it and launch run.sh

## Developper Section

### Build it yourself

* install requirements : java 1.6+, JDK 1.6+, maven 2.
``` bash
$ sudo apt-get install maven2
```
* clone the entire project to your machine (git clone)
* build application
``` bash
$ mvn clean install -Dmaven.test.skip
```
* Package is built here : **distrib/target/editor-dist.zip**
  
### Extending it

Knowledge of Maven or Gradle (and corresponding eclipse plugins) are necessary to follow these steps.

* Build all before (maven install command make artefact locally available)
* create your java project and setup necessary dependencies (with maven or gradle or manually). You will at least need **net.mgsx.numart:framework:0.0.1-SNAPSHOT**
* create your atom classes in net.mgsx.numart package
* build your project
* add your jar module to the classpath by editing **run.sh** script
* run the program
