package net.mgsx.numart.raytracing.renderer;

import net.mgsx.numart.framework.annotations.Caution;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Ray3D;
import net.mgsx.numart.procedural.renderer.AbstractRenderer;
import net.mgsx.numart.procedural.renderer.Rasterizer;
import net.mgsx.numart.procedural.renderer.SoftwareRenderer;
import net.mgsx.numart.raytracing.core.Geometry;
import net.mgsx.numart.raytracing.core.Light;
import net.mgsx.numart.raytracing.core.Material;
import net.mgsx.numart.raytracing.core.Scene;
import net.mgsx.numart.raytracing.impact.GeometryImpact;
import net.mgsx.numart.raytracing.impact.MaterialImpact;

public class SceneRenderer extends AbstractRenderer implements SoftwareRenderer {

	@ParamGroup
	public Scene scene;
	
	@Caution
	@ParamInt(min=0, max=5)
	public int maxBounce = 3;
	
	@DefaultFactory
	public void defaults()
	{
		scene = ReflectUtils.createDefault(Scene.class);
	}
	public SceneRenderer() {
		
	}
	public SceneRenderer(Scene scene) {
		this.scene = scene;
	}
	
	public void render(Rasterizer rasterizer, int width, int height, int x1, int y1, int x2, int y2) {
		// l'angle vertical de la camera est de 90�
		final double fov = Math.PI / 4;
		// on en d�duit la distance de la grille virtuelle.
		final double distance = ((double)height / 2) / Math.tan(fov / 2);
		for(int y=y1 ; y<y2 ; y++){
			double ya = ((double)y - (double)height/2) / distance;
			double yb = ((double)(y+1) - (double)height/2) / distance;
			for(int x=x1 ; x<x2 ; x++){
				double xa = ((double)x - (double)width/2) / distance;
				double xb = ((double)(x+1) - (double)width/2) / distance;
				rasterizer.setPixel(x, y, Color.clamp(renderPixel(xa, -ya, xb, -yb, antialias)));
			}
		}
		
	}
	
	private Color renderPixel(double x, double y){
		// direction du rayon.
		Point3D direction = scene.observer.getDirection(new Point3D(x, y, 1));
		direction.normalize();
		// rayon infini depuis l'observer dans la direction donn�e.
		Ray3D ray = new Ray3D(new Point3D(scene.observer.position), direction);
		
		// on lance le tracing recursif.
		return rayTrace(ray, maxBounce);
	}
	private Color rayTrace(Ray3D ray, int bounce)
	{
		// recherche d'un impact avec un objet de la scene.
		GeometryImpact geometryImpact = getRayImpact(ray);
		if(geometryImpact == null){
			// pas d'impact, on retourne la couleur d'arrière plan.
			return new Color(0); // TODO param
		}
		Geometry geometry = geometryImpact.geometry;
		
		// construction de l'impact sur le matériel.
		MaterialImpact materialImpact = new MaterialImpact();
		materialImpact.normal = geometryImpact.normal;
		materialImpact.rayDirection = new Point3D(ray.direction);
		materialImpact.texCoord = geometryImpact.texCoord;
		
		// compute the normal from material (bump mapping)
		Point3D normal = geometry.material.getNormal(materialImpact);
		materialImpact.normal = normal;
		
		// on calcul le point d'impact (avec bias de 1% pour �viter les probl�mes d'arrondis)
		double bias = 0.99;
		Point3D position = Point3D.add(ray.position, Point3D.multiply(ray.direction, geometryImpact.time * bias));
		
		
		// on met à jour le rayon refleté.
		ray.position = position;
		ray.direction.reflect(normal);
		
		Color color = geometry.material.getEmmissive(materialImpact); // new Color(0,0,0,1);
		
		// on recherche les lumi�res visible depuis l'impact.
		for(Light light : scene.getLights()){
			// on obtient la direction de la lumi�re.
			Ray3D lightRay = light.getRay(ray);
			if(getRayImpact(lightRay) == null){
				// pas d'impact donc la lumi�re est visible � cette endroit.
				double dot = ray.direction.dot(lightRay.direction);
				// la lumi�re est en face.
				color = Color.add(color, getLightColor(light, geometry.material, materialImpact, Math.max(0, dot), lightRay.getLength()));
			}else{
				color = Color.add(color, Color.multiply(geometry.material.getAmbientColor(materialImpact), light.ambientColor));
			}
		}
		
		final double reflexion = geometry.material.getReflexion(materialImpact).a; // TODO
		
		// on continue le raytracing s'il reste des rebonds.
		if(bounce > 0 && reflexion > 0){
			color = Color.mix(color, rayTrace(ray, bounce - 1), reflexion); 
		}
		return color;
	}
	
	private static Color getLightColor(Light light, Material material, MaterialImpact impact, double dot, double distance){
		return Color.multiply(Color.add(
				Color.multiply(material.getDiffuseColor(impact), Color.multiply(light.diffuseColor, 1)),
				Color.multiply(material.getSpecularColor(impact), Color.multiply(light.specularColor, Math.pow(dot, material.getShininess(impact))))
				), light.getAttenuation(distance));
	}
	
	private GeometryImpact getRayImpact(Ray3D ray)
	{
		GeometryImpact geometryImpact = new GeometryImpact();
		if(! ray.isInfinite()){
			geometryImpact.time = ray.getLength();
		}
		for(Geometry g : scene.getGeometries()){
			GeometryImpact i = g.getImpact(ray);
			if(i != null && (geometryImpact.geometry == null || i.time < geometryImpact.time))
			{
				geometryImpact = i;
			}
		}
		if(geometryImpact.geometry == null)
			geometryImpact = null;
		return geometryImpact;
	}
	private Color renderPixel(double xa, double ya, double xb, double yb, int n){
		Color c = null;
		double xm = (xa + xb)/2;
		double ym = (ya + yb)/2;
		if(n > 1){
			c = Color.mix(
				Color.mix(renderPixel(xa, ya, xm, ym, n-1), renderPixel(xm, ya, xb, ym, n-1)),
				Color.mix(renderPixel(xa, ym, xm, yb, n-1), renderPixel(xm, ym, xb, yb, n-1)));
		}else{
			c = renderPixel(xm, ym);
		}
		return c;
	}

}
