package net.mgsx.numart.raytracing.light;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Ray3D;
import net.mgsx.numart.raytracing.core.Light;

public class PointLight extends Light {
	
	// la position de la lumière.
	@ParamPoint(min=-100, max=100)
	public Point3D position = new Point3D();
	
	@ParamDouble
	public double kC = 1;
	
	@ParamDouble
	public double kL = 0;
	
	@ParamDouble
	public double kQ = 0;
	
	@Override
	public double getAttenuation(double distance) {
		return 1 / (kC + (kL + kQ * distance) * distance);
	}

	@Override
	public Ray3D getRay(Ray3D ray) {
		// TODO commenter.
		Ray3D r = new Ray3D();
		r.position = new Point3D(ray.position);
		r.direction = Point3D.subtract(position, ray.position);
		double length = r.direction.length();
		r.setLength(length);
		r.direction.multiply(1 / length);
		return r;
	}
}
