package net.mgsx.numart.raytracing.geometry;

import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Ray3D;
import net.mgsx.numart.raytracing.core.Geometry;
import net.mgsx.numart.raytracing.impact.GeometryImpact;

public class Plan extends Geometry {
	
	@ParamPoint(min=-100, max=100)
	public Point3D position = new Point3D(0,0,0);
	
	@ParamVector
	public Point3D normal = new Point3D(0,1,0);
	
	
	
	public Plan() {
		super();
	}
	public Plan(Point3D position, Point3D normal) {
		super();
		this.position = position;
		this.normal = normal;
	}

	@Override
	public GeometryImpact getImpact(Ray3D ray) {
		double s1 = ray.direction.dot(normal);
		double s2 = Point3D.subtract(ray.position, position).dot(normal);
		if(s1 < 0 && s2 > 0){
			GeometryImpact i = new GeometryImpact();
			i.geometry = this;
			i.normal = new Point3D(normal);
			i.time = -s2 / s1;
			i.texCoord = Point3D.subtract(Point3D.multiply(ray.direction, i.time), position);
			return i;
		}
		return null;
	}
}
