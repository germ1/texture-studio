package net.mgsx.numart.raytracing.material;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.SolidColor;
import net.mgsx.numart.raytracing.core.Material;
import net.mgsx.numart.raytracing.impact.MaterialImpact;

public class BasicMaterial extends Material {
	// une simple texture pour les diffuse, ambient et specular.
	
	@ParamDouble
	public double reflexion = 0.2;
	
	@ParamGroup
	public ColorFromSpace texture = new SolidColor();

	@ParamGroup
	public ScalarFromSpace bumpMap = null;
	
	@ParamDouble(min=-1.0, max=1.0)
	public double bumpFactor = 0.0;
	
	@Override
	public Color getColor(MaterialImpact impact) {
		return texture.getColor(impact.texCoord);
	}
	@Override
	public Color getReflexion(MaterialImpact impact) {
		return new Color(0,0,0,reflexion);
	}
	
	@Override
	public Point3D getNormal(MaterialImpact impact) 
	{
		if(bumpMap != null)
		{
			// precision for the sampling
			final double e = 0.001;
			
			// find the u and vector from normal
			Point3D u = Point3D.cross(impact.normal, Point3D.cross(new Point3D(e, 0, 0), impact.normal));
			Point3D v = Point3D.cross(impact.normal, Point3D.cross(new Point3D(0, e, 0), impact.normal));
			
			// resample to have bump on u and v
			double du = 
				bumpMap.getValue(new Point3D(impact.texCoord.x + u.x, impact.texCoord.y + u.y, impact.texCoord.z + u.z)) -
				bumpMap.getValue(new Point3D(impact.texCoord.x - u.x, impact.texCoord.y - u.y, impact.texCoord.z - u.z));
			double dv = 
				bumpMap.getValue(new Point3D(impact.texCoord.x + v.x, impact.texCoord.y + v.y, impact.texCoord.z + v.z)) -
				bumpMap.getValue(new Point3D(impact.texCoord.x - v.x, impact.texCoord.y - v.y, impact.texCoord.z - v.z));
			
			// make the T and B vector from bump (with scale factor)
			Point3D T = new Point3D(e, 0, du * bumpFactor);
			T.normalize();
			Point3D B = new Point3D(0, e, dv * bumpFactor);
			B.normalize();
			
			// make the N vector from T and B
			Point3D N = Point3D.cross(T, B);
			
			// return the final perturbed normal.
			return new Point3D(T.dot(impact.normal), B.dot(impact.normal), N.dot(impact.normal));
		}
		return super.getNormal(impact);
	}
}
