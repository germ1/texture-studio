package net.mgsx.numart.raytracing.impact;

import net.mgsx.numart.math.Point3D;

public class MaterialImpact {
	public Point3D rayDirection;
	public Point3D normal;
	public Point3D texCoord;
}
