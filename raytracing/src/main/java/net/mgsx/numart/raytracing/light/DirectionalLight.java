package net.mgsx.numart.raytracing.light;

import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Ray3D;
import net.mgsx.numart.raytracing.core.Light;

public class DirectionalLight extends Light 
{
	
	@ParamVector
	public Point3D direction = new Point3D(0,0,1);
	
	@Override
	public Ray3D getRay(Ray3D ray) {
		return new Ray3D(new Point3D(ray.position), Point3D.multiply(direction, -1));
	}

}
