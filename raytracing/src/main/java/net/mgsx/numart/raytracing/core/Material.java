package net.mgsx.numart.raytracing.core;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.raytracing.impact.MaterialImpact;

// TODO faire une interface plutot qu'une classe
public abstract class Material extends NamedElement
{
	abstract public Color getColor(MaterialImpact impact);
	
	@Logarithmic(10)
	@ParamDouble(min=1, max=100)
	public double shininess = 10.0;
	
	@ParamDouble(min=0, max=1)
	public double emmission = 0;
	
	/**
	 * retourne la reflexion du materiel.
	 * null ou noir : pas de reflexion
	 * blanc : reflexion totale.
	 * gris : reflexion non filtr�e.
	 * couleur : reflexion filtr� par la couleur.
	 */
	public Color getReflexion(MaterialImpact impact){
		// pas de reflexion.
		return new Color(0,0,0,0);
	}
	public Color getDiffuseColor(MaterialImpact impact){
		return getColor(impact);
	}
	public Color getSpecularColor(MaterialImpact impact){
		// TODO texture for specular ???
		return new Color(1,1,1,1); // getColor(impact);
	}
	public Color getAmbientColor(MaterialImpact impact){
		return getColor(impact);
	}
	public double getShininess(MaterialImpact impact){
		return shininess;
	}
	public Point3D getNormal(MaterialImpact impact)
	{
		// no normal perturbations.
		return impact.normal;
	}
	public Color getEmmissive(MaterialImpact impact)
	{
		return Color.multiply(getColor(impact), emmission);
	}
}
