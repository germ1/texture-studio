package net.mgsx.numart.raytracing.core;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Ray3D;
import net.mgsx.numart.raytracing.impact.GeometryImpact;
import net.mgsx.numart.raytracing.material.BasicMaterial;

public abstract class Geometry extends NamedElement
{
	@ParamGroup
	public Material material = new BasicMaterial();
	
	abstract public GeometryImpact getImpact(Ray3D ray);
	
}
