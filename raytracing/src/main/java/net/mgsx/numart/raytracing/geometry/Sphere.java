package net.mgsx.numart.raytracing.geometry;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Ray3D;
import net.mgsx.numart.raytracing.core.Geometry;
import net.mgsx.numart.raytracing.impact.GeometryImpact;

public class Sphere extends Geometry {
	
	@ParamPoint(min=-100, max=100)
	public Point3D center;
	
	@ParamDouble(max=10)
	public double radius;
	
	public Sphere() {
		super();
	}
	public Sphere(Point3D center, double radius) {
		super();
		this.center = center;
		this.radius = radius;
	}
	public Point3D getCenter() {
		return center;
	}
	@Override
	public GeometryImpact getImpact(Ray3D ray) {
		Point3D u = ray.direction;
		Point3D A = ray.position;
		Point3D C = center;
		Point3D L = Point3D.subtract(C, A);
		double d = L.dot(u);
		double l2 = L.dot(L);
		double r2 = radius*radius;
		if(d < 0 && l2 > r2)
			return null;
		double d2 = d * d;
		double m2 = l2 - d2;
		if(m2 > r2)
			return null;
		double q = Math.sqrt(r2 - m2);
		double t = 0;
		if(l2 > r2){
			t = d-q;
		}else{
			t = d+q;
		}
		Point3D I = Point3D.add(A, Point3D.multiply(u, t));
		GeometryImpact impact = new GeometryImpact();
		impact.geometry = this;
		impact.time = t;
		impact.texCoord = Point3D.subtract(I, C);
		impact.normal = Point3D.multiply(Point3D.subtract(I, C), 1 / radius);
		return impact;
		/*
		Point3D d = ray.getDirection();
		Point3D co = Point3D.subtract(ray.getPosition(), center);
		double s1 = d.dot(d);
		double s2 = d.dot(co);
		double s3 = co.dot(co) - radius*radius;
		double delta = s2*s2 - s1*s3;
		if(delta >= 0){
			double t1 = (s2 - Math.sqrt(delta)) / s1;
			double t2 = (s2 + Math.sqrt(delta)) / s1;
			if(t1 >= 0 || t2 >= 0){
				double t = (t1 < 0 || t2 < 0) ? Math.max(t1, t2) : Math.min(t1, t2);
				Impact3D impact = new Impact3D();
				impact.normal = Point3D.multiply(d, t);
				impact.normal.add(co);
				impact.normal.normalize();
				impact.time = t;
				impact.texCoord = new Point3D(ray.getPosition());
				impact.texCoord.add(Point3D.multiply(d, t));
				return impact;
			}
		}
		return null;*/
	}
	
	
}
