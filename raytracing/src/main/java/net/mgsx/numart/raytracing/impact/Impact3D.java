package net.mgsx.numart.raytracing.impact;

import net.mgsx.numart.math.Point3D;

public class Impact3D {
	public Point3D normal;
	public Point3D texCoord;
	public double time;
}
