package net.mgsx.numart.raytracing.core;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamList;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Polar;
import net.mgsx.numart.procedural.texture.impl.Damier;
import net.mgsx.numart.procedural.texture.impl.SolidColor;
import net.mgsx.numart.procedural.texture.impl.TextureMixer;
import net.mgsx.numart.raytracing.geometry.Plan;
import net.mgsx.numart.raytracing.geometry.Sphere;
import net.mgsx.numart.raytracing.light.DirectionalLight;
import net.mgsx.numart.raytracing.material.BasicMaterial;

public class Scene extends NamedElement
{
	
	@ParamGroup
	public Observer observer;

	@ParamList
	public List<Light> lights = new ArrayList<Light>();

	@ParamList
	public List<Geometry> geometries = new ArrayList<Geometry>();
	
	@DefaultFactory
	public void defaults()
	{
		observer = ReflectUtils.createDefault(Observer.class);
		 
		Sphere sphere = new Sphere(new Point3D(0,0,0), 2);
		sphere.material = ReflectUtils.createDefault(BasicMaterial.class);
		addGeometry(sphere);
		
		DirectionalLight light = new DirectionalLight();
		light.ambientColor = new Color(0.2);
		light.diffuseColor = new Color(0.6);
		light.specularColor = new Color(1.0);
		light.direction = new Polar(Math.PI / 4, -Math.PI / 2.5).toUnitVector();
		addLight(light);
		
		// add plan for reflections
		TextureMixer planTex = new TextureMixer();
		planTex.a = new SolidColor(new Color(0,0,0));
		planTex.b = new SolidColor(new Color(1,1,1));
		planTex.mix = new Damier();

		BasicMaterial planMat = new BasicMaterial();
		planMat.reflexion = 0;
		planMat.texture = planTex;
		planMat.emmission = 1;
		
		Plan plan = new Plan();
		plan.position = new Point3D(0, -2, 0);
		plan.normal = new Point3D(0, 1, 0);
		plan.material = planMat;
		
		addGeometry(plan);
		
	}
	public final List<Geometry> getGeometries() {
		return geometries;
	}
	public final List<Light> getLights() {
		return lights;
	}
	public void addGeometry(Geometry geometry) {
		geometries.add(geometry);
	}
	public void addLight(Light light) {
		lights.add(light);
	}


}
