package net.mgsx.numart.raytracing.light;

import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Ray3D;
import net.mgsx.numart.raytracing.core.Light;

public class GlobalLight extends Light {

	public GlobalLight() {
		super();
		this.ambientColor = new Color(0);
		this.diffuseColor = new Color(0,0,0.2);
		this.specularColor = new Color(0);
	}
	
	@Override
	public Ray3D getRay(Ray3D ray) {
		return new Ray3D(ray);
	}
	
}
