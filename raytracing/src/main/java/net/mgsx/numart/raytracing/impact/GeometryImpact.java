package net.mgsx.numart.raytracing.impact;

import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.raytracing.core.Geometry;

/**
 * class holding the result of an impact with a geometry 
 */
public class GeometryImpact{
	public Geometry geometry;
	public double time;
	public Point3D normal;
	public Point3D texCoord;
}