package net.mgsx.numart.raytracing.material;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.raytracing.core.Material;
import net.mgsx.numart.raytracing.impact.MaterialImpact;

public class DiffuseMaterial extends Material {
	
	@ParamGroup
	public Material front;
	
	@ParamGroup
	public Material back;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double factor = 1;
	
	@Override
	public Color getColor(MaterialImpact impact) {
		return Color.mix(back.getColor(impact), front.getColor(impact), getT(impact));
	}

	@Override
	public Point3D getNormal(MaterialImpact impact) {
		// return front.getNormal(impact);
		return Point3D.mixUnitVector(back.getNormal(impact), front.getNormal(impact), getT(impact));
	}
	
	private double getT(MaterialImpact impact)
	{
		double dot = Math.abs(impact.normal.dot(impact.rayDirection));
		return Math.pow(dot, factor);
	}
}
