package net.mgsx.numart.raytracing.core;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Ray3D;

public abstract class Light extends NamedElement 
{
	
	@ParamColor
	public Color ambientColor = new Color(0);
	
	@ParamColor
	public Color diffuseColor = new Color(0.7);
	
	@ParamColor
	public Color specularColor = new Color(1);
	
	public void setColor(Color color){
		ambientColor = new Color(color);
		diffuseColor = new Color(color);
		specularColor = new Color(color);
	}
	
	/**
	 * retourne un rayon avec pour origine un point donn� et
	 * pour arriv�e la lumi�re.
	 * @param origin
	 * @return
	 */
	public abstract Ray3D getRay(Ray3D ray);
	
	public double getAttenuation(double distance){
		return 1;
	}
}
