package net.mgsx.numart.raytracing.core;


import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.SpaceFromSpace;

public class Observer extends NamedElement
{
	@ParamPoint(min=-100, max=100)
	public Point3D position;
	
	@ParamVector
	public Point3D direction;
	
	@ParamVector
	public Point3D up;
	
	// TODO revoir ce paramètre
	@ParamGroup
	public SpaceFromSpace transformer = null;
	
	@DefaultFactory
	public void defaults()
	{
		direction = new Point3D(0, 0, 1);
		up = new Point3D(0, 1, 0);
		position = new Point3D(0, 0, -6);
		transformer = null;
	}
	public Observer() {
		
	}
	public Observer(Point3D position, Point3D direction, Point3D up) {
		super();
		this.position = position;
		this.direction = direction;
		this.up = up;
	}
	public Point3D getDirection(Point3D point)
	{
		return transformer == null ? point : transformer.getPoint(point);
	}
}
