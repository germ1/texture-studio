package net.mgsx.numart.math;

public class Box2D 
{
	public double x,y,w,h;

	public Box2D()
	{
		
	}
	public Box2D(double x, double y, double w, double h) {
		super();
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	public Box2D(Box2D box) {
		super();
		this.x = box.x;
		this.y = box.y;
		this.w = box.w;
		this.h = box.h;
	}
	public double distance(Box2D box)
	{
		return center().distance(box.center());
	}
	public double surface()
	{
		return w * h;
	}
	public Point2D center()
	{
		return new Point2D(x + w/2, y + h/2);
	}
	public void positive()
	{
		if(w < 0)
		{
			x += w;
			w = -w;
		}
		if(h < 0)
		{
			y += h;
			h = -h;
		}
	}
	public boolean contains(Box2D box)
	{
		return box.x >= x && box.y >= y && 
				(box.x + box.w) <= (x + w) && 
				(box.y + box.h) <= (y + h);
	}
	
	public void vSplit(double xRate, Box2D leftBox, Box2D rightBox)
	{
		leftBox.x = x;
		leftBox.y = y;
		leftBox.w = w * xRate;
		leftBox.h = h;
		
		rightBox.x = x + w * xRate;
		rightBox.y = y;
		rightBox.w = w * (1 - xRate);
		rightBox.h = h;
	}
	
	public void reshape(double border)
	{
		double b = Math.min(Math.min(w, h), border * 2);
		x += b / 2;
		y += b / 2;
		w -= b;
		h -= b;
	}
	
}
