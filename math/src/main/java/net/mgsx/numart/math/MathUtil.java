package net.mgsx.numart.math;

public abstract class MathUtil {
	/**
	 * retourne le plus petit entier inferieur.
	 * @param value
	 * @return
	 */
	public static int integerValue(double value){
		return (int)Math.floor(value);
	}
	
	public static double moduloValue(double value, double modulo)
	{
		return (value < 0 ? value + modulo : value) - ((int)(value / modulo) * modulo);
	}
	
	/**
	 * retourne la partie fractionnaire.
	 * @param value
	 * @return
	 */
	public static double remainValue(double value){
		return value - (double)integerValue(value);
	}
	
	/**
	 * retourne la spline cubique.
	 * @param t le param�tre de 0 � 1.
	 * @param points les 4 points d'interpolation.
	 * @return
	 * @see Texturing & Modeling - A Procedural Approach - 3rd Ed. - page 34-35.
	 */
	public static double cubicSpline(double t, double [] points){
		double c3 = -0.5 * points[0] + 1.5 * points[1] - 1.5 * points[2] + 0.5 * points[3];
		double c2 = points[0] - 2.5 * points[1] + 2 * points[2] - 0.5 * points[3];
		double c1 = - 0.5 * points[0] + 0.5 * points[2];
		double c0 = points[1];
		return (((c3 * t + c2) * t + c1) * t + c0);
	}
	
	public static double limit(double value){
		return Math.max(0, Math.min(1, value));
	}
	
	/**
	 * 
	 * @param t
	 * @param rate 
	 * @return
	 */
	public static double envelop(double t, double rate)
	{
		if(rate > 0)
		{
			return Math.pow(t, 1 + rate);
		}
		else if(rate < 0)
		{
			return 1 - Math.pow(t, 1 - rate);
		}
		else
		{
			return t;
		}
	}
	
	public static double envelopAR(double t, double a, double r, double ka, double kr)
	{
		if(t < a)
		{
			return envelop(t / a, ka);
		}
		else if(t > r)
		{
			return 1 - envelop((t - r) / (1 - r), kr);
		}
		else
		{
			return 1;
		}
		
	}
	
	public static double random(double min, double max)
	{
		return Math.random() * (max - min) + min;
	}
	public static double random(double min, double max, double base, double rate)
	{
		return (1 - rate) * base + rate * random(min, max);
	}
	
}
