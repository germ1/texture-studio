package net.mgsx.numart.math;

public class Filter2D 
{
	private double [] buffer = {0,0,0,0};
	private int size;
	
	public double put(double realValue)
	{
		double average;
		
		if(size < buffer.length)
		{
			size++;
			average = realValue;
		}
		else
		{
			double theoValue = MathUtil.cubicSpline(1.5, buffer);
			
			for(int i=0 ; i<3 ; i++)
			{
				buffer[i] = buffer[i+1];
			}
			
			average = realValue * 0.5 + theoValue * 0.5;
			
			buffer[3] = average;
		}
		return average;
	}
}
