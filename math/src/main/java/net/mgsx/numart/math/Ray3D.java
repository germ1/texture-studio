package net.mgsx.numart.math;

public class Ray3D {
	public Point3D position;
	public Point3D direction;
	private double length;
	private boolean infinite;
	
	public Ray3D() {
	}
	public Ray3D(Ray3D ray) {
		position = new Point3D(ray.position);
		direction = new Point3D(ray.direction);
		length = ray.length;
		infinite = ray.infinite;
	}
	public Ray3D(Point3D position, Point3D direction) {
		super();
		this.position = position;
		this.direction = direction;
		this.infinite = true;
	}
	public Ray3D(Point3D position, Point3D direction, double length) {
		super();
		this.position = position;
		this.direction = direction;
		this.length = length;
		this.infinite = false;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
		this.infinite = false;
	}
	public void setInfiniteLength(){
		this.infinite = true;
	}
	public boolean isInfinite(){
		return this.infinite;
	}
}
