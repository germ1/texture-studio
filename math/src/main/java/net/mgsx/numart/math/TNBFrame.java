package net.mgsx.numart.math;

public class TNBFrame 
{
	/** Tangent (unit vector) */
	public Point3D T;
	
	/** Normal (unit vector) */
	public Point3D N;
	
	/** Binormal (unit vector) */
	public Point3D B;
	
}
