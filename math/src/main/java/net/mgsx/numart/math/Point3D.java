package net.mgsx.numart.math;


public class Point3D 
{
	public double x;
	
	public double y;
	
	public double z;

	public Point3D() {
		
	}
	public float [] toFloat(){
		return new float []{(float)x,(float)y,(float)z};
	}
	
	public Point3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public Point3D(double value) {
		this.x = 
		this.y = 
		this.z = value;
	}
	public Point3D(Point3D p) {
		this.x = p.x;
		this.y = p.y;
		this.z = p.z;
	}
	public void set(Point3D p) {
		this.x = p.x;
		this.y = p.y;
		this.z = p.z;
	}

	public void reflect(Point3D n){
		n.normalize();
		add(multiply(n, dot(n) * -2.0));
		normalize();
	}
	public void add(Point3D p){
		x += p.x;
		y += p.y;
		z += p.z;
	}
	public void reset(){
		x = 0;
		y = 0;
		z = 0;
	}
	public void sub(Point3D p){
		x -= p.x;
		y -= p.y;
		z -= p.z;
	}
	public void multiply(double s){
		x *= s;
		y *= s;
		z *= s;
	}
	public static Point3D multiply(Point3D p, double s){
		return new Point3D(p.x*s, p.y*s, p.z*s);
	}
	public static Point3D multiply(Point3D p, Point3D p2){
		return new Point3D(p.x*p2.x, p.y*p2.y, p.z*p2.z);
	}
	public double dot(Point3D p){
		return x*p.x + y*p.y + z*p.z;
	}
	public static Point3D abs(Point3D p){
		return new Point3D(
				Math.abs(p.x),
				Math.abs(p.y),
				Math.abs(p.z));
	}
	public void normalize(){
		multiply(1 / length());
	}
	public double length(){
		return Math.sqrt(dot(this));
	}
	public static Point3D subtract(Point3D a, Point3D b){
		return new Point3D(a.x-b.x,a.y-b.y,a.z-b.z);
	}
	public static Point3D mix(Point3D a, Point3D b, double t){
		return new Point3D(
				(1-t) * a.x + t * b.x,
				(1-t) * a.y + t * b.y,
				(1-t) * a.z + t * b.z);
	}
	public static Point3D add(Point3D a, Point3D b){
		return new Point3D(a.x+b.x,a.y+b.y,a.z+b.z);
	}
	public Point3D floor()
	{
		return new Point3D(Math.floor(x), Math.floor(y), Math.floor(z));
	}
	public static Point3D cross(Point3D a, Point3D b){
		return new Point3D(
				a.y * b.z - a.z * b.y,
				a.z * b.x - a.x * b.z,
				a.x * b.y - a.y * b.x);
	}
	public static Point3D mixUnitVector(Point3D a, Point3D b, double t)
	{
		return Polar.mix(new Polar(a), new Polar(b), t).toUnitVector();
	}
}
