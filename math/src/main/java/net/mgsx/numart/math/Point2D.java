package net.mgsx.numart.math;

public class Point2D {
	public double x, y;

	public Point2D() {
	}
	public Point2D(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	public double distance(Point2D point)
	{
		double dx = x - point.x;
		double dy = y - point.y;
		return Math.sqrt(dx*dx+dy*dy);
	}
	
}
