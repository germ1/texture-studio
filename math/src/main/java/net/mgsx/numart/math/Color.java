package net.mgsx.numart.math;

public class Color {
	
	public double r;
	
	public double g;
	
	public double b;
	
	public double a;

	public Color() {
		
	}
	public float [] toFloat(){
		return new float []{(float)r,(float)g,(float)b,(float)a};
	}
	public Color(double level) {
		this.r = level;
		this.g = level;
		this.b = level;
		this.a = 1.0;
	}
	public Color(Color color) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.a = color.a;
	}
	public Color(double r, double g, double b) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = 1.0;
	}
	public Color(double r, double g, double b, double a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public static Color mix(Color a, Color b){
		return mix(a, b, 0.5);
	}
	
	/**
	 * for high performances, use {@link #mix(Color, Color, Color, double)} instead.
	 * @param a
	 * @param b
	 * @param t
	 * @return
	 */
	public static Color mix(Color a, Color b, double t){
		double it = 1 - t;
		return new Color(a.r * it + b.r * t, a.g * it + b.g * t, a.b * it + b.b * t, a.a * it + b.a * t);
	}
	
	public static Color mix(Color result, Color a, Color b, double t){
		double it = 1 - t;
		result.r = a.r * it + b.r * t;
		result.g = a.g * it + b.g * t;
		result.b = a.b * it + b.b * t;
		result.a = a.a * it + b.a * t;
		return result;
	}
	

	public Color multiply(double s){
		return new Color(r*s,g*s,b*s,a*s);
	}
	public Color multiply(Color c){
		return new Color(r*c.r,g*c.g,b*c.b,a*c.a);
	}
	public Color add(Color c){
		return new Color(r+c.r,g+c.g,b+c.b,a+c.a);
	}
	
	public int getRGB(){
		limit();
		return (((int)(r * 255)) << 16) | (((int)(g * 255)) << 8) | (((int)(b * 255)) << 0);
	}
	
	public static Color fromARGB(int code)
	{
		return new Color(
				from8Bits(code >> 16),
				from8Bits(code >> 8),
				from8Bits(code >> 0),
				from8Bits(code >> 24));
	}
	private static double from8Bits(int code)
	{
		return (double)(code & 0xFF) / 255.0;
	}
	
	public static Color multiply(Color a, Color b){
		return new Color(a.r*b.r, a.g*b.g, a.b*b.b, a.a*b.a);
	}
	public static Color multiply(Color a, double s){
		return new Color(a.r*s, a.g*s, a.b*s, a.a*s);
	}
	public static Color add(Color a, Color b){
		return new Color(a.r+b.r, a.g+b.g, a.b+b.b, a.a+b.a);
	}

	public void limit(){
		r = limit(r);
		g = limit(g);
		b = limit(b);
		a = limit(a);
	}
	public void set(Color c)
	{
		r = c.r;
		g = c.g;
		b = c.b;
		a = c.a;
	}
	
	public static Color clamp(Color color)
	{
		color.r = Math.min(1, Math.max(0, color.r));
		color.g = Math.min(1, Math.max(0, color.g));
		color.b = Math.min(1, Math.max(0, color.b));
		color.a = Math.min(1, Math.max(0, color.a));
		return color;
	}
	
	private static double limit(double v){
		return v < 0 ? 0 : (v > 1 ? 1 : v);
	}
}
