package net.mgsx.numart.math;

public class BBox 
{
	public Point3D position;
	public Point3D scale;
	public BBox(double x, double y, double z, double sx, double sy, double sz) {
		position = new Point3D(x, y, z);
		scale = new Point3D(sx, sy, sz);
	}
	public BBox(Point3D position, Point3D scale) {
		this.position = position;
		this.scale = scale;
	}
}
