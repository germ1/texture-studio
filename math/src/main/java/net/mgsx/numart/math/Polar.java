package net.mgsx.numart.math;

public class Polar {

	public double theta;
	public double phi;
	
	public Polar() {
	}

	public Polar(double theta, double phi) {
		super();
		this.theta = theta;
		this.phi = phi;
	}
	public Polar(Point3D unitVector) 
	{
		// TODO revoir la conversion (atan2(0,0))
		theta = Math.acos(unitVector.z);
		phi = Math.atan2(unitVector.y, unitVector.x);
	}
	public Point3D toUnitVector()
	{
		return new Point3D(
				Math.sin(theta) * Math.cos(phi),
				Math.sin(theta) * Math.sin(phi),
				Math.cos(theta));
	}
	public static Polar mix(Polar a, Polar b, double t)
	{
		double it = 1 - t;
		return new Polar(a.theta * it + b.theta * t, a.phi * it + b.phi * t);
	}
}
