package net.mgsx.numart.math;

import org.junit.Assert;
import org.junit.Test;

public class MathUtilTest {

	private static final double EPSILON = 1e-10;
	
	@Test
	public void testIntegerValue_nominal()
	{
		double value = 12.2435;
		Assert.assertEquals(12, MathUtil.integerValue(value), EPSILON);
	}
	
	@Test
	public void testRemainValue_nominal()
	{
		double value = 12.2435;
		Assert.assertEquals(.2435, MathUtil.remainValue(value), EPSILON);
	}
	
	@Test
	public void testIntegerValue_negative()
	{
		double value = -12.2435;
		Assert.assertEquals(-13, MathUtil.integerValue(value), EPSILON);
	}
	
	@Test
	public void testRemainValue_negative()
	{
		double value = -12.2435;
		Assert.assertEquals(1 - .2435, MathUtil.remainValue(value), EPSILON);
	}
}
