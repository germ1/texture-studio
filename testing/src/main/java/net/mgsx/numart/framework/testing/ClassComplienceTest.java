package net.mgsx.numart.framework.testing;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.Precompute;
import net.mgsx.numart.framework.annotations.RequirePrecompute;
import net.mgsx.test.LabelledParametrized;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

@RunWith(LabelledParametrized.class)
public class ClassComplienceTest extends AbstractComplienceTest
{
	private Class<?> cls;
	
	public ClassComplienceTest(String name, Class<?> cls) {
		super(name);
		this.cls = cls;
	}
	
	@Parameters
	public static Collection<Object[]> generateTests()
	{
		List<Object[]> suites = new ArrayList<Object[]>();
		for(Class<?> clazz : getAllClassesToTest(NamedElement.class))
		{
			Object[] parameters = {clazz.getSimpleName(), clazz};
			suites.add(parameters);
		}
		return suites;
	}
	
	@Test
	public void testDefaultConstructor() throws Exception
	{
		if(!Modifier.isAbstract(cls.getModifiers()))
		{
			Constructor<?> cs = cls.getConstructor();
			Assert.assertNotNull(cs);
			Object instance = cs.newInstance();
			Assert.assertNotNull(instance);
		}
	}
	
	@Test
	public void testPrecomputation() throws Exception
	{
		int numFieldsRequiresPrecomputation = 0;
		for(Field field : cls.getFields())
		{
			if(field.getAnnotation(RequirePrecompute.class) != null)
			{
				numFieldsRequiresPrecomputation++;
			}
		}
		int numMethodsPrecomputing = 0;
		for(Method method : cls.getMethods())
		{
			if(method.getAnnotation(Precompute.class) != null)
			{
				numMethodsPrecomputing++;
			}
		}
		if(numFieldsRequiresPrecomputation > 0)
		{
			Assert.assertTrue(numMethodsPrecomputing > 0);
		}
		Assert.assertTrue(numMethodsPrecomputing <= 1);
	}
}
