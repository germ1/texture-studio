package net.mgsx.numart.framework.testing;

import java.util.Set;

import net.mgsx.numart.framework.utils.ReflectUtils;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypesScanner;

public class AbstractComplienceTest {

	public AbstractComplienceTest(String name) {
		super();
	}
	
	protected static <T> Set<Class<? extends T>> getAllClassesToTest(Class<T> subTypeOf)
	{
		return getAllClassesToTest(ReflectUtils.SCAN_PACKAGE, subTypeOf);
	}
	protected static <T> Set<Class<? extends T>> getAllClassesToTest(String packageToScan, Class<T> subTypeOf)
	{
		TypesScanner scanner = new TypesScanner();
		
		SubTypesScanner stScanner = new SubTypesScanner(false);
		
		Reflections ref = new Reflections(packageToScan, scanner, stScanner);
		
		return ref.getSubTypesOf(subTypeOf);
	}
}
