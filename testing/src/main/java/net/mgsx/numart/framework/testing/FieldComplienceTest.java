package net.mgsx.numart.framework.testing;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.test.LabelledParametrized;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

@RunWith(LabelledParametrized.class)
public class FieldComplienceTest extends AbstractComplienceTest
{
	@SuppressWarnings("unused")
	private Class<?> cls;
	private Field field;
	
	public FieldComplienceTest(String name, Class<?> cls, Field field) {
		super(name);
		this.field = field;
		this.cls = cls;
	}
	
	@Parameters
	public static Collection<Object[]> generateTests()
	{
		List<Object[]> suites = new ArrayList<Object[]>();
		for(Class<?> clazz : getAllClassesToTest(NamedElement.class))
		{
			for(Field field : clazz.getFields())
			{
				Object[] parameters = {
						clazz.getSimpleName() + "." + field.getName(), 
						clazz,
						field};
				suites.add(parameters);
			}
		}
		return suites;
	}
	
	@Test
	public void testParam() throws Exception
	{
		if(field.getAnnotation(ParamDouble.class) != null)
		{
			Assert.assertEquals(double.class, field.getType());
		}
		// TODO run others tests
	}

}
