package net.mgsx.numart.framework.testing;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.Precompute;
import net.mgsx.test.LabelledParametrized;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

@RunWith(LabelledParametrized.class)
public class MethodComplienceTest extends AbstractComplienceTest
{
	private Class<?> cls;
	private Method method;
	
	public MethodComplienceTest(String name, Class<?> cls, Method method) {
		super(name);
		this.method = method;
		this.cls = cls;
	}
	
	@Parameters
	public static Collection<Object[]> generateTests()
	{
		List<Object[]> suites = new ArrayList<Object[]>();
		for(Class<?> clazz : getAllClassesToTest(NamedElement.class))
		{
			for(Method method : clazz.getMethods())
			{
				Object[] parameters = {
						clazz.getSimpleName() + "." + method.getName(), 
						clazz,
						method};
				suites.add(parameters);
			}
		}
		return suites;
	}
	
	@Test
	public void testPrecomputation() throws Exception
	{
		if(method.getAnnotation(Precompute.class) != null)
		{
			Assert.assertEquals(0, method.getTypeParameters().length);
			Assert.assertEquals(void.class, method.getReturnType());
			Object instance = cls.newInstance();
			method.invoke(instance);
		}
	}

}
