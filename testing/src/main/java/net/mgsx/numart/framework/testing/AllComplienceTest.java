package net.mgsx.numart.framework.testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	ClassComplienceTest.class,
	MethodComplienceTest.class,
	FieldComplienceTest.class
	})
abstract public class AllComplienceTest 
{
}
