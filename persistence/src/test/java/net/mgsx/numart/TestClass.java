package net.mgsx.numart;

import java.util.List;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamList;

@SuppressWarnings("unused")
public class TestClass
{
	@ParamDouble
	public double publicParameterDouble;
	public double publicDouble;
	@ParamDouble
	private double privateParameterDouble;
	private double privateDouble;
	@ParamDouble
	protected double protectedParameterDouble;
	protected double protectedDouble;
	@ParamDouble
	double packageParameterDouble;
	double packageDouble;
	
	
	@ParamGroup
	public TestClass dependency;
	
	@ParamList
	public List<TestClass> children;
	
}