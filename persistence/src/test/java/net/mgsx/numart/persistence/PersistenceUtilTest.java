package net.mgsx.numart.persistence;

import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;

import net.mgsx.numart.TestClass;
import net.mgsx.numart.framework.utils.ReflectUtils;

import org.junit.Assert;
import org.junit.Test;

public class PersistenceUtilTest {
	
	
	private static void assertSame(Object expected, Object actual) throws Exception
	{
		if(expected == null)
		{
			Assert.assertNull(actual);
		}
		else
		{
			Assert.assertNotNull(actual);
			
			Class<?> cls = expected.getClass();
			Assert.assertSame(cls, actual.getClass());
			
			if(expected instanceof Iterable<?>)
			{
				Iterator<?> ita = ((Iterable<?>)expected).iterator();
				Iterator<?> itb = ((Iterable<?>)actual).iterator();
				while(ita.hasNext())
				{
					Assert.assertTrue(itb.hasNext());
					Object aValue = ita.next();
					Object bValue = itb.next();
					assertSame(aValue, bValue);
				}
			}
			else
			{
				for(Field field : cls.getFields())
				{
					Object expectedValue = field.get(expected);
					Object actualValue = field.get(actual);
					if(!ReflectUtils.isPersistentField(field))
					{
						Assert.assertNull("field " + field + " should be null", actualValue);
					}
					else
					{
						if(field.getType().isPrimitive())
						{
							Assert.assertEquals(field.toString(), 
									expectedValue, actualValue);
						}
						else
						{
							assertSame(expectedValue, actualValue);
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void testGenX(Object root) throws Exception
	{
		PersistenceUtil persistenceUtil = new PersistenceUtil();
		
		StringWriter writer = new StringWriter();
		
		persistenceUtil.save(writer, root);
		
		String xml = writer.toString();
		
		StringReader reader = new StringReader(xml);
		
		Object o  = persistenceUtil.load(reader);
		
		assertSame(root, o);
	}
	
	@Test
	public void testDeps()
	{
		PersistenceUtil persistenceUtil = new PersistenceUtil();

		TestClass sampleParent = new TestClass();
		TestClass childA = new TestClass();
		TestClass childB = new TestClass();
		sampleParent.children = new ArrayList<TestClass>();
		sampleParent.children.add(childA);
		sampleParent.children.add(childB);
		childA.dependency = childB;
		
		StringWriter writer = new StringWriter();
		
		persistenceUtil.save(writer, sampleParent);
		
		String xml = writer.toString();
		
		StringReader reader = new StringReader(xml);
		
		TestClass o  = (TestClass)persistenceUtil.load(reader);
		Assert.assertNotNull(o);
		Assert.assertEquals(2, o.children.size());
		Assert.assertNotNull(o.children.get(0).dependency);
		Assert.assertEquals(o.children.get(1), o.children.get(0).dependency);
	}
	@Test
	public void testVisibility()
	{
		PersistenceUtil persistenceUtil = new PersistenceUtil();

		TestClass sample = new TestClass();
		sample.publicDouble = 1.0;
		sample.publicParameterDouble = 2.0;
		
		StringWriter writer = new StringWriter();
		
		persistenceUtil.save(writer, sample);
		
		String xml = writer.toString();
		
		System.out.println(xml);
		
		Assert.assertTrue (xml.contains("publicParameterDouble"));
		Assert.assertTrue(xml.contains("publicDouble"));
		Assert.assertFalse(xml.contains("privateParameterDouble"));
		Assert.assertFalse(xml.contains("privateDouble"));
		Assert.assertFalse(xml.contains("protectedParameterDouble"));
		Assert.assertFalse(xml.contains("protectedDouble"));
		Assert.assertFalse(xml.contains("packageParameterDouble"));
		Assert.assertFalse(xml.contains("packageDouble"));
		
		StringReader reader = new StringReader(xml);
		
		Object o  = persistenceUtil.load(reader);
		Assert.assertNotNull(o);
	}
	
}
