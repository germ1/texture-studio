package net.mgsx.numart.persistence;

public class IdentityResolver implements ModelResolver {

	@Override
	public String resolveTargetType(String sourceType) {
		return sourceType;
	}

	@Override
	public String resolveTargetProperty(Object object, String sourceProperty) {
		return sourceProperty;
	}

	@Override
	public String resolveSourceProperty(Object object, String targetProperty) {
		return targetProperty;
	}

}
