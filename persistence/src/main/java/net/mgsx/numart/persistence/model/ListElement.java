package net.mgsx.numart.persistence.model;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("list")
public class ListElement extends PropertyElement 
{
	@XStreamImplicit
	public List<ObjectElement> elements;
}
