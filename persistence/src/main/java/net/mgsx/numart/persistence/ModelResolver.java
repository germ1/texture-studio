package net.mgsx.numart.persistence;

public interface ModelResolver 
{
	/**
	 * Résolution de type
	 * @param type le type innexistant dans le modèle cible
	 * @return le type correspondant dans le modèle cible 
	 * 		ou null si pas de correspondance.
	 */
	String resolveTargetType(String sourceType);
	
	/**
	 * Résolution de propriété
	 * @param object l'objet cible
	 * @param property la propriété innexistante dans le model cible
	 * @return la propriété correspondante dans le modèle cible 
	 * 		ou null si aucune propriété	ne correspond.
	 */
	String resolveTargetProperty(Object object, String sourceProperty);
	
	/**
	 * Résolution de propriété
	 * @param object l'objet cible
	 * @param property la propriété innexistante dans le modèle source
	 * @return la propriété correspondante dans le modèle source 
	 * 		ou null si aucune propriété	ne correspond.
	 */
	String resolveSourceProperty(Object object, String targetProperty);
}
