package net.mgsx.numart.persistence;

import net.mgsx.tools.logging.Logger;

public class LoggerResolver implements ModelResolver {

	@Override
	public String resolveTargetType(String sourceType) {
		Logger.log(Logger.ERROR, "type " + sourceType + " not found");
		return null;
	}

	@Override
	public String resolveTargetProperty(Object object, String sourceProperty) {
		Logger.log(Logger.ERROR, "property " + sourceProperty + " not found for " + object.getClass().getName());
		return null;
	}

	@Override
	public String resolveSourceProperty(Object object, String targetProperty) {
		Logger.log(Logger.ERROR, "property " + targetProperty + " required for " + object.getClass().getName());
		return null;
	}

}
