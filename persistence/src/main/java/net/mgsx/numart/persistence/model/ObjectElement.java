package net.mgsx.numart.persistence.model;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("element")
public class ObjectElement 
{
	@XStreamAsAttribute
	public String type;
	
	@XStreamImplicit
	public List<PropertyElement> properties;
}
