package net.mgsx.numart.persistence.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("param")
public class ParamElement extends PropertyElement 
{
	@XStreamAsAttribute
	public Object value;
}
