package net.mgsx.numart.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.framework.utils.ReflectUtils.Visitor;
import net.mgsx.numart.persistence.model.ListElement;
import net.mgsx.numart.persistence.model.ObjectElement;
import net.mgsx.numart.persistence.model.ParamElement;
import net.mgsx.numart.persistence.model.PropertyElement;
import net.mgsx.numart.persistence.model.RootElement;
import net.mgsx.tools.logging.Logger;

import com.thoughtworks.xstream.XStream;

/**
 * Gestionnaire de persistence.
 * 
 * <br>
 * 
 * Module permettant de charger ou sauver des fichiers xml
 * 
 * @author mgsx
 *
 */
public class PersistenceUtil {

	/**
	 * default application folder
	 * TODO move to editor project
	 */
	public static File defaultFolder = null;
	
	
	private ModelResolver resolver;
	
	public PersistenceUtil() {
		this.resolver = new IdentityResolver();
	}
	
	public PersistenceUtil(ModelResolver resolver) {
		this.resolver = resolver;
	}
	
	
	private XStream getContext()
	{
		XStream defaultContext = new XStream();
		XStream context = defaultContext;
		
		context.setMode(XStream.ID_REFERENCES);
		context.useAttributeFor(String.class);
		context.useAttributeFor(int.class);
		context.useAttributeFor(long.class);
		context.useAttributeFor(double.class);
		context.useAttributeFor(boolean.class);
		context.useAttributeFor(float.class);

		// TODO faire quelque chose de plus automatique
		context.processAnnotations(new Class[]{
				RootElement.class, 
				ObjectElement.class, 
				ParamElement.class,
				ListElement.class,
				PropertyElement.class});
		
		return context;
	}
	
	

	/**
	 * Load an object from a file.
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public Object load(File file) throws FileNotFoundException
	{
		return load(new FileReader(file));
	}
	
	private Object getOrCreate(ObjectElement element, Map<ObjectElement, Object> mapObjectElement)
	{
		Object object = mapObjectElement.get(element);
		if(object == null)
		{
			object = ReflectUtils.newInstance(element.type);
			if(object == null)
			{
				String type = resolver.resolveTargetType(element.type);
				if(type != null)
				{
					object = ReflectUtils.newInstance(type);
				}
			}
			mapObjectElement.put(element, object);
		}
		return object;
	}
	
	private void loadParamRecursive(Object object, String name, Object value, Map<ObjectElement, Object> mapObjectElement)
	{
		final Object mappedValue;
		if(value instanceof ObjectElement)
		{
			mappedValue = loadRecursive((ObjectElement)value, mapObjectElement);
		}
		else
		{
			mappedValue = value;
		}
		if(ReflectUtils.trySet(object, name, mappedValue))
		{
			ReflectUtils.set(object, name, mappedValue);
		}
		else
		{
			String resolvedName = resolver.resolveTargetProperty(object, name);
			if(resolvedName != null && !resolvedName.equals(name))
				loadParamRecursive(object, resolvedName, value, mapObjectElement);
		}
		
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void loadListRecursive(Object object, String name, List<ObjectElement> elements, Map<ObjectElement, Object> mapObjectElement)
	{
		if(ReflectUtils.instanceOf(object, name, List.class))
		{
			List actualLst = (List)ReflectUtils.get(object, name);
			if(actualLst == null)
			{
				actualLst = (List)ReflectUtils.newInstance(ArrayList.class);
				ReflectUtils.set(object, name, actualLst);
			}
			if(elements != null)
			{
				for(ObjectElement obj : elements)
				{
					actualLst.add(loadRecursive(obj, mapObjectElement));
				}
			}
		}
		else
		{
			String resolvedName = resolver.resolveTargetProperty(object, name);
			if(resolvedName != null && !resolvedName.equals(name))
			{
				loadListRecursive(object, resolvedName, elements, mapObjectElement);
			}
			else
			{
				// TODO handle or log
			}
		}
		
	}
	
	private Object loadRecursive(ObjectElement element, Map<ObjectElement, Object> mapObjectElement)
	{
		final Object object = getOrCreate(element, mapObjectElement);
		if(element.properties != null)
		{
			for(PropertyElement property : element.properties)
			{
				if(property instanceof ListElement)
				{
					ListElement lst = (ListElement)property;
					loadListRecursive(object, lst.name, lst.elements, mapObjectElement);
				}
				else
				{
					ParamElement param = (ParamElement)property;
					loadParamRecursive(object, param.name, param.value, mapObjectElement);
				}
			}
		}
		return object;
	}
	
	/**
	 * Load an object from a strem
	 * @param reader
	 * @return
	 */
	public Object load(Reader reader)
	{
		XStream context = getContext();
		Object root = context.fromXML(reader);
		Object realRoot = null;
		
		// de-encapsulate from a root element (if so)
		if(root instanceof RootElement)
		{
			RootElement rootElement = (RootElement)root;
			
			final Map<ObjectElement, Object> mapObjectElement = new HashMap<ObjectElement, Object>();
		
			realRoot = loadRecursive(rootElement.elements.get(0), mapObjectElement);
		}
		else
		{
			Logger.error(Logger.FILEIO, "RootElement expected"); 
		}
		
		
		// check for precomputation, ie compute not serialized fields.
		ReflectUtils.checkForPrecomputation(realRoot);
		return realRoot;
	}
	
	/**
	 * Save an object to a file
	 * @param file
	 * @param root
	 * @throws IOException
	 */
	public void save(File file, Object root) throws IOException
	{
		save(new FileWriter(file), root);
	}
	/**
	 * Save an object to a stream
	 * @param writer
	 * @param root
	 */
	public void save(Writer writer, final Object root)
	{
		final XStream context = getContext();
		
		// Encapsulate in a RootElement
		final RootElement rootElement = new RootElement();
		rootElement.elements = new ArrayList<ObjectElement>();
		
		final Map<Object, ObjectElement> mapObjectElement = new HashMap<Object, ObjectElement>();
		
		ReflectUtils.visitGraph(root, new Visitor(){
			@Override
			public void visit(Object object) 
			{
				ObjectElement element = new ObjectElement();
				mapObjectElement.put(object, element);
			}
		});
		ReflectUtils.visitGraph(root, new Visitor(){
			@SuppressWarnings("rawtypes")
			@Override
			public void visit(Object object) 
			{
				
				ObjectElement element = mapObjectElement.get(object);
				element.type = object.getClass().getName();
				element.properties = new ArrayList<PropertyElement>();
				for(Field field : object.getClass().getFields())
				{
					if(ReflectUtils.isPersistentField(field))
					{
						PropertyElement property;
						Object value = ReflectUtils.get(object, field);
						if(value instanceof Iterable)
						{
							ListElement lst = new ListElement();
							property = lst;
							lst.elements = new ArrayList<ObjectElement>();
							for(Object o : (Iterable)value)
							{
								lst.elements.add(mapObjectElement.get(o));
							}
						}
						else
						{
							ParamElement param = new ParamElement();
							property = param;
							Object mappedValue = mapObjectElement.get(value);
							param.value = mappedValue != null ? mappedValue : value;
							
						}
						property.name = field.getName();
						element.properties.add(property);
					}
				}
			}
		});
		
		rootElement.elements.add(mapObjectElement.get(root));
		
		if(Logger.enabled(Logger.FILEIO))
		{
			StringWriter swriter = new StringWriter();
			context.toXML(rootElement, swriter);
			Logger.log(Logger.FILEIO, swriter.toString());
		}
		
		context.toXML(rootElement, writer);
	}
	
	
}
