package net.mgsx.numart.persistence.model;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

abstract public class PropertyElement 
{
	@XStreamAsAttribute
	public String name;
}
