package net.mgsx.numart.realtime;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.testing.AbstractComplienceTest;
import net.mgsx.test.LabelledParametrized;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

@RunWith(LabelledParametrized.class)
public class RealTimeRuntimeTest extends AbstractComplienceTest
{
	private Class<?> cls;
	
	public RealTimeRuntimeTest(String name, Class<?> cls) {
		super(name);
		this.cls = cls;
	}
	
	@Parameters
	public static Collection<Object[]> generateTests()
	{
		List<Object[]> suites = new ArrayList<Object[]>();
		for(Class<?> clazz : getAllClassesToTest("net.mgsx.numart.realtime", RTSequence.class))
		{
			if(!clazz.isInterface() && !Modifier.isAbstract(clazz.getModifiers()))
			{
				Object[] parameters = {clazz.getSimpleName(), clazz};
				suites.add(parameters);
			}
		}
		return suites;
	}
	
	@Ignore
	@Test
	public void testPrepare() throws Exception
	{
		GL gl = new GLMock();
		//GLU glu = mockery.mock(GLU.class);
		// GL gl = mockery.mock(GL.class);
		
		GLU glu = new GLU();
		RTSequence instance = (RTSequence)cls.newInstance();
		
		instance.prepare(gl, glu);
	}
	@Ignore
	@Test
	public void testApply() throws Exception
	{
		GL gl = new GLMock();
		//GLU glu = mockery.mock(GLU.class);
		// GL gl = mockery.mock(GL.class);
		
		RTSequence instance = (RTSequence)cls.newInstance();
		GLU glu = new GLU();
		instance.prepare(gl, glu);
		instance.apply(gl, glu, 0);
	}

}
