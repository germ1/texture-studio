package net.mgsx.numart.realtime.particles;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;

public class ParticleSystemTest 
{
	@Test
	public void testPoolExtension()
	{
		ParticleSystem ps = new ParticleSystem(1);
		Particle p;
		p = ps.createParticle();
		Assert.assertNotNull(p);
		p = ps.createParticle();
		Assert.assertNotNull(p);
		Iterator<Particle> i = ps.iterator();
		Assert.assertNotNull(i.next());
		Assert.assertNotNull(i.next());
		Assert.assertFalse(i.hasNext());
	}
}
