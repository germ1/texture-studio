/*
uniform float myVar; 

void main()
{
	float v = 0.5 + 0.2 * noise1(gl_FragCoord.x*3241. + 1234.9 + gl_FragCoord.y * 4583.); 
	gl_FragColor = vec4(v,v,v,1);
}
*/


/*
uniform sampler2D tex0;
uniform sampler3D tex1;

uniform float time;

void main()
{
    // vec4 volumeValue = texture3D(tex1, vec3(gl_TexCoord[0].s, gl_TexCoord[0].t, time));
    vec4 clr = texture2D(tex0, gl_TexCoord[0].st);
    // vec2 mapping = vec2(gl_TexCoord[0].s + volumeValue.r, gl_TexCoord[0].t + volumeValue.r);
    // vec4 clrMod = texture2D(tex0, gl_TexCoord[0].st);
    gl_FragColor = clr;
}
*/



uniform sampler2D tex0;
uniform sampler3D tex1;
uniform float time;

uniform float scale;
uniform float amplitude;

void main()
{ 
   
   // vec4 clr2 = texture3D(tex1, vec3(gl_TexCoord[1].s * scale  + time, gl_TexCoord[1].t * scale  + time, gl_TexCoord[1].z * scale  + time ));
   
   // vec4 clr = texture2D(tex0, vec2(gl_TexCoord[0].s + clr2.r * amplitude, gl_TexCoord[0].t + clr2.r * amplitude));
    gl_FragColor = texture2D(tex0, gl_TexCoord[0].st);
}
