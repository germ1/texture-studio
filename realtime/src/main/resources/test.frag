/*
uniform float myVar; 

void main()
{
	float v = 0.5 + 0.2 * noise1(gl_FragCoord.x*3241. + 1234.9 + gl_FragCoord.y * 4583.); 
	gl_FragColor = vec4(v,v,v,1);
}
*/

uniform sampler2D tex0, tex1;

uniform float time;

void main()
{
    // Sampling The Texture And Passing It To The Frame Buffer
    vec4 clr = texture2D(tex0, gl_TexCoord[0].st);
    vec4 clr2 = texture2D(tex1, gl_TexCoord[0].st);
    float f = clr2.r * 0.2 * time;
    gl_FragColor = clr2 * texture2D(tex0, gl_TexCoord[0].st + vec2(f * gl_TexCoord[0].s, f * gl_TexCoord[0].t));
}
