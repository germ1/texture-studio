#version 120

uniform sampler3D tex0;

uniform float time;
uniform float scale;
uniform float amplitude;

uniform vec4 colorA1;
uniform vec4 colorA2;
uniform vec4 colorB1;
uniform vec4 colorB2;


vec4 damier(vec4 c1, vec4 c2, vec4 position)
{
	int px = int(position.x);
	int py = int(position.y);
	int pz = int(position.z);
	int xor = mod(px + py + pz, 2);
	return xor == 0 ? c1 : c2; 
}

vec4 snoise(vec4 position)
{
	return texture3D(tex0, vec3(position.x * scale  + time, position.y * scale  + time, position.z * scale  + time ));
}

vec4 distortion(vec4 position)
{
	return position + amplitude * (snoise(position) - 0.5);
}

void main()
{ 
	vec4 colorA = damier(colorA1, colorA2, distortion(gl_TexCoord[0]));
	vec4 colorB = damier(colorB1, colorB2, distortion(gl_TexCoord[0] + vec4(0.7, 0.3, 0.8, 0.5)));
    float hardness = 0.01;
    gl_FragColor = mix(colorA, colorB, smoothstep(0.5 - hardness, 0.5 + hardness, snoise(gl_TexCoord[0])));
}
