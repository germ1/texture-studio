package net.mgsx.numart.wave.logic;

import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.wave.logic.Playground.CellZone;
import net.mgsx.numart.wave.logic.entities.Energy;
import net.mgsx.numart.wave.logic.entities.Structure;

public class WaveGame 
{
	public Playground playground;
	public Menu menu;
	
	public WaveGame() {
		playground = new Playground();
		menu = new Menu();
	}
	
	public void update()
	{
		// generic behavior
		for(MenuItem i : menu.grid.cells)
		{
			i.update();
		}
		
		// behavior map edition
		if(menu.grid.selectedItem != null && playground.grid.selectedItem != null)
		{
			MenuItem sel = menu.grid.selectedItem;
			menu.grid.clearSelection();
			sel.use();
			
			CellZone zone = playground.grid.selectedItem;
			Structure s = (Structure)ReflectUtils.newInstance(sel.type);
			
			zone.zone.structure = s;
			
			if(s instanceof Energy)
			{
				s.powered = false;
				powerCell(zone);
				// TODO power all
			}
			else
			{
				// check power around
				for(CellZone neighboor : playground.grid.getNeighboors(zone))
				{
					if(neighboor.zone.structure != null && 
						neighboor.zone.structure.powered)
					{
						s.powered = true;
						break;
					}
				}
			}
			
		}
		playground.grid.clearSelection();
	}
	
	private void powerCell(CellZone cell)
	{
		if(cell.zone.structure != null && !cell.zone.structure.powered)
		{
			cell.zone.structure.powered = true;
			for(CellZone neighboor : playground.grid.getNeighboors(cell))
			{
				powerCell(neighboor);
			}
		}
	}
}
