package net.mgsx.numart.wave.fwk;

abstract public class Behavior<T> 
{
	abstract public Behavior<T> update(T ctx);
}
