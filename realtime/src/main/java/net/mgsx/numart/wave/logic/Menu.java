package net.mgsx.numart.wave.logic;

import net.mgsx.numart.wave.fwk.InteractiveGrid;
import net.mgsx.numart.wave.logic.entities.Collector;
import net.mgsx.numart.wave.logic.entities.Energy;
import net.mgsx.numart.wave.logic.entities.Tower;

public class Menu
{
	public InteractiveGrid<MenuItem> grid;
	
	public Menu() 
	{
		grid = new InteractiveGrid<MenuItem>(16, 1);
		grid.cells.add(new MenuItem(Energy.class));
		grid.cells.add(new MenuItem(Collector.class));
		grid.cells.add(new MenuItem(Tower.class));
	}

}
