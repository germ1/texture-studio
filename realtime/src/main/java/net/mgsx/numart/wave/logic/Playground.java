package net.mgsx.numart.wave.logic;

import net.mgsx.numart.wave.fwk.InteractiveGrid;
import net.mgsx.numart.wave.fwk.InteractiveGrid.Item;
import net.mgsx.numart.wave.logic.entities.Zone;

public class Playground {

	public static class CellZone extends Item
	{
		public Zone zone = new Zone();
	}
	
	public InteractiveGrid<CellZone> grid;
	
	public Lineground [] lines;
	
	public Playground() {
		grid = new InteractiveGrid<Playground.CellZone>(8, 5);
		lines = new Lineground [5];
		for(int i=0 ; i < 5*8 ; i++)
		{
			grid.cells.add(new CellZone());
		}
	}
}
