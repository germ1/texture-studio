package net.mgsx.numart.wave.logic.states;

import net.mgsx.numart.wave.fwk.Behavior;
import net.mgsx.numart.wave.logic.MenuItem;

public class MenuItemCharging extends Behavior<MenuItem>{

	@Override
	public Behavior<MenuItem> update(MenuItem ctx) 
	{
		ctx.charge += 0.003;
		if(ctx.charge >= 1)
		{
			ctx.charge = 1;
			return new MenuItemAvailable();
		}
		return this;
	}

}
