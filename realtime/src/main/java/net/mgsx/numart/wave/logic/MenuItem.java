package net.mgsx.numart.wave.logic;

import net.mgsx.numart.wave.fwk.Behavior;
import net.mgsx.numart.wave.fwk.InteractiveGrid.Item;
import net.mgsx.numart.wave.logic.entities.Structure;
import net.mgsx.numart.wave.logic.states.MenuItemCharging;

public class MenuItem extends Item 
{
	public Class<? extends Structure> type;
	
	public double charge = 0;
	
	public Behavior<MenuItem> behavior;
	
	public MenuItem(Class<? extends Structure> type) {
		super();
		this.type = type;
		behavior = new MenuItemCharging();
	}
	
	public void update()
	{
		behavior = behavior.update(this);
	}
	
	public void use()
	{
		charge = 0;
		behavior = new MenuItemCharging();
	}

}
