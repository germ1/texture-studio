package net.mgsx.numart.wave.logic;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.wave.logic.entities.Alien;
import net.mgsx.numart.wave.logic.entities.Bullet;

public class Lineground {
	public List<Alien> aliens = new ArrayList<Alien>();
	public List<Bullet> bullets = new ArrayList<Bullet>();
}
