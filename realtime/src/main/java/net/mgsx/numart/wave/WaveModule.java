package net.mgsx.numart.wave;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.control.Pointer;
import net.mgsx.numart.framework.control.impl.MousePointer;
import net.mgsx.numart.math.Box2D;
import net.mgsx.numart.math.Point2D;
import net.mgsx.numart.realtime.RTNode;
import net.mgsx.numart.realtime.RTTexture;
import net.mgsx.numart.wave.logic.MenuItem;
import net.mgsx.numart.wave.logic.WaveGame;
import net.mgsx.numart.wave.logic.Playground.CellZone;
import net.mgsx.numart.wave.logic.entities.Collector;
import net.mgsx.numart.wave.logic.entities.Diamond;
import net.mgsx.numart.wave.logic.entities.Energy;
import net.mgsx.numart.wave.logic.entities.Tower;
import net.mgsx.numart.wave.logic.states.MenuItemAvailable;
import net.mgsx.numart.wave.logic.states.MenuItemCharging;

public class WaveModule extends RTNode
{

	@ParamGroup
	public RTTexture texEnergy;
	@ParamGroup
	public RTTexture texCollector;
	@ParamGroup
	public RTTexture texTower;
	@ParamGroup
	public RTTexture texDiamond;
	
	public MousePointer pointer;
	
	private WaveGame game;
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		pointer = new MousePointer();
		
		texEnergy.prepare(gl, glu);
		texCollector.prepare(gl, glu);
		texDiamond.prepare(gl, glu);
		texTower.prepare(gl, glu);
		
		game = new WaveGame();
		game.menu.grid.box = new Box2D(-1, 0.75, 2, 0.25);
		game.playground.grid.box = new Box2D(-1, -1, 2, 2 - 0.2);
		game.menu.grid.box.reshape(0.05);
		game.playground.grid.box.reshape(0.05);
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		pointer.update();
		boolean action = pointer.aquire();
		
		game.menu.grid.action(new Point2D(pointer.getX(), pointer.getY()), action);
		game.playground.grid.action(new Point2D(pointer.getX(), pointer.getY()), action);
		
		game.update();
		
		
		// drawings (read-only)
		
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_MODULATE);
		
		gl.glEnable(GL.GL_TEXTURE);

		gl.glColor3d(1, 1, 1);
		gl.glLineWidth(2);

		// TODO draw !
		gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
		drawRect(gl, game.menu.grid.box);
		drawRect(gl, game.playground.grid.box);
		
		gl.glLineWidth(1);
		for(CellZone z : game.playground.grid.cells)
		{
			Box2D itemBox = game.playground.grid.getBox(z);
			drawRect(gl, itemBox);
		}
		
		Box2D fullTexBox = new Box2D(0, 0, 1, 1);
		
		for(CellZone z : game.playground.grid.cells)
		{
			Box2D itemBox = game.playground.grid.getBox(z);
			drawRect(gl, itemBox);
			
		}
		for(CellZone z : game.playground.grid.cells)
		{
			Box2D itemBox = game.playground.grid.getBox(z);
			if(z.zone.structure != null)
			{
				getTexture(z.zone.structure.getClass()).apply(gl, glu, time);
				double shade = z.zone.structure.powered ? 1 : 0.5; 
				gl.glColor3d(shade, shade, shade);
				drawBox(gl, itemBox, fullTexBox);
			}
		}

		for(MenuItem item : game.menu.grid.cells)
		{
			gl.glColor3d(1, 1, 1);
			Box2D itemBox = game.menu.grid.getBox(item);
			getTexture(item.type).apply(gl, glu, time);
			drawBox(gl, itemBox, fullTexBox);
			
			if(item.behavior instanceof MenuItemCharging)
			{
				gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
				gl.glColor4d(0, 0, 0, 0.5);
				Box2D mask = new Box2D(itemBox);
				mask.h *= 1 - item.charge;
				mask.y  -= mask.h - itemBox.h;
				drawBox(gl, mask, fullTexBox);
			}
			if(item.selected)
			{
				gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
				gl.glColor3d(1, 1, 0);
				drawRect(gl, itemBox);
			}
		}
		
		
		gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
		gl.glLineWidth(1);
		gl.glColor3d(0.5,0.5, 1);
		drawCross(gl, new Point2D(pointer.getX(), pointer.getY()), 0.02);
		
	}
	
	RTTexture getTexture(Class<?> cls)
	{
		if(cls == Collector.class)
		{
			return texCollector;
		}
		if(cls == Diamond.class)
		{
			return texDiamond;
		}
		if(cls == Energy.class)
		{
			return texEnergy;
		}
		if(cls == Tower.class)
		{
			return texTower;
		}
		return null;
	}
	
	
	private void drawBox(GL gl, Box2D box, Box2D texture)
	{
		gl.glBegin(GL.GL_TRIANGLE_STRIP);
		gl.glTexCoord2d(texture.x, texture.y);
		gl.glVertex3d(box.x, box.y, 0);
		gl.glTexCoord2d(texture.x + texture.w, texture.y);
		gl.glVertex3d(box.x + box.w, box.y, 0);
		gl.glTexCoord2d(texture.x, texture.y + texture.h);
		gl.glVertex3d(box.x, box.y + box.h, 0);
		gl.glTexCoord2d(texture.x + texture.w, texture.y + texture.h);
		gl.glVertex3d(box.x + box.w, box.y + box.h, 0);
		gl.glEnd();
	}

	public void drawRect(GL gl, Box2D box)
	{
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex2d(box.x, box.y);
		gl.glVertex2d(box.x + box.w, box.y);
		gl.glVertex2d(box.x + box.w, box.y + box.h);
		gl.glVertex2d(box.x, box.y + box.h);
		gl.glEnd();
	}
	public void drawCross(GL gl, Point2D pos, double len)
	{
		gl.glBegin(GL.GL_LINES);
		gl.glVertex2d(pos.x - len, pos.y);
		gl.glVertex2d(pos.x + len, pos.y);
		gl.glVertex2d(pos.x, pos.y - len);
		gl.glVertex2d(pos.x, pos.y + len);
		gl.glEnd();
	}
}
