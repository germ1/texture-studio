package net.mgsx.numart.wave.fwk;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.math.Box2D;
import net.mgsx.numart.math.Point2D;
import net.mgsx.numart.wave.fwk.InteractiveGrid.Item;

public class InteractiveGrid<T extends Item> 
{
	abstract static public class Item
	{
		public int col, row;
		public boolean selected, focused;
	}
	
	public Box2D box = new Box2D();
	
	public int cols, rows;
	
	public List<T> cells = new ArrayList<T>();
	
	public T selectedItem;
	public T focusedItem;
	
	public InteractiveGrid(int cols, int rows) {
		super();
		this.cols = cols;
		this.rows = rows;
	}

	public void action(Point2D position, boolean trigger)
	{
		if(position.y >= box.y && position.x >= box.x)
		{
			int row = (int)(rows * (position.y - box.y) / box.h);
			int col = (int)(cols * (position.x - box.x) / box.w);
			int index = row * cols + col;
			if(row >= 0 && row < rows && col >= 0 && col < cols && index < cells.size())
			{
				T cell = cells.get(index);
				if(trigger)
				{
					if(cell.selected)
					{
						clearSelection();
					}
					else
					{
						clearSelection();
						selectedItem = cell;
						selectedItem.selected = true;
					}
				}
				else
				{
					
				}
				clearFocus();
				focusedItem = cell;
				focusedItem.focused = true;
			}
			else
			{
				clearFocus();
			}
		}
	}
	
	public void clearFocus()
	{
		if(focusedItem != null)
		{
			focusedItem.focused = false;
			focusedItem = null;
		}
	}
	public void clearSelection()
	{
		if(selectedItem != null)
		{
			selectedItem.selected = false;
			selectedItem = null;
		}
	}
	
	public List<T> getNeighboors(T item)
	{
		int index = cells.indexOf(item);
		int row = index / cols;
		int col = index % cols;
		
		List<T> l = new ArrayList<T>();
		if(col > 0)
		{
			l.add(cells.get(row * cols + col - 1));
		}
		if(col < cols - 1)
		{
			l.add(cells.get(row * cols + col + 1));
		}
		if(row > 0)
		{
			l.add(cells.get((row - 1) * cols + col));
		}
		if(row < rows - 1)
		{
			l.add(cells.get((row + 1) * cols + col));
		}
		return l;
	}
	
	public Box2D getBox(T item)
	{
		Box2D b = new Box2D(box.x, box.y, box.w / (double)cols, box.h / (double)rows);
		int index = cells.indexOf(item);
		b.x += (index % cols) * box.w / (double)cols;
		b.y += (index / cols) * box.h / (double)rows;
		return b;
	}
}
