package net.mgsx.numart.vivarium.logic;

public interface State
{
	public State update(Context ctx);
}