package net.mgsx.numart.vivarium.util;

public class Generator 
{
	private double time;
	
	public int update(double frequency, double dtime)
	{
		double periode = 1. / frequency;
		time += dtime;
		int tics = (int)Math.floor(time / periode);
		time -= (double)tics * periode;
		return tics;
	}
}
