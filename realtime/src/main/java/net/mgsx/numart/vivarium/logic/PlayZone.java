package net.mgsx.numart.vivarium.logic;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.math.Box2D;
import net.mgsx.numart.vivarium.util.Generator;

public class PlayZone extends Behavior
{
	public Box2D box;
	public Player player = new Player();
	public List<Fish> fish = new ArrayList<Fish>();
	public List<Food> food = new ArrayList<Food>();
	
	@SubBehavior(initState = FishPoolFillState.class)
	public FishPool fishPool = new FishPool();
	
	public class FishPool extends Behavior
	{
		
	}
	
	public boolean isFoodLeft()
	{
		for(Food f : food)
		{
			if(f.energy > 0)
			{
				return true;
			}
		}
		return false;
	}
	
	public class FishPoolFillState implements State
	{
		Generator gen = new Generator();
		
		@Override
		public State update(Context ctx) 
		{
			// on arrête d'ajouter des poissons
			// lorsqu'il n'y a plus de nourriture.
			if(!ctx.cfg.happyEnding || isFoodLeft())
			{
				// ajout de poisson
				int nFish = gen.update(ctx.cfg.fishRate, ctx.dtime);
				for(int i=0 ; i<nFish ; i++)
				{
					Fish f = new Fish();
					f.energy = 3;
					f.level = 1;
					fish.add(f);
				}
				return this;
			}
			return new DefaultFinalState();
			
		}
	}
	public class FishPoolFillStat implements State
	{
		@Override
		public State update(Context ctx) {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	@Override
	protected State initState() {
		return new InitState();
	}

	private class InitState implements FinalState
	{
		@Override
		public State update(Context ctx) 
		{
			food.add(new Food());
			food.add(new Food());
			food.add(new Food());
			food.add(new Food());
			food.add(new Food());
			return new RunningState();
		}
	}
	private class DisapearState implements State
	{
		@Override
		public State update(Context ctx) 
		{
			// update fish
			for(Fish f : fish.toArray(new Fish[]{}))
			{
				f.update(ctx);
			}
			return this;
		}
	}
	
	private class RunningState implements State
	{
		@Override
		public State update(Context ctx) 
		{
			// opponent attack / soin
			Player opponent = ctx.game.zoneA == ctx.zone ? ctx.game.zoneB.player : ctx.game.zoneA.player;
			if(ctx.cfg.opponentAttack)
			{
				if(box != null && opponent.box != null && // TODO init
						box.contains(opponent.box))
				{
					int n = opponent.points / 100; // TODO
					for(int i=0 ; i<n ; i++)
					{
						Fish f = new Fish();
						f.level = 1;
						f.energy = Math.max(1, Math.min(opponent.points / 1000, 5));
						fish.add(f);
					}
					opponent.points = 0;
				}
			}
			
			// update des poissons
			for(Fish f : fish.toArray(new Fish[]{}))
			{
				// update fish
				f.update(ctx);
				
				if(f.energy <= 0)
				{
					if(ctx.cfg.killSound != null)
					{
						ctx.cfg.killSound.play();
					}

					
					fish.remove(f);
					player.points += 100;
					
					ctx.cfg.addParticles(f.box.center());
				}
				
				// compute collision (soin)
				if(opponent.points > 0 && opponent.box.contains(f.box))
				{
					f.energy += ctx.dtime * ctx.cfg.rescueRate;
					opponent.points -= 1;
				}
				
				// compute collision (poison)
				if(player.box.contains(f.box))
				{
					double surfaceRate = f.box.surface() / player.box.surface(); 
					
					// equation : rapport de surface : [1 à 0 [
					double t = Math.log(0.5) / Math.log(ctx.cfg.damageHalf);
					double damage = ctx.cfg.damageMax * Math.pow(surfaceRate, t);
					
					f.energy -= ctx.dtime * damage;
				}
				
				while(f.energy > f.level && f.level < 5)
				{
					f.level++;
					f.box.reshape(-ctx.cfg.fishSize * 0.2);
				}
				while(f.energy < f.level-1 && f.level > 1)
				{
					f.level--;
					f.box.reshape(ctx.cfg.fishSize * 0.2);
				}
			}
			
			// vérification de la nourriture
			boolean remainFood = false;
			for(Food f : food)
			{
				if(f.energy > 0)
				{
					remainFood = true;
				}
			}
			if(!remainFood)
			{
				if(ctx.cfg.happyEnding)
				{
					for(Fish f : fish.toArray(new Fish[]{}))
					{
						f.joinCenter(ctx);
					}
					return new DisapearState();
				}
			}
			
			return this;
		}
	}

}
