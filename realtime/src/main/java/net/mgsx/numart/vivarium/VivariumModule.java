package net.mgsx.numart.vivarium;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.Trigger;
import net.mgsx.numart.framework.audio.Sound;
import net.mgsx.numart.framework.audio.SoundTrack;
import net.mgsx.numart.framework.control.TwoPlayerBoxControl;
import net.mgsx.numart.math.Box2D;
import net.mgsx.numart.math.Point2D;
import net.mgsx.numart.realtime.RTNode;
import net.mgsx.numart.realtime.RTShader;
import net.mgsx.numart.realtime.geometry.RTParticles;
import net.mgsx.numart.realtime.geometry.RTTextDrawer;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.particles.ParticleSystem;
import net.mgsx.numart.vivarium.logic.Context;
import net.mgsx.numart.vivarium.logic.Fish;
import net.mgsx.numart.vivarium.logic.Food;
import net.mgsx.numart.vivarium.logic.Game;
import net.mgsx.numart.vivarium.logic.PlayZone;
import net.mgsx.numart.vivarium.logic.Player;

import com.sun.opengl.util.GLUT;

public class VivariumModule extends RTNode
{
	@ParamDouble
	public double gameBorder = 0.05;
	
	@ParamDouble
	public double zoneBorder = 0.05;
	
	@ParamDouble
	public double fishSize = 0.05;
	
	@ParamDouble
	public double fishSpeed = 0.05;
	
	@Logarithmic
	@ParamDouble(min=1, max=100000)
	public double fishEating = 1000;
	
	@Logarithmic
	@ParamDouble(min=1, max=100000)
	public double fishPoison = 1000;
	
	@Logarithmic
	@ParamDouble(min=0.5, max=2.0)
	public double fishSpeedLevel = 1;
	
	
	@Logarithmic
	@ParamDouble(min=0.01, max=100)
	public double fishRate = 1;
	
	@Logarithmic
	@ParamDouble(min=0.01,max=100)
	public double fishEnergy = 1;
	
	@Logarithmic
	@ParamDouble(min=0.01,max=100)
	public double rescueRate = 1;
	
	@ParamBoolean
	public boolean opponentAttack = true;
	
	@ParamBoolean
	public boolean happyEnding = false;
	
	@Logarithmic
	@ParamDouble(min=0.01,max=100)
	public double damageMax = 1;
	@ParamDouble
	public double damageHalf = 1;
	
	@ParamBoolean
	public boolean displayAsLines = true;
	
	@ParamGroup
	public RTParticles particles;
	
	@ParamGroup
	public TwoPlayerBoxControl controller;
	
	@ParamGroup
	public RTTextDrawer textDrawer;
	
	
	@ParamGroup
	public RTShader shaderPlayer = null;
	
	@ParamGroup
	public RTShader shaderZone = null;
	
	@ParamGroup
	public RTShader shaderFish = null;
	
	@ParamGroup
	public RTShader shaderFood = null;
	
	@ParamDouble
	public double fxVolume = 1;
	
	@ParamGroup
	public Sound killSound = null;
	
	private Game game;
	
	private GLUT glut;
	
	private ParticleSystem ps;
	
	private SoundTrack fxTrack;
	
	public void addParticles(final Point2D point)
	{
		int num = 5;
		for(int i=0 ; i<num ; i++)
		{
			Particle p = ps.createParticle();
			p.position.x = point.x;
			p.position.y = point.y;
			double a = Math.PI * 2 / (double)num * (i + 0.5);
			double s = 0.8;
			p.speed.x = s * Math.cos(a);
			p.speed.y = s * Math.sin(a);
			p.life = 0.3;
			p.angle = a;
			p.size = 0.03;
		}
	}
	
	@Trigger
	public void reset()
	{
		game = new Game();
	}
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		if(killSound != null)
		{
			killSound.prepare();
			
			fxTrack = new SoundTrack();
			fxTrack.clips.add(killSound);
		}
		
		ps = new ParticleSystem(1000);
		
		game = new Game();
		glut = new GLUT();
		
		if(textDrawer != null)
			textDrawer.prepare(gl, glu);
		
		if(shaderFish != null) 
			shaderFish.prepare(gl, glu);
		if(shaderFood != null) 
			shaderFood.prepare(gl, glu);
		if(shaderPlayer != null) 
			shaderPlayer.prepare(gl, glu);
		if(shaderZone != null) 
			shaderZone.prepare(gl, glu);
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		fxTrack.setVolume(fxVolume);
		// update logic
		
		controller.update();
		
//		game.zoneA.player.status = controller.getStatus(0);
//		game.zoneB.player.status = controller.getStatus(1);
		
		game.zoneA.player.box.x = controller.getX(0, 0);
		game.zoneA.player.box.y = controller.getY(0, 0);
		game.zoneA.player.box.w = controller.getX(0, 1) - game.zoneA.player.box.x;
		game.zoneA.player.box.h = controller.getY(0, 1) - game.zoneA.player.box.y;
		
		game.zoneB.player.box.x = controller.getX(1, 0);
		game.zoneB.player.box.y = controller.getY(1, 0);
		game.zoneB.player.box.w = controller.getX(1, 1) - game.zoneB.player.box.x;
		game.zoneB.player.box.h = controller.getY(1, 1) - game.zoneB.player.box.y;
		
		game.zoneA.player.box.positive();
		game.zoneB.player.box.positive();
		
		Context ctx = new Context();
		ctx.cfg = this;
		ctx.dtime = 1. / 60.; // TODO fps
		ctx.game = game;
		ctx.zone = null;
		
		ps.update(null, null, ctx.dtime);
		
		game.update(ctx);
		
		// draw entities
		
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		
		gl.glEnable(GL.GL_TEXTURE);

		gl.glColor3d(1, 1, 1);
		gl.glLineWidth(3);

		drawGame(gl,glu,time);
		
	}
	
	private void drawGame(GL gl, GLU glu, double time)
	{
		// draw
		drawZone(game.zoneA, gl,glu,time);
		drawZone(game.zoneB, gl, glu, time);
		
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);
		Box2D pBox = new Box2D();
		for(Particle p : ps)
		{
			pBox.x = p.position.x - p.size/2;
			pBox.y = p.position.y - p.size/2;
			pBox.w = p.size;
			pBox.h = p.size;
			drawBox(gl, shaderFish, pBox);
		}
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		
		activateShader(shaderPlayer, gl, glu, time);
		
		drawPlayer(game.zoneA.player, gl,glu,time);
		drawPlayer(game.zoneB.player, gl, glu, time);
		
		if(textDrawer != null)
		{
			textDrawer.apply(gl, glu, time);
			textDrawer.drawText(gl, glu, String.format("player 1 : %d", game.zoneA.player.points), new Box2D(game.zoneA.box.x, game.zoneA.box.y + game.zoneA.box.h, 0.05, 0.1));
			textDrawer.drawText(gl, glu, String.format("player 2 : %d", game.zoneB.player.points), new Box2D(game.zoneB.box.x, game.zoneB.box.y + game.zoneB.box.h, 0.05, 0.1));
		}
	}
	private void drawZone(PlayZone zone, GL gl, GLU glu, double time)
	{
		activateShader(shaderZone, gl, glu, time);
		
		gl.glColor3d(1, 1, 1);
		drawBox(gl, shaderZone, zone.box);
		
		activateShader(shaderFood, gl, glu, time);
		
		gl.glColor3d(0, 1, 1);
		int i=0;
		for(Food f : zone.food)
		{
			double rate = f.energy;
			double whbase = zone.box.w / (double)(zone.food.size() + 1);
			f.box.w = 
			f.box.h = rate * whbase;
			f.box.x = zone.box.x + (i + 0.5) / (zone.food.size() + 1) - f.box.w / 2;
			f.box.y = zone.box.y + zone.box.h - whbase - f.box.h / 2;
			f.box.reshape(0.02); // TODO Param
			drawBox(gl, shaderFood, f.box);
			
//			gl.glRasterPos2d(f.box.x + f.box.w + 0.05, f.box.y);
//			glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, String.format("%f", f.energy));
			
			i++;
		}
		
		activateShader(shaderFish, gl, glu, time);
		
		for(Fish f : zone.fish)
		{
			double ratio = f.energy / fishEnergy;
			gl.glColor3d(1, ratio, 0);
			drawBox(gl, shaderFish, f.box);
			
//			if(zone.player.box.contains(f.box))
//			{
//				gl.glRasterPos2d(f.box.x + f.box.w + 0.05, f.box.y);
//				glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, String.format("%s", f.state.getClass().getSimpleName()));
//			}
		}
	}
	private void drawPlayer(Player player, GL gl, GLU glu, double time)
	{
		if(player.id == 0)
			gl.glColor3d(0.5, 0.5, 1);
		else
			gl.glColor3d(0.5, 1, 0.5);
		
		drawBox(gl, shaderPlayer, player.box);
		
		gl.glRasterPos2d(player.box.x + player.box.w + 0.05, player.box.y);
		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, String.format("%d", player.points));
	}

	private void drawBox(GL gl, RTShader shader, Box2D box)
	{
		if(displayAsLines || shader == null)
		{
			gl.glBegin(GL.GL_LINE_LOOP);
			gl.glVertex2d(box.x, box.y);
			gl.glVertex2d(box.x + box.w, box.y);
			gl.glVertex2d(box.x + box.w, box.y + box.h);
			gl.glVertex2d(box.x, box.y + box.h);
			gl.glEnd();
		}
		else
		{
			gl.glBegin(GL.GL_TRIANGLE_STRIP);
			gl.glTexCoord2i(0, 0);
			gl.glVertex3d(box.x, box.y, 0);
			gl.glTexCoord2i(1, 0);
			gl.glVertex3d(box.x + box.w, box.y, 0);
			gl.glTexCoord2i(0, 1);
			gl.glVertex3d(box.x, box.y + box.h, 0);
			gl.glTexCoord2i(1, 1);
			gl.glVertex3d(box.x + box.w, box.y + box.h, 0);
			gl.glEnd();
		}
	}
	
	private void activateShader(RTShader shader, GL gl, GLU glu, double time){
		if(displayAsLines || shader == null)
		{
			gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
		}
		else
		{
			shader.apply(gl, glu, time);
		}
	}

}
