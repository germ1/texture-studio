package net.mgsx.numart.vivarium.logic;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO faire une classe template (Context)
public abstract class Behavior {

	// private static boolean debug = true;
	
	/**
	 * 
	 * une propriété subBehavior est une sous-machine
	 * à état.
	 * la classe est responsable de l'instanciation de cette machine
	 * mais la mise à jour de la machine (update) est automatisé.
	 * La mise à jour se fait bottom-top (enfants puis parent) 
	 * 
	 * @author mgsx
	 *
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	public @interface SubBehavior {
		public Class<? extends State> initState() default NullState.class;
	}
	
	public State state;
	
	private static Map<Class<?>, List<Behavior>> subBehaviorsRegistry = new HashMap<Class<?>, List<Behavior>>();
	private List<Behavior> subBehaviors;
	private List<Behavior> getSubBehaviors()
	{
		if(subBehaviors == null)
		{
			subBehaviors = subBehaviorsRegistry.get(this.getClass());
			if(subBehaviors == null)
			{
				List<Behavior> l = new ArrayList<Behavior>();
  				for(Field f : this.getClass().getFields())
				{
					SubBehavior a = f.getAnnotation(SubBehavior.class);
					if(a != null)
					{
						try {
							Behavior b = (Behavior)f.get(this);
							b.state = (State)a.initState().getConstructors()[0].newInstance(this);
							l.add(b);
						} catch (Exception e) {
							e.printStackTrace();
						} 
					}
				}
				subBehaviors = l;
			}
		}
		return subBehaviors;
	}
	
	public void update(Context ctx)
	{
		// set init state at begining
		if(state == null)
		{
			// TODO (1) find annotation on state (reference state class)
			state = initState();
			state = state.update(ctx);
		}
		
		// update child behaviors
		for(Behavior b : getSubBehaviors())
		{
			b.update(ctx);
		}
		
		// update this behavior
		state = state.update(ctx);
	}
	
	public boolean finalState()
	{
		return state instanceof FinalState;
	}
	
	private static class NullState implements State
	{
		@Override
		public State update(Context ctx) {
			return this;
		}
	}
	
	/**
	 * Utility state for stuck end state (doing nothing more)
	 */
	public static class DefaultFinalState implements FinalState
	{
		@Override
		public State update(Context ctx) {
			return this;
		}
	}
	
	// TODO use annotation (1)
	protected State initState()
	{
		return new NullState();
	}
}
