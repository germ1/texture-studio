package net.mgsx.numart.vivarium.logic;

import net.mgsx.numart.math.Box2D;

public class Food extends Behavior 
{
	public Box2D box = new Box2D();
	public double energy = 1;
	
	@Override
	protected State initState() {
		return new InitState();
	}
	
	private class InitState implements State
	{

		@Override
		public State update(Context ctx) {
			if(energy <= 0)
			{
				return new Behavior.DefaultFinalState();
			}
			return this;
		}
		
	}
}
