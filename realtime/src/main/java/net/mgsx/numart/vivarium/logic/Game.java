package net.mgsx.numart.vivarium.logic;

import net.mgsx.numart.math.Box2D;

public class Game extends Behavior 
{
	public PlayZone zoneA = new PlayZone();
	public PlayZone zoneB = new PlayZone();
	
	public Game() {
		zoneA.player.id = 0;
		zoneB.player.id = 1;
	}
	
	@Override
	protected State initState() {

		return new InitState();
	}
	
	
	
	private class InitState implements State
	{
		@Override
		public State update(Context ctx) {
			// make boxes
			Box2D gameBox = new Box2D(-1, -1, 2, 2);
			gameBox.reshape(ctx.cfg.gameBorder);
			
			Box2D leftBox = new Box2D();
			Box2D rightBox = new Box2D();
			gameBox.vSplit(0.5, leftBox, rightBox);
			leftBox.reshape(ctx.cfg.zoneBorder);
			rightBox.reshape(ctx.cfg.zoneBorder);
			zoneA.box = leftBox;
			zoneB.box = rightBox;
			
			updateZones(ctx);
			
			return new Waiting();
		}
		
	}
	private class Running implements State
	{
		@Override
		public State update(Context ctx) {
			if(zoneA.player.ready() && zoneB.player.ready())
			{
				updateZones(ctx);
				return this;
			}
			return new Running();
		}
		
	}
	public class Waiting implements State
	{
		@Override
		public State update(Context ctx) 
		{
			if(zoneA.player.ready() && zoneB.player.ready())
			{
				return new Running();
			}
			updateZones(ctx);
			return this;
		}
		
	}
	private void updateZones(Context ctx)
	{
		ctx.zone = zoneA;
		zoneA.update(ctx);
		ctx.zone = zoneB;
		zoneB.update(ctx);
	}
	
}
