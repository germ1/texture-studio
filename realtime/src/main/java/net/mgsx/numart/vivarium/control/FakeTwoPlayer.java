package net.mgsx.numart.vivarium.control;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.control.MultiPointer;
import net.mgsx.numart.framework.control.Pointer;
import net.mgsx.numart.framework.control.TwoPlayerBoxControl;
import net.mgsx.numart.framework.control.impl.MousePointer;

public class FakeTwoPlayer implements TwoPlayerBoxControl {

	Pointer pointer = new MousePointer();

	@ParamDouble
	public double w = 0.2;
	@ParamDouble
	public double h = 0.2;
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		pointer.update();
	}

	@Override
	public double getX(int player, int pointer) {
		switch(player)
		{
		case 0:
			switch(pointer)
			{
			case 0: return this.pointer.getX() - w;
			case 1: return this.pointer.getX() + w;
			}
		case 1:
			switch(pointer)
			{
			case 0: return 1 + this.pointer.getX() - w;
			case 1: return 1 + this.pointer.getX() + w;
			}
		}
		return 0;
	}

	@Override
	public double getY(int player, int pointer) {
		switch(player)
		{
		case 0:
			switch(pointer)
			{
			case 0: return this.pointer.getY() - h;
			case 1: return this.pointer.getY() + h;
			}
		case 1:
			switch(pointer)
			{
			case 0: return this.pointer.getY() - h;
			case 1: return this.pointer.getY() + h;
			}
		}
		return 0;
	}
	

}
