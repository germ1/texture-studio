package net.mgsx.numart.vivarium.logic;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.math.Box2D;
import net.mgsx.numart.math.Point2D;

public class Fish extends Behavior
{
	public Box2D box;
	public double energy;
	public int level;
	
	public void joinCenter(Context ctx)
	{
		// target zone center
		TargetingState ts = new TargetingState();
		ts.target = ctx.zone.box.center();
		ts.nextState = new DefaultFinalState();
		state = ts;
	}
	
	@Override
	protected State initState() {
		return new InitState();
	}
	
	public class InitState implements State
	{
		@Override
		public State update(Context ctx) {
			// energy = ctx.cfg.fishEnergy;
			box = new Box2D();
			box.w = ctx.cfg.fishSize;
			box.h = ctx.cfg.fishSize;
			box.x = ctx.zone.box.x + Math.random() * (ctx.zone.box.w - box.w);
			box.y = ctx.zone.box.y + (Math.random()) * 0.05;
			
			return new DecisionState();
		}
	}
	
	public class EatingState implements State
	{
		Food food;
		
		public EatingState(Food food) {
			super();
			this.food = food;
		}

		@Override
		public State update(Context ctx) 
		{
			// tant qu'il reste de la nourriture
			if(food.energy > 0)
			{
				// on mange (2^level)
				double eat = Math.min(
						food.energy, 
						Math.pow(2, level - 1) / ctx.cfg.fishEating // TODO param formula (old = level)
						);
				food.energy -= eat; 
				
				// on perd de l'energie
				energy -= 1 / ctx.cfg.fishPoison;
				
				return this;
			}
			return new DecisionState();
		}
	}
	public class TargetingState implements State
	{
		public Point2D target;
		public State nextState;
		@Override
		public State update(Context ctx) 
		{
			// calcul de la direction et distance de la cible.
			double dx = target.x - box.x;
			double dy = target.y - box.y;
			double len = Math.sqrt(dx*dx+dy*dy);
			
			// le niveau influ sur la vitesse
			double factor = Math.pow(ctx.cfg.fishSpeedLevel, level);
			
			double v = factor * ctx.cfg.fishSpeed * ctx.dtime;
			
			// si on dépasse la cible alors on est arrivé.
			if(v > len)
			{
				box.x = target.x;
				box.y = target.y;
				return nextState;
			}
			// sinon on se déplace vers la cible
			else
			{
				box.x += v * dx / len;
				box.y += v * dy / len;
			}
			return this;
		}
	}
	public class DecisionState implements State
	{
		@Override
		public State update(Context ctx) 
		{
			// recherche de la nourriture avoisinante (restante)
			List<Food> foodLeft = new ArrayList<Food>();
			for(Food food : ctx.zone.food)
			{
				if(food.energy>0)
				{
					foodLeft.add(food);
					double distance = food.box.distance(box);
					// mange la nourriture si à proximité
					if(distance < 0.01)
					{
						return new EatingState(food);
					}
				}
			}
			
			// s'il reste de la nourriture 
			if(!foodLeft.isEmpty())
			{
				// on prend une des nourriture restante au hazard
				Food food = foodLeft.get((int)(Math.random() * foodLeft.size()));
				double distance = food.box.distance(box);
				
				// si on est assez proche, on va vers la nourriture
				if(distance < 0.5)
				{
					TargetingState nextState = new TargetingState();
					nextState.nextState = new EatingState(food);
					nextState.target = food.box.center();
					return nextState;
				}
				
				// si on est loin, on va vers l'avant (x aléatoire)
				double newY = Math.min(ctx.zone.box.y + ctx.zone.box.h - box.h,
						box.y + ctx.zone.box.h * 0.2);
				Point2D target = new Point2D(
						ctx.zone.box.x + (ctx.zone.box.w - box.w) * Math.random(), 
						newY);
				TargetingState nextState = new TargetingState();
				nextState.nextState = new DecisionState();
				nextState.target = target;
				return nextState;
			}
			
			// s'il n'y a plus de nourriture, on va vers
			// un endroit au hazard dans la zone.
			Point2D target = new Point2D(
					ctx.zone.box.x + (ctx.zone.box.w - box.w) * Math.random(), 
					ctx.zone.box.y + (ctx.zone.box.h - box.h) * Math.random());
			TargetingState nextState = new TargetingState();
			nextState.nextState = new DecisionState();
			nextState.target = target;
			return nextState;
		}
	}
}
