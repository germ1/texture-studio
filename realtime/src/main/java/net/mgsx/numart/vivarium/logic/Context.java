package net.mgsx.numart.vivarium.logic;

import net.mgsx.numart.vivarium.VivariumModule;

public class Context 
{
	public double dtime;
	public Game game;
	public PlayZone zone;
	public VivariumModule cfg;
}
