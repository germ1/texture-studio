package net.mgsx.numart.realtime.geometry;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.realtime.RTGeometry;

import com.sun.opengl.util.BufferUtil;

public class RTPuzzle extends RTGeometry
{
	@ParamInt(min=1, max=10)
	public int gridX = 3;
	@ParamInt(min=1, max=10)
	public int gridY = 3;
	
	@ParamInt(min=1, max=10)
	public int cutSize = 3;
	
	@ParamDouble(min=0, max=1)
	public double randomness = 0.2;
	
	@ParamDouble
	public double ecartement = 0.1;
	
	@ParamBoolean
	public boolean clamp = true;
	
	protected FloatBuffer [] vertexBuffers;
	protected FloatBuffer [] texCoordBuffers;
	protected IntBuffer indexBuffer;
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		super.prepare(gl, glu);
		
		// build indexes
		
		indexBuffer = IntBuffer.allocate(2+cutSize*4);
		for(int i=0 ; i<1+cutSize*4 ; i++)
		{
			indexBuffer.put(i);
		}
		indexBuffer.put(1);
		indexBuffer.rewind();
		
		// build geometry
		
		vertexBuffers = new FloatBuffer[gridX * gridY];
		texCoordBuffers = new FloatBuffer[gridX * gridY];
		
		double dx = 1 / (double)gridX;
		double dy = 1 / (double)gridY;
		
		for(int gy=0 ; gy<gridY ; gy++)
		{
			double yCenter = ((double)gy + 0.5) / (double)(gridY);
			for(int gx=0 ; gx<gridX ; gx++)
			{
				double xCenter = ((double)gx + 0.5) / (double)(gridX);
				
				int gIndex = gy * gridX + gx;
				
				FloatBuffer vertexBuffer = vertexBuffers[gIndex] = BufferUtil.newFloatBuffer((1 + 4 * cutSize) * 3);
				FloatBuffer texCoordBuffer = texCoordBuffers[gIndex] = BufferUtil.newFloatBuffer((1 + 4 * cutSize) * 2);
				
				// creation du centre
				
				vertexBuffer.put(0, 0f);
				vertexBuffer.put(1, 0f);
				vertexBuffer.put(2, 0f);
				
				texCoordBuffer.put(0, (float)xCenter);
				texCoordBuffer.put(1, (float)yCenter);
				
				for(int c=0 ; c<cutSize ; c++)
				{
					int index;
					
					// partie haute
					index = 1 + cutSize * 0 + c;
					
					if(gy > 0)
					{
						// recopie de la partie basse du carré du haut
						copyPoint((gy - 1) * gridX + gx, gIndex, 1 + cutSize * 3 - c, index, 0, -dy);
					}
					else if(c == 0 && gx > 0)
					{
						copyPoint(gy * gridX + gx - 1, gIndex, 1 + cutSize * 1, index, -dx, 0);
					}
					else
					{
						// construction.
						createUPoint(gx, gy, c);
					}
					
					// partie droite : construction
					index = 1 + cutSize * 1 + c;
					if(c == 0 && gy > 0)
					{
						copyPoint((gy - 1) * gridX + gx, gIndex, 1 + cutSize * 2, index, 0, -dy);
					}
					else
					{
						createRPoint(gx, gy, c);
					}
					
					// partie basse : construction
					index = 1 + cutSize * 2 + c;
					createDPoint(gx, gy, c);
					
					// partie gauche
					index = 1 + cutSize * 3 + c;
					
					if(gx > 0)
					{
						// recopie la partie droite du carré de gauche
						copyPoint(gy * gridX + gx - 1, gIndex, 1 + cutSize * 2 - c, index, -dx, 0);
					}
					else
					{
						// construction.
						createLPoint(gx, gy, c);
					}
				}
				
				vertexBuffer.rewind();
				texCoordBuffer.rewind();
			}
		}
		
		while(vertexBuffers[0].hasRemaining())
		{
			System.out.println(vertexBuffers[0].get());
		}
		vertexBuffers[0].rewind();
	}
	
	private void copyPoint(int sgi, int tgi, int sbi, int tbi, double dx, double dy)
	{
		FloatBuffer sVB = vertexBuffers[sgi];
		FloatBuffer sTB = texCoordBuffers[sgi];
		FloatBuffer tVB = vertexBuffers[tgi];
		FloatBuffer tTB = texCoordBuffers[tgi];
		
		tVB.put(tbi * 3 + 0, sVB.get(sbi * 3 + 0) + (float)dx);
		tVB.put(tbi * 3 + 1, sVB.get(sbi * 3 + 1) + (float)dy);
		tVB.put(tbi * 3 + 2, sVB.get(sbi * 3 + 2));
		
		tTB.put(tbi * 2 + 0, sTB.get(sbi * 2 + 0));
		tTB.put(tbi * 2 + 1, sTB.get(sbi * 2 + 1));
		
	}
	
	private void createPoint(int gx, int gy, int index, double px, double py)
	{
		// centre
		double cx = ((double)gx + 0.5) / (double)gridX;
		double cy = ((double)gy + 0.5) / (double)gridY;
		
		int gIndex = gy * gridX + gx;
		FloatBuffer vertexBuffer = vertexBuffers[gIndex];
		FloatBuffer texCoordBuffer = texCoordBuffers[gIndex];
		
		vertexBuffer.put(index * 3 + 0, (float)(px - cx));
		vertexBuffer.put(index * 3 + 1, (float)(py - cy));
		vertexBuffer.put(index * 3 + 2, 0.f);

		texCoordBuffer.put(index * 2 + 0, (float)px);
		texCoordBuffer.put(index * 2 + 1, (float)py);
		
	}
	
	private double getRandom(boolean randomize)
	{
		if(clamp && !randomize)
		{
			return 0;
		}
		return Math.random() * randomness * 2 - 1;
	}
	
	private void createUPoint(int gx, int gy, int c)
	{
		int index = 1 + cutSize * 0 + c;
		
		// point
		double px = ((double)gx + (double)c / (double)cutSize) / (double)gridX;
		double py = ((double)gy + getRandom(gy > 0) / (double)cutSize) / (double)gridY;
		
		createPoint(gx, gy, index, px, py);
	}
	private void createRPoint(int gx, int gy, int c)
	{
		int index = 1 + cutSize * 1 + c;
		
		// point
		double px = ((double)gx + 1 + getRandom(gx < gridX-1) / (double)cutSize) / (double)gridX;
		double py = ((double)gy + (double)c / (double)cutSize) / (double)gridY;
		
		createPoint(gx, gy, index, px, py);
	}
	private void createDPoint(int gx, int gy, int c)
	{
		int index = 1 + cutSize * 2 + c;
		
		// point
		double px = ((double)gx + (double)(cutSize - c) / (double)cutSize) / (double)gridX;
		double py = ((double)gy + 1 + getRandom(gy < gridY-1) / (double)cutSize) / (double)gridY;
		
		createPoint(gx, gy, index, px, py);
	}
	private void createLPoint(int gx, int gy, int c)
	{
		int index = 1 + cutSize * 3 + c;
		
		// point
		double px = ((double)gx + getRandom(gx > 0) / (double)cutSize) / (double)gridX;
		double py = ((double)gy + (double)(cutSize - c) / (double)cutSize) / (double)gridY;
		
		createPoint(gx, gy, index, px, py);
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		
	    for(int i=0 ; i<gridX*gridY ; i++)
	    {
	    	int gx = i % gridX;
	    	int gy = i / gridX;
	    	
	    	gl.glPushMatrix();
	    	
	    	gl.glTranslated((double)gx * (1 + ecartement) / (double)gridX, (double)gy * (1 + ecartement) / (double)gridY, 0);
	    	
	    	applyElement(i, gl, glu, time);
	    	
	    	gl.glPopMatrix();
	    }
		
	}

	public void applyElement(int element, GL gl, GLU glu, double time) 
	{
		gl.glVertexPointer(3, GL.GL_FLOAT, 0, vertexBuffers[element]);
		gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, texCoordBuffers[element]);
		gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
	    gl.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY);
    	gl.glDrawElements(GL.GL_TRIANGLE_FAN, 2+cutSize*4, GL.GL_UNSIGNED_INT, indexBuffer);
	}

}
