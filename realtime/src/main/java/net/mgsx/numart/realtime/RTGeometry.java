package net.mgsx.numart.realtime;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Polar;
import net.mgsx.numart.realtime.animation.RTBasicRotation;
import net.mgsx.numart.realtime.material.RTBasicMaterial;
import net.mgsx.numart.realtime.shader.RTBasicLightShader;

abstract public class RTGeometry extends RTObject implements RTSequence
{
	
	@ParamGroup
	public RTAnimation animation;

	@ParamGroup
	public RTMaterial material;
	
	@ParamGroup
	public RTShader shader;
	
	@Logarithmic(10)
	@ParamPoint(min=0.01, max=100)
	public Point3D scale = new Point3D(1, 1, 1);
	
	@ParamBoolean
	public boolean wireframe = false;
	
	@DefaultFactory
	public void defaults()
	{
		animation = ReflectUtils.createDefault(RTBasicRotation.class);
		material = ReflectUtils.createDefault(RTBasicMaterial.class);
		shader = ReflectUtils.createDefault(RTBasicLightShader.class);
	}

	@Override
	public void prepare(GL gl, GLU glu) 
	{
		if(shader != null)
		{
			shader.prepare(gl, glu);
		}
		if(animation != null)
		{
			animation.prepare(gl, glu);
		}
		if(material != null)
		{
			material.prepare(gl, glu);
		}
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
//		gl.glEnable(GL.GL_FLAT);
//		gl.glShadeModel(GL.GL_FLAT);
//		gl.glEnable(GL.GL_AUTO_NORMAL);
//		 gl.glEnable( GL.GL_NORMALIZE );

		 
		 
		 gl.glTranslated(position.x, position.y, position.z);

		if(shader != null)
		{
			shader.apply(gl, glu, time);
		}
		if(animation != null)
		{
			animation.apply(gl, glu, time);
		}
		if(material != null)
		{
			material.apply(gl, glu, time);
		}
		gl.glScaled(scale.x, scale.y, scale.z);
		
		Polar pol = new Polar(rotation);
		gl.glRotated(pol.theta * 180 / Math.PI, 1, 0, 0);
		gl.glRotated(pol.phi * 180 / Math.PI, 0, 0, 1);
		
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, wireframe ? GL.GL_LINE : GL.GL_FILL );
		
	}
}
