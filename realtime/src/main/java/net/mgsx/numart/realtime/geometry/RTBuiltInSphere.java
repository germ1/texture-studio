package net.mgsx.numart.realtime.geometry;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamInt;

public class RTBuiltInSphere extends RTBuiltInGeometry {
	
	@ParamInt(min=1, max=100)
	public int radialSlices = 20;
	
	@ParamInt(min=1, max=100)
	public int verticalSlices = 20;
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
		glu.gluSphere(q, size, radialSlices, verticalSlices);
	}
}
