package net.mgsx.numart.realtime;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

public interface RTSequence 
{
	// TODO pass init context with gl, glu and glut ...
	void prepare(GL gl, GLU glu);
	
	// TODO pass render context with gl, glu, glut, diffrent time info, ...etc
	void apply(GL gl, GLU glu, double time);
}
