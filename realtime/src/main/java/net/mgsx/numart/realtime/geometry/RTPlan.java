package net.mgsx.numart.realtime.geometry;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.Noise;
import net.mgsx.numart.procedural.texture.util.ScalarUtils;
import net.mgsx.numart.realtime.RTGeometry;

import com.sun.opengl.util.BufferUtil;

public class RTPlan extends RTGeometry 
{
	@ParamInt(min=2, max=100)
	public int slicesU = 10;
	
	@ParamInt(min=2, max=100)
	public int slicesV = 10;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double height = 1;
	
	@ParamGroup
	public ScalarFromSpace displacementMap;
	
	private FloatBuffer vertexBuffer;
	private FloatBuffer normalBuffer;
	private FloatBuffer texCoordBuffer;
	private IntBuffer [] indexBuffer;
	
	@DefaultFactory
	public void defaults()
	{
		displacementMap = ReflectUtils.createDefault(Noise.class);
	}
	
	@Override
	public void prepare(GL gl, GLU glu) {
		// TODO Auto-generated method stub
		super.prepare(gl, glu);
		
		int size = slicesU * slicesV;
		vertexBuffer = BufferUtil.newFloatBuffer(size * 3);
		normalBuffer = BufferUtil.newFloatBuffer(size * 3);
		texCoordBuffer = BufferUtil.newFloatBuffer(size * 2);
		indexBuffer = new IntBuffer[slicesU - 1]; 
		
		for(int v=0 ; v<slicesV ; v++)
		{
			for(int u=0 ; u<slicesU ; u++)
			{
				double px = (double)u / (double)slicesU;
				double py = (double)v / (double)slicesV;
				double pz = 0.5;
				
				pz = height * (displacementMap.getValue(new Point3D(px, py, pz)) - 0.5);
				
				vertexBuffer.put((float)px);
				vertexBuffer.put((float)py);
				vertexBuffer.put((float)pz);
				
				double tu = (double)u / (double)slicesU;
				double tv = (double)v / (double)slicesV;

				texCoordBuffer.put((float)tu);
				texCoordBuffer.put((float)tv);
				
				Point3D normal = ScalarUtils.getNormal(displacementMap, new Point3D(0,0,1), new Point3D(px, py, pz), height, 0.5);
				normalBuffer.put((float)normal.x);
				normalBuffer.put((float)normal.y);
				normalBuffer.put((float)normal.z);
				
			}
		}
		for(int u=0 ; u<slicesU-1 ; u++)
		{
			indexBuffer[u] = BufferUtil.newIntBuffer(slicesV * 2);
			for(int v=0 ; v<slicesV ; v++)
			{
				indexBuffer[u].put((v * slicesU + u));
				indexBuffer[u].put((v * slicesU + u) + 1);
			}
			indexBuffer[u].rewind();
		}
		vertexBuffer.rewind();
		texCoordBuffer.rewind();
		normalBuffer.rewind();
		
		
		 gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
	     gl.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY);
	     gl.glEnableClientState(GL.GL_NORMAL_ARRAY);
		
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		// TODO Auto-generated method stub
		super.apply(gl, glu, time);
		gl.glTranslatef(-0.5f, -0.0f, -0.5f);
		
		// gl.glEnable(GL.GL_AUTO_NORMAL ); 
		
		gl.glRotatef(80, 1, 0, 0);
	//	gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL );
		
		gl.glVertexPointer(3, GL.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, texCoordBuffer);
		gl.glNormalPointer(GL.GL_FLOAT, 0, normalBuffer);
		for(int u=0 ; u< slicesU-1 ; u++)
		{
			gl.glDrawElements(GL.GL_TRIANGLE_STRIP, slicesV * 2, GL.GL_UNSIGNED_INT, indexBuffer[u]);
		}
	}
}
