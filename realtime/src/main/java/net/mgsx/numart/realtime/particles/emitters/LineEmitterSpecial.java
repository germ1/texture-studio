package net.mgsx.numart.realtime.particles.emitters;

import net.mgsx.numart.framework.ValueControl;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Particle;

public class LineEmitterSpecial extends LineEmitter 
{
	@ParamDouble
	public double sPosition;
	
	@ParamDouble
	public double sRange;
	
	@ParamGroup
	public ValueControl value;
	
	@Override
	protected void initParticlePosition(Particle p) 
	{
		double sr;
		if(value != null)
		{
			sr = value.getValue() / 2;
		}
		else
		{
			sr = sRange;
		}
		double rnd = Math.random();
		p.id = Math.abs(rnd - sPosition) < sr ? 1 : 0;
		double r = (rnd * 2 - 1) * radius;
		p.position.set(Point3D.add(position, Point3D.multiply(direction, r)));
	}
}
