package net.mgsx.numart.realtime.shader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.realtime.RTShader;

abstract public class RTProgramShader extends RTShader 
{
	protected int program;
	
	abstract protected String getVertexShaderSource(); 
	abstract protected String getFragmentShaderSource(); 
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		program = gl.glCreateProgram();
		
		int vertex = createShader(gl, GL.GL_VERTEX_SHADER, getVertexShaderSource());
		int fragment = createShader(gl, GL.GL_FRAGMENT_SHADER, getFragmentShaderSource());
		
		gl.glAttachShader(program, vertex);
		gl.glAttachShader(program, fragment);
		
		gl.glLinkProgram(program);
		
		gl.glUseProgram(program);
		
		System.out.println(getInfo(gl, vertex));
		System.out.println(getInfo(gl, fragment));
		System.out.println(getInfo(gl, program));
		
		// gl.glValidateProgram(program);
		
		
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		gl.glUseProgram(program);
	}
	
	private int createShader(GL gl, int shadertype, String sourceCode){
		int shader = gl.glCreateShader(shadertype);
		
		gl.glShaderSource(shader, 1, new String[]{sourceCode}, new int[]{sourceCode.length()}, 0);
		
		gl.glCompileShader(shader);
		
		return shader;
	}
	private String getInfo(GL gl, int shader){
		
		int [] lengthArray = new int[1];
		gl.glGetShaderiv(shader, GL.GL_INFO_LOG_LENGTH, lengthArray, 0);
		int length = lengthArray[0];
		
		if(length == 0){
			return "";
		}else{
			byte[] buffer = new byte[length];
			gl.glGetShaderInfoLog(shader, length, new int[]{length}, 0, buffer, 0);
			
			String info = new String(buffer).trim();
			return info;
		}
	}
	protected static String getShaderFromResource(String resName)
	{
		String s = "";
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(resName)));
			String line;
			while(null != (line = reader.readLine()))
			{
				s += line + "\n";
			}
			reader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return s;
	}

}
