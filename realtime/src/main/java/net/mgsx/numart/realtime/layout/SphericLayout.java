package net.mgsx.numart.realtime.layout;

import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.annotations.ParamLong;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Polar;
import net.mgsx.numart.realtime.RTGeometry;

public class SphericLayout extends RTGeometry 
{
	@ParamGroup
	public RTGeometry template;

	@ParamDouble(min=0, max=1000)
	public double radius;
	
	@ParamInt(min=1, max=100)
	public int replications;

	@ParamLong
	public long seed = 0xdeadbeef;
	
	@Override
	public void prepare(GL gl, GLU glu) {
		super.prepare(gl, glu);
		if(template != null)
		{
			template.prepare(gl, glu);
		}
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		
		if(template != null)
		{
			Random random = new Random(seed);
			
			// template.apply(gl, glu, time)
			
			for(int i=0 ; i<replications ; i++)
			{
				gl.glPushMatrix();
				
				double pu = random.nextDouble();
				double pv = random.nextDouble();
				
				Polar polar = new Polar(Math.PI * 2 *  pu, Math.PI * pv);
				
				Point3D p = polar.toUnitVector();
				
				p.multiply(radius/1000);
				
				
				gl.glRotated(pu * 360, 1, 0, 0);
				gl.glRotated(pv * 360, 0, 1, 0);
				gl.glTranslated(p.x, p.y, p.z);
				
				template.apply(gl, glu, time);
				
				gl.glPopMatrix();
			}
		}
	}
}
