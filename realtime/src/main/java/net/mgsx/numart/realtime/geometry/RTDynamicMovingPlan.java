package net.mgsx.numart.realtime.geometry;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.Noise;
import net.mgsx.numart.realtime.RTGeometry;
import net.mgsx.numart.realtime.util.TimeCounter;

import com.sun.opengl.util.BufferUtil;

public class RTDynamicMovingPlan extends RTGeometry 
{
	@ParamInt(min=2, max=100)
	public int slicesU = 10;
	
	@ParamInt(min=2, max=100)
	public int slicesV = 10;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double height = 1;
	
	@ParamGroup
	public ScalarFromSpace displacementMap;
	
	@ParamDouble(min=-5, max=5)
	public double spd = 1;
	
	@ParamDouble(min=-Math.PI, max=Math.PI)
	public double angle = 0;
	
	private TimeCounter counterU;
	private TimeCounter counterV;
	
	private FloatBuffer vertexBuffer;
	private FloatBuffer texCoordBuffer;
	private IntBuffer [] indexBuffer;
	
	@DefaultFactory
	public void defaults()
	{
		displacementMap = ReflectUtils.createDefault(Noise.class);
	}
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		super.prepare(gl, glu);
		
		counterU = new TimeCounter();
		counterV = new TimeCounter();
		
		int size = slicesU * slicesV;
		vertexBuffer = BufferUtil.newFloatBuffer(size * 3);
		texCoordBuffer = BufferUtil.newFloatBuffer(size * 2);
		indexBuffer = new IntBuffer[slicesU - 1]; 
		
		for(int v=0 ; v<slicesV ; v++)
		{
			for(int u=0 ; u<slicesU ; u++)
			{
				double px = (double)u / (double)slicesU;
				// XXX double py = Math.pow((double)v / (double)slicesV, 0.2);
				double py = (double)v / (double)slicesV;
				
				vertexBuffer.put((float)px - 0.5f);
				vertexBuffer.put((float)py - 0.5f);
				vertexBuffer.put(0.f);
				
				double tu = (double)u / (double)slicesU;
				double tv = (double)v / (double)slicesV;

				texCoordBuffer.put((float)tu);
				texCoordBuffer.put((float)tv);
				
			}
		}
		for(int u=0 ; u<slicesU-1 ; u++)
		{
			indexBuffer[u] = BufferUtil.newIntBuffer(slicesV * 2);
			for(int v=0 ; v<slicesV ; v++)
			{
				indexBuffer[u].put((v * slicesU + u));
				indexBuffer[u].put((v * slicesU + u) + 1);
			}
			indexBuffer[u].rewind();
		}
		vertexBuffer.rewind();
		texCoordBuffer.rewind();
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		
		double utime = counterU.update(spd * Math.cos(angle), time);
		double vtime = counterV.update(spd * Math.sin(angle), time);
		
		for(int v=0 ; v<slicesV ; v++)
		{
			for(int u=0 ; u<slicesU ; u++)
			{
				double px = vertexBuffer.get((v * slicesU + u) * 3) + utime;
				double py = vertexBuffer.get((v * slicesU + u) * 3 + 1) + vtime;
				double pz = 0.0;
				
				pz = height * (displacementMap.getValue(new Point3D(px, py, pz)) - 0.5);
				
				vertexBuffer.put((v * slicesU + u) * 3 + 2, (float)pz);
				
				texCoordBuffer.put((v * slicesU + u) * 2 + 0, (float)px);
				texCoordBuffer.put((v * slicesU + u) * 2 + 1, (float)py);
			}
		}

		gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
	     gl.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY);

	     
		gl.glVertexPointer(3, GL.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, texCoordBuffer);
		for(int u=0 ; u< slicesU-1 ; u++)
		{
			gl.glDrawElements(GL.GL_TRIANGLE_STRIP, slicesV * 2, GL.GL_UNSIGNED_INT, indexBuffer[u]);
		}
	}

}
