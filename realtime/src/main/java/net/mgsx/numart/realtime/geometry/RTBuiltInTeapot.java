package net.mgsx.numart.realtime.geometry;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamBoolean;

import com.sun.opengl.util.GLUT;

public class RTBuiltInTeapot extends RTBuiltInGeometry {
	
	private GLUT glut = new GLUT();
	
	@ParamBoolean
	public boolean status = true;
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
		glut.glutSolidTeapot(size, status);
	}
}
