package net.mgsx.numart.realtime.animation;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.realtime.RTAnimation;

public class TimeStretch extends RTAnimation {

	@ParamGroup
	public RTAnimation animation;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double speed = 1.0;
	
	@Override
	public void prepare(GL gl, GLU glu) {
		if(animation != null)
		{
			animation.prepare(gl, glu);
		}
	}

	@Override
	public void apply(GL gl, GLU glu, double time) {
		if(animation != null)
		{
			animation.apply(gl, glu, time * speed);
		}
	}

}
