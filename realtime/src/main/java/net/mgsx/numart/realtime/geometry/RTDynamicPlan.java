package net.mgsx.numart.realtime.geometry;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.Noise;
import net.mgsx.numart.realtime.RTGeometry;

import com.sun.opengl.util.BufferUtil;

public class RTDynamicPlan extends RTGeometry 
{
	@ParamInt(min=2, max=100)
	public int slicesU = 10;
	
	@ParamInt(min=2, max=100)
	public int slicesV = 10;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double height = 1;
	
	@ParamBoolean
	public boolean automapping = false;
	
	@ParamGroup
	public ScalarFromSpace displacementMap;
	
	private FloatBuffer vertexBuffer;
	private FloatBuffer texCoordBuffer;
	private IntBuffer [] indexBuffer;
	
	@DefaultFactory
	public void defaults()
	{
		displacementMap = ReflectUtils.createDefault(Noise.class);
	}
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		super.prepare(gl, glu);
		
		int size = slicesU * slicesV;
		vertexBuffer = BufferUtil.newFloatBuffer(size * 3);
		texCoordBuffer = BufferUtil.newFloatBuffer(size * 2);
		indexBuffer = new IntBuffer[slicesU - 1]; 
		
		for(int v=0 ; v<slicesV ; v++)
		{
			for(int u=0 ; u<slicesU ; u++)
			{
				double px = (double)u / (double)slicesU;
				double py = (double)v / (double)slicesV;
				
				vertexBuffer.put((float)px - 0.5f);
				vertexBuffer.put((float)py - 0.5f);
				vertexBuffer.put(0.f);
				
				double tu = (double)u / (double)slicesU;
				double tv = (double)v / (double)slicesV;

				texCoordBuffer.put((float)tu);
				texCoordBuffer.put((float)tv);
				
			}
		}
		for(int u=0 ; u<slicesU-1 ; u++)
		{
			indexBuffer[u] = BufferUtil.newIntBuffer(slicesV * 2);
			for(int v=0 ; v<slicesV ; v++)
			{
				indexBuffer[u].put((v * slicesU + u));
				indexBuffer[u].put((v * slicesU + u) + 1);
			}
			indexBuffer[u].rewind();
		}
		vertexBuffer.rewind();
		texCoordBuffer.rewind();
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		
		if(displacementMap instanceof NamedElement)
			((NamedElement)displacementMap).refresh();
		
		for(int v=0 ; v<slicesV ; v++)
		{
			for(int u=0 ; u<slicesU ; u++)
			{
				double px = vertexBuffer.get((v * slicesU + u) * 3);
				double py = vertexBuffer.get((v * slicesU + u) * 3 + 1);
				double pz = 0.0;
				
				pz = height * (displacementMap.getValue(new Point3D(px, py, pz)) - 0.5);
				
				vertexBuffer.put((v * slicesU + u) * 3 + 2, (float)pz);
			}
		}
		
		if(automapping)
		{
			for(int u=1 ; u<slicesU ; u++)
			{
				int index;
				
				index = u - 1;
				
				double pxPrevU = vertexBuffer.get(index * 3 + 0);
				double pyPrevU = vertexBuffer.get(index * 3 + 1);
				double pzPrevU = vertexBuffer.get(index * 3 + 2);
				
				double tuPrevU = texCoordBuffer.get(index * 2 + 0);
				double tvPrevU = texCoordBuffer.get(index * 2 + 1);

				index = u;
				
				double px = vertexBuffer.get(index * 3 + 0);
				double py = vertexBuffer.get(index * 3 + 1);
				double pz = vertexBuffer.get(index * 3 + 2);
				
				double dxu = px - pxPrevU;
				double dyu = py - pyPrevU;
				double dzu = pz - pzPrevU;
				
				double du = Math.sqrt(dxu * dxu + dyu * dyu + dzu * dzu);
				double tu = tuPrevU + (du);
				double tv = tvPrevU;
				
				texCoordBuffer.put(index * 2 + 0, (float)tu);
				texCoordBuffer.put(index * 2 + 1, (float)tv);
			}
			for(int v=1 ; v<slicesV ; v++)
			{
				for(int u=1 ; u<slicesU ; u++)
				{
					int index;
					
					index = v * slicesU + u - 1;
					
					double pxPrevU = vertexBuffer.get(index * 3 + 0);
					double pyPrevU = vertexBuffer.get(index * 3 + 1);
					double pzPrevU = vertexBuffer.get(index * 3 + 2);
					
					double tuPrevU = texCoordBuffer.get(index * 2 + 0);
					double tvPrevU = texCoordBuffer.get(index * 2 + 1);

					index = (v-1) * slicesU + u;
					
					double pxPrevV = vertexBuffer.get(index * 3 + 0);
					double pyPrevV = vertexBuffer.get(index * 3 + 1);
					double pzPrevV = vertexBuffer.get(index * 3 + 2);

					double tuPrevV = texCoordBuffer.get(index * 2 + 0);
					double tvPrevV = texCoordBuffer.get(index * 2 + 1);

					index = v * slicesU + u;
					
					double px = vertexBuffer.get(index * 3 + 0);
					double py = vertexBuffer.get(index * 3 + 1);
					double pz = vertexBuffer.get(index * 3 + 2);
					
					double dxu = px - pxPrevU;
					double dyu = py - pyPrevU;
					double dzu = pz - pzPrevU;
					
					double dxv = px - pxPrevV;
					double dyv = py - pyPrevV;
					double dzv = pz - pzPrevV;
					
					double du = Math.sqrt(dxu * dxu + dyu * dyu + dzu * dzu);
					double dv = Math.sqrt(dxv * dxv + dyv * dyv + dzv * dzv);
					
					double tu = 0.5 * (tuPrevU + tuPrevV + (du));
					double tv = 0.5 * (tvPrevU + tvPrevV + (dv));
					
//					double tu = 0.5 * (tuPrevU + tuPrevV + (du));
//					double tv = tvPrevV + dv ;

//					double tu = tuPrevU + (du);
//					double tv = tvPrevV + dv ;
					
					texCoordBuffer.put(index * 2 + 0, (float)tu);
					texCoordBuffer.put(index * 2 + 1, (float)tv);
				}
			}
		}

		gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
	     gl.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY);

	     
		gl.glVertexPointer(3, GL.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, texCoordBuffer);
		for(int u=0 ; u< slicesU-1 ; u++)
		{
			gl.glDrawElements(GL.GL_TRIANGLE_STRIP, slicesV * 2, GL.GL_UNSIGNED_INT, indexBuffer[u]);
		}
	}

}
