package net.mgsx.numart.realtime;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.framework.utils.ReflectUtils.Visitor;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.realtime.util.RTStats;
import net.mgsx.numart.realtime.util.RTVisualStats;
import net.mgsx.tools.ipc.LoopWorkflow;
import net.mgsx.tools.ipc.MultiResourceContext;

import com.sun.opengl.util.GLUT;

public class RTSceneRenderer extends NamedElement implements RTRenderer
{
	@ParamGroup
	public RTScene scene;

	@ParamColor
	public Color background = new Color(0);
	
	public GLU glu;
	
	public GLUT glut;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double timeStretch = 1.0;
	
	private long previousTime;
	
	private float time;
	
	@ParamBoolean
	public boolean displayStats = false;
	
	public RTStats stats;
	
	private MultiResourceContext collector;
	
	@DefaultFactory
	public void defaults()
	{
		scene = ReflectUtils.createDefault(RTScene.class);
	}

	private void initCollector()
	{
		collector = new MultiResourceContext();
		ReflectUtils.visitGraph(this, new Visitor() {
			
			@Override
			public void visit(Object object) {
				if(object instanceof LoopWorkflow)
				{
					collector.addProcess((LoopWorkflow)object);
				}
			}
		});
	}
	
	@Override
	public void init(GL gl) 
	{
		initCollector();
		
		stats = new RTVisualStats();
		glu = new GLU();
		glut = new GLUT();
		
		for(RTNode g : scene.geometries)
		{
			g.prepare(gl, glu);
		}
		if(scene.camera != null)
		{
			scene.camera.prepare(gl, glu);
		}
		if(stats instanceof RTSequence)
		{
			((RTSequence)stats).prepare(gl, glu);
		}
		
		previousTime = System.currentTimeMillis();
		time = 0;
	}

	
	
	@Override
	public void render(GL gl, int width, int height) 
	{
		// attente de toutes les ressources pour le rendu.
		collector.acquire();
		try
		{
			_render(gl, width, height);
		}
		finally
		{
			collector.release();
		}
	}
	private void _render(GL gl, int width, int height) 
	{
		
		if(displayStats && stats != null)
		{
			stats.beginRendering();
		}
		
		long currentTime = System.currentTimeMillis();
		long dtime = currentTime - previousTime;
		float fdtime = (float)((double)dtime * 0.001);
		previousTime = currentTime;
		time += fdtime * timeStretch;
		
		gl.glClearDepth(1.0f);
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glDepthFunc(GL.GL_LEQUAL);

		gl.glCullFace(GL.GL_FRONT);
		gl.glDisable(GL.GL_CULL_FACE);

		gl.glClearColor((float)background.r, (float)background.g, (float)background.b, (float)background.a);
		
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		
		gl.glShadeModel(GL.GL_SMOOTH); // TODO try FLAT for perf !
		
		if(scene.camera != null)
		{
			scene.camera.apply(gl, glu, time, width, height);
		}

		// lights.
		if(scene.lights.size() > 0)
		{
			gl.glEnable(GL.GL_LIGHTING);
			for(int i=0 ; i<8 && i<scene.lights.size() ; i++)
			{
				RTLight light = scene.lights.get(i);
				gl.glEnable(GL.GL_LIGHT0 + i);
				light.apply(gl, glu, time);
			}
			
		}
		else
		{
			gl.glDisable(GL.GL_LIGHTING);
		}
		
		// TODO mettre la couleur front ...
		// gl.glColor4f( 1.f,  1.f,  1.f, 1.f);
		
		for(RTNode g : scene.geometries)
		{
			gl.glPushMatrix();
			gl.glDisable(GL.GL_BLEND); // TODO xxx perf issue

			g.apply(gl, glu, time);
			gl.glPopMatrix();
		}
		
		if(displayStats && stats != null)
		{
			if(stats instanceof RTSequence)
			{
				((RTSequence)stats).apply(gl, glu, time);
			}
			stats.endRendering();
		}
	}

	@Override
	public void dispose() 
	{
		if(collector != null)
		{
			collector.finish();
		}
	}
	
	@Override
	public void destroy() {
		super.destroy();
		if(collector != null)
		{
			collector.finish();
		}
	}

	
}
