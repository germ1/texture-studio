package net.mgsx.numart.realtime.particles.deflectors;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Particle;

public class VortexDeflector implements Deflector 
{
	@ParamPoint(min=-1, max=1)
	public Point3D position = new Point3D();
	
	@ParamDouble(min=-5, max=5)
	public double force = 1;
	
	@Override
	public void update(Particle p, double deltaTime) 
	{
		Point3D d = Point3D.subtract(position, p.position);
		// TODO optimze
		double distance = d.length();
		if(distance != 0)
		{
			d.normalize();
			double gravity = 10 * force / distance;
			
			gravity = Math.min(gravity, 10 * force);
			
			p.force.add(Point3D.multiply(d, gravity));
		}
		
	}

}