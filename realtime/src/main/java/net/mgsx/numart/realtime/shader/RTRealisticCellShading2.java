package net.mgsx.numart.realtime.shader;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamDouble;



public class RTRealisticCellShading2 extends RTProgramShader 
{
	@ParamDouble
	public double mix = 0.5;
	private int mixVarLocation;
	
	@Override
	protected String getVertexShaderSource() {
		return getShaderFromResource("RealisticCellShading2.vertex");
	}

	@Override
	protected String getFragmentShaderSource() {
		return getShaderFromResource("RealisticCellShading2.frag");
	}
	
	@Override
	public void prepare(GL gl, GLU glu) {
		super.prepare(gl, glu);
		mixVarLocation = gl.glGetUniformLocation(program, "mix");
		
	}
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
		gl.glUniform1f(mixVarLocation, (float)mix);
	}
}
