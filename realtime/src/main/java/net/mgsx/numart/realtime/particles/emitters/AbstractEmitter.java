package net.mgsx.numart.realtime.particles.emitters;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.RTObject;
import net.mgsx.numart.realtime.particles.Emitter;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.particles.ParticleFactory;

public abstract class AbstractEmitter extends RTObject implements Emitter 
{
	@Logarithmic
	@ParamDouble(min=0.1, max=10000)
	public double frequency = 1;
	
	@Logarithmic
	@ParamDouble(min=0.001, max=1000)
	public double force = 1;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double lifeTime = 2;
	
	@ParamDouble
	public double sprayAngle = 0.1;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double sizeBase = 1;
	
	@ParamDouble
	public double sizeRandomness = 0.1;
	
	private double time = 0;
	
	@Override
	public void update(ParticleFactory factory, double deltaTime) 
	{
		time += deltaTime;
		while(time > 1. / frequency)
		{
			Particle p = factory.createParticle();
			p.lifeMax = p.life = lifeTime;
			
			initParticlePosition(p);
			
			p.speed.x = rotation.x * force + sprayAngle * (Math.random() - 0.5); // TODO faire une vraie equation
			p.speed.y = rotation.y * force + sprayAngle * (Math.random() - 0.5);
			p.speed.z = rotation.z * force + sprayAngle * (Math.random() - 0.5);
			p.angle = 0;
			p.size = sizeBase * (1 - sizeRandomness * (1 + Math.random()));
			time -= 1. / frequency;
		}
	}
	
	protected abstract void initParticlePosition(Particle p);

}
