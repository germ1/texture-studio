package net.mgsx.numart.realtime.particles.emitters;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Particle;

public class LineEmitter extends AbstractEmitter 
{
	@Logarithmic
	@ParamDouble(min=0.001, max=1000)
	public double radius = 1;
	
	@ParamVector
	public Point3D direction = new Point3D(1,0,0);
	
	@Override
	protected void initParticlePosition(Particle p) 
	{
		double r = (Math.random() * 2 - 1) * radius;
		p.position.set(Point3D.add(position, Point3D.multiply(direction, r)));
	}
}
