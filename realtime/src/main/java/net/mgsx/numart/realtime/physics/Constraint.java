package net.mgsx.numart.realtime.physics;

import net.mgsx.numart.realtime.particles.Particle;

public abstract class Constraint 
{
	double force;
	Particle pSlave;
	public Constraint(double force, Particle pSlave) {
		super();
		this.force = force;
		this.pSlave = pSlave;
	}
	
	abstract void apply(double deltaTime);
}
