package net.mgsx.numart.realtime.shader;

import net.mgsx.numart.framework.annotations.UserDoc;


@UserDoc
(
title="GLSL Basic Light Shader",
description="Use all basic material/light properties with a directionnal light"
)

public class RTBasicLightShader extends RTProgramShader 
{
	@Override
	protected String getVertexShaderSource() {
		return getShaderFromResource("basicLight.vertex");
	}

	@Override
	protected String getFragmentShaderSource() {
		return getShaderFromResource("basicLight.frag");
	}
	
}
