package net.mgsx.numart.realtime.layout;

import java.nio.FloatBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.realtime.MeshProvider;
import net.mgsx.numart.realtime.RTGeometry;

public class MeshLayout extends RTGeometry {

	@ParamGroup
	public RTGeometry template;
	
	@ParamGroup
	public MeshProvider mesh;
	
	@Override
	public void prepare(GL gl, GLU glu) {
		super.prepare(gl, glu);
		template.prepare(gl, glu);
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
		FloatBuffer vertex = mesh.getVertex();
		while(vertex.hasRemaining())
		{
			float x = vertex.get();
			float y = vertex.get();
			float z = vertex.get();
			gl.glPushMatrix();
			gl.glTranslatef(x, y, z);
			template.apply(gl, glu, time);
			gl.glPopMatrix();
		}
		vertex.rewind();
		
	}

}
