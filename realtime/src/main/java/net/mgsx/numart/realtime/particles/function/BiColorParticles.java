package net.mgsx.numart.realtime.particles.function;

import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.particles.ParticleColor;

public class BiColorParticles implements ParticleColor {

	@ParamColor
	public Color colorA;
	
	@ParamColor
	public Color colorB;
	
	@Override
	public void colorForParticle(Color result, Particle p) 
	{
		if(p.id > 0)
			result.set(colorB);
		else
			result.set(colorA);

	}

}
