package net.mgsx.numart.realtime.animation;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.RTAnimation;

public class RTBasicRotation extends RTAnimation {

	@ParamVector
	public Point3D orientation = new Point3D(0,0,1);
	
	@Override
	public void prepare(GL gl, GLU glu) {

	}

	@Override
	public void apply(GL gl, GLU glu, double time) {
		gl.glRotated(time * 180.0 / Math.PI, orientation.x, orientation.y, orientation.z);
	}

}
