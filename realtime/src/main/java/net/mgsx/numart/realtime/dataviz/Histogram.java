package net.mgsx.numart.realtime.dataviz;

import net.mgsx.numart.math.Color;

public class Histogram 
{
	public static class Axis
	{
		public String name;
		public Color color;
	}
	
	public Color color;

	public double min;
	public double max;
	
	public Axis xAxis;
	public Axis yAxis;
	
	public double [] values;
	private int count;
	
	private int position;
	
	public Histogram(int size) {
		values = new double [size];
		count = 0;
		position = 0;
		xAxis = new Axis();
		yAxis = new Axis();
	}
	
	public void push(double value)
	{
		if(count < values.length)
		{
			count++;
		}
		values[position % values.length] = value;
		position++;
	}
	
	public int size()
	{
		return count;
	}
	public int capacity()
	{
		return values.length;
	}
	public double get(int index)
	{
		return values[((position - index - 1) % values.length + values.length) % values.length];
	}
	
	
}
