package net.mgsx.numart.realtime.shader;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.realtime.RTTexture;
import net.mgsx.numart.realtime.RTTexture3D;
import net.mgsx.numart.realtime.texture.RTSoftwareTexture;
import net.mgsx.numart.realtime.texture.RTSoftwareTexture3D;


public class RTVolumeShader extends RTProgramShader 
{
	
	@ParamGroup
	public RTTexture textureA;
	
	@ParamGroup
	public RTTexture3D textureB;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=30)
	public double scale = 1;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=30)
	public double amplitude = 0.3;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=30)
	public double speed = 1.0;
	
	private int timeVarLocation;
	private int scaleVarLocation;
	private int amplitudeVarLocation;
	
	@DefaultFactory
	public void defaults()
	{
		textureA = ReflectUtils.createDefault(RTSoftwareTexture.class);
		textureB = ReflectUtils.createDefault(RTSoftwareTexture3D.class);
	}
	
	@Override
	protected String getVertexShaderSource() {
		return getShaderFromResource("volume.vertex");
	}

	@Override
	protected String getFragmentShaderSource() {
		return getShaderFromResource("volume.frag");
	}
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		gl.glActiveTexture(GL.GL_TEXTURE0);
		textureA.prepare(gl, glu);
		gl.glActiveTexture(GL.GL_TEXTURE1);
		textureB.prepare(gl, glu);
		super.prepare(gl, glu);
		timeVarLocation = gl.glGetUniformLocation(program, "time");
		scaleVarLocation = gl.glGetUniformLocation(program, "scale");
		amplitudeVarLocation = gl.glGetUniformLocation(program, "amplitude");
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
		
		// gl.glClientActiveTexture(GL.GL_TEXTURE0);
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glActiveTexture(GL.GL_TEXTURE0);
		textureA.apply(gl, glu, time * speed);
		gl.glActiveTexture(GL.GL_TEXTURE1);
		textureB.apply(gl, glu, time * speed);
		
		gl.glUniform1f(timeVarLocation, (float)(time * speed));
		gl.glUniform1f(scaleVarLocation, (float)scale);
		gl.glUniform1f(amplitudeVarLocation, (float)amplitude);
	}
	
}
