package net.mgsx.numart.realtime.texture;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.procedural.renderer.Rasterizer3D;
import net.mgsx.numart.procedural.renderer.Texture3DRenderer;
import net.mgsx.numart.realtime.RTTexture3D;

// TODO make luminenceMap from Luminence3DRenderer ...
public class RTSoftwareTexture3D extends NamedElement implements RTTexture3D
{
	@ParamGroup
	public Texture3DRenderer texture;

	@Logarithmic(2)
	@ParamInt(min=3, max=12)
	public int texturePower = 4;
	
	private int glTexture;
	
	@DefaultFactory
	public void defaults()
	{
		texture = ReflectUtils.createDefault(Texture3DRenderer.class);
	}
	public RTSoftwareTexture3D() {
	}
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		final int textureSize = 1 << texturePower;
		final int width = textureSize;
		final int height = textureSize;
		final int depth = textureSize;
		final ByteBuffer pixels = ByteBuffer.allocateDirect(width * height * depth * 4);
		pixels.order(ByteOrder.nativeOrder());
		
		Rasterizer3D rasterizer = new Rasterizer3D() {
			@Override
			public void setPixel(int x, int y, int z, Color color) {
				int index = (((z * height + (height - 1 - y)) * width + x)) * 4;
				pixels.put(index + 0, RTSoftwareTexture.colorToByte(color.r));
				pixels.put(index + 1, RTSoftwareTexture.colorToByte(color.g));
				pixels.put(index + 2, RTSoftwareTexture.colorToByte(color.b));
				pixels.put(index + 3, RTSoftwareTexture.colorToByte(color.a));
			}
		};
		
		texture.render(rasterizer, width, height, depth, 0, 0, width, height, 0, depth);
		
//		System.out.println("max texture unit = " + RTSoftwareTexture.getInt(gl, GL.GL_MAX_TEXTURE_UNITS));
//		System.out.println("texture 3D = " + RTSoftwareTexture.getInt(gl, GL.GL_MAX_3D_TEXTURE_SIZE));
//		System.out.println("texture 2D = " + RTSoftwareTexture.getInt(gl, GL.GL_MAX_TEXTURE_SIZE));
//		
		
		glTexture = RTSoftwareTexture.genTexture(gl);
		RTSoftwareTexture.checkError(gl);
		
		gl.glBindTexture(GL.GL_TEXTURE_3D, glTexture);
		RTSoftwareTexture.checkError(gl);
		
		gl.glEnable(GL.GL_TEXTURE_3D);
		RTSoftwareTexture.checkError(gl);
		
		gl.glTexParameteri(GL.GL_TEXTURE_3D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
	    gl.glTexParameteri(GL.GL_TEXTURE_3D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
	    gl.glTexParameteri(GL.GL_TEXTURE_3D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT);
		gl.glTexParameteri(GL.GL_TEXTURE_3D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT);
		gl.glTexParameteri(GL.GL_TEXTURE_3D, GL.GL_TEXTURE_WRAP_R, GL.GL_REPEAT);
			
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE);
		gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 4);
		pixels.position(0);
		pixels.mark();
		
		RTSoftwareTexture.checkError(gl);
		
		gl.glTexImage3D(GL.GL_TEXTURE_3D, 0, GL.GL_RGBA, width, 
                height, depth, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, pixels);
		
		RTSoftwareTexture.checkError(gl);
	}
	

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		gl.glEnable(GL.GL_TEXTURE_3D);
		gl.glBindTexture(GL.GL_TEXTURE_3D, glTexture);
	}
	
}
