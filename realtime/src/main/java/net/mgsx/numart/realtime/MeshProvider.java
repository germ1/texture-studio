package net.mgsx.numart.realtime;

import java.nio.FloatBuffer;

public interface MeshProvider {

	FloatBuffer getVertex();
	FloatBuffer getNormals();
}
