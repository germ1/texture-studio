package net.mgsx.numart.realtime.particles.emitters;

import net.mgsx.numart.realtime.particles.Particle;

public class SprayEmitter extends AbstractEmitter
{
	@Override
	protected void initParticlePosition(Particle p) 
	{
		p.position.x = position.x;
		p.position.y = position.y;
		p.position.z = position.z;
	}

}
