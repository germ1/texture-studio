package net.mgsx.numart.realtime.geometry;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamList;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.realtime.RTGeometry;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Emitter;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.particles.ParticleColor;
import net.mgsx.numart.realtime.particles.ParticleSystem;
import net.mgsx.numart.realtime.particles.deflectors.GravityDeflector;
import net.mgsx.numart.realtime.particles.emitters.SprayEmitter;
import net.mgsx.numart.realtime.particles.function.FireLifecycle;

public class RTParticles extends RTGeometry
{
	@ParamList
	public List<Emitter> emitters = new ArrayList<Emitter>();
	
	@ParamList
	public List<Deflector> deflectors = new ArrayList<Deflector>();
	
	public ParticleSystem system;
	
	@ParamBoolean
	public boolean pointMode = false;
	
	@ParamBoolean
	public boolean colorMod = true;
	
	@ParamBoolean
	public boolean alphaMod = true;
	
	@ParamGroup
	public ParticleColor colorFunction = null;
	
	@DefaultFactory
	public void defaults()
	{
		emitters.add(ReflectUtils.createDefault(SprayEmitter.class));
		deflectors.add(ReflectUtils.createDefault(GravityDeflector.class));
		colorFunction = new FireLifecycle();
	}
	
	@Override
	public void prepare(GL gl, GLU glu) {
		super.prepare(gl, glu);
		ptime = 0;
		system = new ParticleSystem(1000); // TODO const
	}
	
	private double ptime = 0;
	
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		
		 // TODO compute delta time earlier (context) ...
		double dtime = time == ptime ? 1. / 60. : time - ptime;
		ptime = time;
		
		// XXX temporaire
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glDisable(GL.GL_DEPTH_TEST);
		
		gl.glEnable( GL.GL_COLOR_MATERIAL );
		
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);
		
		gl.glEnable(GL.GL_TEXTURE);
		
		system.update(emitters, deflectors, dtime);
		
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_COMBINE);
		
		Color pColor = new Color(1,1,1,1);
		
		for(Particle p : system)
		{
			double factorAlpha = alphaMod ? MathUtil.envelopAR(1 - p.life / p.lifeMax, 0.1, 0.8, 1, 1) : 1; // TODO params
			
			if(colorMod && colorFunction != null)
				colorFunction.colorForParticle(pColor, p);
				
			gl.glColor4d(pColor.r,pColor.g,pColor.b, factorAlpha);
			
			if(pointMode)
			{
				gl.glBegin(GL.GL_POINT);
				gl.glTexCoord2f(0.5f, 0.5f);
				gl.glVertex3d(p.position.x, p.position.y, p.position.z);
				gl.glEnd();
			}
			else
			{
				double dx = (Math.cos(p.angle) + Math.sin(p.angle)) * p.size;
				double dy = (Math.sin(p.angle) - Math.cos(p.angle)) * p.size;
				
				gl.glBegin(GL.GL_TRIANGLE_STRIP);
				gl.glTexCoord2i(0, 0);
				gl.glVertex3d(p.position.x - dx, p.position.y + dy, p.position.z);
				gl.glTexCoord2i(1, 0);
				gl.glVertex3d(p.position.x + dx, p.position.y + dy, p.position.z);
				gl.glTexCoord2i(0, 1);
				gl.glVertex3d(p.position.x - dx, p.position.y - dy, p.position.z);
				gl.glTexCoord2i(1, 1);
				gl.glVertex3d(p.position.x + dx, p.position.y - dy, p.position.z);
				gl.glEnd();
			}
		}
	}
}
