package net.mgsx.numart.realtime.util;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.realtime.RTSequence;

public class RTVisualStats implements RTStats, RTSequence {

	private static class STAT{double fps, cpu;}
	
	private static int MAX_BUFFER_SIZE = 100;

	private STAT [] buffer;
	private int bufferSize;

	private long beginRenderTime;
	private long endRenderTime;
	
	private int renderCount;
	
	@Override
	public void prepare(GL gl, GLU glu) {
		buffer = new STAT [MAX_BUFFER_SIZE];
		renderCount = 0;
		bufferSize = 0;
	}

	private void resetState(GL gl, GLU glu)
	{
		gl.glUseProgram(0);
		
//		gl.glEnable( GL.GL_COLOR_MATERIAL );
//		gl.glColorMaterial( GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE );	
//		
//		gl.glDisable(GL.GL_TEXTURE);
//		gl.glDisable(GL.GL_LIGHTING);
		gl.glBindTexture(GL.GL_TEXTURE0, 0);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		resetState(gl, glu);
		
		final double wRatio =  1. / MAX_BUFFER_SIZE;
		final double hRatio = 1. / 100.;
		
		gl.glLineWidth(1);
		
		gl.glTranslated(-0.95, -0.95, 0);
		gl.glScaled(0.2, 0.2, 0.2);
		
		gl.glColor3f(1,1,1);
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex2d(0, 0);
		gl.glVertex2d(0, 1);
		gl.glVertex2d(2, 1);
		gl.glVertex2d(2, 0);
		gl.glEnd();
		gl.glBegin(GL.GL_LINES);
		gl.glVertex2d(1, 0);
		gl.glVertex2d(1, 1);
		gl.glEnd();
		
		gl.glBegin(GL.GL_LINE_STRIP);
		int size = Math.min(bufferSize, MAX_BUFFER_SIZE);
		for(int i=0 ; i<size ; i++)
		{
			STAT value = buffer[(bufferSize - size + i) % MAX_BUFFER_SIZE];
			//gl.glVertex2d((double)i * wRatio, 0);
			double v = value.fps; // MathUtil.limit(value.fps / 100) * 100;
			gl.glVertex3d((double)i * wRatio, v * hRatio, 0);
		}
		gl.glEnd();
		
		gl.glColor3f(1,1,0);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex2d(0, 0.6);
		gl.glVertex2d(1, 0.6);
		gl.glEnd();
		
		gl.glTranslated(1, 0, 0);
		gl.glBegin(GL.GL_LINE_STRIP);
		for(int i=0 ; i<size ; i++)
		{
			STAT value = buffer[(bufferSize - 1 - i) % MAX_BUFFER_SIZE];
			//gl.glVertex2d((double)i * wRatio, 0);
			double v = value.cpu; // MathUtil.limit(value.cpu);
			gl.glVertex3d((double)i * wRatio, v * hRatio, 0);
		}
		gl.glEnd();
		
		
//		gl.glWindowPos2d(10, 20);
		
		// glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, DISPLAYED_TEXT);
//		String strFPS = Double.toString(Math.round(fpsAverage * 10) / 10);
//		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, strFPS + " fps");
		
	}

	@Override
	public void beginRendering() 
	{
		// compute FPS
		if(renderCount > 0)
		{
			long prev = beginRenderTime;
			beginRenderTime = System.currentTimeMillis();
			STAT stat = buffer[(bufferSize - 1) % MAX_BUFFER_SIZE];
			long timeMS = beginRenderTime - prev;
			double time = (double)timeMS * 0.001;
			stat.fps = 1.0 / time;
			// System.out.println(stat.fps);
		}
		else
		{
			beginRenderTime = System.currentTimeMillis();
		}
	}

	@Override
	public void endRendering() {
		
		long prev = endRenderTime;
		
		endRenderTime = System.currentTimeMillis();
		renderCount++;
		
		long globalTimeMS = endRenderTime - prev;
		double globalTime = (double)globalTimeMS * 0.001;
		
		long renderTimeMS = endRenderTime - beginRenderTime;
		double renderTime = (double)renderTimeMS * 0.001;
		double cpu = renderTime / globalTime;
		
		// compute cpu
		STAT stat;
		if(bufferSize >= MAX_BUFFER_SIZE)
		{
			stat = buffer[bufferSize % MAX_BUFFER_SIZE];
		}
		else
		{
			stat = new STAT();
			buffer[bufferSize] = stat;
		}
		bufferSize++;
		stat.cpu = cpu;
	}

}
