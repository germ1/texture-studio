package net.mgsx.numart.realtime;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamDouble;


public class RTCamera extends NamedElement {

	@ParamBoolean
	public boolean orthographic = false;
	
	@Logarithmic(10)
	@ParamDouble(min=0.001, max=1000)
	public double near = 0.001;
	
	@Logarithmic(10)
	@ParamDouble(min=0.001, max=1000)
	public double far = 10.0;
	
	@ParamDouble(min=0, max=Math.PI)
	public double fov = Math.PI/4;
	
	public void prepare(GL gl, GLU glu) {
	}
	public void apply(GL gl, GLU glu, double time, int width, int height) {
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		if(orthographic)
		{
			glu.gluOrtho2D(0,0,-width,-height);
			gl.glMatrixMode(GL.GL_MODELVIEW);
			gl.glLoadIdentity();
			gl.glScaled(1,(double)width/(double)height,-1);
		}
		else
		{
			gl.glViewport(0,0,width,height);
			glu.gluPerspective(fov * 180 / Math.PI, (double)width/(double)height, 0.1 * near, 0.1 * far);
			
			gl.glMatrixMode(GL.GL_MODELVIEW);
			gl.glLoadIdentity();
			double s = 0.001;
			gl.glScaled(s,s,s);
			gl.glTranslated(0, 0, -3);
		}
	}
	
	
	
}
