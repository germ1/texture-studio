package net.mgsx.numart.realtime;

import javax.media.opengl.GL;

import net.mgsx.numart.framework.Renderer;

public interface RTRenderer extends Renderer {

	void init(GL gl);
	void render(GL gl, int width, int height);
	void dispose();
}
