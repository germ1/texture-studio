package net.mgsx.numart.realtime;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Point3D;

public abstract class RTObject extends RTNode 
{
	@ParamPoint(min=-10, max=10)
	public Point3D position = new Point3D(0, 0, 0);
	
	@ParamVector
	public Point3D rotation = new Point3D(0, 1, 0);
	
	@Override
	public void prepare(GL gl, GLU glu) {
	}
	@Override
	public void apply(GL gl, GLU glu, double time) {
	}
}
