package net.mgsx.numart.realtime.particles;

import net.mgsx.numart.math.Point3D;

public class Particle
{
	public double life;
	public double lifeMax;
	public Point3D position = new Point3D();
	public Point3D speed = new Point3D();
	public Point3D force = new Point3D();
	public double angle;
	public double size;
	public int id;
}