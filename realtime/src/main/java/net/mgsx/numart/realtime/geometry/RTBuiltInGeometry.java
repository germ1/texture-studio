package net.mgsx.numart.realtime.geometry;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.realtime.RTGeometry;

public class RTBuiltInGeometry extends RTGeometry {

	@Logarithmic
	@ParamDouble(min=0.01, max=100)
	public double size = 1;
	
	protected GLUquadric q;
	
	@DefaultFactory
	public void defaults()
	{
		super.defaults();
		size = 0.8;
	}
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		super.prepare(gl, glu);
		q = glu.gluNewQuadric();
		glu.gluQuadricNormals(q, GLU.GLU_EXTERIOR);
		glu.gluQuadricTexture(q, true);
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		glu.gluQuadricDrawStyle(q, GLU.GLU_FILL);
		
		
		// glu.gluQuadricDrawStyle(q, GLU.GLU_POINT);
		// TODO switch to built-in geometry ...
		// glu.glu(q, size, 20, 20);
		// new GLUT().glutSolidTeapot(size);
		//glu.gluSphere(q, size, 20, 20);
		// gl.glScaled(size, size, size);
		// new GLUT().glutSolidDodecahedron(); //size);
		// glu.glu
	}
}
