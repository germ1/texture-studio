package net.mgsx.numart.realtime.particles;

import java.util.Iterator;
import java.util.List;

import net.mgsx.numart.math.Point3D;


public class ParticleSystem implements Iterable<Particle>, ParticleFactory
{
	private Particle [] particles;
	
	private int aliveParticles;

	private int capacity;
	
	private ParticleFactory factory;
	
	public ParticleSystem(int capacity) 
	{
		this(capacity, new ParticleFactory() {
			@Override
			public Particle createParticle() {
				return new Particle();
			}
		});
	}
	public int size()
	{
		return aliveParticles;
	}
	public ParticleSystem(int capacity, ParticleFactory factory) 
	{
		this.factory = factory;
		this.capacity = capacity;
		particles = new Particle [capacity];
		for(int i=0 ; i<particles.length ; i++)
		{
			particles[i] = factory.createParticle();
		}
		aliveParticles = 0;
	}
	
	public void update(List<Emitter> emitters, List<Deflector> deflectors, double deltaTime)
	{
		if(emitters != null)
		{
			for(Emitter emitter : emitters)
			{
				emitter.update(this, deltaTime);
			}
		}
		for(int i=0 ; i<aliveParticles ; )
		{
			Particle p = particles[i];
			p.force.reset();
			
			if(deflectors != null)
			{
				for(Deflector d : deflectors)
				{
					d.update(p, deltaTime);
				}
			}
			p.speed.add(Point3D.multiply(p.force, deltaTime));
			p.position.add(Point3D.multiply(p.speed, deltaTime));
			
			p.life -= deltaTime;
			
			if(p.life <= 0)
			{
				if(i < aliveParticles-1)
				{
					particles[i] = particles[aliveParticles-1];
					particles[aliveParticles-1] = p;
				}
				aliveParticles--;
			}
			else
			{
				i++;
			}
		}
	}
	
	@Override
	public Particle createParticle()
	{
		if(aliveParticles >= particles.length)
		{
			Particle [] tmp = new Particle [particles.length + capacity];
			int i=0;
			for(i=0 ; i<particles.length ; i++)
			{
				tmp[i] = particles[i];
			}
			for( ; i<tmp.length ; i++)
			{
				tmp[i] = factory.createParticle();
			}
			particles = tmp;
		}
		Particle p = particles[aliveParticles];
		aliveParticles++;
		return p;
	}

	@Override
	public Iterator<Particle> iterator() 
	{
		return new Iterator<Particle>() {
			private int index = 0;
			@Override
			public boolean hasNext() {
				return index < aliveParticles;
			}

			@Override
			public Particle next() {
				return particles[index++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}
}
