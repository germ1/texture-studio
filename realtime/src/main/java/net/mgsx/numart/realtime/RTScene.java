package net.mgsx.numart.realtime;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamList;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.realtime.geometry.RTBuiltInTeapot;
import net.mgsx.numart.realtime.light.RTDirectionnalLight;

public class RTScene 
{
	@DefaultFactory
	public void defaults()
	{
		geometries.add(ReflectUtils.createDefault(RTBuiltInTeapot.class));
		lights.add(ReflectUtils.createDefault(RTDirectionnalLight.class));
		camera = ReflectUtils.createDefault(RTCamera.class);
	}
	@ParamList
	public List<RTNode> geometries = new ArrayList<RTNode>();
	
	@ParamList
	public List<RTLight> lights = new ArrayList<RTLight>();
	
	@ParamGroup
	public RTCamera camera;
}
