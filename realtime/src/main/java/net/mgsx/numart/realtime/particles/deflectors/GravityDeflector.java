package net.mgsx.numart.realtime.particles.deflectors;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Particle;

public class GravityDeflector implements Deflector {

	@ParamVector
	public Point3D orientation = new Point3D(0,-1,0);
	
	@Logarithmic(10)
	@ParamDouble(min=0.0001, max=100)
	public double gravity = 0.1;
	
	@Override
	public void update(Particle p, double deltaTime) 
	{
		p.force.add(Point3D.multiply(orientation, gravity));
	}

}
