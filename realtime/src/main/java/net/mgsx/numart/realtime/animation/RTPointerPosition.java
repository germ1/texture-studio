package net.mgsx.numart.realtime.animation;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.control.Pointer;
import net.mgsx.numart.realtime.RTAnimation;

public class RTPointerPosition extends RTAnimation
{
	@ParamGroup
	public Pointer pointer;
	
	@Override
	public void prepare(GL gl, GLU glu) {
	}

	@Override
	public void apply(GL gl, GLU glu, double time) {
		pointer.update();
		gl.glTranslated(pointer.getX(), pointer.getY(), 0);
	}

}
