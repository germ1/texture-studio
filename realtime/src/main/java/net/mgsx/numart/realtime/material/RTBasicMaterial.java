package net.mgsx.numart.realtime.material;

import java.nio.FloatBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.realtime.RTMaterial;

public class RTBasicMaterial extends RTMaterial {

	@ParamColor
	public Color ambient = new Color(0.2);
	
	@ParamColor
	public Color diffuse = new Color(0.7);
	
	@ParamColor
	public Color specular = new Color(1.0);
	
	@ParamColor
	public Color emissive = new Color(0.0);
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=1000)
	public double shininess = 30.0;
	
	@Override
	public void prepare(GL gl, GLU glu) {

	}

	@Override
	public void apply(GL gl, GLU glu, double time) {
		gl.glEnable( GL.GL_COLOR_MATERIAL );
        gl.glColorMaterial( GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE );		   
        gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, FloatBuffer.wrap(ambient.toFloat()));
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, FloatBuffer.wrap(diffuse.toFloat()));
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, FloatBuffer.wrap(specular.toFloat()));
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, FloatBuffer.wrap(new float[]{ (float)shininess }));
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, FloatBuffer.wrap(emissive.toFloat()));

	}

}
