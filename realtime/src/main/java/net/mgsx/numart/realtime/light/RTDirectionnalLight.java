package net.mgsx.numart.realtime.light;

import java.nio.FloatBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.framework.annotations.ParamVector;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.math.Polar;
import net.mgsx.numart.realtime.RTLight;

public class RTDirectionnalLight extends RTLight {

	@ParamColor
	public Color ambient = new Color(0.2);
	@ParamColor
	public Color diffuse = new Color(0.7);
	@ParamColor
	public Color specular = new Color(1.0);
	@ParamVector
	public Point3D direction = new Polar(0, 0).toUnitVector();
	@ParamPoint(min=-100,max=100)
	public Point3D position = new Polar(0, 0).toUnitVector();
	
	@DefaultFactory
	public void defaults()
	{
	}
	
	@Override
	public void prepare(GL gl, GLU glu) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void apply(GL gl, GLU glu, double time) {
		
		gl.glLightModeli(GL.GL_LIGHT_MODEL_LOCAL_VIEWER, GL.GL_TRUE);
		
		gl.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, FloatBuffer.wrap(ambient.toFloat()));
		gl.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, FloatBuffer.wrap(diffuse.toFloat()));
		gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, FloatBuffer.wrap(specular.toFloat()));
		gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, FloatBuffer.wrap(new float[]{(float)direction.x, (float)direction.y, (float)direction.z, 0}));
//		gl.glLightf(GL.GL_LIGHT0, GL.GL_CONSTANT_ATTENUATION, 0);
//		gl.glLightf(GL.GL_LIGHT0, GL.GL_LINEAR_ATTENUATION, 2);
//		gl.glLightf(GL.GL_LIGHT0, GL.GL_QUADRATIC_ATTENUATION, 3);

//		gl.glLightf(GL.GL_LIGHT0, GL.GL_SPOT_CUTOFF, 45.0f);
//		gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPOT_DIRECTION, FloatBuffer.wrap(direction.toFloat()));
//		gl.glLightf(GL.GL_LIGHT0, GL.GL_SPOT_EXPONENT, 2.0f);		
	}
}
