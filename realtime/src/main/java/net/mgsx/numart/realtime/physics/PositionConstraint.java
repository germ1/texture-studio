package net.mgsx.numart.realtime.physics;

import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Particle;

public class PositionConstraint extends Constraint 
{
	Point3D targetPosition;
	
	public PositionConstraint(double force, Particle pSlave,
			Point3D targetPosition) {
		super(force, pSlave);
		this.targetPosition = targetPosition;
	}

	@Override
	void apply(double deltaTime) {
		// TODO Auto-generated method stub
		Particle ps = pSlave;
		
		Point3D freePosition = ps.position;
		
		ps.position.set(Point3D.mix(freePosition, targetPosition, force));
		
		ps.speed.multiply(.99);
		
	}
	
	
}
