package net.mgsx.numart.realtime.util;

public class TimeCounter 
{
	double currentAbsoluteTime = 0;
	double currentRelativeTime = 0;
	public double update(double speed, double newAbsoluteTime)
	{
		double base = newAbsoluteTime - currentAbsoluteTime;
		currentRelativeTime += base * speed;
		currentAbsoluteTime = newAbsoluteTime;
		return currentRelativeTime;
	}
}
