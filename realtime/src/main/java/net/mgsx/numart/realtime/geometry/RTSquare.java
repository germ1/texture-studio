package net.mgsx.numart.realtime.geometry;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.control.MultiPointer;
import net.mgsx.numart.math.Filter2D;
import net.mgsx.numart.realtime.RTGeometry;

public class RTSquare extends RTGeometry 
{
	@ParamGroup
	public MultiPointer pointers;
	
	private Filter2D [] filters = {new Filter2D(), new Filter2D(), new Filter2D(), new Filter2D()};
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
		
		pointers.update();
		
		double x1 = pointers.getX(0);
		double x2 = pointers.getX(1);
		double y1 = pointers.getY(0);
		double y2 = pointers.getY(1);
		
		x1 = filters[0].put(x1);
		x2 = filters[1].put(x2);
		y1 = filters[2].put(y1);
		y2 = filters[3].put(y2);
		
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glColor3d(1, 1, 1);
		gl.glLineWidth(3);
		
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex2d(x1, y1);
		gl.glVertex2d(x2, y1);
		gl.glVertex2d(x2, y2);
		gl.glVertex2d(x1, y2);
		gl.glEnd();
		
	}
}
