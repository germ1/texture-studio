package net.mgsx.numart.realtime.streaming;

import java.util.concurrent.ConcurrentLinkedQueue;

public class MeshStream 
{
	private Cell [] map;
	private int mapWidth;
	private int mapHeight;
	
	private ConcurrentLinkedQueue<Cell> pipe = new ConcurrentLinkedQueue<MeshStream.Cell>();
	
	public static class Box
	{
		public int x;
		public int y;
		public int w;
		public int h;
		public Box(int x, int y, int w, int h) {
			super();
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}
		
	}
	
	public static interface ClientLoader
	{
		void load(Box box);
	}
	
	private static class Cell
	{
		public Cell() {
		}
		volatile boolean loaded = false;
		volatile int x;
		volatile int y;
	}
	
	private class LoadTask implements Runnable
	{

		@Override
		public void run() {
			while(active)
			{
				Cell cell = pipe.poll();
				if(cell != null)
				{
					//System.out.println("compute cell " + cell.x + "-" + cell.y);
					clientLoader.load(new Box(cell.x, cell.y, 1, 1));
					cell.loaded = true;
				}
				else
				{
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						return;
					}
				}
			}
		}
		
	}
	private volatile boolean active = false;
	private ClientLoader clientLoader;
	
	private Thread loadThread;
	
	public MeshStream(int mapWidth, int mapHeight, ClientLoader clientLoader) {
		super();
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		this.clientLoader = clientLoader;
		map = new Cell [mapWidth * mapHeight];
	}
	public void start()
	{
		stop();
		active = true;
		loadThread = new Thread(new LoadTask(), "MeshLoader");
		loadThread.start();
	}
	public void stop()
	{
		release();
		if(loadThread != null && loadThread.isAlive())
		{
			try {
				loadThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void release(){
		active = false;
	}
	
	public Box requestBox(Box requestedBox)
	{
		boolean complete = true;
		for(int y = 0 ; y < requestedBox.h ; y++)
		{
			int py = y + requestedBox.y;
			int dpy = ((py % mapHeight) + mapHeight) % mapHeight;
			
			for(int x = 0 ; x < requestedBox.w ; x++)
			{
				int px = x + requestedBox.x;
				int dpx = ((px % mapWidth) + mapWidth) % mapWidth;
				int index = dpy * mapWidth + dpx;
				Cell cell = map[index];
				if(cell == null)
				{
					cell = new Cell();
					map[index] = cell;
					// request load for that cell.
					cell.x = px;
					cell.y = py;
					cell.loaded = false;
					if(!pipe.contains(cell))
					{
						pipe.add(cell);
					}
					complete = false;
				}
				else if(px != cell.x || py != cell.y)
				{
					// request load for that cell.
					cell.x = px;
					cell.y = py;
					cell.loaded = false;
					if(!pipe.contains(cell))
					{
						pipe.add(cell);
					}
					complete = false;
				}
				else if(!map[index].loaded)
				{
					// wait for load.
					complete = false;
				}
				else
				{
					// ok
				}
				
			}
		}
		if(complete)
		{
			return requestedBox;
		}
		return null;
	}
	
	
	
	
}
