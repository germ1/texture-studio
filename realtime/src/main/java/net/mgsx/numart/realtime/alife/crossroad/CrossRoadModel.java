package net.mgsx.numart.realtime.alife.crossroad;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Particle;

class CrossRoadModel 
{
	public abstract static class Node
	{
		public Point3D position;
	}
	
	public static class Pipe
	{
		public Node source;
		public Node target;
	}
	
	public static class Inter extends Node
	{
		public List<Pipe> pipes = new ArrayList<Pipe>();
	}
	
	public static class Pump extends Node
	{
		public Pipe pipe;
	}
	
	public static class Entity
	{
		public State state;
	}
	
	public abstract static class State
	{
	}
	
	public static class OnPipe extends State
	{
		public Node source;
		public Node target;
		public double position;
	}
	public static class OnNode extends State
	{
		public Node node;
	}
	
	public List<Pipe> pipes = new ArrayList<Pipe>();
	public List<Node> nodes = new ArrayList<Node>();
	public List<Entity> entities = new ArrayList<Entity>();
}
