package net.mgsx.numart.realtime.physics;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Particle;

public class ElasticForm
{
	
	private Particle [] points;
	private Point3D [] anchors;
	private List<Constraint> constraints;
	
	public ElasticForm(int nPoints) {
		points = new Particle [nPoints];
		anchors = new Point3D [nPoints];
		for(int i=0 ; i<nPoints ; i++)
		{
			points[i] = new Particle();
		}
		constraints = new ArrayList<Constraint>();
	}
	
	public void addConstraint(int p1, int p2, double length, double force)
	{
		addConstraint(new LengthConstraint(force, points[p1], length, points[p2]));
	}
	public void addConstraint(Constraint c)
	{
		constraints.add(c);
	}
	
	public void removeConstraint(int p1, int p2)
	{
		// TODO
	}
	
	public void anchor(int p, Point3D anchor)
	{
		anchors[p] = anchor;
	}
	
	public void free(int p)
	{
		anchors[p] = null;
	}
	
	private void applyDeflectors(double deltaTime, List<Deflector> deflectors)
	{
		for(int i=0 ; i<points.length ; i++)
		{
			final Point3D am = anchors[i];
			final Particle p = points[i];
			if(am != null)
			{
				p.position.set(am);
				p.speed.reset();
				p.force.reset();
			}
			else
			{
				for(Deflector deflector : deflectors)
				{
					deflector.update(p, deltaTime);
				}
			}
		}
	}
	
	private void updatePoints(double deltaTime)
	{
		for(int i=0 ; i<points.length ; i++)
		{
			final Particle p = points[i];
			p.speed.add(Point3D.multiply(p.force, deltaTime));
			p.position.add(Point3D.multiply(p.speed, deltaTime));
			p.force.reset();
		}		
	}
	private void applyConstraints(double deltaTime)
	{
		for(Constraint c : constraints)
		{
			c.apply(deltaTime);
		}
	}
	
	public void update(double deltaTime, List<Deflector> deflectors)
	{
		// Apply deflectors
		applyDeflectors(deltaTime, deflectors);
		
		// update speed/position
		updatePoints(deltaTime);

		// apply constraints
		applyConstraints(deltaTime);
	}
	
	public Particle [] points()
	{
		return points;
	}
}
