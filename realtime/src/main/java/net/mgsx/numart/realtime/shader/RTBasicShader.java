package net.mgsx.numart.realtime.shader;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.realtime.RTShader;
import net.mgsx.numart.realtime.RTTexture;

public class RTBasicShader extends RTShader 
{
	@ParamGroup
	public RTTexture texture;
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		texture.prepare(gl, glu);
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		gl.glActiveTexture(GL.GL_TEXTURE0);
		texture.apply(gl, glu, time);
	}

	
	
}
