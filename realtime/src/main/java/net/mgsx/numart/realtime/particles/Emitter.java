package net.mgsx.numart.realtime.particles;

public interface Emitter 
{
	void update(ParticleFactory factory, double deltaTime);
}
