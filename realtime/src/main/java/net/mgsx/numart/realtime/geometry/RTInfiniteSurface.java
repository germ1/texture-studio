package net.mgsx.numart.realtime.geometry;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.realtime.MeshProvider;
import net.mgsx.numart.realtime.RTGeometry;
import net.mgsx.numart.realtime.streaming.MeshStream;
import net.mgsx.numart.realtime.streaming.MeshStream.Box;
import net.mgsx.numart.realtime.util.TimeCounter;

abstract public class RTInfiniteSurface extends RTGeometry implements MeshProvider
{
	@ParamInt(min=2, max=500)
	public int slicesU = 10;
	
	@ParamInt(min=2, max=500)
	public int slicesV = 10;
	
	@ParamDouble(min=-5, max=5)
	public double spd = 1;
	
	@ParamDouble(min=-Math.PI, max=Math.PI)
	public double angle = 0;
	
	protected FloatBuffer vertexBuffer;
	protected FloatBuffer normalBuffer;
	protected FloatBuffer texCoordBuffer;
	protected IntBuffer [] indexBuffer;
	
	private MeshStream mesh;
	
	private TimeCounter counterU;
	private TimeCounter counterV;
	
	@Override
	public FloatBuffer getVertex() {
		return vertexBuffer;
	}
	
	@Override
	public FloatBuffer getNormals() {
		return normalBuffer;
	}
	
	@Override
	public void destroy() 
	{
		if(mesh != null)
		{
			mesh.release();
			mesh = null;
		}
		super.destroy();
	}
	
	protected abstract void loadPart(Box box);
	
	@Override
	public void prepare(GL gl, GLU glu) {
		super.prepare(gl, glu);
		
		counterU = new TimeCounter();
		counterV = new TimeCounter();
		
		if(mesh != null)
		{
			mesh.stop();
		}
		
		mesh = new MeshStream(slicesU, slicesV, new MeshStream.ClientLoader() {
			@Override
			public void load(Box box) {
				loadPart(box);
			}
		});
		mesh.start();
	}
	
	@ParamInt(min=-500, max=500)
	public int boxX = 0, boxY = 0;
	@ParamInt(min=0, max=500)
	public int boxWidth = 4, boxHeight = 4;
	
	private Box enlarge(Box box, int e)
	{
		return new Box(box.x - e, box.y - e, box.w + 2 * e, box.h + 2 * e);
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		
		double utime = counterU.update(spd * Math.cos(angle), time);
		double vtime = counterV.update(spd * Math.sin(angle), time);
		
		super.apply(gl, glu, time);
		gl.glTranslatef(-0.5f, -0.0f, -0.5f);
		
		gl.glRotatef(80, 1, 0, 0);
		
		gl.glVertexPointer(3, GL.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, texCoordBuffer);
		gl.glNormalPointer(GL.GL_FLOAT, 0, normalBuffer);
		
		double decalX = utime;
		double decalY = vtime;
		
		gl.glTranslated(-decalX / (double)slicesU, -decalY / (double)slicesV, 0); // TODO ??? 
		
		Box requestedBox = new Box((int)decalX + boxX, (int)decalY + boxY, boxWidth, boxHeight);
		
		Box box = mesh.requestBox(requestedBox);
		
		mesh.requestBox(enlarge(requestedBox, 2)); // TODO param
		
		if(box != null)
		{
			
			//
			//     ///////  //////////
			//     //   //  //      //
			//     // D //  //  C   //
			//     //   //  //      //
			//     ///////  //////////
			//
			//     ///////  //////////--------
			//     // B //  //  A   //-  B   -
			//     ///////  //////////--------
			//              -        --      -
			//              -   C    --  D   -
			//              -        --      -
			//              ------------------
			//
			
			int ax = ((box.x % slicesU) + slicesU) % slicesU;
			int ay = ((box.y % slicesV) + slicesV) % slicesV;
			int aw = Math.min((slicesU) - ax, box.w);
			int ah = Math.min((slicesV+1) - ay, box.h);
			
			int bx = 0;
			int by = ay;
			int bw = box.w - aw;
			int bh = ah;
			
			int cx = ax;
			int cy = 0;
			int cw = aw;
			int ch = box.h - ah;
			
			int dx = 0;
			int dy = 0;
			int dw = bw;
			int dh = ch;
			
			
			// render base part (A)
			// gl.glColor3d(1,1,1);
			render(gl, ax, ay, aw, ah);
			// gl.glColor3d(1,0,0);
			render(gl, bx, by, bw, bh);
			// gl.glColor3d(0,1,0);
			render(gl, cx, cy, cw, ch);
			// gl.glColor3d(0,0,1);
			render(gl, dx, dy, dw, dh);
			
		}
		
	}

	abstract protected void render(GL gl, int x, int y, int w, int h);
	
	
}
