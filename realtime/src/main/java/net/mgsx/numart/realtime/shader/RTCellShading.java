package net.mgsx.numart.realtime.shader;



public class RTCellShading extends RTProgramShader 
{
	@Override
	protected String getVertexShaderSource() {
		return getShaderFromResource("cellShading.vertex");
	}

	@Override
	protected String getFragmentShaderSource() {
		return getShaderFromResource("cellShading.frag");
	}
	
}
