package net.mgsx.numart.realtime.geometry;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.Point3DInput;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.RTGeometry;

public class RTMultiGeometry extends RTGeometry 
{
	@ParamGroup
	public RTGeometry geometry;
	
	@ParamGroup
	public Point3DInput points;
	
	@DefaultFactory
	public void defaults()
	{
		super.defaults();
		geometry = (RTGeometry)ReflectUtils.newInstance(RTBuiltInSphere.class);
	}
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		super.prepare(gl, glu);
		if(geometry != null)
		{
			geometry.prepare(gl, glu);
		}
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		if(geometry != null && points != null)
		{
			for(Point3D point : points.getPoints3D())
			{
				if(point != null)
				{
					gl.glPushMatrix();
					gl.glTranslated(point.x, point.y, point.z);
					geometry.apply(gl, glu, time);
					gl.glPopMatrix();
				}
			}
		}
	}

}
