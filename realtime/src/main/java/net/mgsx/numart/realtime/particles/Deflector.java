package net.mgsx.numart.realtime.particles;

public interface Deflector 
{
	void update(Particle p, double deltaTime);
}
