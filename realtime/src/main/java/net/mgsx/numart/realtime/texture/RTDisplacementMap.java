package net.mgsx.numart.realtime.texture;

import java.nio.FloatBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.procedural.renderer.Rasterizer;
import net.mgsx.numart.procedural.renderer.TextureRenderer;
import net.mgsx.numart.realtime.RTTexture;

public class RTDisplacementMap extends RTTexture 
{
	@ParamGroup
	public TextureRenderer texture;

	@Logarithmic(2)
	@ParamInt(min=3, max=12)
	public int texturePower = 8;
	
	private int glTexture;
	
	@DefaultFactory
	public void defaults()
	{
		texture = ReflectUtils.createDefault(TextureRenderer.class);
	}

	@Override
	public void prepare(GL gl, GLU glu) 
	{
		final int textureSize = 1 << texturePower;
		final int width = textureSize;
		final int height = textureSize;
		final FloatBuffer pixels = FloatBuffer.allocate(width * height * 4);
		// pixels.order(ByteOrder.nativeOrder());
		
		Rasterizer rasterizer = new Rasterizer() {
			@Override
			public void setPixel(int x, int y, Color color) {
				pixels.put(((height - 1 - y) * width + x) * 4 + 0, (float)color.r);
				pixels.put(((height - 1 - y) * width + x) * 4 + 1, (float)color.g);
				pixels.put(((height - 1 - y) * width + x) * 4 + 2, (float)color.b);
				pixels.put(((height - 1 - y) * width + x) * 4 + 3, (float)color.a);
			}
		};
		
		texture.render(rasterizer, width, height, 0, 0, width, height);
		
		gl.glEnable(GL.GL_TEXTURE_2D);
		glTexture = genTexture(gl);
		gl.glBindTexture(GL.GL_TEXTURE_2D, glTexture);
		
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
	    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
	    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE);
		gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 4);
		pixels.position(0);
		pixels.mark();
		checkError(gl);
		gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA_FLOAT32_ATI, width, 
                height, 0, GL.GL_RGBA, GL.GL_FLOAT, pixels);
		
		checkError(gl);
		
		int [] tmp = new int[1];
		gl.glGetIntegerv(GL.GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS_ARB, tmp, 0);
		System.out.println(tmp[0]);
	}
	
	protected static byte colorToByte(double d)
	{
		return (byte)Math.max(0, Math.min(255, Math.round(d * 255.)));
	}
	protected static void checkError(GL gl)
	{
		int err = gl.glGetError();
		if(err != GL.GL_NO_ERROR)
		{
			System.err.println(err);
		}
	}
	
	protected static int genTexture(GL gl) {
        final int[] tmp = new int[1];
        gl.glGenTextures(1, tmp, 0);
        return tmp[0];
    }

	protected static int getInt(GL gl, int type) {
        final int[] tmp = new int[1];
        gl.glGetIntegerv(type, tmp, 0);
        return tmp[0];
    }
	
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glBindTexture(GL.GL_TEXTURE_2D, glTexture);
	}
	
}
