package net.mgsx.numart.realtime.particles.emitters;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.control.MultiPointer;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.particles.ParticleFactory;

public class MultiPointerEmitter extends AbstractEmitter
{
	int index = 0;
	
	@ParamGroup
	public MultiPointer pointers;
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		
		super.apply(gl, glu, time);
	}
	
	@Override
	public void update(ParticleFactory factory, double deltaTime) {
		pointers.update();
		super.update(factory, deltaTime);
	}
	
	@Override
	protected void initParticlePosition(Particle p) 
	{
		int id = index % pointers.getCount(); 
		
		p.position.x = pointers.getX(id);
		p.position.y = pointers.getY(id);
		p.position.z = 0;
		
		p.id = id;
		
		index++;
	}

}
