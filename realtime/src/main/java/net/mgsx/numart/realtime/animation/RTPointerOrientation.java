package net.mgsx.numart.realtime.animation;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.control.MultiPointer;
import net.mgsx.numart.realtime.RTAnimation;

public class RTPointerOrientation extends RTAnimation
{
	@ParamGroup
	public MultiPointer pointers;
	
	@Override
	public void prepare(GL gl, GLU glu) {
	}

	@Override
	public void apply(GL gl, GLU glu, double time) {
		pointers.update();
		double theta =  90 + 90 * (pointers.getY(1) - pointers.getY(0));
		double phy = 90 * (pointers.getX(1) - pointers.getX(0));
		gl.glRotated(theta, 1,0,0);
		gl.glRotated(phy, 0,0,1);
	}

}
