package net.mgsx.numart.realtime.dataviz;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.realtime.RTSequence;

public class SingleHistogramRenderer implements RTSequence {

	private Histogram histo;
	
	public SingleHistogramRenderer(Histogram histo) {
		super();
		this.histo = histo;
	}

	@Override
	public void apply(GL gl, GLU glu, double time) {
		
		gl.glColor4d(histo.color.r, histo.color.g, histo.color.b, histo.color.a);
		
		gl.glBegin(GL.GL_LINES);
		
		for(int i=0 ; i<histo.size() ; i++)
		{
			double value = histo.get(i);
			double x = (double)i / (double)histo.capacity();
			
			gl.glVertex2d(x, 0);
			gl.glVertex2d(x, value);
		}
		
		gl.glEnd();
	}

	@Override
	public void prepare(GL gl, GLU glu) {
		
	}
	
}
