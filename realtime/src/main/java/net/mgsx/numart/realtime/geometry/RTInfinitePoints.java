package net.mgsx.numart.realtime.geometry;

import java.nio.IntBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.Noise;
import net.mgsx.numart.realtime.streaming.MeshStream.Box;

import com.sun.opengl.util.BufferUtil;

public class RTInfinitePoints extends RTInfiniteSurface 
{
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double height = 1;
	
	@ParamDouble
	public double faceSize = 0.8;

	@ParamGroup
	public ScalarFromSpace displacementMap;
	
	@DefaultFactory
	public void defaults()
	{
		displacementMap = ReflectUtils.createDefault(Noise.class);
	}
	
	@Override
	public void prepare(GL gl, GLU glu) {
		
		int size = slicesU * slicesV;
		vertexBuffer = BufferUtil.newFloatBuffer(size * 3 * 4);
		normalBuffer = BufferUtil.newFloatBuffer(size * 3 * 4);
		texCoordBuffer = BufferUtil.newFloatBuffer(size * 2 * 4);
		indexBuffer = new IntBuffer[slicesU]; 
		
		for(int u=0 ; u<slicesU ; u++)
		{
			indexBuffer[u] = BufferUtil.newIntBuffer(slicesV * 4);
			for(int v=0 ; v<slicesV ; v++)
			{
				indexBuffer[u].put((v * slicesU + u) * 4 + 0);
				indexBuffer[u].put((v * slicesU + u) * 4 + 1);
				indexBuffer[u].put((v * slicesU + u) * 4 + 2);
				indexBuffer[u].put((v * slicesU + u) * 4 + 3);
			}
			indexBuffer[u].rewind();
		}
		vertexBuffer.rewind();
		texCoordBuffer.rewind();
		normalBuffer.rewind();
		
		
		 gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
	     gl.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY);
	     gl.glEnableClientState(GL.GL_NORMAL_ARRAY);
	     
		super.prepare(gl, glu);
	}
	
	@Override
	protected void render(GL gl, int x, int y, int w, int h)
	{
		for(int u=0 ; u<w ; u++)
		{
			for(int v=0 ; v<h ; v++)
			{
				int index = modulo(x + u, slicesU);
				indexBuffer[index].position(modulo(y + v, slicesV) * 4);
				gl.glDrawElements(GL.GL_TRIANGLE_STRIP, 4, GL.GL_UNSIGNED_INT, indexBuffer[index]);
			}
		}
		
	}

	// TODO dans math utils
	public static int modulo(int nb, int mod)
	{
		return ((nb % mod) + mod) % mod;
	}
	
	@Override
	protected void loadPart(Box box) {
		int u = box.x;
		int v = box.y;
		
		double px = (double)u / (double)slicesU;
		double py = (double)v / (double)slicesV;
		double pz = 0.5;
		
		double qx = ((double)u+faceSize) / (double)slicesU;
		double qy = ((double)v+faceSize) / (double)slicesV;
		
		pz = height * (displacementMap.getValue(new Point3D(px, py, pz)) - 0.5);
		
		int index;
		
		index = (modulo(v, slicesV) * slicesU + modulo(u, slicesU)) * 4;

		vertexBuffer.put(index * 3 + 0, (float)px);
		vertexBuffer.put(index * 3 + 1, (float)qy);
		vertexBuffer.put(index * 3 + 2, (float)pz);
		
		texCoordBuffer.put(index * 2 + 0, 0);
		texCoordBuffer.put(index * 2 + 1, 0);
		
		normalBuffer.put(index * 3 + 0, 0);
		normalBuffer.put(index * 3 + 1, 0);
		normalBuffer.put(index * 3 + 2, -1);
		
		index = (modulo(v, slicesV) * slicesU + modulo(u, slicesU)) * 4 + 1;

		vertexBuffer.put(index * 3 + 0, (float)qx);
		vertexBuffer.put(index * 3 + 1, (float)qy);
		vertexBuffer.put(index * 3 + 2, (float)pz);
		
		texCoordBuffer.put(index * 2 + 0, 1);
		texCoordBuffer.put(index * 2 + 1, 0);
		
		normalBuffer.put(index * 3 + 0, 0);
		normalBuffer.put(index * 3 + 1, 0);
		normalBuffer.put(index * 3 + 2, -1);
		
		index = (modulo(v, slicesV) * slicesU + modulo(u, slicesU)) * 4 + 2;

		vertexBuffer.put(index * 3 + 0, (float)px);
		vertexBuffer.put(index * 3 + 1, (float)py);
		vertexBuffer.put(index * 3 + 2, (float)pz);
		
		normalBuffer.put(index * 3 + 0, 0);
		normalBuffer.put(index * 3 + 1, 0);
		normalBuffer.put(index * 3 + 2, -1);
		
		texCoordBuffer.put(index * 2 + 0, 0);
		texCoordBuffer.put(index * 2 + 1, 1);
		
		index = (modulo(v, slicesV) * slicesU + modulo(u, slicesU)) * 4 + 3;

		vertexBuffer.put(index * 3 + 0, (float)qx);
		vertexBuffer.put(index * 3 + 1, (float)py);
		vertexBuffer.put(index * 3 + 2, (float)pz);
		
		texCoordBuffer.put(index * 2 + 0, 1);
		texCoordBuffer.put(index * 2 + 1, 1);
		
		normalBuffer.put(index * 3 + 0, 0);
		normalBuffer.put(index * 3 + 1, 0);
		normalBuffer.put(index * 3 + 2, -1);
		
		
	}
	
	
}
