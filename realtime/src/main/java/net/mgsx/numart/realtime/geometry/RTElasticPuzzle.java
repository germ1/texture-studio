package net.mgsx.numart.realtime.geometry;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamList;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.RTGeometry;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.physics.ElasticForm;
import net.mgsx.numart.realtime.physics.PositionConstraint;

public class RTElasticPuzzle extends RTGeometry
{
	@ParamList
	public List<Deflector> deflectors = new ArrayList<Deflector>();
	
	@ParamGroup
	public RTPuzzle puzzle;
	
	private ElasticForm bones;
	
	@ParamDouble
	public double force = 0.99;
	
	@Override
	public void prepare(GL gl, GLU glu) {
		super.prepare(gl, glu);
		puzzle.prepare(gl, glu);
		
		bones = new ElasticForm(puzzle.gridX * puzzle.gridY);
		
		for(int i=0 ; i<puzzle.gridX * puzzle.gridY ; i++)
		{
	    	int gx = i % puzzle.gridX;
	    	int gy = i / puzzle.gridX;
	    	
	    	double px = (double)gx / (double)puzzle.gridX;
	    	double py = (double)gy / (double)puzzle.gridY;
  	
	    	
	    	bones.addConstraint(new PositionConstraint(force, bones.points()[i], new Point3D(px, py, 0)));
		}
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		bones.update(1./60., deflectors); // TODO fps
		
		super.apply(gl, glu, time);
		
		for(int i=0 ; i<puzzle.gridX * puzzle.gridY ; i++)
		{
	    	gl.glPushMatrix();
	    	
	    	Particle p = bones.points()[i];
	    	gl.glTranslated(p.position.x, p.position.y, p.position.z);
	    	
	    	puzzle.applyElement(i, gl, glu, time);
	    	
	    	gl.glPopMatrix();
		}
	}
}
