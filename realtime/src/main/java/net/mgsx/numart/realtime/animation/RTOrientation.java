package net.mgsx.numart.realtime.animation;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.Point3DInput;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.RTAnimation;

public class RTOrientation extends RTAnimation {

	@ParamGroup
	public Point3DInput orientation;
	
	@Override
	public void prepare(GL gl, GLU glu) {

	}

	@Override
	public void apply(GL gl, GLU glu, double time) {
		Point3D p3D = orientation.getPoint3D();
		glu.gluLookAt(0, 0, 1, 0, 0, 0,  p3D.x, p3D.y, p3D.z);
	}

}
