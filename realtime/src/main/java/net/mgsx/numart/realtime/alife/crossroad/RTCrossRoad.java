package net.mgsx.numart.realtime.alife.crossroad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.math.BBox;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.RTGeometry;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.Entity;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.Inter;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.Node;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.OnNode;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.OnPipe;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.Pipe;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.Pump;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.State;

public class RTCrossRoad extends RTGeometry 
{
	private CrossRoadModel model;
	
	private GLUquadric q;
	
	@Logarithmic
	@ParamDouble(min=0.01, max=100)
	public double speed = 0.2;
	
	@Logarithmic
	@ParamDouble(min=0.001, max=1)
	public double pumpSize = 0.1;
	
	@Logarithmic
	@ParamDouble(min=0.001, max=1)
	public double pipeSize = 0.1;
	
	@Logarithmic
	@ParamDouble(min=0.001, max=1)
	public double interSize = 0.1;
	
	@Logarithmic
	@ParamDouble(min=0.001, max=1)
	public double entitySize = 0.05;
	
	@ParamInt(min=0, max=7)
	public int iterations = 2;
	
	@Logarithmic
	@ParamInt(min=1, max=1000)
	public int numEntities = 10;
	
	private List<Entity> entities;
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		super.prepare(gl, glu);
		
		model = CrossRoadFactory.build(new BBox(-0.5,-0.5,-0.5,1,1,1), iterations); 
		
		entities = new ArrayList<Entity>();
		
		q = glu.gluNewQuadric();
		glu.gluQuadricNormals(q, GLU.GLU_EXTERIOR);
		glu.gluQuadricTexture(q, true);
	}

	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		
		// TODO Auto-generated method stub
//		gl.glBegin(GL.GL_TRIANGLE_STRIP);
//		gl.glEnd();
		
//		gl.glMatrixMode(GL.GL_PROJECTION);
//		gl.glLoadIdentity();
//		gl.glMatrixMode(GL.GL_MODELVIEW);
//		gl.glLoadIdentity();

//		gl.glEnable(GL.GL_DEPTH_TEST);
		
//		gl.glEnable( GL.GL_COLOR_MATERIAL );
		
//		gl.glEnable(GL.GL_BLEND);
//		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);
		
		gl.glColor4d( 1,  1,  1,  1);
		
		gl.glDisable(GL.GL_BLEND);

		Map<Object, Boolean> visited = new HashMap<Object, Boolean>();
		
		drawRecursif(gl, glu,model.nodes.get(0), visited);
		
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);
		
		gl.glColor4d( 1,  1,  1,  0.5);
		
		while(entities.size() < numEntities)
		{
			Entity e = new Entity();
			OnNode newState = new OnNode();
			newState.node = model.nodes.get((int)(model.nodes.size() * Math.random()));
			e.state = newState;
			entities.add(e);
		}
		while(entities.size() > numEntities)
		{
			entities.remove(0);
		}
		
		// draw entities
		for(Entity e : entities)
		{
			State state = e.state;
			Point3D p = null;
			if(state instanceof OnNode)
			{
				p = ((OnNode) state).node.position;
				Node node = null;
				if(((OnNode) state).node instanceof Pump)
				{
					Pump pump = ((Pump)((OnNode) state).node);
					node = pump.pipe.source == ((OnNode) state).node ? pump.pipe.target : pump.pipe.source;
				}
				else if(((OnNode) state).node instanceof Inter)
				{
					List<Pipe> pipes = ((Inter)((OnNode) state).node).pipes;
					Pipe pipe = pipes.get((int)(pipes.size() * Math.random()));
					node = pipe.source == ((OnNode) state).node ? pipe.target : pipe.source;
				}
				OnPipe newState = new OnPipe();
				newState.position = 0;
				newState.source = ((OnNode) state).node;
				newState.target = node;
				state = newState;
			}
			else if(state instanceof OnPipe)
			{
				Point3D s = ((OnPipe) state).source.position;
				Point3D t = ((OnPipe) state).target.position;
				Point3D d = Point3D.subtract(t, s);
				double l = d.length();
				double q = ((OnPipe) state).position + speed * (1./60.) / l;
				((OnPipe) state).position = q;
				if(q > 1)
				{
					OnNode newState = new OnNode();
					newState.node = ((OnPipe) state).target;
					p = newState.node.position;
					state = newState;
				}
				else
				{
					p = Point3D.mix(s, t, q);
				}
			}
			e.state = state;
			
			gl.glPushMatrix();
			gl.glTranslated(p.x, p.y, p.z);
			glu.gluSphere(q, entitySize, 4, 3);
			gl.glPopMatrix();
		}
	}
	
	private void drawRecursif(GL gl, GLU glu, Object o, Map<Object, Boolean> visited)
	{
		if(o == null)
		{
			o = null;
		}
		if(visited.get(o) == null)
		{
			visited.put(o, Boolean.TRUE);
			if(o instanceof Pump)
			{
				Node n = (Node)o;
				
				gl.glColor4d( 1,  0.5,  0.5,  1);
				
				gl.glPushMatrix();
				gl.glTranslated(n.position.x, n.position.y, n.position.z);
				glu.gluSphere(q, pumpSize, 6, 6);
				gl.glPopMatrix();
				
				drawRecursif(gl, glu,((Pump) o).pipe, visited);
				
			} else if(o instanceof Pipe) {
				Node s = ((Pipe) o).source;
				Node t = ((Pipe) o).target;
				Point3D d = Point3D.subtract(t.position, s.position);
				
				gl.glColor4d( 0.5,  0.5,  0.5,  1);

				gl.glBegin(GL.GL_LINES);
				gl.glVertex3d(s.position.x, s.position.y, s.position.z);
				gl.glVertex3d(t.position.x, t.position.y, t.position.z);
				gl.glEnd();
				
//				gl.glPushMatrix();
//				gl.glTranslated(s.position.x, s.position.y, s.position.z);
//				//gl.glRotated(90, 0, 0, 1);
//				glu.gluLookAt(  0,0,0,  d.x, d.y, d.z,  -1,0, 0 );
//				glu.gluCylinder(q, pipeSize, pipeSize, d.length(), 5, 4);
//				gl.glPopMatrix();
				
				drawRecursif(gl, glu,s, visited);
				drawRecursif(gl, glu,t, visited);
				
			} else if(o instanceof Inter) {
				Node n = (Node)o;
				
				gl.glColor4d( 1,  1,  0.5,  1);

				gl.glPushMatrix();
				gl.glTranslated(n.position.x, n.position.y, n.position.z);
				glu.gluSphere(q, interSize, 6, 6);
				gl.glPopMatrix();
				
				for(Pipe p : ((Inter) o).pipes)
				{
					drawRecursif(gl, glu,p, visited);
				}
			}
		}
	}
	
}
