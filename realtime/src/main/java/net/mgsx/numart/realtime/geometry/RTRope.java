package net.mgsx.numart.realtime.geometry;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.annotations.ParamList;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.RTGeometry;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.physics.ElasticForm;

public class RTRope extends RTGeometry {

	@ParamList
	public List<Deflector> deflectors = new ArrayList<Deflector>();
	
	@ParamPoint(min=-1, max=1)
	public Point3D anchor = new Point3D();
	
	@ParamInt(min=1, max=100)
	public int points = 5; 
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=1)
	public double length = 0.3; 
	
//	@Logarithmic(10)
//	@ParamDouble(min=0.001, max=1000)
	@ParamDouble
	public double elasticity = 0.01; 
	
	@ParamDouble(min=0.01, max=1)
	public double thickness = 0.1;
	
	
	
	@ParamBoolean
	public boolean lineMode = false;
	
	public ElasticForm bones;
	
	@Override
	public void prepare(GL gl, GLU glu) {
		super.prepare(gl, glu);
		bones = new ElasticForm(points);
		for(int i=1 ; i<bones.points().length ; i++)
		{
			bones.points()[i].position.y = -i * length / (double)points;
			bones.addConstraint(i, i-1, length / (double)points, 1 - elasticity);
		}
		
		bones.anchor(0, anchor);
		
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
//		gl.glUseProgram(0);
		
//		gl.glEnable( GL.GL_COLOR_MATERIAL );
//		gl.glColorMaterial( GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE );	
//		
//		gl.glDisable(GL.GL_TEXTURE);
//		gl.glDisable(GL.GL_LIGHTING);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
		
		bones.update(1./60., deflectors);
		
		if(lineMode)
		{
			gl.glLineWidth((float)thickness * 10);
			gl.glBegin(GL.GL_LINE_STRIP);
			for(Particle p : bones.points())
			{
				gl.glVertex3d(p.position.x, p.position.y, p.position.z);
			}
			gl.glEnd();
		}
		else
		{
			gl.glBegin(GL.GL_TRIANGLE_STRIP);
			Particle prev = null;
			
			int i = 0;
			int len = points;
			for(Particle p : bones.points())
			{
				
				Point3D d = prev == null ?
						Point3D.subtract(bones.points()[1].position, p.position)
						: Point3D.subtract(p.position, prev.position);
				d.normalize();
				d.multiply(thickness);
				gl.glTexCoord2d(0, (double)i / (double)len);
				gl.glVertex3d(p.position.x - d.y, p.position.y + d.x, p.position.z);
				gl.glTexCoord2d(1, (double)i / (double)len);
				gl.glVertex3d(p.position.x + d.y, p.position.y - d.x, p.position.z);
				prev = p;
				i++;
			}
			gl.glEnd();
		}
		
//		gl.glBegin(GL.GL_LINES);
//		gl.glVertex3d(anchor.x, anchor.y, anchor.z);
//		gl.glVertex3d(anchor.x + 0.1, anchor.y, anchor.z);
//		gl.glEnd();
//		gl.glBegin(GL.GL_LINES);
//		gl.glVertex3d(anchor.x, anchor.y, anchor.z);
//		gl.glVertex3d(anchor.x, anchor.y + 0.1, anchor.z);
//		gl.glEnd();
//		gl.glBegin(GL.GL_LINES);
//		gl.glVertex3d(anchor.x, anchor.y, anchor.z);
//		gl.glVertex3d(anchor.x, anchor.y, anchor.z + 0.1);
//		gl.glEnd();
	}
}
