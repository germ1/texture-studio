package net.mgsx.numart.realtime.particles.function;

import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.particles.ParticleColor;

public class FireLifecycle implements ParticleColor {

	@Override
	public void colorForParticle(Color result, Particle p) 
	{
		double factorColor = MathUtil.envelopAR(1 - p.life / p.lifeMax, 0.4, 0.5, 1, 1); // TODO params
		result.r = 1;
		result.g = factorColor;
		result.b = factorColor * 0.1;
	}

}
