package net.mgsx.numart.realtime.particles.deflectors;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Particle;

public class ProceduralDeflector implements Deflector {

	@ParamGroup
	public ScalarFromSpace force;
	@ParamGroup
	public ScalarFromSpace angle;
	
	@ParamDouble
	public double forceScale = 0.5;
	
	@Logarithmic
	@ParamDouble(min=0.01, max=100)
	public double speed = 0.5;
	
	private double time = 0;
	
	@Override
	public void update(Particle p, double deltaTime) 
	{
		time += deltaTime * speed;
		Point3D point = Point3D.add(new Point3D(0,0,time), p.position);
		double vAngle = (angle.getValue(point) - 0.5) * 2 * Math.PI;
		Point3D dir = new Point3D(Math.cos(vAngle), Math.sin(vAngle), 0);
		double fValue = force.getValue(point);
		p.force.add(Point3D.multiply(dir, fValue * forceScale *10));
	}

}
