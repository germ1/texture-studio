package net.mgsx.numart.realtime.particles.emitters;

import net.mgsx.numart.framework.Point3DInput;
import net.mgsx.numart.framework.ValueControl;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Particle;

public class LineEmitterSpeDouble extends LineEmitter 
{
	@ParamDouble
	public double sPosition;
	
	@ParamDouble
	public double sRange;
	
	@ParamGroup
	public Point3DInput inputs;
	
	@Override
	protected void initParticlePosition(Particle p) 
	{
		if(inputs != null)
		{
			Point3D [] pts = inputs.getPoints3D();
			for(int i=0 ; i<pts.length ; i++)
			{
				Point3D pt = pts[i];
				double rnd = Math.random();
				p.id = Math.abs(rnd - pt.x) < sRange ? i+1 : 0;
				double r = (rnd * 2 - 1) * radius;
				p.position.set(Point3D.add(position, Point3D.multiply(direction, r)));
				if(p.id > 0) break;
			}
		}
	}
}
