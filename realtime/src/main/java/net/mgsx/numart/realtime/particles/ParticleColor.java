package net.mgsx.numart.realtime.particles;

import net.mgsx.numart.math.Color;

public interface ParticleColor 
{
	void colorForParticle(Color result, Particle p);
}
