package net.mgsx.numart.realtime.geometry;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.realtime.RTGeometry;
import net.mgsx.numart.realtime.alife.ArtificialLife;
import net.mgsx.numart.realtime.particles.Particle;

public class RTArtificialLife extends RTGeometry
{
	private ArtificialLife al;
	
	@Override
	public void prepare(GL gl, GLU glu) {
		super.prepare(gl, glu);
		al = new ArtificialLife();
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		super.apply(gl, glu, time);
		
		al.update(1. / 60.);
		
		// XXX temporaire
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glDisable(GL.GL_DEPTH_TEST);
		
		gl.glEnable( GL.GL_COLOR_MATERIAL );
		
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);
		
		gl.glEnable(GL.GL_TEXTURE);
		
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_COMBINE);
		
		for(Particle p : al)
		{
			double dx = (Math.cos(p.angle) + Math.sin(p.angle)) * p.size;
			double dy = (Math.sin(p.angle) - Math.cos(p.angle)) * p.size;
			
			gl.glColor4d(1, p.life / p.lifeMax, p.life / p.lifeMax, 1);
			
			gl.glBegin(GL.GL_TRIANGLE_STRIP);
			gl.glTexCoord2i(0, 0);
			gl.glVertex3d(p.position.x - dx, p.position.y + dy, p.position.z);
			gl.glTexCoord2i(1, 0);
			gl.glVertex3d(p.position.x + dx, p.position.y + dy, p.position.z);
			gl.glTexCoord2i(0, 1);
			gl.glVertex3d(p.position.x - dx, p.position.y - dy, p.position.z);
			gl.glTexCoord2i(1, 1);
			gl.glVertex3d(p.position.x + dx, p.position.y - dy, p.position.z);
			gl.glEnd();
		}
		
	}
}
