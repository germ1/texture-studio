package net.mgsx.numart.realtime.geometry;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.math.Box2D;
import net.mgsx.numart.realtime.RTSequence;
import net.mgsx.numart.realtime.RTTexture;

public class RTTextDrawer implements RTSequence
{
	@ParamGroup
	public RTTexture texture;
	
	@ParamInt(min=1,max=64)
	public int columns;
	@ParamInt(min=1,max=64)
	public int columnsPadding;
	@ParamInt(min=1,max=64)
	public int rows;
	@ParamInt(min=0,max=255)
	public int offset;
	
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		texture.prepare(gl, glu);
	}
	@Override
	public void apply(GL gl, GLU glu, double time) 
	{
		texture.apply(gl, glu, time);
	}

	public void drawText(GL gl, GLU glu, String text, Box2D firstLetter) 
	{
		double ubase = 1. / (double)columnsPadding;
		double vbase = 1. / (double)rows;
		
		Box2D letter = new Box2D(firstLetter);
		Box2D tc = new Box2D();
		for(int i=0 ; i<text.length() ; i++)
		{
			int charIndex = text.charAt(i) - offset;
			int row = charIndex / columns;
			int col = charIndex % columns;
			
			tc.x = (double)col * ubase;
			tc.y = 1 - (double)row * vbase;
			tc.w = ubase;
			tc.h = vbase;
			
			drawBox(gl, letter, tc);
			
			letter.x += firstLetter.w;
		}
	}
	private void drawBox(GL gl, Box2D box, Box2D texture)
	{
		gl.glBegin(GL.GL_TRIANGLE_STRIP);
		gl.glTexCoord2d(texture.x, texture.y);
		gl.glVertex3d(box.x, box.y, 0);
		gl.glTexCoord2d(texture.x + texture.w, texture.y);
		gl.glVertex3d(box.x + box.w, box.y, 0);
		gl.glTexCoord2d(texture.x, texture.y + texture.h);
		gl.glVertex3d(box.x, box.y + box.h, 0);
		gl.glTexCoord2d(texture.x + texture.w, texture.y + texture.h);
		gl.glVertex3d(box.x + box.w, box.y + box.h, 0);
		gl.glEnd();
	}

}
