package net.mgsx.numart.realtime.shader;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.realtime.RTTexture;

public class RTGLSLTest extends RTProgramShader {

	@ParamGroup
	public RTTexture textureA;
	
	@ParamGroup
	public RTTexture textureB;
	
	
	
	private int timeVarLocation;
	
	@Override
	protected String getVertexShaderSource() {
		return getShaderFromResource("test.vertex");
	}

	@Override
	protected String getFragmentShaderSource() {
		return getShaderFromResource("test.frag");
	}

	@Override
	public void prepare(GL gl, GLU glu) 
	{
		textureA.prepare(gl, glu);
		textureB.prepare(gl, glu);
		super.prepare(gl, glu);
		timeVarLocation = gl.glGetUniformLocation(program, "time");
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
		
		gl.glActiveTexture(GL.GL_TEXTURE0);
		textureA.apply(gl, glu, time);
		
		gl.glActiveTexture(GL.GL_TEXTURE1);
		textureB.apply(gl, glu, time);
		
		gl.glUniform1f(timeVarLocation, (float)time);
	}
}
