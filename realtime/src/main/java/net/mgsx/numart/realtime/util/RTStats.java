package net.mgsx.numart.realtime.util;

public interface RTStats 
{
	void beginRendering();
	void endRendering();
}
