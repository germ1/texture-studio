package net.mgsx.numart.realtime.shader;

import net.mgsx.numart.framework.annotations.UserDoc;


@UserDoc
(
title="GLSL Basic Light Shader",
description="Use all basic material/light properties with a directionnal light"
)

public class RTParalaxShader extends RTProgramShader 
{
	@Override
	protected String getVertexShaderSource() {
		return getShaderFromResource("paralax.vertex");
	}

	@Override
	protected String getFragmentShaderSource() {
		return getShaderFromResource("paralax.frag");
	}
	
}
