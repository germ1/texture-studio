package net.mgsx.numart.realtime.particles;

public interface ParticleFactory 
{
	public Particle createParticle();
}
