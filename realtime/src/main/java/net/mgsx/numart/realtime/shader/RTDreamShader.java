package net.mgsx.numart.realtime.shader;

import java.nio.FloatBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.realtime.RTTexture3D;
import net.mgsx.numart.realtime.texture.RTSoftwareTexture3D;

public class RTDreamShader  extends RTProgramShader 
{
	
	@ParamColor
	public Color a1;
	
	@ParamColor
	public Color a2;
	
	@ParamColor
	public Color b1;
	
	@ParamColor
	public Color b2;
	
	
	@ParamGroup
	public RTTexture3D textureNoise;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=30)
	public double scale = 1;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=30)
	public double amplitude = 0.3;
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=30)
	public double speed = 1.0;
	
	private int timeVarLocation;
	private int scaleVarLocation;
	private int amplitudeVarLocation;
	
	@DefaultFactory
	public void defaults()
	{
		a1 = new Color(0,0,0);
		a2 = new Color(1,1,1);
		b1 = new Color(1,0,0);
		b2 = new Color(1,1,0);
		
		textureNoise = ReflectUtils.createDefault(RTSoftwareTexture3D.class);
	}
	
	@Override
	protected String getVertexShaderSource() {
		return getShaderFromResource("dream.vertex");
	}

	@Override
	protected String getFragmentShaderSource() {
		return getShaderFromResource("dream.frag");
	}
	@Override
	public void prepare(GL gl, GLU glu) 
	{
		gl.glActiveTexture(GL.GL_TEXTURE0);
		textureNoise.prepare(gl, glu);
		super.prepare(gl, glu);
		timeVarLocation = gl.glGetUniformLocation(program, "time");
		scaleVarLocation = gl.glGetUniformLocation(program, "scale");
		amplitudeVarLocation = gl.glGetUniformLocation(program, "amplitude");
		// TODO
	}
	
	@Override
	public void apply(GL gl, GLU glu, double time) {
		super.apply(gl, glu, time);
		
		// gl.glClientActiveTexture(GL.GL_TEXTURE0);
//		gl.glEnable(GL.GL_BLEND);
//		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glActiveTexture(GL.GL_TEXTURE0);
		textureNoise.apply(gl, glu, time * speed);
		
		gl.glUniform1f(timeVarLocation, (float)(time * speed));
		gl.glUniform1f(scaleVarLocation, (float)scale);
		gl.glUniform1f(amplitudeVarLocation, (float)amplitude);
		// TODO
		int a1VarLocation = gl.glGetUniformLocation(program, "colorA1");
		gl.glUniform4fv(a1VarLocation, 1, FloatBuffer.wrap(a1.toFloat()));
		int a2VarLocation = gl.glGetUniformLocation(program, "colorA2");
		gl.glUniform4fv(a2VarLocation, 1, FloatBuffer.wrap(a2.toFloat()));
		int b1VarLocation = gl.glGetUniformLocation(program, "colorB1");
		gl.glUniform4fv(b1VarLocation, 1, FloatBuffer.wrap(b1.toFloat()));
		int b2VarLocation = gl.glGetUniformLocation(program, "colorB2");
		gl.glUniform4fv(b2VarLocation, 1, FloatBuffer.wrap(b2.toFloat()));
	}
	
}
