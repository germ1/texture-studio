package net.mgsx.numart.realtime.geometry;

import java.nio.IntBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.Noise;
import net.mgsx.numart.procedural.texture.util.ScalarUtils;
import net.mgsx.numart.realtime.streaming.MeshStream.Box;

import com.sun.opengl.util.BufferUtil;

public class RTInfinitePlan extends RTInfiniteSurface 
{
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double height = 1;
	
	@ParamGroup
	public ScalarFromSpace displacementMap;
	
	@DefaultFactory
	public void defaults()
	{
		displacementMap = ReflectUtils.createDefault(Noise.class);
	}
	
	@Override
	public void prepare(GL gl, GLU glu) {
		
		int size = slicesU * slicesV;
		vertexBuffer = BufferUtil.newFloatBuffer(size * 3);
		normalBuffer = BufferUtil.newFloatBuffer(size * 3);
		texCoordBuffer = BufferUtil.newFloatBuffer(size * 2);
		indexBuffer = new IntBuffer[slicesU+1]; 
		
		for(int u=0 ; u<slicesU+1 ; u++)
		{
			indexBuffer[u] = BufferUtil.newIntBuffer((slicesV + 1) * 2);
			for(int v=0 ; v<slicesV+1 ; v++)
			{
				indexBuffer[u].put((v % slicesV) * slicesU + (u % slicesU));
				indexBuffer[u].put((v % slicesV) * slicesU + ((u + 1) % slicesU));
			}
			indexBuffer[u].rewind();
		}
		vertexBuffer.rewind();
		texCoordBuffer.rewind();
		normalBuffer.rewind();
		
		
		 gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
	     gl.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY);
	     gl.glEnableClientState(GL.GL_NORMAL_ARRAY);
	     
		super.prepare(gl, glu);
	}
	
	@Override
	protected void render(GL gl, int x, int y, int w, int h)
	{
		if(w > 0 && h > 0)
		{
			for(int u=0 ; u<w ; u++)
			{
				int index = x + u;
				indexBuffer[index].position(y * 2);
				gl.glDrawElements(GL.GL_TRIANGLE_STRIP, h * 2, GL.GL_UNSIGNED_INT, indexBuffer[index]);
			}
			
		}
		
	}

	@Override
	protected void loadPart(Box box) {
		int u = box.x;
		int v = box.y;
		
		int index = (((v % slicesV) + slicesV) % slicesV) * slicesU + (((u % slicesU) + slicesU) % slicesU);
		
		double px = (double)u / (double)slicesU;
		double py = (double)v / (double)slicesV;
		double pz = 0.0;
		
		pz = height * (displacementMap.getValue(new Point3D(px, py, pz)) - 0.5);
		
		vertexBuffer.put(index * 3 + 0, (float)px);
		vertexBuffer.put(index * 3 + 1, (float)py);
		vertexBuffer.put(index * 3 + 2, (float)pz);
		
		double tu = (double)u / (double)slicesU;
		double tv = (double)v / (double)slicesV;

		texCoordBuffer.put(index * 2 + 0, (float)tu);
		texCoordBuffer.put(index * 2 + 1, (float)-tv); // TODO voir pour le renversement !
		
		Point3D normal = ScalarUtils.getNormal(displacementMap, new Point3D(0,0,1), new Point3D(px, py, pz), height, 0.5);
		normal.normalize(); // XXX pas nécessaire ...
		normalBuffer.put(index * 3 + 0, (float)normal.x);
		normalBuffer.put(index * 3 + 1, (float)normal.y);
		normalBuffer.put(index * 3 + 2, (float)normal.z);
		
	}
	
	
}
