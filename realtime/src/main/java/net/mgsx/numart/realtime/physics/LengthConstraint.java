package net.mgsx.numart.realtime.physics;

import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Particle;

public class LengthConstraint extends Constraint
{
	double length;
	Particle pMaster;
	
	
	
	public LengthConstraint(double force, Particle pSlave, double length, Particle pMaster) {
		super(force, pSlave);
		this.length = length;
		this.pMaster = pMaster;
	}



	@Override
	void apply(double deltaTime) 
	{
		Particle ps = pSlave;
		Particle pm = pMaster;
		
		Point3D vms = Point3D.subtract(ps.position, pm.position);
		double distance = vms.length();
		
//		double delta = distance - c.length;
		if(distance != 0)
		{
			vms.normalize();
		}
		else
		{
			vms.x = Math.random();
			vms.y = Math.random();
			vms.z = Math.random();
			vms.normalize();
		}
		Point3D freePosition = ps.position;
		
		Point3D constraintPosition = Point3D.add(pm.position, Point3D.multiply(vms, length));
		
		ps.position.set(Point3D.mix(freePosition, constraintPosition, force));
		
		ps.speed.multiply(.99);
		
	}
}