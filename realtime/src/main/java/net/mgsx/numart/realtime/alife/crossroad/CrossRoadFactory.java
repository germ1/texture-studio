package net.mgsx.numart.realtime.alife.crossroad;

import net.mgsx.numart.math.BBox;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.Inter;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.Pipe;
import net.mgsx.numart.realtime.alife.crossroad.CrossRoadModel.Pump;

public class CrossRoadFactory {

	public static CrossRoadModel build(BBox box, int iterations)
	{
		final CrossRoadModel model = new CrossRoadModel();
		
		// créer deux pump relié par un lien
		
		Pump source = new Pump();
		Pump target = new Pump();
		
		model.nodes.add(source);
		model.nodes.add(target);
		
		Pipe pipe = new Pipe();
		pipe.source = source;
		pipe.target = target;
		
		source.pipe = pipe;
		target.pipe = pipe;
		
		source.position = Point3D.add(box.position, new Point3D(0,           box.scale.y * 0.5, box.scale.z * 0.5));
		target.position = Point3D.add(box.position, new Point3D(box.scale.x, box.scale.y * 0.5, box.scale.z * 0.5));
		
		// puis appel récursif à la construction
		
		if(iterations > 0)
		{
			build(model, pipe, box, 0, iterations - 1);
		}
		
		return model;
	}
	
	public static void build(CrossRoadModel model, Pipe pipe, BBox box, int axe, int iterations)
	{
		int newAxe = ((int)(axe + Math.random() * 2)) % 3;
		
		double t1 =  Math.random() * 0.5 + 0.25;
		double t2 =  Math.random() * 0.5 + 0.25;
		
		// insert an inter on the pipe
		Inter inter = new Inter();
		inter.position = Point3D.mix(pipe.source.position, pipe.target.position, t1);
		
		// add a pipe from inter to target 
		Pipe pipe2 = new Pipe();
		pipe2.source = inter;
		pipe2.target = pipe.target;
		
		// relink pipe1
		pipe.target = inter;
		
		// add a new pipe
		Pipe pipe3 = new Pipe();
		pipe3.source = inter;
		
		// add a new pump to pipe3
		Pump pump = new Pump();
		pump.pipe = pipe3;
		
		pipe3.target = pump;

		
		int irnd = (int)(Math.random() * 6);
		Point3D dir = Point3D.subtract(pipe.target.position, pipe.source.position);
		dir.normalize();
		
		Point3D tan;
		
		Point3D [] matrix = new Point3D[]{
			new Point3D(0,1,0),
			new Point3D(0,0,1),
			new Point3D(1,0,0)
		};
		
		tan = matrix[newAxe];
		if(irnd % 2 == 1)
		{
			tan = Point3D.multiply(tan, -1);
		}
		dir = tan;
		pump.position = Point3D.add(inter.position, Point3D.multiply(dir, t2 * box.scale.length() * 0.5));

		
		inter.pipes.add(pipe);
		inter.pipes.add(pipe2);
		inter.pipes.add(pipe3);
		
		model.nodes.add(inter);
		model.nodes.add(pump);
		model.pipes.add(pipe2);
		model.pipes.add(pipe3);
		
		if(iterations > 0)
		{
			// TODO les box 
			BBox bbox = new BBox(box.position, Point3D.multiply(box.scale, 0.33));
			
			build(model, pipe, bbox, axe, iterations - 1);
			build(model, pipe2, bbox, axe, iterations - 1);
			build(model, pipe3, bbox, newAxe, iterations - 1);
		}
		
	}
	
}
