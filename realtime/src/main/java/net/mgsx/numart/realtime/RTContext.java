package net.mgsx.numart.realtime;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import com.sun.opengl.util.GLUT;

/** FUTUR */
public class RTContext 
{
	public GL gl;
	public GLU glu;
	public GLUT glut;
	
	public double absoluteTime;
	public double relativeTime;
	public double deltaTime;
}
