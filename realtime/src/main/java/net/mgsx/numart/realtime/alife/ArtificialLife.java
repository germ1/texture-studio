package net.mgsx.numart.realtime.alife;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Emitter;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.particles.ParticleFactory;
import net.mgsx.numart.realtime.particles.ParticleSystem;


public class ArtificialLife implements Emitter, Deflector, Iterable<Particle>, ParticleFactory
{
	public static class Element extends Particle
	{
		double power = 1;
		State state;
	}
	
	private static interface State 
	{
		State execute(Element element, double deltaTime);
	}
	
	private class InitState implements State
	{
		@Override
		public State execute(Element element, double deltaTime) 
		{
			final double range = 0.3;
			
			Point3D target = new Point3D(
					MathUtil.random(-range, range),
					MathUtil.random(-range, range),
					MathUtil.random(-range, range));
			return new TargetingState(target);
		}
	}
	private class TargetingState implements State
	{
		private Point3D target;
		
		public TargetingState(Point3D target) {
			super();
			this.target = target;
		}

		@Override
		public State execute(Element element, double deltaTime) 
		{
			for(Particle p : ArtificialLife.this)
			{
				Element other = (Element)p;
				Point3D odiff = Point3D.subtract(element.position, other.position);
				if((odiff.length() < p.size * 10) && odiff.length() > 0)
				{
					odiff.normalize();
					// element.force.add(Point3D.multiply(odiff, element.power * 1));
					target = Point3D.add(element.position, Point3D.multiply(odiff, -p.size));
					p.speed.multiply(0.9);
					double commonLife = (other.life + p.life) * 1.001;
					p.life = other.life = commonLife / 2;
					if(p.life > p.lifeMax) p.life = p.lifeMax;
					if(other.life > other.lifeMax) other.life = other.lifeMax;
//					element.life = 0;
//					pendingCreation++;
//					break;
				}
			}
			
			Point3D diff = Point3D.subtract(target, element.position);
			if(diff.length() < 0.01)
			{
				return new InitState();
			}
			diff.normalize();
			element.force.add(Point3D.multiply(diff, element.power));
			return this;
		}
	}
	
	private ParticleSystem ps = new ParticleSystem(1000, this);
	
	private List<Emitter> emitters = new ArrayList<Emitter>();
	private List<Deflector> deflectors = new ArrayList<Deflector>();
	
	public ArtificialLife() {
		emitters.add(this);
		deflectors.add(this);
	}
	
	public void update(double deltaTime)
	{
		ps.update(emitters, deflectors, deltaTime);
	}

	@Override
	public void update(Particle p, double deltaTime) {
		Element e = (Element)p;
		if(ps.size() < 5)
		{
			p.life = 1;
		}
		e.state = e.state.execute(e, deltaTime);
	}

	@Override
	public void update(ParticleFactory factory, double deltaTime) {
		while(ps.size() < 20)
		{
			Element e = (Element)factory.createParticle();
			final double range = 0.9;
			
			Point3D target = new Point3D(
					MathUtil.random(-range, range),
					MathUtil.random(-range, range),
					MathUtil.random(-range, range));
			e.position = target;
			
			e.lifeMax = e.life = 4 + Math.random() * 4;
			e.power = 0.05 + Math.random() * 0.3;
			e.size = 0.01 + Math.random() * 0.02;
			e.state = new InitState();
		}
		
	}

	@Override
	public Iterator<Particle> iterator() {
		return ps.iterator();
	}

	@Override
	public Particle createParticle() {
		return new Element();
	}
}
