package net.mgsx.numart.realtime.alife;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.realtime.particles.Deflector;
import net.mgsx.numart.realtime.particles.Emitter;
import net.mgsx.numart.realtime.particles.Particle;
import net.mgsx.numart.realtime.particles.ParticleFactory;
import net.mgsx.numart.realtime.particles.ParticleSystem;


public class ArtificialLife2 implements Emitter, Deflector, Iterable<Particle>, ParticleFactory
{
	public static class Element extends Particle
	{
		double power = 1;
		State state;
		public int type;
		public double [] weights;
	}
	
	private static interface State 
	{
		State execute(Element element, double deltaTime);
	}
	
	private class InitState implements State
	{
		@Override
		public State execute(Element element, double deltaTime) 
		{
			final double range = 0.3;
			
			double distance = 1e30;
			
			Point3D target = new Point3D(
					MathUtil.random(-range, range),
					MathUtil.random(-range, range),
					MathUtil.random(-range, range));
			
//			for(Particle p : ArtificialLife2.this)
//			{
//				
//				Element other = (Element)p;
//				Point3D odiff = Point3D.subtract(element.position, other.position);
//				
//				final double lodiff = odiff.length();
//				
//				if(lodiff < distance && p != element)
//				{
//					if(((ArtificialLife2.Element)p).type == element.type)
//					{
//						distance = lodiff;
//						target = new Point3D(p.position);
//					}
//				}
//			}
			
			return new TargetingState(target);
		}
	}
	private class TargetingState implements State
	{
		private Point3D target;
		
		public TargetingState(Point3D target) {
			super();
			this.target = target;
		}

		@Override
		public State execute(Element element, double deltaTime) 
		{
			double distance = 1e30;
			Point3D moyenne = new Point3D();
			double weight = 0;
			for(Particle p : ArtificialLife2.this)
			{
				
				Element other = (Element)p;
				Point3D odiff = Point3D.subtract(element.position, other.position);
				
				final double lodiff = odiff.length();
				
				if(lodiff < distance && p != element)
				{
					double w = p.size * p.size * element.weights[((ArtificialLife2.Element)p).type];
//					if(((ArtificialLife2.Element)p).type == element.type)
//					{
						
						moyenne.add(Point3D.multiply(p.position, w));
						weight+=w;
//					}
				}
				
				if((lodiff < p.size * 10) && lodiff > 0)
				{
					// odiff.normalize();
					// element.force.add(Point3D.multiply(odiff, element.power * 1));
					// target = Point3D.add(element.position, Point3D.multiply(odiff, -p.size));
					p.speed.multiply(0.97);
//					double commonLife = (other.life + p.life) * 1.001;
//					p.life = other.life = commonLife / 2;
//					if(p.life > p.lifeMax) p.life = p.lifeMax;
//					if(other.life > other.lifeMax) other.life = other.lifeMax;
//					element.life = 0;
//					pendingCreation++;
//					break;
					if(lodiff < p.size * 5)
					{
						p.speed.multiply(0);
					}
				}
			}
			
			if(weight > 0)
			{
				moyenne.multiply(1 / weight);
				target = new Point3D(moyenne);
			}
			
			Point3D diff = Point3D.subtract(target, element.position);
//			if(diff.length() < 0.01)
//			{
//				return new InitState();
//			}
			double ratio = Math.min(1, diff.length() / (2 * element.size));
			diff.normalize();
			element.force.add(Point3D.multiply(diff, element.power));
			return this;
		}
	}
	
	private ParticleSystem ps = new ParticleSystem(1000, this);
	
	private List<Emitter> emitters = new ArrayList<Emitter>();
	private List<Deflector> deflectors = new ArrayList<Deflector>();
	
	public ArtificialLife2() {
		emitters.add(this);
		deflectors.add(this);
	}
	
	public void update(double deltaTime)
	{
		ps.update(emitters, deflectors, deltaTime);
	}

	@Override
	public void update(Particle p, double deltaTime) {
		Element e = (Element)p;
		if(ps.size() < 5)
		{
			p.life = 1;
		}
		e.state = e.state.execute(e, deltaTime);
	}

	@Override
	public void update(ParticleFactory factory, double deltaTime) {
		while(ps.size() < 40)
		{
			Element e = (Element)factory.createParticle();
			final double range = 0.9;
			
			Point3D target = new Point3D(
					MathUtil.random(-range, range),
					MathUtil.random(-range, range),
					MathUtil.random(-range, range));
			e.position = target;
			
			e.lifeMax = e.life = 8 + Math.random() * 2;
			e.power = 0.6; // 0.05 + Math.random() * 0.3;
			e.size = 0.02; //(0.01 + Math.random() * 0.02) * 1;
			e.state = new InitState();
			
			e.type = (int)(Math.random() * 4);
			
			switch (e.type) {
			case 0:
				e.weights = new double[]{0,1,0,0};
				break;
			case 1:
				e.weights = new double[]{0,0,1,0};
				break;
			case 2:
				e.weights = new double[]{0,0,0,1};
				break;
			case 3:
				e.weights = new double[]{1,0,0,0};
				break;
			default:
				break;
			}
			
			
		}
		
	}

	@Override
	public Iterator<Particle> iterator() {
		return ps.iterator();
	}

	@Override
	public Particle createParticle() {
		Element element = new Element();
		return element;
	}
}
