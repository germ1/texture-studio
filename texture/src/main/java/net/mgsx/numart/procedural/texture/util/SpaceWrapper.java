package net.mgsx.numart.procedural.texture.util;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.SpaceFromSpace;

public class SpaceWrapper extends NamedElement implements ColorFromSpace {

	@ParamGroup
	public SpaceFromSpace transformer;
	
	@ParamGroup
	public ColorFromSpace wrappedTexture;
	
	public SpaceWrapper() {
		super();
	}
	public SpaceWrapper(ColorFromSpace wrappedTexture, SpaceFromSpace transformer) {
		super();
		this.wrappedTexture = wrappedTexture;
		this.transformer = transformer;
	}

	@Override
	public Color getColor(Point3D texCoord) {
		return wrappedTexture.getColor(transformer.getPoint(texCoord));
	}

}
