package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class TextureMixer extends NamedElement implements ColorFromSpace {

	@ParamGroup
	public ColorFromSpace a;
	
	@ParamGroup
	public ColorFromSpace b;
	
	@ParamGroup
	public ScalarFromSpace mix;
	
	@DefaultFactory
	public void defaults()
	{
		a = new SolidColor(new Color(0,0,0));
		b = new SolidColor(new Color(1,1,1));
		mix = ReflectUtils.createDefault(FractalNoise.class);
	}
	@Override
	public Color getColor(Point3D texCoord) {
		return Color.mix(a.getColor(texCoord), b.getColor(texCoord), mix.getValue(texCoord));
	}

}
