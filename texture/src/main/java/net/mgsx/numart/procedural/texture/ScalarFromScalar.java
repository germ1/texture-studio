package net.mgsx.numart.procedural.texture;

public interface ScalarFromScalar {

	double getValue(double value);
}
