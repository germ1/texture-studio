package net.mgsx.numart.procedural.texture;

import net.mgsx.numart.framework.NamedElement;

public abstract class ColorFromSpaceOperator extends NamedElement implements ColorFromSpace
{
	public ColorFromSpace operand;
}
