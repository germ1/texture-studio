package net.mgsx.numart.procedural.texture;

import net.mgsx.numart.math.Point3D;

public interface SpaceFromSpace {

	Point3D getPoint(Point3D point);
}
