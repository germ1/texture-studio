package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class FractalTree implements ScalarFromSpace {

	@ParamInt(min=1, max=10)
	public int iterations;
	
	@ParamDouble
	public double range = 0.1;
	
	@ParamDouble
	public double persistence = 0.5; // TODO injecter ce paramètre ...

	@ParamDouble(min=0, max=Math.PI)
	public double angle = Math.PI / 6;

	@Override
	public double getValue(Point3D point) 
	{
		point = new Point3D(
				MathUtil.remainValue(point.x),
				MathUtil.remainValue(point.y),
				MathUtil.remainValue(point.z));
		return getValueRecursive(point, iterations, range);
	}
	
	public double getValueRecursive(Point3D point, int iteration, double size)
	{
		if(iteration <= 0 || point.x > 1 || point.x < 0 || point.y > 1 || point.y < 0)
		{
			return 0;
		}
		// TODO voir si on rend à la fin ou au début (variation de la taille ...)
		if(iteration <= 1)
		{
			// TODO revoir cette equation ...
			return Math.abs(point.x - 0.5) < size / 100 ? 1 : 0;
		}
		// TODO revoir cette constante
		double ratio = 3;
		// TODO calculer les matrices à l'init (precomputation ...)
		Point3D pointA = new Point3D(
				(Math.cos(angle) * (point.x - 0.5) - Math.sin(angle) * (point.y - 1 / ratio)) * ratio + 0.5, 
				(Math.sin(angle) * (point.x - 0.5) + Math.cos(angle) * (point.y - 1 / ratio)) * ratio,
				point.z);
		Point3D pointB = new Point3D(
				(Math.cos(-angle) * (point.x - 0.5) - Math.sin(-angle) * (point.y - 2 / ratio)) * -ratio + 0.5, 
				(Math.sin(-angle) * (point.x - 0.5) + Math.cos(-angle) * (point.y - 2 / ratio)) * ratio,
				point.z);
		Point3D pointC = new Point3D(
				(1-point.x - 0.5) * 3 + 0.5, 
				(point.y - 1. / 3.) * 3,
				point.z);
		Point3D pointD = new Point3D(
				(point.x - 0.5) * 3 + 0.5, 
				(point.y - 2. / 3.) * 3,
				point.z);
		Point3D pointE = new Point3D(
				(point.x - 0.5) * 3 + 0.5, 
				(point.y) * 3,
				point.z);
		
		double nextSize = size * ratio;
		
		// TODO voir si on fait de l'itératif sur les 2 branches ou les 5 ?
		return 
			getValueRecursive(pointA, iteration - 1, nextSize) + 
			getValueRecursive(pointB, iteration - 1, nextSize) + 
			getValueRecursive(pointC, iteration - 1, nextSize) + 
			getValueRecursive(pointD, iteration - 1, nextSize) + 
			getValueRecursive(pointE, iteration - 1, nextSize);
	}
}
