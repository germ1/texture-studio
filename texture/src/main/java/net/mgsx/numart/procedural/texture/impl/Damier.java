package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class Damier extends NamedElement implements ScalarFromSpace {
	public double getValue(Point3D point) {
		return ((floor2(point.x) % 2) ^ (floor2(point.y) % 2) ^ (floor2(point.z) % 2)) > 0 ?
				1 : 0;
	}
	// TODO faire dans util
	private static int floor2(double t){
		return (int)Math.floor(t < 0 ? 1 - t : t); 
	}
}
