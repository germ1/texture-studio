package net.mgsx.numart.procedural.texture.util;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

import net.mgsx.numart.math.Color;
import net.mgsx.numart.procedural.renderer.Rasterizer;
import net.mgsx.numart.procedural.renderer.SoftwareRenderer;

public class ImageUtils {

	public static BufferedImage loadImage(File file) throws IOException
	{
		return ImageIO.read(file);
	}
	
	public static void renderToFile(SoftwareRenderer renderer, String filename, int width, int height)
	{
		// default image.
		final BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_3BYTE_BGR);

		// render.
		renderer.render(new Rasterizer(){
			public void setPixel(int x, int y, Color color) {
				image.setRGB(x, y, color.getRGB());
			}}, width, height, 0, 0, width, height);

		// save.
		saveImageAsPng(image, filename, "jpg");
		
	}
	
	public static void saveImageAsPng(File file, BufferedImage image) throws FileNotFoundException, IOException {
		ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
		FileImageOutputStream fios = new FileImageOutputStream(file);
		writer.setOutput(fios);
		writer.write(image);
	}
	
	public static void saveImageAsPng(RenderedImage rendering, String filename, String ext) {
		ImageWriter writer = ImageIO.getImageWritersByFormatName(ext).next();
		File file = new File(filename);
		try {
			FileImageOutputStream fios = new FileImageOutputStream(file);
			writer.setOutput(fios);
			writer.write(rendering);
			System.out.println(filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// TODO system
	public static void openImageFile(String filename) {
		try {
			Runtime.getRuntime().exec("eog " + filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// TODO system
	public static void openMovieFile(String filename) {
		try {
			Runtime.getRuntime().exec("totem " + filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
