package net.mgsx.numart.procedural.texture;

import net.mgsx.numart.math.Point3D;

public interface ScalarFromSpace {
	
	
	double getValue(final Point3D point);
}
