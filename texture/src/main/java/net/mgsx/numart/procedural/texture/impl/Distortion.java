package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class Distortion extends NamedElement implements ScalarFromSpace {

	@ParamDouble
	public double rate = 0.2;
	
	@ParamGroup(merge=true)
	public FractalNoise noise = new FractalNoise(0xdeadbeef, 7, 0.9);
	
	public double getValue(Point3D point) {
		double d = 2 * noise.getValue(point) - 1;
		return d * rate;
	}

}
