package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.procedural.texture.ScalarOperator;

public class Ramp extends ScalarOperator {

	@ParamDouble
	public double min = 0;
	
	@ParamDouble
	public double max = 1;
	
	public double getValue(double value) {
		double newValue = (value - min) / (max - min);
		return value < min ? 0 : (value > max ? 1 : newValue);
	}

}
