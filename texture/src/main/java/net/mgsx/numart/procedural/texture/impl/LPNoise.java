package net.mgsx.numart.procedural.texture.impl;

import java.util.Random;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.annotations.ParamLong;
import net.mgsx.numart.framework.annotations.Precompute;
import net.mgsx.numart.framework.annotations.RequirePrecompute;
import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class LPNoise  extends NamedElement implements ScalarFromSpace {

	///////////////////////////////////////
	// PERMUTATION
	
	// taille des tables en bits.
	private static final int tableSizeBits = 8;
	
	private static final int tableSize = 1 << tableSizeBits;
	
	private static final int permMask = tableSize - 1;
	
	
	// seed pour la table de permutation.
	private static long permutationSeed = 0xdeadbeaf;
	// la table de permutation.
	private static int [] permutationTable = null;
	
	private static void initPermutationTable(){
		if(permutationTable == null){
			int size = tableSize;
			permutationTable = new int [size];
			Random rand = new Random(permutationSeed);
			for(int i=0 ; i<size ; i++){
				permutationTable[i] = rand.nextInt(size);
			}
		}
	}
	private static int getPermIndex(int n){
		return permutationTable[n & permMask];
	}
	private static int getPermIndex(int x, int y, int z){
		return getPermIndex(x + getPermIndex(y + getPermIndex(z)));
	}
	
	///////////////////////////////////////
	// NOISE
	
	@RequirePrecompute
	@ParamLong
	public long noiseSeed = 0xdeadbeef;
	
	@ParamInt(min=2, max=256)
	public int modulo;
	
	private double [] noiseTable = null;
	
	public LPNoise()
	{
	}
	
	public LPNoise(long seed){
		this.noiseSeed = seed;
	}
	
	@Precompute
	public void initNoiseTable()
	{
		initPermutationTable();
		int size = tableSize;
		noiseTable = new double [size];
		Random rand = new Random(noiseSeed);
		int max = 0x7fffffff;
		for(int i=0 ; i<size ; i++){
			noiseTable[i] = (double)rand.nextInt(max) / ((double)max + 1);
		}
	}
	
	private double getLattice(int x, int y, int z){
		return noiseTable[getPermIndex(x, y, z)];
	}
	
	///////////////////////////////////////
	// IMPLEMENTATION

	public double getValue(Point3D point) {
		double [] tabx = new double [4];
		double [] taby = new double [4];
		double [] tabz = new double [4];
		// d�composition des coordonn�es.
		int ix = MathUtil.integerValue(point.x * modulo );
		double fx = MathUtil.remainValue(point.x * modulo );
		int iy = MathUtil.integerValue(point.y * modulo );
		double fy = MathUtil.remainValue(point.y * modulo );
		int iz = MathUtil.integerValue(point.z * modulo );
		double fz = MathUtil.remainValue(point.z * modulo );
		// interpolation sur les valeurs.
		for(int k=-1 ; k<=2 ; k++){
			for(int j=-1 ; j<=2 ; j++){
				for(int i=-1 ; i<=2 ; i++){
					tabx[i+1] = getLattice(
							((ix+i)%modulo + modulo)%modulo, 
							((iy+j)%modulo + modulo)%modulo, 
							((iz+k)%modulo + modulo)%modulo);
				}
				taby[j+1] = MathUtil.cubicSpline(fx, tabx);
			}
			tabz[k+1] = MathUtil.cubicSpline(fy, taby);
		}
		return MathUtil.cubicSpline(fz, tabz);
	}

}
