package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;

public class TextureMod extends NamedElement implements ColorFromSpace {

	@ParamGroup
	public ColorFromSpace a;
	
	@ParamGroup
	public ColorFromSpace b;
	
	@ParamDouble
	public double burn;
	
	@DefaultFactory
	public void defaults()
	{
		a = new SolidColor(new Color(0,0,0));
		b = new SolidColor(new Color(1,1,1));
		burn = 0.0;
	}
	@Override
	public Color getColor(Point3D texCoord) {
		Color colorA = a.getColor(texCoord);
		Color colorB = b.getColor(texCoord);
		
		if(burn == 0)
		{
			double alphaB = colorB.a;
			return Color.add(Color.multiply(colorA, 1 - alphaB), Color.multiply(colorB, alphaB));
		}
		Color mul = Color.multiply(colorA, Color.add(colorB, new Color(2*burn)));
		return Color.add(Color.multiply(mul, 1), new Color(-burn));
	}

}
