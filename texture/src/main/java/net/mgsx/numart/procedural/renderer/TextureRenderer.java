package net.mgsx.numart.procedural.renderer;

import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamBoolean;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.impl.TextureMixer;

public class TextureRenderer extends AbstractRenderer implements SoftwareRenderer {

	@ParamPoint(min=-1.0, max=1.0)
	public Point3D offset = new Point3D(0.0, 0.0, 0.0);
	
	@Logarithmic(2)
	@ParamPoint(min=1.0/8.0, max=128.0)
	public Point3D size = new Point3D(1,1,1);
	
	@ParamGroup
	public ColorFromSpace texture;
	
	@ParamBoolean
	public boolean spheric = false;
	
	@DefaultFactory
	public void defaults()
	{
		texture = ReflectUtils.createDefault(TextureMixer.class);
		size = new Point3D(4,4,4);
	}

	public TextureRenderer() {
	}
	public TextureRenderer(ColorFromSpace texture) {
		this.texture = texture;
	}
	public void render(Rasterizer rasterizer, int width, int height, int x1, int y1, int x2, int y2) {
		double ya, yb, xa, xb;
		double angleA = 0, angleB = 0;
		for(int y=y1 ; y<y2 ; y++){
			ya = offset.y + size.y * (double)y / (double)height;
			yb = offset.y + size.y * (double)(y+1) / (double)height;
			if(spheric)
			{
				angleA = Math.atan2((ya - size.y/2) / (size.y/2), 1);
				// xa = Math.cos(angleA) * (xa - size.x/2) + size.x/2;
				angleB = Math.atan2((yb - size.y/2) / (size.y/2), 1);
				// xb = Math.cos(angleB) * (xb - size.x/2) + size.x/2;
				ya = (angleA / (Math.PI / 4) + 1) * size.y/2;
				yb = (angleB / (Math.PI / 4) + 1) * size.y/2;
			}
			for(int x=x1 ; x<x2 ; x++){
				xa = offset.x + size.x * (double)x / (double)width;
				xb = offset.x + size.x * (double)(x+1) / (double)width;
				if(spheric)
				{
					xa = Math.cos(angleA) * (xa - size.x/2) + size.x/2;
					xb = Math.cos(angleB) * (xb - size.x/2) + size.x/2;
				}
				rasterizer.setPixel(x, y, Color.clamp(renderPixel(xa, -ya, xb, -yb, antialias)));
			}
		}
		
	}
	private Color renderPixel(double xa, double ya, double xb, double yb, int n){
		Color c = null;
		double xm = (xa + xb)/2;
		double ym = (ya + yb)/2;
		if(n > 1){
			c = Color.mix(
				Color.mix(renderPixel(xa, ya, xm, ym, n-1), renderPixel(xm, ya, xb, ym, n-1)),
				Color.mix(renderPixel(xa, ym, xm, yb, n-1), renderPixel(xm, ym, xb, yb, n-1)));
		}else{
			c = renderPixel(xm, ym);
		}
		return c;
	}
	private Color renderPixel(double x, double y){
		return texture.getColor(new Point3D(x, y, offset.z));
	}

}
