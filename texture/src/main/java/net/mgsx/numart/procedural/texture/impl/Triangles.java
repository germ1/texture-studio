package net.mgsx.numart.procedural.texture.impl;


import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class Triangles extends NamedElement implements ScalarFromSpace {

	public double angle = Math.PI / 3;
	
	public double getValue(Point3D point) {
		
		// Projection
		
		double xangle = Math.PI/2;
		double yangle = -Math.PI/2 - angle;
		
		Point3D vx = new Point3D(Math.cos(xangle), Math.sin(xangle), 0);
		Point3D vy = new Point3D(Math.cos(yangle), Math.sin(yangle), 0);
		Point3D vw = Point3D.add(vx, vy);
		
		// distance
		
		double dx = distance(vx, point);
		double dy = distance(vy, point);
		double dw = distance(vw, point);
		
		double d = Math.max(dx, Math.max(dy, dw));
		
		return d;
	}
	
	public double distance(Point3D vector, Point3D point)
	{
		double dot = vector.dot(point);
		double reste = dot - Math.floor(dot);
		double abs = Math.abs(reste * 2 - 1);
		return abs;
	}
	
}
