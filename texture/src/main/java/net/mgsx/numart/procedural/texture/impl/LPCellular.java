package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.annotations.ParamLong;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.IndexFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class LPCellular extends NamedElement implements ScalarFromSpace, IndexFromSpace {

	private static int [] POISSON_COUNT = {
		4,3,1,1,1,2,4,2,2,2,5,1,0,2,1,2,2,0,4,3,2,1,2,1,3,2,2,4,2,2,5,1,2,3,
		2,2,2,2,2,3,2,4,2,5,3,2,2,2,5,3,3,5,2,1,3,3,4,4,2,3,0,4,2,2,2,1,3,2,
		2,2,3,3,3,1,2,0,2,1,1,2,2,2,2,5,3,2,3,2,3,2,2,1,0,2,1,1,2,1,2,2,1,3,
		4,2,2,2,5,4,2,4,2,2,5,4,3,2,2,5,4,3,3,3,5,2,2,2,2,2,3,1,1,4,2,1,3,3,
		4,3,2,4,3,3,3,4,5,1,4,2,4,3,1,2,3,5,3,2,1,3,1,3,3,3,2,3,1,5,5,4,2,2,
		4,1,3,4,1,5,3,3,5,3,4,3,2,2,1,1,1,1,1,2,4,5,4,5,4,2,1,5,1,1,2,3,3,3,
		2,5,2,3,3,2,0,2,1,1,4,2,1,3,2,1,2,2,3,2,5,5,3,4,5,5,2,4,4,5,3,2,2,2,
		1,4,2,3,3,4,2,5,4,2,4,2,2,2,4,5,3,2
	};
	
	private static double DENSITY_ADJUST = 1; //0.398150;
	
	public int maxOrder = 2;
	
	@ParamLong
	public long seed;

	@ParamInt(min=1, max=32)
	public int modulo = 4;
	
	@ParamDouble(min=-2,max=2)
	public double l0 = 1;
	
	@ParamDouble(min=-2,max=2)
	public double l1 = 0;
	
	@ParamDouble(min=-2,max=2)
	public double l2 = 0;
	
	
	@ParamInt(min=1,max=2)
	public int type = 1;
	
	@Override
	public double getValue(Point3D point) 
	{
		NodeInfo [] infos = searchInfos(point); 
		
//		double value = infos[0].distance * l0 + infos[1].distance * l1;
//		return (1 - value) * 2 - 1;
		double value = infos[0].distance * l0 + infos[1].distance * l1 + l2;
		return (1 - value * 2);
		
// XXX KEEP
//		switch(mode)
//		{
//		default:
//		case 1:
//			return (1 - infos[0].distance) * 2 - 1;
//		case 2:
//			return ((1 - infos[1].distance) * 2 - 1) - ((1 - infos[0].distance) * 2 - 1);
//		case 3:
//			return (infos[1].distance - infos[0].distance) / 2;
//		case 4: 
//			return (1 - infos[0].distance) * 2 - 1 ;
//		}
	}
	
	@Override
	public long getIndex(Point3D point) {
		NodeInfo [] infos = searchInfos(point);
		return infos[0].id;
	}
	
	private NodeInfo [] searchInfos(Point3D point)
	{
		NodeInfo [] infos = new NodeInfo [maxOrder];
		
		for(int i=0 ; i<maxOrder ; i++)
		{
			infos[i] = new NodeInfo();
			infos[i].distance = 1e30;
			infos[i].delta = new Point3D(0, 0, 0);
		}
		
		Point3D newPoint = Point3D.multiply(point, (double)modulo);
		
		for(int ii=-1 ; ii<=1 ; ii++)
		{
			for(int jj=-1 ; jj<=1 ; jj++)
			{
				for(int kk=-1 ; kk<=1 ; kk++)
				{
					addSamples(newPoint, 
							(long)Math.floor(newPoint.x) + ii,
							(long)Math.floor(newPoint.y) + jj,
							(long)Math.floor(newPoint.z) + kk,
							infos);
				}
			}
		}
		
//		for(int i=0 ; i<maxOrder ; i++)
//		{
//			infos[i].distance = Math.sqrt(infos[i].distance) / DENSITY_ADJUST;
//			infos[i].delta.multiply(1.0 / DENSITY_ADJUST);
//		}
//
		return infos;
	}
	
	private void addSamples(Point3D point, long xi, long yi, long zi, NodeInfo [] infos)
	{
		// modulo = 1;
		long mxi = ((xi % modulo) + modulo) % modulo;
		long myi = ((yi % modulo) + modulo) % modulo;
		long mzi = ((zi % modulo) + modulo) % modulo;
		// long seed = 702395077 * mxi + 915488749 * myi + 2120969693 * mzi;
		
		long seed = 0;
		long seedB = this.seed;
		seedB = churn(seedB);
		seed += seedB * mxi;
		seedB = churn(seedB);
		seed += seedB * myi;
		seedB = churn(seedB);
		seed += seedB * mzi;
		
		final int count = POISSON_COUNT[(int)(((seed % 256) + 256) % 256)];
		
		seed = churn(seed);
		
		final Point3D f = new Point3D(0,0,0);
		
		for(int j=0 ; j<count ; j++)
		{
			long id=seed;
			seed = churn(seed);
			f.x = (((double)seed + 0.5) / Math.pow(2, 64))/2.0 + 1.0;
			seed = churn(seed);
			f.y = (((double)seed + 0.5) / Math.pow(2, 64))/2.0 + 1.0;
			seed = churn(seed);
			f.z = (((double)seed + 0.5) / Math.pow(2, 64))/2.0 + 1.0;
			seed = churn(seed);
			
			Point3D d = Point3D.subtract(Point3D.add(new Point3D(xi, yi, zi), f), point);
			
			double distance = d.dot(d);
			switch(type)
			{
			default:
			case 1:
				distance = d.dot(d);
				break;
			case 2:
				distance = Math.abs(d.x) + Math.abs(d.y) + Math.abs(d.z);
				break;
			}

			// push in the list
			
			int index = infos.length - 1;
			if(distance < infos[index].distance)
			{
				infos[index].distance = distance;
				infos[index].id = id;
				infos[index].delta = d;
				
				while(index > 0 && infos[index].distance < infos[index-1].distance)
				{
					NodeInfo tmpInfo = infos[index-1];
					infos[index-1] = infos[index];
					infos[index] = tmpInfo;
				}
			}
			
		}
		
	}
	
	private long churn(long seed)
	{
		return 1402024253 * seed + 586950981;
	}
	
	private static class NodeInfo
	{
		double distance;
		long id;
		Point3D delta;
	}

}
