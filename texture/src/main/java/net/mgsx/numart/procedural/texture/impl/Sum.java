package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class Sum extends NamedElement implements ScalarFromSpace {

	@ParamGroup
	public ScalarFromSpace a;
	
	@ParamGroup
	public ScalarFromSpace b;
	
	
	public Sum() {
		super();
	}
	
	public Sum(ScalarFromSpace a, ScalarFromSpace b) {
		super();
		this.a = a;
		this.b = b;
	}



	public double getValue(Point3D point) {
		return Math.max(0, Math.min(1, a.getValue(point) + b.getValue(point)));
	}

}
