package net.mgsx.numart.procedural.texture;

import net.mgsx.numart.math.Point3D;

public abstract class ScalarOperator extends ScalarFromSpaceOperator implements ScalarFromScalar
{
	@Override
	public final double getValue(Point3D point) {
		return getValue(operand.getValue(point));
	}
}
