package net.mgsx.numart.procedural.renderer;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.Caution;
import net.mgsx.numart.framework.annotations.ParamInt;

abstract public class AbstractRenderer extends NamedElement implements SoftwareRenderer 
{
	@Caution("high value takes more CPU")
	@ParamInt(min=1,max=4)
	public int antialias;

}
