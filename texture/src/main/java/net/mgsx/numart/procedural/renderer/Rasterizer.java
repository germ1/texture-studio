package net.mgsx.numart.procedural.renderer;

import net.mgsx.numart.math.Color;

public interface Rasterizer {
	public void setPixel(int x, int y, Color color);
}
