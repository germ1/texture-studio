package net.mgsx.numart.procedural.texture.impl;

import java.util.Random;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.IndexFromSpace;

public class IndexedColor extends NamedElement implements ColorFromSpace {

	@ParamGroup
	public ColorFromSpace baseColor = new SolidColor(new Color(0.5));
	
	@ParamGroup
	public IndexFromSpace index;
	
	@ParamDouble(min=0.0, max=1.0)
	public double lumRange = 0.0;
	
	@ParamDouble(min=0.0, max=1.0)
	public double satRange = 0.0;
	
	@ParamDouble(min=0.0, max=1.0)
	public double hueRange = 0.0;
	
	@Override
	public Color getColor(Point3D texCoord) 
	{
		Color bc = baseColor.getColor(texCoord);
		
		// RGB => HSV conversion
		float [] vals = new float [3];
		java.awt.Color awtColor = new java.awt.Color((float)bc.r, (float)bc.g, (float)bc.b);
		java.awt.Color.RGBtoHSB(awtColor.getRed(), awtColor.getGreen(), awtColor.getBlue(), vals);
		double hue = vals[0];
		double sat = vals[1];
		double lum = vals[2];
		
		// get the random
		long id = index.getIndex(texCoord);
		Random rnd = new Random(id);
		
		// make the function.
		hue = MathUtil.limit(hue + rnd.nextDouble() * hueRange);
		sat = MathUtil.limit(sat + rnd.nextDouble() * satRange);
		lum = MathUtil.limit(lum + rnd.nextDouble() * lumRange);
		
		// HSV => RGB conversion
		awtColor = new java.awt.Color(java.awt.Color.HSBtoRGB((float)hue, (float)sat, (float)lum));
		awtColor.getColorComponents(vals);
		return new Color(vals[0], vals[1], vals[2]);
	}

}
