package net.mgsx.numart.procedural.renderer;

import net.mgsx.numart.math.Color;

public interface Rasterizer3D {
	public void setPixel(int x, int y, int z, Color color);
}
