package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.math.Point3D;

public class Marble extends FractalNoise {
	
	@Logarithmic(10)
	@ParamDouble(min=0.01, max=100)
	public double factor = 8.0;
	
	public Marble(){
		super();
	}
	public Marble(long seed){
		super(seed, 5, 0.5);
	}
	@Override
	public double getValue(Point3D point) {
		double value = super.getValue(point);
		return MathUtil.limit(Math.abs((value - 0.5) * factor));
	}
	
	
}
