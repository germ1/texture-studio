package net.mgsx.numart.procedural.renderer;

import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;

public class Texture3DRenderer extends TextureRenderer implements SoftwareRenderer {

	public Texture3DRenderer() {
	}
	public Texture3DRenderer(ColorFromSpace texture) {
		this.texture = texture;
	}
	public void render(Rasterizer3D rasterizer, int width, int height, int depth, int x1, int y1, int x2, int y2, int z1, int z2) {
		double ya, yb, xa, xb, za, zb;
		for(int z=z1 ; z<z2 ; z++){
			za = offset.z + size.z * (double)z / (double)depth;
			zb = offset.z + size.z * (double)(z+1) / (double)depth;
			for(int y=y1 ; y<y2 ; y++){
				ya = offset.y + size.y * (double)y / (double)height;
				yb = offset.y + size.y * (double)(y+1) / (double)height;
				for(int x=x1 ; x<x2 ; x++){
					xa = offset.x + size.x * (double)x / (double)width;
					xb = offset.x + size.x * (double)(x+1) / (double)width;
					rasterizer.setPixel(x, y, z, Color.clamp(renderPixel(xa, -ya, xb, -yb, za, zb, antialias)));
				}
			}
		}
	}
	private Color renderPixel(double xa, double ya, double xb, double yb, double za, double zb, int n){
		Color c = null;
		double xm = (xa + xb)/2;
		double ym = (ya + yb)/2;
		double zm = (za + zb)/2;
		if(n > 1){
			c = Color.mix(
					Color.mix(
						Color.mix(renderPixel(xa, ya, xm, ym, za, zm, n-1), renderPixel(xm, ya, xb, ym, za, zm, n-1)),
						Color.mix(renderPixel(xa, ym, xm, yb, za, zm, n-1), renderPixel(xm, ym, xb, yb, za, zm, n-1))
					),
					Color.mix(
						Color.mix(renderPixel(xa, ya, xm, ym, zm, zb, n-1), renderPixel(xm, ya, xb, ym, zm, zb, n-1)),
						Color.mix(renderPixel(xa, ym, xm, yb, zm, zb, n-1), renderPixel(xm, ym, xb, yb, zm, zb, n-1))
					)
				);
		}else{
			c = renderPixel(xm, ym, zm);
		}
		return c;
	}
	private Color renderPixel(double x, double y, double z){
		return texture.getColor(new Point3D(x, y, z));
	}

}
