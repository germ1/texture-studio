package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.Caution;
import net.mgsx.numart.framework.annotations.DefaultFactory;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.math.Point3D;

public class FractalNoise extends Noise {
	
	@Caution
	@ParamInt(min=1,max=10)
	public int octavia;
	
	@ParamDouble
	public double persistence;
	
	@DefaultFactory
	public void defaults()
	{
		octavia = 3;
		persistence = 0.8;
	}
	public FractalNoise()
	{
		super();
	}
	public FractalNoise(long seed, int octavia, double persistence) {
		super(seed);
		this.octavia = octavia;
		this.persistence = persistence;
	}
	@Override
	public double getValue(Point3D point) {
		double value = 0;
		Point3D p = new Point3D(point);
		double amp = 1;
		double fac = 0;
		for(int i=0 ; i<octavia ; i++){
			value += 2 * (super.getValue(p) - 0.5) * amp;
			fac += amp;
			amp *= persistence;
			p.multiply(2);
		}
		return ((value / fac) + 1) / 2;
	}
}
