package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class Compositor extends NamedElement implements ColorFromSpace 
{
	@ParamGroup
	public ColorFromSpace color;
	
	@ParamGroup
	public ScalarFromSpace alpha;
	
	@Override
	public Color getColor(Point3D point) {
		Color c = color.getColor(point);
		c.a = alpha.getValue(point);
		return c;
	}

}
