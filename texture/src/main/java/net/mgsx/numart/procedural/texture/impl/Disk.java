package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.IndexFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class Disk extends NamedElement implements ScalarFromSpace, IndexFromSpace {

	@Override
	public double getValue(Point3D point) {
		
		double dx = reste(point.x);
		double dy = reste(point.y);
		double dz = reste(point.z);
		
		return Point3D.subtract(new Point3D(dx,dy,dz), new Point3D(0.5, 0.5, 0.5)).length();
	}
	
	@Override
	public long getIndex(Point3D point) {
		int ix = (int)Math.floor(point.x);
		int iy = (int)Math.floor(point.y);
		int iz = (int)Math.floor(point.z);
		long seed = 702395077 * ix + 915488749 * iy + 2120969693 * iz;
		return seed;
	}

	private static double reste(double t){
		
		int base = (int)Math.floor(t);
		
		double reste = t - base;
		
		return reste;
	}


}
