package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.procedural.texture.ScalarOperator;

public class Inverse extends ScalarOperator {
	
	@Override
	public double getValue(double value) {
		return 1 - value;
	}
}
