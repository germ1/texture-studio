package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.annotations.Precompute;
import net.mgsx.numart.framework.utils.ObjectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;

public class TextureRandomizer extends NamedElement implements ColorFromSpace
{
	@ParamGroup
	public ColorFromSpace base;
	
	@ParamInt(min=1, max=16)
	public int size;
	
	@ParamDouble
	public double randomness = 0.5;
	
	private ColorFromSpace [] copy;
	private int gridDim;
	
	@Precompute
	public void init()
	{
		gridDim = size;
		copy = new ColorFromSpace [size * size];
		for(int i=0 ; i<copy.length ; i++)
		{
			copy[i] = (ColorFromSpace)ObjectUtils.randomizeCopy(base, true, randomness);
		}
	}
	
	@Override
	public Color getColor(Point3D point) 
	{
		int x = (int)Math.floor(point.x * (double)gridDim); 
		int y = (int)Math.floor(point.y * (double)gridDim); 
		int index = (((y % gridDim) + gridDim) % gridDim) * gridDim + (((x % gridDim) + gridDim) % gridDim); 
		
		Color c = null;
		try{
		c = copy[index].getColor(point);
		}catch(Exception e){e.printStackTrace();}
		return c;
	}

}
