package net.mgsx.numart.procedural.texture.util;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;
import net.mgsx.numart.procedural.texture.impl.TextureMixer;

/**
 * @deprecated use {@link TextureMixer} instead
 * 
 * TODO create easy converters (ScalarFromSpace to ColorFromSpace)
 * 
 */
@Deprecated
public class TextureFromScalar extends NamedElement implements ColorFromSpace {

	@ParamColor
	public Color zeroColor;
	
	@ParamColor
	public Color oneColor;
	
	@ParamGroup
	public ScalarFromSpace scalarFromSpace;
	
	public TextureFromScalar(){
		
	}
	public TextureFromScalar(ScalarFromSpace scalarFromSpace){
		this.scalarFromSpace = scalarFromSpace;
		this.zeroColor = new Color(0);
		this.oneColor = new Color(1);
	}
	public TextureFromScalar(ScalarFromSpace scalarFromSpace, Color zeroColor, Color oneColor){
		this.scalarFromSpace = scalarFromSpace;
		this.zeroColor = zeroColor;
		this.oneColor = oneColor;
	}
	
	@Override
	public Color getColor(Point3D texCoord) {
		return Color.mix(zeroColor, oneColor, scalarFromSpace.getValue(texCoord));
	}

}
