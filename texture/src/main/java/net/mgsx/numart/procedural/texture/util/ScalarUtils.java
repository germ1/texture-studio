package net.mgsx.numart.procedural.texture.util;

import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class ScalarUtils {

	public static Point3D getNormal(ScalarFromSpace bumpMap, Point3D normal, Point3D texCoord, double bumpFactor, double e)
	{
		// find the u and vector from normal
		Point3D u = Point3D.cross(normal, Point3D.cross(new Point3D(e, 0, 0), normal));
		Point3D v = Point3D.cross(normal, Point3D.cross(new Point3D(0, e, 0), normal));
		
		// resample to have bump on u and v
		double du = 
			bumpMap.getValue(new Point3D(texCoord.x + u.x, texCoord.y + u.y, texCoord.z + u.z)) -
			bumpMap.getValue(new Point3D(texCoord.x - u.x, texCoord.y - u.y, texCoord.z - u.z));
		double dv = 
			bumpMap.getValue(new Point3D(texCoord.x + v.x, texCoord.y + v.y, texCoord.z + v.z)) -
			bumpMap.getValue(new Point3D(texCoord.x - v.x, texCoord.y - v.y, texCoord.z - v.z));
		
		// make the T and B vector from bump (with scale factor)
		Point3D T = new Point3D(e, 0, du * bumpFactor);
		T.normalize();
		Point3D B = new Point3D(0, e, dv * bumpFactor);
		B.normalize();
		
		// make the N vector from T and B
		Point3D N = Point3D.cross(T, B);
		
		// return the final perturbed normal.
		return new Point3D(T.dot(normal), B.dot(normal), N.dot(normal));
	}
}
