package net.mgsx.numart.procedural.texture.impl;

import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamList;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class GradientColor extends NamedElement implements ColorFromSpace
{
	@ParamList
	public List<GradientElement> elements = new ArrayList<GradientElement>();

	@ParamGroup
	public ScalarFromSpace mix;
	
	@Logarithmic
	@ParamDouble(min=0.1, max=1000)
	public double hardness = 1;
	
	@Override
	public Color getColor(Point3D point) 
	{
		double value = mix.getValue(point);
		GradientElement a = null, b;
		for(GradientElement element : elements)
		{
			if(element.position > value)
			{
				if(a == null) return element.color.getColor(point);
				b = element;
				double t = b.position == a.position ? 0 : Math.pow((value - a.position) / (b.position - a.position), hardness);
				return Color.mix(a.color.getColor(point), b.color.getColor(point), t);
			}
			a = element;
		}
		// TODO Auto-generated method stub
		return elements.get(elements.size() - 1).color.getColor(point);
	}

}
