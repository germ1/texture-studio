package net.mgsx.numart.procedural.texture.util;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromScalar;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class ScalarWrapper extends NamedElement implements ScalarFromSpace {

	@ParamGroup
	public ScalarFromSpace wrappedScalar;
	
	@ParamGroup
	public ScalarFromScalar transformer;
	
	public ScalarWrapper() {
		super();
	}
	public ScalarWrapper(ScalarFromSpace wrappedScalar,
			ScalarFromScalar transformer) {
		super();
		this.wrappedScalar = wrappedScalar;
		this.transformer = transformer;
	}



	public double getValue(Point3D point) {
		return transformer.getValue(wrappedScalar.getValue(point));
	}

}
