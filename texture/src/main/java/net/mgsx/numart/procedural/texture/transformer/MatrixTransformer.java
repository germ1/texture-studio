package net.mgsx.numart.procedural.texture.transformer;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.Logarithmic;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.SpaceFromSpace;

public class MatrixTransformer extends NamedElement implements SpaceFromSpace {

	@ParamPoint(min=-10.0, max=10.0)
	public Point3D offset = new Point3D(0.0, 0.0, 0.0);
	
	@Logarithmic(2)
	@ParamPoint(min=1.0/8.0, max=128.0)
	public Point3D size = new Point3D(1,1,1);

	@Override
	public Point3D getPoint(Point3D point) {
		return Point3D.add(Point3D.multiply(point, size), offset);
	}

}
