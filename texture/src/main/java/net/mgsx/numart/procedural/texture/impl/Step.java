package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.UserDoc;
import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.procedural.texture.ScalarOperator;

@UserDoc
(
title="Rampe",
description="Transorme un scalaire en limitant au delà de certains seuils"
)

public class Step extends ScalarOperator 
{
	@UserDoc(title="Seuil", description="milieu de la rampe")
	@ParamDouble(min=0.0, max=1.0)
	public double threshold = 0.5;
	
	@UserDoc(title="Amplitude", description="largeur de la rampe")
	@ParamDouble(min=0.0, max=1.0)
	public double range = 1.0;
	
	@Override
	public double getValue(double value) 
	{
		if(range > 0)
		{
			return MathUtil.limit(0.5 + (value - threshold) / range);
		}
		else
		{
			return value > threshold ? 1.0 : 0.0;
		}
	}

}
