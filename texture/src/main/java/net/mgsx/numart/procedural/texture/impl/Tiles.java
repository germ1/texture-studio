package net.mgsx.numart.procedural.texture.impl;


import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.IndexFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

public class Tiles extends NamedElement implements ScalarFromSpace, IndexFromSpace {

	@Override
	public double getValue(Point3D point) {
		
		double dx = distance(point.x);
		double dy = distance(point.y);
		double dz = distance(point.z);
		
		return Math.max(dx, Math.max(dy, dz));
	}
	
	@Override
	public long getIndex(Point3D point) {
		int ix = (int)Math.floor(point.x);
		int iy = (int)Math.floor(point.y);
		int iz = (int)Math.floor(point.z);
		long seed = 702395077 * ix + 915488749 * iy + 2120969693 * iz;
		return seed;
	}

	private static double distance(double t){
		
		int base = (int)Math.floor(t);
		
		// from 0 (left) to 1 (right)
		double reste = t - base;
		
		// from 0 (middle) to 1 (external)
		double resteAbs = Math.abs(reste * 2 - 1);
		
		return resteAbs;
	}


}
