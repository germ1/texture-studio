package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpace;

// TODO revoir l'algo de cette texture ...
public class Spider extends NamedElement implements ScalarFromSpace {

	@ParamInt(min=1, max=10)
	public int branches = 3;
	
	@ParamInt(min=1, max=10)
	public int iterations = 6;
	
	@ParamDouble
	public double randomness = 0.2;
	
	@ParamDouble(max=10)
	public double power = 5;
	
	public Point3D [] points;
	
	public void regenerate(){
		points = null;
	}
	
	private void generate(){
		int count = branches * iterations;
		points = new Point3D [count];
		Point3D [] subpoints = new Point3D [branches];
		Point3D center = new Point3D(0.5, 0.5, 0.5);
		double length = 0.5;
		double angle = 0;
		double deltaAngle = Math.PI * 2 / (double)branches;
		for(int b=0 ; b<branches ; b++){
			Point3D p = new Point3D(
					center.x + Math.cos(angle) * length,
					center.y + Math.sin(angle) * length,
					0); // TODO z
			subpoints[b] = p;
			points[b] = p;
			angle += deltaAngle;
		}
		generateRec(points, branches, subpoints, iterations - 2);
	}
	
	private double getRandom(){
		return 1 + randomness * 2 * (Math.random() - 0.5);
	}
	
	private void generateRec(Point3D [] recpoints, int offset, Point3D [] subpoints, int iterations){
		// calcul du centre.
		Point3D center = new Point3D(0, 0, 0);
		for(int i=0 ; i<subpoints.length ; i++){
			center.x += subpoints[i].x / (double)subpoints.length;
			center.y += subpoints[i].y / (double)subpoints.length;
		}
		// calcul des nouveaux points.
		Point3D [] newsubpoints = new Point3D [subpoints.length];
		for(int i=0 ; i<subpoints.length ; i++){
			Point3D newpoint = new Point3D(
				getRandom() * center.x / 3 + getRandom() * subpoints[i].x / 3 + getRandom() * subpoints[(i+1)%subpoints.length].x / 3, 
				getRandom() * center.y / 3 + getRandom() * subpoints[i].y / 3 + getRandom() * subpoints[(i+1)%subpoints.length].y / 3,
				0); // TODO z
			newsubpoints[i] = newpoint;
			// enregistrement.
			recpoints[offset++] = newpoint;
		}
		// g�nration r�cursive.
		if(iterations > 0){
			generateRec(recpoints, offset, newsubpoints, iterations - 1);
		}
	}
	
	public int getIndex(Point3D point){
		if(points == null){
			generate();
		}
		// recherche du point le plus proche.
		int index = 0;
		double l1 = 1e30;
		for(int i=0 ; i<points.length ; i++){
			Point3D p = points[i];
			double dx = p.x - point.x;
			double dy = p.y - point.y;
			double m2 = dx * dx + dy * dy;
			if(m2 < l1){
				l1 = m2;
				index = i;
			}
		}
		return index;
	}
	
	public double getValue(Point3D point) {
		if(points == null){
			generate();
		}
		// recherche du point le plus proche.
		Point3D p1 = null;
		double l1 = 1e30;
		for(int i=0 ; i<points.length ; i++){
			Point3D p = points[i];
			double dx = p.x - point.x;
			double dy = p.y - point.y;
			double m2 = dx * dx + dy * dy;
			if(m2 < l1){
				l1 = m2;
				p1 = p;
			}
		}
		// recherche du deuxi�me point le plus proche.
		double l2 = 1e30;
		for(int i=0 ; i<points.length ; i++){
			Point3D p = points[i];
			double dx = p.x - point.x;
			double dy = p.y - point.y;
			double m2 = dx * dx + dy * dy;
			if(p != p1 && m2 < l2){
				l2 = m2;
			}
		}
		// calcul du taux.
		double t = l1 / l2;
		return 1 - Math.min(1, Math.pow(t, power));
	}

}
