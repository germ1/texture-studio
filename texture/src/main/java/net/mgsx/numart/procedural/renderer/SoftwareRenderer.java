package net.mgsx.numart.procedural.renderer;

import net.mgsx.numart.framework.Renderer;



public interface SoftwareRenderer extends Renderer {
	public void render(Rasterizer rasterizer, int width, int height, int x1, int y1, int x2, int y2);
}
