package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.procedural.texture.ColorFromSpace;

public class GradientElement {

	@ParamGroup
	public ColorFromSpace color;
	
	@ParamDouble
	public double position;
	
}
