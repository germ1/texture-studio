package net.mgsx.numart.procedural.texture.impl;


import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.framework.annotations.ParamPoint;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.SpaceFromSpace;

public class SpaceDistortion extends NamedElement implements SpaceFromSpace {

	@ParamGroup(merge=true)
	public FractalNoise noise = new FractalNoise(0xdeadbeef, 5, 0.7);
	
	@ParamPoint(max=1)
	public Point3D rate = new Point3D(0.2, 0.2, 0.2);
	
	public Point3D getPoint(Point3D point) {
		double d = 2 * noise.getValue(point) - 1;
		return new Point3D(
				point.x + d * rate.x, 
				point.y + d * rate.y, 
				point.z + d * rate.z);
	}

}
