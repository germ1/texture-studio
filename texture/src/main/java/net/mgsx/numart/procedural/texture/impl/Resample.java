package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.procedural.texture.ScalarOperator;

public class Resample extends ScalarOperator 
{
	@ParamDouble
	public double stepsize = 0.2;
	
	@Override
	public double getValue(double value) 
	{
		return stepsize == 0 ? value : stepsize * (double)((int)(value / stepsize));
	}

}
