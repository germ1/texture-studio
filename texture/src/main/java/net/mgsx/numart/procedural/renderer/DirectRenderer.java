package net.mgsx.numart.procedural.renderer;

import java.awt.image.BufferedImage;

import net.mgsx.numart.framework.Renderer;

public interface DirectRenderer extends Renderer
{
	public void render(BufferedImage target);
}
