package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.Caution;
import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.IndexFromSpace;
import net.mgsx.numart.procedural.texture.ScalarFromSpaceOperator;

public class Turbulence extends ScalarFromSpaceOperator implements IndexFromSpace
{
	@Caution
	@ParamInt(min=1,max=10)
	public int octavia = 1;
	
	@ParamDouble(min=-1, max=1)
	public double persistence = 0;
	
	@Override
	public double getValue(Point3D point) {
		double value = 0;
		Point3D p = new Point3D(point);
		double amp = 1;
		double fac = 0;
		for(int i=0 ; i<octavia ; i++){
			value += Math.abs(2 * (operand.getValue(p) - 0.5)) * amp;
			fac += Math.abs(amp);
			amp *= persistence;
			p.multiply(2);
		}
		return (value / fac);
	}

	@Override
	public long getIndex(Point3D point) {
		if(operand instanceof IndexFromSpace)
		{
			Point3D p = new Point3D(point);
			p.multiply(Math.pow(2, octavia - 1));
			return ((IndexFromSpace)operand).getIndex(p);
		}
		return 0;
	}
	
	

}
