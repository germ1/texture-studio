package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.annotations.ParamDouble;
import net.mgsx.numart.procedural.texture.ScalarOperator;

public class Sinusoidal extends ScalarOperator {

	@ParamDouble(min=0.0, max=1.0)
	public double phase = 0.0;
	
	@ParamDouble(min=0.0, max=20.0) // TODO no max but decent max
	public double frequency = 3.0;
	
	public double getValue(double value) 
	{
		// phase at 0 means return 0 for 0 value.
		// frequency at 1 (with 0 phase) means full phase and return 1 for 0 value.
		// frequency at 0 returns constant (phase dependent)
		return 0.5 * (1 - Math.cos(2 * Math.PI * (value * frequency + phase)));
	}

}