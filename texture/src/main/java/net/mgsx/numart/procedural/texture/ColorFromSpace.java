package net.mgsx.numart.procedural.texture;

import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;

/**
 * 3D texture
 * 
 * @author mgsx
 *
 */
public interface ColorFromSpace 
{
	/**
	 * Map a point in a 3D space to a color
	 * @param point texture coordinates
	 * @return a color
	 */
	Color getColor(Point3D point);
}
