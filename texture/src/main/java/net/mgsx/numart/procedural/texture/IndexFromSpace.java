package net.mgsx.numart.procedural.texture;

import net.mgsx.numart.math.Point3D;

/**
 * Interface pour les éléments pouvant déterminer
 * des indices dans l'espace. 
 */
public interface IndexFromSpace 
{
	/**
	 * Retourne un indice en fonction d'un point dans l'espace.
	 * @param point un point dans l'espace
	 * @return un nombre
	 */
	long getIndex(Point3D point);
}
