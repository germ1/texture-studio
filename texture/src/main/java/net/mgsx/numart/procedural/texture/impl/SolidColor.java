package net.mgsx.numart.procedural.texture.impl;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamColor;
import net.mgsx.numart.framework.annotations.QuickParameter;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;

public class SolidColor extends NamedElement implements ColorFromSpace {
	
	@QuickParameter
	@ParamColor
	public Color color = new Color(0.5);
	
	public SolidColor() {
		super();
	}
	public SolidColor(Color color) {
		super();
		this.color = color;
	}
	
	@Override
	public Color getColor(Point3D texCoord) {
		return color;
	}

}
