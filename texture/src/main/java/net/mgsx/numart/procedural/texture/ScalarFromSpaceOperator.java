package net.mgsx.numart.procedural.texture;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamGroup;

abstract public class ScalarFromSpaceOperator extends NamedElement implements ScalarFromSpace 
{
	@ParamGroup
	public ScalarFromSpace operand;
}
