package net.mgsx.numart.procedural.texture.util;

import net.mgsx.numart.framework.annotations.ParamGroup;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ScalarFromSpaceOperator;
import net.mgsx.numart.procedural.texture.SpaceFromSpace;

public class ScalarFromSpaceTransformer extends ScalarFromSpaceOperator 
{
	@ParamGroup
	public SpaceFromSpace transformer;
	
	@Override
	public double getValue(Point3D point) {
		return operand.getValue(transformer.getPoint(point));
	}

}
