package net.mgsx.numart.procedural.texture.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import net.mgsx.numart.framework.NamedElement;
import net.mgsx.numart.framework.annotations.ParamFile;
import net.mgsx.numart.framework.annotations.Precompute;
import net.mgsx.numart.framework.annotations.RequirePrecompute;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.MathUtil;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.ColorFromSpace;
import net.mgsx.numart.procedural.texture.util.ImageUtils;

public class TextureFile extends NamedElement implements ColorFromSpace 
{
	@RequirePrecompute
	@ParamFile
	public String filepath;
	
	private int width;
	private int height;
	private Color [] bitmap;
	
	/** empreinte pour recharger le fichier que s'il a été modifié 
	 * ou que le fichier est un autre */
	private String token = null;

	@Precompute
	public void init()
	{
		if(filepath != null)
		{
			
			File file = new File(filepath);
			String newToken = file.getAbsolutePath() + "!" + file.lastModified();
			if(token == null || !token.equals(newToken))
			{
				try {
					BufferedImage image = ImageUtils.loadImage(file);
					
					width = image.getWidth();
					height = image.getHeight();
					bitmap = new Color [width * height];
					for(int y=0 ; y<height ; y++)
					{
						for(int x=0 ; x<width ; x++)
						{
							bitmap[(height - 1 - y) * width + x] = Color.fromARGB(image.getRGB(x, y));
						}
					}
					token = newToken;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public Color getColor(Point3D point) 
	{
		if(bitmap == null)
			return new Color();
		// TODO interpolation
		int x = (int)MathUtil.moduloValue(point.x * width, width);
		int y = (int)MathUtil.moduloValue(point.y * height, height);
		return bitmap[y * width + x];
	}

}
