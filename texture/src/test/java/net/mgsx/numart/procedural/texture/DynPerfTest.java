package net.mgsx.numart.procedural.texture;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.mgsx.numart.framework.annotations.ParamInt;
import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Point3D;
import net.mgsx.numart.procedural.texture.impl.Cellular;
import net.mgsx.numart.procedural.texture.impl.Noise;
import net.mgsx.numart.procedural.texture.impl.Tiles;
import net.mgsx.numart.procedural.texture.impl.Triangles;

import org.junit.Test;

public class DynPerfTest {

	private static interface Variant
	{
		String getName();
		int getRange();
		void setState(Object object, int index);
	}
	private static class VariantFactory
	{
		static Variant createVariant(Field field)
		{
			if(field.getAnnotation(ParamInt.class) != null)
				return new IntVariantAdapter(field);
			return new UnsupportedVariantAdapter(field);
		}
	}
	private abstract static class VariantAdapter implements Variant
	{
		Field field;
		VariantAdapter(Field field){
			this.field = field;
		}
		public String getName(){return field.getName();}
	}
	private static class UnsupportedVariantAdapter extends VariantAdapter
	{

		UnsupportedVariantAdapter(Field field) {
			super(field);
		}

		@Override
		public int getRange() {
			return 1;
		}

		@Override
		public void setState(Object object, int index) {
			// Nothing
		}
		
	}
	private static class IntVariantAdapter extends VariantAdapter
	{

		IntVariantAdapter(Field field) {
			super(field);
		}

		@Override
		public int getRange() {
			return 1 + field.getAnnotation(ParamInt.class).max() - field.getAnnotation(ParamInt.class).min();
		}

		@Override
		public void setState(Object object, int index) {
			ReflectUtils.set(object, field, field.getAnnotation(ParamInt.class).min() + index);
		}
		
	}
	
	public static interface Invocation
	{
		void invoke(Object object);
	}
	
	Invocation invocation;
	
	int iteration;
	long totaltime;
	int totaliteration;
	
	private void genRecursive(Object object, List<Variant> variants, int index)
	{
		if(iteration < totaliteration)
		{
			if(index < variants.size())
			{
				Variant variant = variants.get(index);
				for(int i=0 ; i<variant.getRange() ; i++)
				{
					variant.setState(object, i);
					genRecursive(object, variants, index + 1);
				}
			}
			else
			{
				long t0 = System.currentTimeMillis();
				invocation.invoke(object);
				long t1 = System.currentTimeMillis();
				long t = t1 - t0;
				totaltime += t;
				iteration++;
			}
		}
	}
	
	public void genTest(Class<?> cls, Invocation invocation, int requestedIterations)
	{
		this.invocation = invocation;
		
		// initialisation
		totaltime = 0;
		iteration = 0;
		totaliteration = requestedIterations;
		// on créé une instance proprement initialisée
		// pour jouer le test.
		Object object = ReflectUtils.newInstance(cls);
		ReflectUtils.checkForPrecomputation(object);
		
		// on liste les variants du test
		List<Variant> variants = new ArrayList<Variant>();
		for(Field field : ReflectUtils.getFields(cls))
		{
			variants.add(VariantFactory.createVariant(field));
		}
		
		// calcul du nombre de combinaisons
		int totalrange = 1;
		for(Variant variant : variants)
		{
			totalrange *= variant.getRange();
		}
		
		// on appel la méthode récursive pour traiter tous les variants
		// de manière conjugué (produit cartésien de tous les variants)
		for(iteration=0 ; iteration<totaliteration ; )
		{
			genRecursive(object, variants, 0);
		}
		
		// impression des résultats :
		System.out.println(cls.getSimpleName() + " : " + ((double)totaltime / 1000) + " s");
	}
	
	public void genTestSFS(Class<?> cls)
	{
		genTest(cls, new Invocation() 
		{
			Point3D point = new Point3D();
			
			@Override
			public void invoke(Object object) {
				((ScalarFromSpace)object).getValue(point);
			}
		}, 320 * 240);
		
	}
	
	@Test
	public void test()
	{
		genTestSFS(Cellular.class);
		genTestSFS(Noise.class);
		genTestSFS(Tiles.class);
		genTestSFS(Triangles.class);
		
		
	}
}
