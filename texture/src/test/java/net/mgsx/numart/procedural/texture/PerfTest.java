package net.mgsx.numart.procedural.texture;

import net.mgsx.numart.framework.utils.ReflectUtils;
import net.mgsx.numart.math.Color;
import net.mgsx.numart.math.Point3D;
import net.mgsx.test.PerfMockery;

import org.junit.Test;

public class PerfTest 
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void testAll()
	{
		PerfMockery mockery = new PerfMockery();
//		ScalarFromSpace noise = mockery.mock(ScalarFromSpace.class, (ScalarFromSpace)ReflectUtils.newInstance(Noise.class, true));
//		noise.getValue(new Point3D());
		
		mockery.mockMap.put(String.class, "mock");
		
		mockery.mockMap.put(ScalarFromSpace.class, new ScalarFromSpace() {
			@Override
			public double getValue(Point3D point) {
				return 0;
			}
		});
		mockery.mockMap.put(ScalarFromScalar.class, new ScalarFromScalar() {
			@Override
			public double getValue(double value) {
				return value;
			}
		});
		
		mockery.mockMap.put(Point3D.class, new Point3D());
		mockery.mockMap.put(Color.class, new Color());
		
		
		ScalarFromSpace sfs = mockery.mock(
				ScalarFromSpace.class, 
				"net.mgsx.numart.procedural.texture",
				new PerfMockery.Factory(){
					@Override
					public Object newInstance(Class cls) throws Exception {
						return ReflectUtils.newInstance(cls, true);
					}});
		try{
			sfs.getValue(new Point3D());
		}catch(Throwable e)
		{
			e.printStackTrace();
		}
		ColorFromSpace cfs = mockery.mock(
				ColorFromSpace.class, 
				"net.mgsx.numart.procedural.texture",
				new PerfMockery.Factory(){
					@Override
					public Object newInstance(Class cls) throws Exception {
						return ReflectUtils.newInstance(cls, true);
					}});
		try{
			cfs.getColor(new Point3D());
		}catch(Throwable e)
		{
			e.printStackTrace();
		}
		
		System.out.println(mockery.getReport());
	}
}
