package net.mgsx.numart.procedural.texture;

import java.util.Collection;

import net.mgsx.numart.math.Point3D;
import net.mgsx.test.AbstractClassSetTest;
import net.mgsx.test.LabelledParametrized;
import net.mgsx.test.LabelledParametrized.Parameters;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(LabelledParametrized.class)
public class ColorFromSpaceTest extends AbstractClassSetTest
{
	private Class<?> cls;
	
	public ColorFromSpaceTest(String name, Class<?> cls) {
		super(name);
		this.cls = cls;
	}
	
	@Parameters
	public static Collection<Object[]> generateTests()
	{
		Collection<Object[]> col =
				   generateTests("net.mgsx.numart", ColorFromSpace.class);
		
		return col;
	}
	
	@Test
	public void testGetValue() throws Exception
	{
		ColorFromSpace sfs = (ColorFromSpace)cls.newInstance();
		sfs.getColor(new Point3D());
	}
	@Test
	@Ignore
	public void testGetValueNull() throws Exception
	{
		ColorFromSpace sfs = (ColorFromSpace)cls.newInstance();
		sfs.getColor(null);
	}

}
