package net.mgsx.numart.runtime.rendering;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

// TODO industrialiser (extract annotatio, move to tools project ...)
public class CmdlineContext 
{
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	public static @interface Argument
	{
		boolean required() default false;
		String description() default "";
		String name() default "";
		String mnemonic() default "";
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface Command
	{
		String description() default "";
		String name() default "";
		String mnemonic() default "";
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	public static @interface Script
	{
		String description() default "";
		String name() default "";
	}
	
	public static void exec(String[] args, Class<?> cls) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		Object instance = cls.newInstance();
		CmdlineContext cmdlineContext = new CmdlineContext(args, instance);
		cmdlineContext.run();
	}
	
	String [] args;
	
	Object Handler;

	private CmdlineContext(String[] args, Object handler) {
		super();
		this.args = args;
		Handler = handler;
	}
	
	public void run() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{
		Map<String, Method> mapCommand = new HashMap<String, Method>();
		Map<String, String> mapArgument = new HashMap<String, String>();
		String command = "";
		
		for(int i=0 ; i<args.length ; i++)
		{
			String arg = args[i];
			if(arg.startsWith("-"))
			{
				mapArgument.put(arg, args[i+1]);
				i++;
			}
			else if(i == 0)
			{
				command = arg;
			}
		}
		
		String help = "";
		
		Script aScript = Handler.getClass().getAnnotation(Script.class);
		help += "---------------------------------------------" + "\n";
		help += aScript.name() + "\n";
		help += "---------------------------------------------" + "\n";
		help += aScript.description() + "\n";
		help += "\n";
		help += "Usage : \n";
		help += "\n";
		
		for(Method method : Handler.getClass().getMethods())
		{
			Command aCommand = method.getAnnotation(Command.class);
			if(aCommand != null)
			{
				help += "command " + aCommand.name() + " [" + aCommand.mnemonic() + "] : " + aCommand.description() + "\n";
				mapCommand.put(aCommand.mnemonic(), method);
			}
		}
		help += "\n";
		for(Field field : Handler.getClass().getFields())
		{
			Argument aArg = field.getAnnotation(Argument.class);
			if(aArg != null)
			{
				help += "option " + aArg.name() + " [" + aArg.mnemonic() + "] : " + aArg.description() + "\n";
				field.set(Handler, mapArgument.get(aArg.mnemonic()));
			}
		}
		if(args.length < 1)
		{
			System.out.println(help);
		}
		else
		{
			Method m = mapCommand.get(command);
			if(m == null)
			{
				System.out.println("unsupported command " + command);
				// System.out.println(help);
			}
			else
			{
				m.invoke(Handler);
			}
		}
	}
	
	
}
