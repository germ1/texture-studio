package net.mgsx.numart.runtime.rendering;

import java.io.File;
import java.io.FileNotFoundException;

import net.mgsx.numart.persistence.PersistenceUtil;
import net.mgsx.numart.realtime.RTSceneRenderer;
import net.mgsx.numart.runtime.rendering.CmdlineContext.Argument;
import net.mgsx.numart.runtime.rendering.CmdlineContext.Command;
import net.mgsx.numart.runtime.rendering.CmdlineContext.Script;
import net.mgsx.numart.runtime.rendering.ScreenManager.Screen;

@Script
(
	name = "Numart Runtime",
	description = "Numart headless tools"
)
public class NumartRuntime 
{

	/**
	 * NumartRuntime entry point
	 * 
	 * @param args cmdline arguments, pass nothing to have
	 * 				full documentation.
	 * 
	 * @throws Exception on unhandled error
	 * 
	 */
	public static void main(String[] args) throws Exception 
	{
		CmdlineContext.exec(args, NumartRuntime.class);
	}
	
	@Argument
	(
		name = "File",
		mnemonic = "-f",
		required = true,
		description = "Path to the xml file to load"
	)
	public String file;
	
	@Command
	(
		name = "Load",
		mnemonic = "load",
		description = "Load an xml file and run it until escape is pressed"
	)
	public void load() throws FileNotFoundException
	{
		Object root = new PersistenceUtil().load(new File(file));
		if(root instanceof RTSceneRenderer)
		{
			Screen s = ScreenManager.getConfigurations().iterator().next();
			FullscreenFrame.open((RTSceneRenderer)root, s);
		}
		else
		{
			System.err.println("unsupported type " + root.getClass().getName());
		}
	}

}
