package net.mgsx.numart.runtime.rendering;

import java.awt.DisplayMode;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.List;

public class ScreenManager 
{
	public static class Screen
	{
		public GraphicsDevice device;
		public DisplayMode displayMode;
		public GraphicsConfiguration configuration;
	}
	
	public static Screen getConfiguration(GraphicsDevice device)
	{
		Screen s = new Screen();
		s.device = device;
		s.displayMode = device.getDisplayMode();
		s.configuration = device.getDefaultConfiguration();
		return s;
	}
	public static List<Screen> getConfigurations()
	{
		List<Screen> result = new ArrayList<Screen>();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		for(GraphicsDevice d : ge.getScreenDevices())
		{
//			for(GraphicsConfiguration c : d.getConfigurations())
//			{
				for(DisplayMode dm : d.getDisplayModes())
				{
					Screen s = new Screen();
					s.device = d;
					s.displayMode = dm;
					s.configuration = d.getDefaultConfiguration();
					result.add(s);
				}
//			}
		}
		return result;
	}
}
