package net.mgsx.numart.runtime.rendering;

import java.awt.BorderLayout;
import java.awt.DisplayMode;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.List;

import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;

import net.mgsx.numart.realtime.RTRenderer;
import net.mgsx.numart.runtime.rendering.ScreenManager.Screen;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.FPSAnimator;

public class FullscreenFrame extends Frame implements GLEventListener 
{
	private static final long serialVersionUID = 1L;

	private static FullscreenFrame instance;

	public static void open(RTRenderer renderer, Screen screen) {
		if(instance != null)
		{
			instance.dispose();
			instance = null;
		}
		instance = new FullscreenFrame(renderer, screen);
	}

	private Screen oldScreen;

	private boolean fullscreen = false;
	private boolean displayChanged = false;

	private GLCanvas canvas;
	private Animator animator;

	private RTRenderer renderer;

	public static List<DisplayMode> getDisplayModes()
	{
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = ge.getDefaultScreenDevice();
		return Arrays.asList(device.getDisplayModes());
	}
	
	private void initDisplayMode(Screen screen) 
	{
		oldScreen = ScreenManager.getConfiguration(screen.device);
	}

	private FullscreenFrame(RTRenderer renderer, Screen screen) 
	{
		this.renderer = renderer;
		initDisplayMode(screen);

		// Use all scrren for this frame in case fullscreen does not work.
		setSize(screen.displayMode.getWidth(), screen.displayMode.getHeight());

		setUndecorated(true);

		if (screen.device.isFullScreenSupported()) 
		{
			try {
				screen.device.setFullScreenWindow(this);
				fullscreen = true;
			} catch (Exception e) {
				screen.device.setFullScreenWindow(null);
				fullscreen = false;
			}
			// Once an application is in full-screen exclusive mode,
			// it may be able to take advantage of actively setting the display
			// mode.
			if (fullscreen && screen.device.isDisplayChangeSupported()) {
				try {
					screen.device.setDisplayMode(screen.displayMode);
					displayChanged = true;
				} catch (Exception e) {
					oldScreen.device.setDisplayMode(oldScreen.displayMode);
					displayChanged = false;
				}
			}
		}

		setFocusable(true);

		KeyListener keyListener = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				finish();
				dispose();
			}
		};
		
		addKeyListener(keyListener);
		
		setLayout(new BorderLayout());

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				finish();
			}
		});

		canvas = new GLCanvas();
		
		canvas.addKeyListener(keyListener);
		
		
		add(canvas);
		animator = new FPSAnimator(canvas, 60, true);
		canvas.addGLEventListener(this);
		setVisible(true);
		requestFocus();

		animator.start();
	}
	
	private void finish()
	{
		if(animator != null)
		{
			animator.stop();
		}

		if (fullscreen) {
			if (oldScreen.device.isFullScreenSupported()) {
				oldScreen.device.setFullScreenWindow(null);
				System.out.println("Exit fullscreen done.");
				if (displayChanged && oldScreen.device.isDisplayChangeSupported()) {
					oldScreen.device.setDisplayMode(oldScreen.displayMode);
				}
				fullscreen = false;
			}
		}
		
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		renderer.render(drawable.getGL(), drawable.getWidth(), drawable.getHeight());
	}

	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean arg1,
			boolean arg2) {

	}

	@Override
	public void init(GLAutoDrawable drawable) {
		renderer.init(drawable.getGL());
	}

	@Override
	public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3,
			int arg4) {
	}

}
