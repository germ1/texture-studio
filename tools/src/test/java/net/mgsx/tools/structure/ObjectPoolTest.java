package net.mgsx.tools.structure;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ObjectPoolTest 
{
	private static final int poolcapacity = 2; 
	private ObjectPool<String> pool;
	
	@Before
	public void init()
	{
		pool = new ObjectPool<String>(poolcapacity){

			@Override
			protected String[] allocate(int size) {
				return new String[size];
			}

			@Override
			protected String allocate() {
				return new String();
			}
			
		};
	}
	
	@Test
	public void testNominal()
	{
		Assert.assertEquals(0, pool.size());
		
		pool.create();
		
		Assert.assertEquals(1, pool.size());

		String s2 = pool.create();
		
		Assert.assertEquals(2, pool.size());

		pool.create();

		Assert.assertEquals(3, pool.size());

		for(Iterator<String> i = pool.iterator() ; i.hasNext() ; )
		{
			String s = i.next();
			if(s == s2)
			{
				i.remove();
			}
		}
		
		Assert.assertEquals(2, pool.size());
	}
	@Test(expected=IllegalStateException.class)
	public void testRemoveWithoutNext()
	{
		pool.iterator().remove();
	}
	@Test(expected=IllegalStateException.class)
	public void testRemoveTwice()
	{
		pool.create();
		pool.create();
		Iterator<String> i = pool.iterator();
		i.next();
		i.next();
		i.remove();
		i.remove();
	}
}
