package net.mgsx.tools.structure;

import java.util.concurrent.Semaphore;

import org.junit.Assert;
import org.junit.Test;

public class FIFOTest 
{
	@Test(timeout=1000)
	public void testNominal()
	{
		final FIFO<String> fifo = new FIFO<String>();
		
		Assert.assertTrue(fifo.empty());
		Assert.assertNull(fifo.peek());
		fifo.push(null);
		Assert.assertTrue(fifo.empty());
		Assert.assertNull(fifo.peek());
		fifo.push("toto");
		Assert.assertFalse(fifo.empty());
		Assert.assertEquals("toto", fifo.peek());
		Assert.assertEquals("toto", fifo.poll());
		Assert.assertTrue(fifo.empty());
		Assert.assertNull(fifo.peek());
	}
	
	@Test(timeout=1000)
	public void testThreads() throws Exception
	{
		final FIFO<String> fifo = new FIFO<String>();
		
		final String pushedValue = "aaa";
		
		final Semaphore lock = new Semaphore(0);
		
		Thread threadPolling = new Thread(new Runnable() {
			@Override
			public void run() {
				fifo.poll();
				lock.release();
			}
		});
		
		Thread threadPushing = new Thread(new Runnable() {
			@Override
			public void run() {
				fifo.push(pushedValue);
			}
		});
		
		threadPolling.start();
		
		fifo.push("bbb");
		lock.acquire();
		Assert.assertTrue(fifo.empty());
		
		threadPushing.start();
		threadPushing.join();
		threadPolling.join();
		
		Assert.assertFalse(fifo.empty());
		
		Assert.assertSame(pushedValue, fifo.peek());
	}
}
