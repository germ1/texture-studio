package net.mgsx.tools.structure.graph;

import java.util.List;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMock.class)
public class GraphTest 
{
	private Mockery context;
	
	private Graph<Object, Object> graph;
	
	private Node [] nodes;
	
	private Link [][] links;

	private final int NUM_NODES = 3;

	private static class Node
	{
		int id;

		public Node(int id) {
			this.id = id;
		}
		
		@Override
		public String toString() {
			return "Node " + id;
		}
	}
	private static class Link
	{
		int idSource;
		int idTarget;

		public Link(int idSource, int idTarget) {
			this.idSource = idSource;
			this.idTarget = idTarget;
		}
		
		@Override
		public String toString() {
			return "Link " + idSource + "-" + idTarget;
		}
	}
	
	@Before
	public void createGraph()
	{
		
		context = new Mockery();
		
		// make the objects
		
		graph =  new Graph<Object, Object>();

		nodes = new Node [NUM_NODES];
		
		for(int i=0 ; i<NUM_NODES ; i++)
		{
			nodes[i] = new Node(i);
		}
		
		links = new Link [NUM_NODES][NUM_NODES];
		for(int i=0 ; i<NUM_NODES ; i++)
		{
			for(int j=0 ; j<NUM_NODES ; j++)
			{
				links[i][j] = new Link(i, j);
			}
		}
	}
	
	@Test
	public void testAddNode() throws Exception
	{
		// start with a fresh graph
		graph =  new Graph<Object, Object>();
		
		@SuppressWarnings("unchecked")
		final GraphListener<Object, Object> graphListener = (GraphListener<Object, Object>)context.mock(GraphListener.class);
		
		graph.addGraphListener(graphListener);
		
		final Object node1 = new Object();

		context.checking(new Expectations(){{
			
			one(graphListener).onNodeAdded(with(node1));
			
		}});
		
		List<Object> allNodes;
		
		// test add node
		
		graph.addNode(node1);
		
		allNodes = graph.getAllNodes();
		Assert.assertEquals(1, allNodes.size());
		Assert.assertSame(node1, allNodes.get(0));
		
		// test add doublon node
		
		graph.addNode(node1);
		
		allNodes = graph.getAllNodes();
		Assert.assertEquals(1, allNodes.size());
		Assert.assertSame(node1, allNodes.get(0));
		
	}
	
	@Ignore("à corriger")
	@Test
	public void testRemoveNode() throws Exception
	{
		// setup the links
		graph.link(nodes[0], nodes[1], links[0][1]);
		graph.link(nodes[2], nodes[0], links[2][0]);
		
		
		@SuppressWarnings("unchecked")
		final GraphListener<Object, Object> graphListener = (GraphListener<Object, Object>)context.mock(GraphListener.class);
		
		@SuppressWarnings("unchecked")
		final LinkListener<Object, Object> linkListener = (LinkListener<Object, Object>)context.mock(LinkListener.class, "l1");

		@SuppressWarnings("unchecked")
		final LinkListener<Object, Object> link2Listener = (LinkListener<Object, Object>)context.mock(LinkListener.class, "l2");

		@SuppressWarnings("unchecked")
		final NodeListener<Object, Object> node1Listener = (NodeListener<Object, Object>)context.mock(NodeListener.class, "NodeListener1");

		@SuppressWarnings("unchecked")
		final NodeListener<Object, Object> node2Listener = (NodeListener<Object, Object>)context.mock(NodeListener.class, "NodeListener2");

		graph.addGraphListener(graphListener);
		graph.addNodeListener(node1Listener, nodes[0]);
		graph.addNodeListener(node2Listener, nodes[1]);
		graph.addLinkListener(linkListener, nodes[0], links[0][1]);
		graph.addLinkListener(link2Listener, nodes[2], links[2][0]);
		
		// test remove not existing node
		graph.removeNode(new Object());
		
		// test remove node
		
		context.checking(new Expectations(){{
			
			one(graphListener).onNodeRemoved(with(nodes[0]));
			one(graphListener).onLinkRemoved(with(nodes[0]), with(nodes[1]), with(links[0][1]));
			one(graphListener).onLinkRemoved(with(nodes[2]), with(nodes[0]), with(links[2][0]));
			
			one(node1Listener).onRemoved();
			one(node1Listener).onLinkRemoved(with(nodes[0]), with(nodes[1]), with(links[0][1]));
			one(node1Listener).onLinkRemoved(with(nodes[2]), with(nodes[0]), with(links[2][0]));
			
			one(node2Listener).onLinkRemoved(with(nodes[0]), with(nodes[1]), with(links[0][1]));
			
			one(linkListener).onRemoved();
			
			one(link2Listener).onRemoved();
			
		}});
		
		List<Object> allNodes;
		
		graph.removeNode(nodes[0]);
		
		allNodes = graph.getAllNodes();
		Assert.assertEquals(2, allNodes.size());
		
		// test remove 2
		
		context.checking(new Expectations(){{
			
			one(graphListener).onNodeRemoved(with(nodes[1]));
			one(node2Listener).onRemoved();
			
		}});
		
		graph.removeNode(nodes[1]);
		
		allNodes = graph.getAllNodes();
		Assert.assertEquals(1, allNodes.size());
	}
	
	@Test
	public void testLink()
	{
		@SuppressWarnings("unchecked")
		final GraphListener<Object, Object> graphListener = (GraphListener<Object, Object>)context.mock(GraphListener.class);
		
		graph.addGraphListener(graphListener);
		
		context.checking(new Expectations(){{
			
			one(graphListener).onNodeAdded(with(nodes[0]));
			one(graphListener).onNodeAdded(with(nodes[1]));
			one(graphListener).onLinkAdded(with(nodes[0]), with(nodes[1]), with(links[0][1]));
		}});
		
		graph.link(nodes[0], nodes[1], links[0][1]);
		
		Assert.assertEquals(2, graph.getAllNodes().size());
		Assert.assertEquals(1, graph.getAllTargetLinks(nodes[0]).size());
		Assert.assertEquals(0, graph.getAllTargetLinks(nodes[1]).size());
	}
	
	@Test
	public void testLink_change()
	{
		graph.addNode(nodes[2]);
		graph.link(nodes[0], nodes[1], links[0][1]);

		@SuppressWarnings("unchecked")
		final GraphListener<Object, Object> graphListener = (GraphListener<Object, Object>)context.mock(GraphListener.class);
		
		graph.addGraphListener(graphListener);
		
		context.checking(new Expectations(){{
			
			one(graphListener).onLinkChanged(with(nodes[0]), with(links[0][1]), with(nodes[1]), with(nodes[2]));
		}});
		
		
		graph.link(nodes[0], nodes[2], links[0][1]);
		
		Assert.assertEquals(3, graph.getAllNodes().size());
		Assert.assertEquals(1, graph.getAllTargetLinks(nodes[0]).size());
		Assert.assertEquals(0, graph.getAllTargetLinks(nodes[1]).size());
		Assert.assertEquals(0, graph.getAllTargetLinks(nodes[2]).size());
		
	}
	
	@Test
	public void testLink_null()
	{
		graph.link(nodes[0], nodes[1], links[0][1]);

		@SuppressWarnings("unchecked")
		final GraphListener<Object, Object> graphListener = (GraphListener<Object, Object>)context.mock(GraphListener.class);
		
		graph.addGraphListener(graphListener);
		
		context.checking(new Expectations(){{
			
			one(graphListener).onLinkChanged(with(nodes[0]), with(links[0][1]), with(nodes[1]), with((Object)null));
		}});
		
		
		graph.link(nodes[0], null, links[0][1]);
		
		Assert.assertEquals(1, graph.getAllTargetLinks(nodes[0]).size());
	}
	@Test
	public void testUnlink()
	{
		graph.link(nodes[0], nodes[1], links[0][1]);
		
		@SuppressWarnings("unchecked")
		final GraphListener<Object, Object> graphListener = (GraphListener<Object, Object>)context.mock(GraphListener.class);
		
		graph.addGraphListener(graphListener);
		
		context.checking(new Expectations(){{
			
			one(graphListener).onLinkRemoved(with(nodes[0]), with(nodes[1]), with(links[0][1]));
		}});
		
		graph.unlink(nodes[0], links[0][1]);

		Assert.assertEquals(0, graph.getAllTargetLinks(nodes[0]).size());
		Assert.assertEquals(0, graph.getAllTargetLinks(nodes[1]).size());
		Assert.assertEquals(0, graph.getAllTargetLinks(nodes[2]).size());
	}
}
