package net.mgsx.tools.ipc;

import net.mgsx.test.AsyncRunner.Timeout;
import net.mgsx.test.AsyncTest;
import net.mgsx.test.SynchronizedMock;
import net.mgsx.tools.ipc.Collector.CollectorTask;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CollectorTest extends AsyncTest
{
	Collector subject;
	
	@Before
	public void before()
	{
		subject = new Collector();
	}
	
	@After
	public void after()
	{
		if(subject != null)
		{
			subject.cleanup();
		}
	}
	
	@Test(timeout=1000)
	public void testCasInitial()
	{
		// le collecteur peut prendre la main
		// (lock en cas d'erreur)
		subject.begin();
		subject.end();
	}
	
	@Test(expected=IllegalStateException.class)
	public void testExceptionDoubleBegin()
	{
		subject.begin();
		subject.begin();
	}
	@Test(expected=IllegalStateException.class)
	public void testExceptionDoubleEnd()
	{
		subject.end();
		subject.end();
	}
	
	@Test(timeout=1000)
	public void testExceptionOnCritical()
	{
		SynchronizedMock<CollectorTask> mockA = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		CollectorTask m = mockA.mock();
		
		subject.addTask(m);
		
		validateFirstSequence(mockA);
		
		mockA.exception(RuntimeException.class).criticalSection();
		
		subject.begin();
		subject.end();
		
		mockA.waitForCall().criticalSection();
		mockA.waitForCall().cleanup();
		
		Assert.assertTrue(mockA.validate());
		
		subject.begin();
	}
	@Test(timeout=1000)
	public void testExceptionOnFree()
	{
		SynchronizedMock<CollectorTask> mockA = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		CollectorTask m = mockA.mock();
		
		subject.addTask(m);
		
		mockA.exception(RuntimeException.class).freeSection();
		
		mockA.waitForCall().setup();
		mockA.waitForCall().freeSection();
		mockA.waitForCall().cleanup();
		
		Assert.assertTrue(mockA.validate());
		
		subject.begin();
	}
	@Test(timeout=1000)
	public void testExceptionOnSetup()
	{
		SynchronizedMock<CollectorTask> mockA = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		CollectorTask m = mockA.mock();
		mockA.exception(RuntimeException.class).setup();
		subject.addTask(m);
		
		mockA.waitForCall().setup();
		mockA.waitForCall().cleanup();

		Assert.assertTrue(mockA.validate());
		
		// Assert.assertEquals(0, subject.size());
	}
	@Test(timeout=1000)
	public void testExceptionOnClean()
	{
		SynchronizedMock<CollectorTask> mockA = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		CollectorTask m = mockA.mock();
		subject.addTask(m);
		validateFirstSequence(mockA);
		mockA.exception(RuntimeException.class).cleanup();
		subject.cleanup();
		mockA.waitForCall().cleanup();

		Assert.assertTrue(mockA.validate());
	}

	private static void validateFirstSequence(SynchronizedMock<CollectorTask> mock)
	{
		mock.waitForCall().setup();
		mock.waitForCall().freeSection();
		mock.waitForCall().criticalSection();
		mock.waitForCall().freeSection();
		
		Assert.assertTrue(mock.validate());
	}
	private static void validateLoopSequence(SynchronizedMock<CollectorTask> mock)
	{
		mock.waitForCall().criticalSection();
		mock.waitForCall().freeSection();
		
		Assert.assertTrue(mock.validate());
	}
	private static void validateExitSequence(SynchronizedMock<CollectorTask> mock)
	{
		mock.waitForCall().cleanup();
		
		Assert.assertTrue(mock.validate());
	}
	
	@Test(timeout=1000)
	public void testSequenceSimple() throws Exception
	{
		SynchronizedMock<CollectorTask> mockA = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		CollectorTask m = mockA.mock();
		
		subject.addTask(m);
		
		validateFirstSequence(mockA);
		
		subject.begin();
		
		
		subject.end();
		
		validateLoopSequence(mockA);
		
		subject.cleanup();
		validateExitSequence(mockA);
	}

	@Test(timeout=1000)
	public void testNominal()
	{
		SynchronizedMock<CollectorTask> mockA = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		SynchronizedMock<CollectorTask> mockB = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		SynchronizedMock<CollectorTask> mockC = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		CollectorTask a = mockA.mock();
		CollectorTask b = mockB.mock();
		CollectorTask c = mockC.mock();
		subject.addTask(a);
		subject.addTask(b);
		subject.addTask(c);
		
		validateFirstSequence(mockA);
		validateFirstSequence(mockB);
		validateFirstSequence(mockC);
		
		subject.begin();
		
		
		subject.end();
		
		validateLoopSequence(mockA);
		validateLoopSequence(mockB);
		validateLoopSequence(mockC);
		
		subject.cleanup();
		validateExitSequence(mockA);
		validateExitSequence(mockB);
		validateExitSequence(mockC);
	}
	
	
	@Timeout(3500)
	@Test(timeout=6000)
	public void testLoop() throws Throwable
	{
		// total : 3200 ms
		final int N = 100;
		
		testLoop(N, 1);
	}
	private void testLoop(int N, int coeff) throws Throwable
	{
		// loop : 32 ms
		final int aTime = 1 * coeff;
		final int bTime = 2 * coeff;
		final int cTime = 3 * coeff;
		final int time = 1 * coeff;
		
		SynchronizedMock<CollectorTask> mockA = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		SynchronizedMock<CollectorTask> mockB = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		SynchronizedMock<CollectorTask> mockC = new SynchronizedMock<CollectorTask>(CollectorTask.class);
		CollectorTask a = mockA.mock(aTime);
		CollectorTask b = mockB.mock(bTime);
		CollectorTask c = mockC.mock(cTime);
		
		subject.addTask(a);
		subject.addTask(b);
		subject.addTask(c);

		validateFirstSequence(mockA);
		validateFirstSequence(mockB);
		validateFirstSequence(mockC);

		for(int i=0 ; i < N ; i++)
		{
			subject.begin();
			
			Thread.sleep(time);		
			
			Assert.assertTrue(mockA.validate());			
			Assert.assertTrue(mockA.validate());			
			Assert.assertTrue(mockA.validate());			
			
			subject.end();
			
			validateLoopSequence(mockA);
			validateLoopSequence(mockB);
			validateLoopSequence(mockC);
		}
		
		subject.cleanup();
		validateExitSequence(mockA);
		validateExitSequence(mockB);
		validateExitSequence(mockC);
	}
	
	public static void main(String[] args) throws Throwable 
	{
		CollectorTest test = new CollectorTest();
		test.before();
		test.testLoop(100, 4000);
		test.after();
	}
	
}
