package net.mgsx.tools.ipc;

import net.mgsx.test.SynchronizedMock;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MultiResourceContextTest 
{
	MultiResourceContext ctx;
	
	@Before
	public void setup()
	{
		ctx = new MultiResourceContext();
	}
	
	@Test(timeout=1000)
	public void testEmpty()
	{
		ctx.acquire();
		ctx.release();
	}
	
	@Test(timeout=1000)
	public void testNominal()
	{
		SynchronizedMock<LoopWorkflow> mockeryA = new SynchronizedMock<LoopWorkflow>(LoopWorkflow.class);
		LoopWorkflow mockA = mockeryA.mock();
		
		ctx.addProcess(mockA);
		
		mockeryA.waitForCall().setup(null);
		mockeryA.waitForCall().proceed(null);
		
		Assert.assertTrue(mockeryA.validate());
		
		ctx.acquire();
		
		Assert.assertTrue(mockeryA.validate());
		
		ctx.release();
		
		mockeryA.waitForCall().proceed(null);
		
		Assert.assertTrue(mockeryA.validate());
		
		ctx.finish();
		
		mockeryA.waitForCall().cleanup(null);
		
		Assert.assertTrue(mockeryA.validate());
	}
	@Test(timeout=1000)
	public void testRemove()
	{
		SynchronizedMock<LoopWorkflow> mockeryA = new SynchronizedMock<LoopWorkflow>(LoopWorkflow.class);
		LoopWorkflow mockA = mockeryA.mock();
		
		ctx.addProcess(mockA);
		
		mockeryA.waitForCall().setup(null);
		mockeryA.waitForCall().proceed(null);
		
		Assert.assertTrue(mockeryA.validate());
		
		ctx.acquire();
		
		Assert.assertTrue(mockeryA.validate());
		
		ctx.release();
		
		mockeryA.waitForCall().proceed(null);
		
		Assert.assertTrue(mockeryA.validate());
		
		ctx.removeProcess(mockA);
		
		mockeryA.waitForCall().cleanup(null);
		
		Assert.assertTrue(mockeryA.validate());
		
		ctx.acquire();
		ctx.release();
		
		ctx.finish();
		
		Assert.assertTrue(mockeryA.validate());
	}
	@Test//(timeout=1000)
	public void testExceptionSetup()
	{
		SynchronizedMock<LoopWorkflow> mockeryA = new SynchronizedMock<LoopWorkflow>(LoopWorkflow.class);
		LoopWorkflow mockA = mockeryA.mock();
		mockeryA.exception(RuntimeException.class).setup(null);
		ctx.addProcess(mockA);
		
		mockeryA.waitForCall().setup(null);
		mockeryA.waitForCall().cleanup(null);
		
		ctx.acquire();
		ctx.release();
		
		ctx.finish();
		
		Assert.assertTrue(mockeryA.validate());
	}
	@Test(timeout=1000)
	public void testExceptionCleanup()
	{
		SynchronizedMock<LoopWorkflow> mockeryA = new SynchronizedMock<LoopWorkflow>(LoopWorkflow.class);
		LoopWorkflow mockA = mockeryA.mock();
		mockeryA.exception(RuntimeException.class).cleanup(null);
		ctx.addProcess(mockA);
		
		mockeryA.waitForCall().setup(null);
		mockeryA.waitForCall().proceed(null);
		
		ctx.acquire();
		ctx.release();
		
		ctx.finish();
		
		mockeryA.waitForCall().cleanup(null);
		
		Assert.assertTrue(mockeryA.validate());
	}
	@Test(timeout=1000)
	public void testExceptionProceed()
	{
		SynchronizedMock<LoopWorkflow> mockeryA = new SynchronizedMock<LoopWorkflow>(LoopWorkflow.class);
		LoopWorkflow mockA = mockeryA.mock();
		mockeryA.exception(RuntimeException.class).proceed(null);
		ctx.addProcess(mockA);
		
		mockeryA.waitForCall().setup(null);
		mockeryA.waitForCall().proceed(null);
		mockeryA.waitForCall().cleanup(null);
		
		ctx.finish();
		
		Assert.assertTrue(mockeryA.validate());
	}
}
