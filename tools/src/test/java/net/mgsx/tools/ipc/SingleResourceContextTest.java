package net.mgsx.tools.ipc;

import net.mgsx.test.ThreadMock;

import org.junit.Test;

public class SingleResourceContextTest 
{
	@Test
	public void testNominal()
	{
		ResourceContext ctx = new SingleResourceContext();
		ctx.acquire();
		ctx.release();
	}
	@Test
	public void testAcquire()
	{
		ResourceContext ctx = new SingleResourceContext();
		ctx.acquire();
		ctx.acquire();
		ctx.release();
		ctx.release();
	}
	@Test
	public void testTwoThreads()
	{
		final ResourceContext ctx = new SingleResourceContext();
		
		ThreadMock tA = new ThreadMock();
		ThreadMock tB = new ThreadMock();
		
		// A lock la resource
		tA.executeSync(new Runnable() {
			@Override
			public void run() {
				ctx.acquire();
			}
		});
		
		// B lock la resource
		tB.executeAsync(new Runnable() {
			@Override
			public void run() {
				ctx.acquire();
			}
		});
		
		tB.assertRunning();
		
		// A unlock la resource
		tA.executeSync(new Runnable() {
			@Override
			public void run() {
				ctx.release();
			}
		});
		
		// validation : A vide et B vide
		tA.dispose();
		tB.dispose();
	}
}
