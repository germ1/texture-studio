package net.mgsx.tools.logging;

public class LogEntry {
	public String title;
	public boolean enabled = false;
	public LogEntry(String title, boolean enabled) {
		super();
		this.title = title;
		this.enabled = enabled;
	}
	
}
