package net.mgsx.tools.logging;

public class Logger 
{
	// TODO refactor : mettre ça dans le FWK
	public static final LogEntry ERROR = new LogEntry("Error", true);
	public static final LogEntry RENDER = new LogEntry("Rendering", false);
	public static final LogEntry MIDI = new LogEntry("Midi", false);
	public static final LogEntry FILEIO = new LogEntry("File", false);
	
	public static void enable(LogEntry log, boolean state)
	{
		log.enabled = state;
	}
	public static void log(LogEntry log, String message)
	{
		if(log.enabled)
		{
			System.out.println("[" + Thread.currentThread().getName() + "] (" + log.title + ") " + message);
		}
	}
	public static void warn(LogEntry log, String message)
	{
		log(log, message);
	}
	public static void error(LogEntry log, Throwable e)
	{
		if(ERROR.enabled || log.enabled)
		{
			System.err.println("[" + Thread.currentThread().getName() + "] ERROR : ");
			e.printStackTrace();
		}
	}
	public static void error(LogEntry log, String message)
	{
		if(ERROR.enabled || log.enabled)
		{
			System.err.println("[" + Thread.currentThread().getName() + "] ERROR : " + message);
		}
	}
	public static boolean enabled(LogEntry log)
	{
		return log.enabled;
	}
}
