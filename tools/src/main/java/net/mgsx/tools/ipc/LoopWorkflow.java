package net.mgsx.tools.ipc;

public interface LoopWorkflow 
{
	public void setup(ResourceContext context);
	
	public void proceed(ResourceContext context);
	
	public void cleanup(ResourceContext context);
}
