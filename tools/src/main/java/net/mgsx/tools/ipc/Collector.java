package net.mgsx.tools.ipc;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Design pattern Collector :
 * A collector wait for several resources to be complete and
 * consistent. Each resources are filled by a single asynchrone task.
 * 
 * <br><br>
 * 
 * [FR]
 * Design pattern Collector : un collecteur attend que plusieurs resources
 * soient disponible et traite ces resources dans une phase critique
 * (begin/end), en dehors de cette phase, les collecteurs peuvent manipuler
 * leur resources partagé. Durant cette phase, les collecteurs peuvent
 * seulement manipuler leur propre resources.
 * 
 * <br><br>
 * 
 * 
 * 
 */
@Deprecated
class Collector 
{
	/**
	 * interface pour une tache de collecteur. 
	 */
	public static interface CollectorTask
	{
		/** initialisation */
		public void setup();
		
		/** phase durant laquelle le collecteur peut manipuler les resources partagées */
		public void criticalSection();
		
		/** phase durant laquelle le collecteur ne peut pas manipuler les resources partagées */
		public void freeSection();
		
		/** phase de nettoyage */
		public void cleanup();
	}
	
	/**
	 *  
	 */
	private class CollectorRunner implements Runnable 
	{
		// le thread réalisant la tache
		Thread thread;
		
		// la tache réalisée
		CollectorTask task;

		// la tache commence donc n'est pas terminée
		volatile boolean done = false;
		
		// lock sur la section critique
		Semaphore resourceRequest = new Semaphore(0);
		Semaphore resourceAvailable = new Semaphore(0);
		
		/**
		 * Initialisation de la tache
		 * @param task
		 */
		public CollectorRunner(CollectorTask task) 
		{
			// enregistrement de la tache associée
			this.task = task;
		}
		
		@Override
		public void run() 
		{
			try 
			{
				// initialisation de la tache
				task.setup();
				
				// la tache boucle indéfiniement
				while(!done)
				{
					try
					{
						// la tache non critique est executé
						task.freeSection();
						
						// puis la critique (avec prise de jeton) :
						
						// prise du jeton
						resourceRequest.acquire();
						
						if(!done)
						{
							// execution de la tache
							task.criticalSection();
						}
					} finally {
						// dans tous les cas, on rend le jeton
						resourceAvailable.release();
					}
				}
				
			} catch (InterruptedException e) {
			} finally {
				
				// dans tous les cas
				task.cleanup();
				removeTask(task);
			}
		}
	}
	
	private Semaphore doneTokens;
	private boolean criticalSection;
	
	private Lock threadLock = new ReentrantLock();
	
	private Map<CollectorTask, CollectorRunner> threads;
	
	public Collector() {
		// 1 seméphore pour le collecteur
		doneTokens = new Semaphore(0);
		criticalSection = false;
		// permet de retrouver les thread à partir d'une tache.
		threads = new ConcurrentHashMap<CollectorTask, CollectorRunner>();
	}
	
	/**
	 * add a collecting task
	 * @param runnable
	 */
	public synchronized void addTask(CollectorTask task)
	{
		// création d'un thread pour la tache.
		CollectorRunner runner = new CollectorRunner(task);
		runner.thread = new Thread(runner, task.getClass().getSimpleName());
		
		// démarrage du thread
		// la tache n'est pas encore authorisée à
		// entrer en section critique.
		runner.thread.start();
		
		// on s'assure que le begin n'est pas exécuté en même temps
		// que la modification de la liste des taches.
		threadLock.lock();
		try
		{
			// ajout à la liste des thread
			threads.put(task, runner);
			// on peut désormais authoriser la tache à
			// exécuter la section critique
			// runner.token.release();
		}
		finally
		{
			threadLock.unlock();
		}
	}
	
	public void removeTask(CollectorTask task)
	{
		threadLock.lock();
		try
		{
			CollectorRunner runner = threads.get(task);
			if(runner != null)
			{
				runner.done = true;
				threads.remove(task);
			}
		} 
		finally
		{
			threadLock.unlock();
		}
	}
	
	public void begin()
	{
		if(criticalSection)
		{
			throw new IllegalStateException("begin can only be called once");
		}
		criticalSection = true;
		threadLock.lock();
		try
		{
			doneTokens.acquire(threads.size());
		} 
		catch (InterruptedException e) 
		{
			throw new RuntimeException(e);
		}
		finally
		{
			threadLock.unlock();
		}
	}
	
	public void end()
	{
		if(!criticalSection)
		{
			throw new IllegalStateException("end can only be called once");
		}
		criticalSection = false;
		
		threadLock.lock();
		try
		{
			for(CollectorRunner runner : threads.values())
			{
				runner.resourceRequest.release();
			}
		}
		finally
		{
			threadLock.unlock();
		}
	}
	
	public void cleanup()
	{
		// TODO if criticalSection ... danger !
		for(CollectorRunner runner : threads.values())
		{
			runner.done = true;
			runner.resourceAvailable.release();
		}
		for(CollectorRunner runner : threads.values())
		{
			try {
				runner.thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
