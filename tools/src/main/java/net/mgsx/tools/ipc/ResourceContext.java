package net.mgsx.tools.ipc;

public interface ResourceContext {

	public abstract void acquire();

	public abstract void release();

}