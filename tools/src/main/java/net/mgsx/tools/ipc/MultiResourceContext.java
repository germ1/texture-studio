package net.mgsx.tools.ipc;

import java.util.HashMap;
import java.util.Map;

public class MultiResourceContext implements ResourceContext
{
	// la map
	private Map<LoopWorkflow, LoopTask> map = new HashMap<LoopWorkflow, LoopTask>();
	
	// verrou pour l'état de la map.
	private ResourceContext mapContext = new SingleResourceContext();
	
	public void addProcess(LoopWorkflow process) 
	{
		// création de la tache et de son contexte.
		LoopTask task = new LoopTask(process, new SingleResourceContext());
		
		// démarrage de la tache.
		task.start();
		
		mapContext.acquire();
		try
		{
			// ajout dans la map
			map.put(process, task);
		}
		finally
		{
			mapContext.release();
		}
	}

	public void removeProcess(LoopWorkflow process) 
	{
		mapContext.acquire();
		try
		{
			LoopTask task = map.get(process);
			if(task != null)
			{
				// demande asynchrone de fin de tache
				// et une requete pour dévérouiller la tache 
				// si elle est en attente de requete.
				task.end = true;
				task.requests.release();
				// suppression de la liste
				map.remove(process);
			}
		}
		finally
		{
			mapContext.release();
		}
	}

	public void finish() {
		mapContext.acquire();
		try
		{
			// demande asynchrone de fin de tache
			// et une requete pour dévérouiller la tache 
			// si elle est en attente de requete.
			for(LoopTask task : map.values())
			{
				task.end = true;
				task.requests.release();
			}
			// attend la fin de chaque thread des taches
			for(LoopTask task : map.values())
			{
				try {
					task.join();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
			// cleanup
			map.clear();
		}
		finally
		{
			mapContext.release();
		}
	}

	public void acquire() 
	{
		mapContext.acquire();
		try
		{
			// attend que chaque resource soit disponible
			for(LoopTask task : map.values().toArray(new LoopTask []{}))
			{
				if(task.end)
				{
					map.remove(task);
				}
				else
				{
					try {
						task.available.acquire();
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		finally
		{
			mapContext.release();
		}
	}

	public void release() 
	{
		mapContext.acquire();
		try
		{
			// ajout d'une requete par tache
			for(LoopTask task : map.values())
			{
				task.requests.release();
			}
		}
		finally
		{
			mapContext.release();
		}
	}

}
