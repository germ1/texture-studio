package net.mgsx.tools.ipc;

import java.util.concurrent.Semaphore;

class LoopTask extends Thread
{
	volatile boolean end = false;
	
	LoopWorkflow process;
	
	ResourceContext context;
	
	// rien de disponible en début de tache
	Semaphore available = new Semaphore(0);
	// une demande en début de tache
	Semaphore requests = new Semaphore(1);
	
	public LoopTask(LoopWorkflow process, ResourceContext context) {
		super();
		this.process = process;
		this.context = context;
	}

	@Override
	public void run() 
	{
		try
		{
			process.setup(context);
			while(!end)
			{
				requests.acquire();
				try
				{
					if(!end)
					{
						process.proceed(context);
					}
				} finally {
					available.release();
				}
			}
		} catch (InterruptedException e) {
		}
		finally
		{
			end = true;
			available.release();
			process.cleanup(context);
		}
	}
	
}