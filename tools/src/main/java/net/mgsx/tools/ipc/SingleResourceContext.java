package net.mgsx.tools.ipc;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SingleResourceContext implements ResourceContext 
{
	private Lock resource = new ReentrantLock();
	
	/* (non-Javadoc)
	 * @see net.mgsx.tools.ipc.ResourceContext2#release()
	 */
	@Override
	public void release() {
		resource.unlock();
	}
	
	/* (non-Javadoc)
	 * @see net.mgsx.tools.ipc.ResourceContext2#acquire()
	 */
	@Override
	public void acquire() {
		resource.lock();
	}
}
