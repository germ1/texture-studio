package net.mgsx.tools.lang;

public class LangTools 
{
	public static <T,U> U map (T key, U defaultValue, Object...keyValues)
	{
		for(int i=0 ; i<keyValues.length ; i+=2)
		{
			if(key == (T)keyValues[i])
			{
				return (U)keyValues[i];
			}
		}
		return defaultValue;
	}
}
