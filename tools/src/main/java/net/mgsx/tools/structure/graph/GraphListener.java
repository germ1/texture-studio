package net.mgsx.tools.structure.graph;

/**
 * Listener for events on a the graph 
 * @param <T>
 * @param <V>
 */
public interface GraphListener<T, V>
{
	void onNodeAdded(T node);
	void onNodeRemoved(T node);
	void onLinkAdded(T sourceNode, T targetNode, V linkData);
	void onLinkRemoved(T sourceNode, T targetNode, V linkData);
	void onLinkChanged(T sourceNode, V linkData, T oldTargetNode, T newTargetNode);
}