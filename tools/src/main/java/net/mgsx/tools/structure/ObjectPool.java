package net.mgsx.tools.structure;

import java.util.Iterator;

/**
 * not thread safe
 */
public abstract class ObjectPool <T> implements Iterable<T>
{
	private T [] pool;
	
	private int poolsize;
	
	private int reallocsize;
	
	public ObjectPool(int capacity) {
		this(capacity, capacity);
	}
	public ObjectPool(int capacity, int realloc) {
		reallocsize = realloc;
		poolsize = 0;
		pool = allocate(capacity);
		for(int i=0 ; i<capacity ; i++)
		{
			pool[i] = allocate();
		}
	}
	
	public int size(){
		return poolsize;
	}
	
	public T create()
	{
		if(poolsize < pool.length - 1)
		{
			return pool[poolsize++];
		}
		T [] newPool = allocate(poolsize + reallocsize);
		for(int i=0 ; i<poolsize ; i++)
		{
			newPool[i] = pool[i];
		}
		for(int i=0 ; i<reallocsize ; i++)
		{
			newPool[poolsize + i] = allocate();
		}
		pool = newPool;
		return pool[poolsize++];
	}
	
	abstract protected T [] allocate(int size);
	
	abstract protected T allocate();
	
	@Override
	public Iterator<T> iterator() 
	{
		return new ObjectPoolIterator();
	}
	
	public class ObjectPoolIterator implements Iterator<T> 
	{
		private int index = 0;
		
		private boolean canRemove = false;
		
		@Override
		public boolean hasNext() {
			return index < poolsize;
		}

		@Override
		public T next() {
			canRemove = true;
			return pool[index++];
		}

		@Override
		public void remove() {
			if(!canRemove)
			{
				throw new IllegalStateException("element can't be removed");
			}
			index--;
			T tmp = pool[poolsize-1];
			pool[poolsize-1] = pool[index];
			pool[index] = tmp;
			poolsize--;
			canRemove = false;
		}

	}	
	
}
