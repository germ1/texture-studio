package net.mgsx.tools.structure.graph;

public class NodeAdapter<T, V> implements NodeListener<T, V> {

	@Override
	public void onLinkAdded(T sourceNode, T targetNode, V linkData) {
	}

	@Override
	public void onLinkRemoved(T sourceNode, T targetNode, V linkData) {
	}

	@Override
	public void onLinkChanged(T sourceNode, V linkData, T oldTargetNode,
			T newTargetNode) {
	}

	@Override
	public void onRemoved() {
	}

	@Override
	public void onUpdate() {
		
	}

}
