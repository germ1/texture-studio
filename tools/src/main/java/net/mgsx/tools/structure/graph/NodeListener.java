package net.mgsx.tools.structure.graph;

/**
 * Listener for events on a specific node 
 * 
 * @author mgsx
 *
 * @param <T> Node type
 * @param <V> Link type
 * 
 */
public interface NodeListener<T, V>
{
	/**
	 * Fired when a new link added from the node to a new target node.
	 * @param sourceNode
	 * @param targetNode
	 * @param linkData
	 */
	void onLinkAdded(T sourceNode, T targetNode, V linkData);
	void onLinkRemoved(T sourceNode, T targetNode, V linkData);
	void onLinkChanged(T sourceNode, V linkData, T oldTargetNode, T newTargetNode);
	
	/**
	 * Fired when the node has been removed from the graph.
	 */
	void onRemoved();
	
	/**
	 * TODO CR documenter
	 */
	void onUpdate();
}