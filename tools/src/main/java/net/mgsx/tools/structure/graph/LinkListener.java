package net.mgsx.tools.structure.graph;

/**
 * Listener for events on a specific link 
 * @param <T>
 * @param <V>
 */
public interface LinkListener<T, V>
{
	void onRemoved();
	void onChanged(T oldTargetNode, T newTargetNode);
}