package net.mgsx.tools.structure.graph;

public class GraphAdapter<T, V> implements GraphListener<T, V> {

	@Override
	public void onNodeAdded(T node) 
	{
		// nothing to do.
	}

	@Override
	public void onNodeRemoved(T node) 
	{
		// nothing to do.
	}

	@Override
	public void onLinkAdded(T sourceNode, T targetNode, V linkData) 
	{
		// nothing to do.
	}

	@Override
	public void onLinkRemoved(T sourceNode, T targetNode, V linkData) 
	{
		// nothing to do.
	}

	@Override
	public void onLinkChanged(T sourceNode, V linkData, T oldTargetNode,
			T newTargetNode) 
	{
		// nothing to do.
	}

}
