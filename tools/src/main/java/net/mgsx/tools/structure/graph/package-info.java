/**
 * 
 * Graph Structure Tools
 * <br>
 * <br>
 * 
 * Create a {@link net.mgsx.tools.structure.graph.Graph} object and then
 * add nodes to thegraph, link nodes, listen to events on graph, node or link.<br>
 * <br>
 * 
 * Listener interfaces for the 3 kind of subject are : <br>
 * {@link net.mgsx.tools.structure.graph.NodeListener}<br>
 * {@link net.mgsx.tools.structure.graph.LinkListener}<br>
 * {@link net.mgsx.tools.structure.graph.GraphListener}<br>
 * <br>
 * 
 * Listener adapter for each interfaces : <br>
 * {@link net.mgsx.tools.structure.graph.NodeAdapter}<br>
 * {@link net.mgsx.tools.structure.graph.LinkAdapter}<br>
 * {@link net.mgsx.tools.structure.graph.GraphAdapter}<br>
 * <br>
 */

package net.mgsx.tools.structure.graph;