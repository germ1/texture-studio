package net.mgsx.tools.structure;

import java.util.LinkedList;

public class FIFO <T>
{
	private LinkedList<T> list = null; 
	
	public FIFO() {
		list = new LinkedList<T>();
	}
	
	public void push(final T element)
	{
		if(element != null)
		{
			synchronized (list) {
				list.push(element);
				if(list.size() == 1)
				{
					list.notifyAll();
				}
			}
		}
	}
	public boolean empty()
	{
		synchronized (list) {
			return list.size() == 0;
		}
	}
	public T peek()
	{
		synchronized (list) 
		{
			return list.peek();
		}
	}
	
	public T poll()
	{
		synchronized (list) 
		{
			if(list.size() == 0)
			{
				try {
					list.wait();
				} catch (InterruptedException e) {
					// omit interruption
				}
			}
			return list.poll();
		}
	}
}
