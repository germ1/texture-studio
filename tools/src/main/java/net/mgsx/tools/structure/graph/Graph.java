package net.mgsx.tools.structure.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Graph contains nodes and links and handle all graph operation
 * such as add or remove nodes, add or remove link, change link.
 * A graph warn all listener about any modifications. 
 * 
 * A graph is a set of nodes of type T and a set of links of type V.
 * 
 * <p>
 * How to create nodes ? <br>
 * 
 * By adding an unlinked node with {@link #addNode(Object)} or by
 * linking to another node with {@link #link(Object, Object, Object)}.
 * </p>
 * <p>
 * How to listen ? <br>
 * 
 * You can listen to a single node with <br>
 * {@link #addNodeListener(NodeListener, Object)}<br>
 * or listen to a specific link binding two nodes with<br>
 * {@link #addLinkListener(LinkListener, Object, Object)}.<br>
 * finally you can listen to the entire graph with<br>
 * {@link #addGraphListener(GraphListener)}<br>
 * <br>
 * Then you should implements<br> 
 * {@link NodeListener}, {@link LinkListener}, {@link GraphListener}<br>
 * or use the adapters<br>
 *  {@link NodeAdapter}, {@link LinkAdapter}, {@link GraphAdapter}.<br>
 * <br>
 * When you no longer need to listen to the graph, you may call
 * the remove methods :<br>
 * {@link #removeNodeListener(NodeListener, Object)}<br>
 * {@link #removeLinkListener(LinkListener, Object, Object)}<br>
 * {@link #removeGraphListener(GraphListener)}.<br>
 * 
 * @todo The Graph implementation don't use weak references yet so keep it
 * in mind.
 * </p>
 */
public class Graph <T, V>
{
	private class Node
	{
		/** the data linked to the node */
		T data;
		Map<V, Link> sources = new HashMap<V, Link>();
		Map<V, Link> targets = new HashMap<V, Link>();
		public Node(T data) {
			this.data = data;
		}
		List<NodeListener<T, V>> listeners = new ArrayList<NodeListener<T, V>>();
	}
	
	private class Link
	{
		V data;
		Node source;
		Node target;
		List<LinkListener<T, V>> listeners = new ArrayList<LinkListener<T, V>>();
	}
	
	private Map<T, Node> mapNodes = new HashMap<T, Node>();
	
	private List<GraphListener<T, V>> listeners = new ArrayList<GraphListener<T, V>>();
	
	public void addGraphListener(GraphListener<T, V> listener)
	{
		if(!listeners.contains(listener))
		{
			listeners.add(listener);
		}
	}
	
	public void removeGraphListener(GraphListener<T, V> listener)
	{
		if(listeners.contains(listener))
		{
			listeners.remove(listener);
		}
	}
	
	public void addNodeListener(NodeListener<T, V> listener, T nodeData)
	{
		Node node = mapNodes.get(nodeData);
		if(node != null)
		{
			if(!node.listeners.contains(listener))
			{
				node.listeners.add(listener);
			}
		}
	}
	public void removeNodeListener(NodeListener<T, V> listener, T nodeData)
	{
		Node node = mapNodes.get(nodeData);
		if(node != null)
		{
			if(node.listeners.contains(listener))
			{
				node.listeners.remove(listener);
			}
		}
	}
	public void addLinkListener(LinkListener<T, V> listener, T source, V linkData)
	{
		addNode(source);
		Node sourceNode = mapNodes.get(source);
		
		// get the graph link (create it with a null value
		// to add listener, the real value may not be set yet).
		Link link = sourceNode.targets.get(linkData);
		if(link == null)
		{
			link(source, null, linkData);
			link = sourceNode.targets.get(linkData);
		}
		if(!link.listeners.contains(listener))
		{
			link.listeners.add(listener);
		}
	}
	public void removeLinkListener(LinkListener<T, V> listener, T source, V linkData)
	{
		Node sourceNode = mapNodes.get(source);
		if(sourceNode != null)
		{
			Link link = sourceNode.targets.get(linkData);
			if(link != null)
			{
				if(link.listeners.contains(listener))
				{
					link.listeners.remove(listener);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void clear()
	{
		for(Object node : mapNodes.keySet().toArray())
		{
			removeNode((T)node);
		}
	}
	
	public void addNode(T nodeData)
	{
		if(nodeData != null && !mapNodes.containsKey(nodeData))
		{
			mapNodes.put(nodeData, new Node(nodeData));
			// prevent concurrent access
			for(Object o : listeners.toArray())
			{
				@SuppressWarnings("unchecked")
				GraphListener<T, V> listener = (GraphListener<T, V>)o;
				listener.onNodeAdded(nodeData);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void removeNode(T nodeData)
	{
		Node node = mapNodes.get(nodeData);
		if(node != null)
		{
			// first remove all links from and to this node.
			for(Object o : node.targets.values().toArray())
			{
				Link link = (Link)o;
				link(link.source.data, null, link.data);
			}
			for(Object o : node.sources.values().toArray())
			{
				Link link = (Link)o;
				link(link.source.data, null, link.data);
			}
			// then remove the node
			mapNodes.remove(nodeData);
			for(NodeListener<T, V> listener : node.listeners)
			{
				listener.onRemoved();
			}
			for(GraphListener<T, V> listener : listeners)
			{
				listener.onNodeRemoved(nodeData);
			}
		}
	}
	
	public void updateNode(T sourceNode)
	{
		Node source = mapNodes.get(sourceNode);
		if(source != null)
		{
			for(NodeListener<T, V> listener : source.listeners)
			{
				listener.onUpdate();
			}
			for(Link parentLink : source.sources.values())
			{
				updateNode(parentLink.source.data);
			}
		}
	}
	
	public void link(T sourceNode, T targetNode, V linkData)
	{
		addNode(sourceNode);
		addNode(targetNode);
		Node source = mapNodes.get(sourceNode);
		Node target = mapNodes.get(targetNode);
		if(!source.targets.containsKey(linkData))
		{
			Link link = new Link();
			link.data = linkData;
			link.source = source;
			link.target = target;
			source.targets.put(linkData, link);
			if(target != null)
			{
				target.sources.put(linkData, link);
			}
			for(GraphListener<T, V> listener : listeners)
			{
				listener.onLinkAdded(sourceNode, targetNode, linkData);
			}
			for(NodeListener<T, V> listener : link.source.listeners)
			{
				listener.onLinkChanged(link.source.data, link.data, null, target == null ? null : target.data);
			}
		}
		else
		{
			Link link = source.targets.get(linkData);
			Node oldTarget = link.target;
			
			// unlink existing target
			if(oldTarget != null)
			{
				oldTarget.sources.remove(linkData);
				for(NodeListener<T, V> listener : oldTarget.listeners)
				{
					listener.onLinkRemoved(link.source.data, oldTarget.data, link.data);
				}
			}
			link.target = target;
			if(target != null)
			{
				target.sources.put(linkData, link);
				for(NodeListener<T, V> listener : link.target.listeners)
				{
					listener.onLinkAdded(link.source.data, link.target.data, link.data);
				}
			}
			T oldTargetData = oldTarget == null ? null : oldTarget.data;
			T targetData = target == null ? null : target.data;
			
			for(Object o : link.listeners.toArray())
			{
				@SuppressWarnings("unchecked")
				LinkListener<T, V> listener = (LinkListener<T, V>)o;
				listener.onChanged(oldTargetData, targetData);
			}
			for(NodeListener<T, V> listener : link.source.listeners)
			{
				listener.onLinkChanged(link.source.data, link.data, oldTargetData, targetData);
			}
			for(GraphListener<T, V> listener : listeners)
			{
				listener.onLinkChanged(link.source.data, link.data, oldTargetData, targetData);
			}
			
		}
		updateNode(sourceNode);
	}
	public void unlink(T sourceNode, V linkData)
	{
		Node source = mapNodes.get(sourceNode);
		if(source != null)
		{
			Link link = source.targets.get(linkData);
			if(link != null)
			{
				link.source.targets.remove(linkData);
				if(link.target != null)
				{
					link.target.sources.remove(linkData);
					for(NodeListener<T, V> listener : link.target.listeners)
					{
						listener.onLinkRemoved(link.source.data, link.target.data, link.data);
					}
				}
				T targetData = link.target == null ? null : link.target.data;
				for(LinkListener<T, V> listener : link.listeners)
				{
					listener.onRemoved();
				}
				for(NodeListener<T, V> listener : link.source.listeners)
				{
					listener.onLinkRemoved(link.source.data, targetData, link.data);
				}
				for(GraphListener<T, V> listener : listeners)
				{
					listener.onLinkRemoved(link.source.data, targetData, link.data);
				}
				updateNode(sourceNode);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getAllNodes()
	{
		return (List<T>)Arrays.asList(mapNodes.keySet().toArray());
	}
	
	@SuppressWarnings("unchecked")
	public List<V> getAllTargetLinks(T nodeData)
	{
		Node node = mapNodes.get(nodeData);
		if(node == null)
		{
			return new ArrayList<V>();
		}
		return (List<V>)Arrays.asList(node.targets.keySet().toArray());
	}
	public List<T> getAllSources(T nodeData)
	{
		List<T> lst = new ArrayList<T>();
		Node node = mapNodes.get(nodeData);
		if(node != null)
		{
			for(Link link : node.sources.values())
			{
				lst.add(link.source.data);
			}
		}
		return lst;
	}
	
}
